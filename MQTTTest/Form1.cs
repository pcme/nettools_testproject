﻿using MQTTnet;
using MQTTnet.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography;

using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Generators;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.OpenSsl;
using Org.BouncyCastle.Security;

namespace MQTTTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            try
            {
                Test2();
            }
            catch (Exception ee)
            {
                System.Diagnostics.Debug.WriteLine(ee.Message);
            }
        }

        void Test1()
        {
            var broker = @"a34ko8n2b19x21-ats.iot.eu-west-2.amazonaws.com"; //<AWS-IoT-Endpoint>           
            var port = 8883;
            var clientId = "client1";
            var certPass = "p@ssw0rd";

            //certificates Path
            var certificatesPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "certs");

            var caCertPath = Path.Combine(certificatesPath, "AmazonRootCA1.pem");
            var caCert = X509Certificate.CreateFromCertFile(caCertPath);

            var deviceCertPath = Path.Combine(certificatesPath, "certificate.cert.pfx");
            var deviceCert = new X509Certificate(deviceCertPath, certPass);

            // Create a new MQTT client.
            var client = new uPLibrary.Networking.M2Mqtt.MqttClient(broker, port, true, caCert, deviceCert, MqttSslProtocols.TLSv1_2 | MqttSslProtocols.TLSv1_1 | MqttSslProtocols.TLSv1_0);

            //Event Handler Wiring
            client.MqttMsgPublishReceived += Client_MqttMsgPublishReceived;
            client.MqttMsgSubscribed += Client_MqttMsgSubscribed;


            //Connect
            client.Connect(clientId);
            System.Diagnostics.Debug.WriteLine($"Connected to AWS IoT with client id: {clientId}.");

            //uncomment following code for basic example
            Example1(client);

         
            // Console.WriteLine($"Press any key to exit!");
           // Console.ReadLine();
        }

        public static void Example1(uPLibrary.Networking.M2Mqtt.MqttClient client)
        {
            //send Messages
            var message = "Hello from .NET Core";
            int i = 1;
            while (i <= 3)
            {
                client.Publish("test/topic", Encoding.UTF8.GetBytes($"{message} {i}"));
                System.Diagnostics.Debug.WriteLine($"Published: {message} {i}");
                i++;
                Thread.Sleep(2000);
            }
            System.Diagnostics.Debug.WriteLine($"Messages Sent Complete.");

            //subscribig to topic
            string topic = "test/topic";
            client.Subscribe(new string[] { topic }, new byte[] { MqttMsgBase.QOS_LEVEL_AT_LEAST_ONCE });
        }
        private static void Client_MqttMsgSubscribed(object sender, MqttMsgSubscribedEventArgs e)
        {

            System.Diagnostics.Debug.WriteLine($"Successfully subscribed to the AWS IoT topic.");
        }
        private static void Client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("Message received: " + Encoding.UTF8.GetString(e.Message));
        }

        IMqttClient _client;

        async void Test2()
        {
            try
            {
                
                var broker = @"a34ko8n2b19x21-ats.iot.eu-west-2.amazonaws.com"; //<AWS-IoT-Endpoint>           
                var port = 8883;
                var clientId = "client5";
                var certPass = "p@ssw0rd";

                var certificatesPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "certs");

                var caCertPath = Path.Combine(certificatesPath, "AmazonRootCA1.pem");
                var caCert = X509Certificate.CreateFromCertFile(caCertPath);

                // var deviceCertPath = Path.Combine(certificatesPath, "certificate.cert.pfx");
                // var deviceCert = new X509Certificate(deviceCertPath, certPass);

                var deviceCert = GetCertificate();

                var options = new MqttClientOptionsBuilder()
                        .WithClientId(clientId)
                        .WithTcpServer(broker, port)
                        .WithKeepAlivePeriod(new TimeSpan(0, 0, 0, 300))
                        .WithCleanSession(true)
                        .WithTls(new MqttClientOptionsBuilderTlsParameters()
                        {
                            UseTls = true,
                            AllowUntrustedCertificates = true,
                            IgnoreCertificateChainErrors = true,
                            IgnoreCertificateRevocationErrors = true,
                            SslProtocol = System.Security.Authentication.SslProtocols.Tls12,
                            Certificates = new List<X509Certificate>
                            {
                            deviceCert, caCert
                            },
                        })
                        .Build();
                var factory = new MqttFactory();
                _client = factory.CreateMqttClient();
                await _client.ConnectAsync(options);

                ShowResponse("Connected");

                await _client.PublishStringAsync("test/topic", "{ \"message\" : \"Hi from .NET\" }");

                ShowResponse("Published");

                _client.ApplicationMessageReceivedAsync += Client_ApplicationMessageReceivedAsync;

                var mqttSubscribeOptions = factory.CreateSubscribeOptionsBuilder()
                 .WithTopicFilter(f => { f.WithTopic("test/topic1"); })
                 .Build();

                await _client.SubscribeAsync(mqttSubscribeOptions, CancellationToken.None);

                ShowResponse("Subscribed");

            }
            catch (Exception ee)
            {
                System.Diagnostics.Debug.WriteLine(ee.Message);
                ShowResponse(ee.Message);
            }
        }


        async void Disconnect()
        {
            try
            {
                if (_client != null)
                {
                    _client.ApplicationMessageReceivedAsync -= Client_ApplicationMessageReceivedAsync;
                    await _client.DisconnectAsync();
                }
            }
            catch (Exception ee)
            {
                System.Diagnostics.Debug.WriteLine(ee.Message);
                ShowResponse(ee.Message);
            }

        }

        private Task Client_ApplicationMessageReceivedAsync(MqttApplicationMessageReceivedEventArgs arg)
        {

            string resp = System.Text.ASCIIEncoding.ASCII.GetString(arg.ApplicationMessage.Payload);
            System.Diagnostics.Debug.WriteLine("Message received: " + BitConverter.ToString(arg.ApplicationMessage.Payload));
            ShowResponse("Rx: " + resp);
            return Task.CompletedTask;
        }

        void ShowResponse(string text)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(() => ShowResponse(text)));
            }
            else
            {
                richTextBox1.AppendText(text);
                richTextBox1.AppendText(Environment.NewLine);
    
            }
        }

        private void btnDisconnect_Click(object sender, EventArgs e)
        {
            Disconnect();
        }


        X509Certificate GetCertificate()
        {
           

            var certificatesPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "certs");
            var keyFile = Path.Combine(certificatesPath, "private.pem.key");
           // var keyText = File.ReadAllText(keyFile);
            var rsa = PrivateKeyFromPemFile(keyFile);

            var pemText = File.ReadAllText(Path.Combine(certificatesPath, "certificate.pem.crt"));
            var bytes = Encoding.ASCII.GetBytes(pemText);

            var ClientCert = new X509Certificate2(bytes);
            ClientCert = ClientCert.CopyWithPrivateKey(rsa);
            ClientCert = new X509Certificate2(ClientCert.Export(X509ContentType.Pfx, "12345678"), "12345678");
            return ClientCert;
        }

        public static RSACryptoServiceProvider PrivateKeyFromPemFile(String filePath)
        {
            using (TextReader privateKeyTextReader = new StringReader(File.ReadAllText(filePath)))
            {
                AsymmetricCipherKeyPair readKeyPair = (AsymmetricCipherKeyPair)new PemReader(privateKeyTextReader).ReadObject();


                RsaPrivateCrtKeyParameters privateKeyParams = ((RsaPrivateCrtKeyParameters)readKeyPair.Private);
                RSACryptoServiceProvider cryptoServiceProvider = new RSACryptoServiceProvider();
                RSAParameters parms = new RSAParameters();

                parms.Modulus = privateKeyParams.Modulus.ToByteArrayUnsigned();
                parms.P = privateKeyParams.P.ToByteArrayUnsigned();
                parms.Q = privateKeyParams.Q.ToByteArrayUnsigned();
                parms.DP = privateKeyParams.DP.ToByteArrayUnsigned();
                parms.DQ = privateKeyParams.DQ.ToByteArrayUnsigned();
                parms.InverseQ = privateKeyParams.QInv.ToByteArrayUnsigned();
                parms.D = privateKeyParams.Exponent.ToByteArrayUnsigned();
                parms.Exponent = privateKeyParams.PublicExponent.ToByteArrayUnsigned();

                cryptoServiceProvider.ImportParameters(parms);

                return cryptoServiceProvider;
            }
        }
    }
}
