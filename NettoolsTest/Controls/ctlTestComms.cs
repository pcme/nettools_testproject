﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Communications;
using Interfaces;

namespace NettoolsTest.Controls
{
    public partial class ctlTestComms : UserControl, IObserver<ICloudConnectionStatus>
    {
        public ctlTestComms()
        {
            InitializeComponent();
        }

        private void ctlTestComms_Load(object sender, EventArgs e)
        {
            tbLoginToken.Text = @"eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1NjY4MDc2MzEsInRvIjo5OTk5LCJ0aGluZ19pZCI6IjA4MmMzMDgwLTZhMDUtNDZlZi04ZTRlLWE0OWM4NDY0YWU4YiJ9.nW9NkhiaIZBZk-xrvAnH-Crd-CbXYBCvmSOEIXu017TP4pgEzCeMmbYz11BovUaF_ypvLRYsU8U5vGq_kKXnhyCYmoByxGzu-SEbZ8efcV4P5QGkMGbr4B-svJ-emqplbEeuub6qFr9i0U5mOythgJm8f9g8hjTlXAIDB6KhZRl7Qeq4K9k4yYIGorith6Fqd7fFnahs1qYqCnWOga_CpnGaDaFB1qXrNGop32kCvcJt8d4qXl1P_7T36WYl1I1DT143EEKHho3pFm5ao8vUQ8FX13uXq5gx-v-7x2qycbdlHzxU15PJDlB_ZmqjMdkqq_TJr4JHB34kE_omJl8-TA";
            tbURL.Text = Properties.Settings.Default.CloudURL; // "https://dc-dev.envea.cloud/mw/api/edge";

            
 
        }

        private async void btnLogin_Click(object sender, EventArgs e)
        {
            tbLoginResponse.Text = "";

            logindata d = new logindata();
            d.credentials = tbLoginToken.Text;

            Cloud cloud = new Cloud();

            Communications.Globals.URL = tbURL.Text;

            string url = tbURL.Text + "/login";
            var resp = await cloud.SendAsync<loginresponse>(url, d);
            if (resp == null)
            {
                tbLoginResponse.Text = "resp null";
                Communications.Globals.jwt = "";
            }
            else
            {
                if (resp.mwloginstatus)
                {
                    tbLoginResponse.Text = resp.jwt;
                    Communications.Globals.jwt = resp.jwt;
                }
                else
                {
                    tbLoginResponse.Text = "Failed";
                    Communications.Globals.jwt = "";
                }
            }
        }

        public void OnNext(ICloudConnectionStatus value)
        {
            
        }

        public void OnError(Exception error)
        {
           
        }

        public void OnCompleted()
        {
            
        }
    }
}
