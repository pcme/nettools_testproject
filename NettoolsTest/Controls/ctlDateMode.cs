﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NettoolsTest.Controls
{
    public partial class ctlDateMode : UserControl
    {
        public ctlDateMode()
        {
            InitializeComponent();
        }

        private void ctlDateMode_Load(object sender, EventArgs e)
        {
            rbFixedClock.Checked = Simulator.ClockMode.Mode;
            rbPCClock.Checked = !Simulator.ClockMode.Mode;

            var now = DateTime.Now;
            dtDate.Value = now.Date;
            dtTime.Value = now;

            Simulator.ClockMode.RunChanged += ClockMode_RunChanged;
            SetEnables();
        }

        private void ClockMode_RunChanged(bool running)
        {
            SetEnables();
        }

        private void rbPCClock_CheckedChanged(object sender, EventArgs e)
        {
            SetEnables();
        }

        private void rbFixedClock_CheckedChanged(object sender, EventArgs e)
        {
            SetEnables();
        }

        void SetEnables()
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(() => SetEnables()));
            }
            else
            {
                if (Simulator.ClockMode.Running)
                {
                    rbFixedClock.Enabled = false;
                    rbPCClock.Enabled = false;
                    dtDate.Enabled = false;
                    dtTime.Enabled = false;
                }
                else
                {
                    Simulator.ClockMode.Mode = rbFixedClock.Checked;
                    rbFixedClock.Enabled = true;
                    rbPCClock.Enabled = true;
                    if (rbFixedClock.Checked)
                    {
                        dtDate.Enabled = true;
                        dtTime.Enabled = true;
                    }
                    else
                    {
                        dtDate.Enabled = false;
                        dtTime.Enabled = false;
                    }
                }
            }
        }

        private void dtDate_ValueChanged(object sender, EventArgs e)
        {
            SetFixedTime();
        }

        private void dtTime_ValueChanged(object sender, EventArgs e)
        {
            SetFixedTime();
        }

        void SetFixedTime()
        {
            DateTime t = dtDate.Value.Date;
            t = t.Add(dtTime.Value.TimeOfDay);
            Simulator.ClockMode.StartDate = t;
        }

       

       
    }
}
