﻿using Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using log4net;

namespace NettoolsTest.Controls
{
    public partial class ctlConnectionStatus : UserControl
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(ctlConnectionStatus));
        public ctlConnectionStatus()
        {
            InitializeComponent();
        }


        public string Label
        {
            set
            {
                label1.Text = value;
            }
            get
            {
                return label1.Text;
            }
        }
        public Interfaces.IStatusProvider StatusProvider { get; set; }

        IDisposable watcher;
        public void BeforeShow()
        {
            if (StatusProvider == null)
                StatusProvider = Simulator.Simulation.Instance;

            if (StatusProvider.Status.Connected)
                pictureBox1.Image = imageList1.Images[1];
            else
                pictureBox1.Image = imageList1.Images[0];


            StatusProvider.ConnectionStatus += Instance_ConnectionStatus;
        }

        private bool _Enabled;
        public bool Enabled
        {
            get
            {
                return _Enabled;
            }
            set
            {
                _Enabled = value;
                if (!value)
                {
                    pictureBox1.Image = imageList1.Images[2];
                }
            }
        }

        private void Instance_ConnectionStatus(ICloudConnectionStatus status)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new MethodInvoker(() => Instance_ConnectionStatus(status)));
            }
            else
            {
                if ((status != null) && (status.Connected))
                    pictureBox1.Image = imageList1.Images[1];
                else
                    pictureBox1.Image = imageList1.Images[0];
            }
        }

        public void BeforeHide()
        {

            if (StatusProvider == null)
                StatusProvider = Simulator.Simulation.Instance;
            StatusProvider.ConnectionStatus -= Instance_ConnectionStatus;
        }
       

        private void ctlConnectionStatus_Load(object sender, EventArgs e)
        {
          
        }
    }
}
