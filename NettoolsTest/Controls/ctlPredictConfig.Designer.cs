﻿
namespace NettoolsTest.Controls
{
    partial class ctlPredictConfig
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tbInterval = new System.Windows.Forms.TextBox();
            this.tbDigitalInput = new System.Windows.Forms.TextBox();
            this.tbHighPeakValue = new System.Windows.Forms.TextBox();
            this.tbHighPeakCycle = new System.Windows.Forms.TextBox();
            this.tbMissingCycle = new System.Windows.Forms.TextBox();
            this.tbNoCycles = new System.Windows.Forms.TextBox();
            this.tbFrequency = new System.Windows.Forms.TextBox();
            this.tbPeakToPeak = new System.Windows.Forms.TextBox();
            this.cbEnabled = new System.Windows.Forms.CheckBox();
            this.label11 = new System.Windows.Forms.Label();
            this.tbLagTime = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.tbLagTime, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.label11, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.label10, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label9, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.label8, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.tbInterval, 1, 10);
            this.tableLayoutPanel1.Controls.Add(this.tbDigitalInput, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.tbHighPeakValue, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.tbHighPeakCycle, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.tbMissingCycle, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.tbNoCycles, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.tbFrequency, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.tbPeakToPeak, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.cbEnabled, 1, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 11;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(696, 849);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Location = new System.Drawing.Point(8, 84);
            this.label10.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(332, 76);
            this.label10.TabIndex = 17;
            this.label10.Text = "Enabled";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Location = new System.Drawing.Point(8, 768);
            this.label9.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(332, 81);
            this.label9.TabIndex = 15;
            this.label9.Text = "Cycle Interval (mins)";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(8, 236);
            this.label3.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(332, 76);
            this.label3.TabIndex = 2;
            this.label3.Text = "Frequency";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label1, 2);
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(8, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(680, 84);
            this.label1.TabIndex = 0;
            this.label1.Text = "Cleaning Cycle Setup";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(8, 160);
            this.label2.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(332, 76);
            this.label2.TabIndex = 1;
            this.label2.Text = "Peak to peak";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Location = new System.Drawing.Point(8, 312);
            this.label4.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(332, 76);
            this.label4.TabIndex = 3;
            this.label4.Text = "No. of rows";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Location = new System.Drawing.Point(8, 388);
            this.label5.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(332, 76);
            this.label5.TabIndex = 4;
            this.label5.Text = "Missing row No.";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Location = new System.Drawing.Point(8, 464);
            this.label6.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(332, 76);
            this.label6.TabIndex = 5;
            this.label6.Text = "High Peak row No.";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Location = new System.Drawing.Point(8, 540);
            this.label7.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(332, 76);
            this.label7.TabIndex = 6;
            this.label7.Text = "High Peak value";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Location = new System.Drawing.Point(8, 616);
            this.label8.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(332, 76);
            this.label8.TabIndex = 7;
            this.label8.Text = "Digital Input";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbInterval
            // 
            this.tbInterval.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbInterval.Location = new System.Drawing.Point(356, 789);
            this.tbInterval.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tbInterval.Name = "tbInterval";
            this.tbInterval.Size = new System.Drawing.Size(332, 38);
            this.tbInterval.TabIndex = 16;
            this.tbInterval.TextChanged += new System.EventHandler(this.tbInterval_TextChanged);
            // 
            // tbDigitalInput
            // 
            this.tbDigitalInput.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDigitalInput.Location = new System.Drawing.Point(356, 635);
            this.tbDigitalInput.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tbDigitalInput.Name = "tbDigitalInput";
            this.tbDigitalInput.Size = new System.Drawing.Size(332, 38);
            this.tbDigitalInput.TabIndex = 14;
            this.tbDigitalInput.TextChanged += new System.EventHandler(this.tbDigitalInput_TextChanged);
            // 
            // tbHighPeakValue
            // 
            this.tbHighPeakValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbHighPeakValue.Location = new System.Drawing.Point(356, 559);
            this.tbHighPeakValue.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tbHighPeakValue.Name = "tbHighPeakValue";
            this.tbHighPeakValue.Size = new System.Drawing.Size(332, 38);
            this.tbHighPeakValue.TabIndex = 13;
            this.tbHighPeakValue.TextChanged += new System.EventHandler(this.tbHighPeakValue_TextChanged);
            // 
            // tbHighPeakCycle
            // 
            this.tbHighPeakCycle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbHighPeakCycle.Location = new System.Drawing.Point(356, 483);
            this.tbHighPeakCycle.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tbHighPeakCycle.Name = "tbHighPeakCycle";
            this.tbHighPeakCycle.Size = new System.Drawing.Size(332, 38);
            this.tbHighPeakCycle.TabIndex = 12;
            this.tbHighPeakCycle.TextChanged += new System.EventHandler(this.tbHighPeakCycle_TextChanged);
            // 
            // tbMissingCycle
            // 
            this.tbMissingCycle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbMissingCycle.Location = new System.Drawing.Point(356, 407);
            this.tbMissingCycle.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tbMissingCycle.Name = "tbMissingCycle";
            this.tbMissingCycle.Size = new System.Drawing.Size(332, 38);
            this.tbMissingCycle.TabIndex = 11;
            this.tbMissingCycle.TextChanged += new System.EventHandler(this.tbMissingCycle_TextChanged);
            // 
            // tbNoCycles
            // 
            this.tbNoCycles.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbNoCycles.Location = new System.Drawing.Point(356, 331);
            this.tbNoCycles.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tbNoCycles.Name = "tbNoCycles";
            this.tbNoCycles.Size = new System.Drawing.Size(332, 38);
            this.tbNoCycles.TabIndex = 10;
            this.tbNoCycles.TextChanged += new System.EventHandler(this.tbNoCycles_TextChanged);
            // 
            // tbFrequency
            // 
            this.tbFrequency.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFrequency.Location = new System.Drawing.Point(356, 255);
            this.tbFrequency.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tbFrequency.Name = "tbFrequency";
            this.tbFrequency.Size = new System.Drawing.Size(332, 38);
            this.tbFrequency.TabIndex = 9;
            this.tbFrequency.TextChanged += new System.EventHandler(this.tbFrequency_TextChanged);
            // 
            // tbPeakToPeak
            // 
            this.tbPeakToPeak.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbPeakToPeak.Location = new System.Drawing.Point(356, 179);
            this.tbPeakToPeak.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tbPeakToPeak.Name = "tbPeakToPeak";
            this.tbPeakToPeak.Size = new System.Drawing.Size(332, 38);
            this.tbPeakToPeak.TabIndex = 8;
            this.tbPeakToPeak.TextChanged += new System.EventHandler(this.tbPeakToPeak_TextChanged);
            // 
            // cbEnabled
            // 
            this.cbEnabled.AutoSize = true;
            this.cbEnabled.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbEnabled.Location = new System.Drawing.Point(356, 91);
            this.cbEnabled.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.cbEnabled.Name = "cbEnabled";
            this.cbEnabled.Size = new System.Drawing.Size(332, 62);
            this.cbEnabled.TabIndex = 18;
            this.cbEnabled.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbEnabled.UseVisualStyleBackColor = true;
            this.cbEnabled.CheckedChanged += new System.EventHandler(this.cbEnabled_CheckedChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label11.Location = new System.Drawing.Point(8, 692);
            this.label11.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(332, 76);
            this.label11.TabIndex = 19;
            this.label11.Text = "Lag Time (s)";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbLagTime
            // 
            this.tbLagTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbLagTime.Location = new System.Drawing.Point(356, 711);
            this.tbLagTime.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tbLagTime.Name = "tbLagTime";
            this.tbLagTime.Size = new System.Drawing.Size(332, 38);
            this.tbLagTime.TabIndex = 20;
            this.tbLagTime.TextChanged += new System.EventHandler(this.tbLagTime_TextChanged);
            // 
            // ctlPredictConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.Name = "ctlPredictConfig";
            this.Size = new System.Drawing.Size(696, 849);
            this.Load += new System.EventHandler(this.ctlPredictConfig_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TextBox tbDigitalInput;
        private System.Windows.Forms.TextBox tbHighPeakValue;
        private System.Windows.Forms.TextBox tbHighPeakCycle;
        private System.Windows.Forms.TextBox tbMissingCycle;
        private System.Windows.Forms.TextBox tbNoCycles;
        private System.Windows.Forms.TextBox tbFrequency;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbPeakToPeak;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbInterval;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox cbEnabled;
        private System.Windows.Forms.TextBox tbLagTime;
        private System.Windows.Forms.Label label11;
    }
}
