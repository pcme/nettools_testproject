﻿using Interfaces;
using Simulator;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NettoolsTest.Controls
{
    public partial class ctlAlarmSimulation : UserControl, IEditControl
    {
        public ctlAlarmSimulation()
        {
            InitializeComponent();

            FillStatusCombo();
        }

        public Data.Controller Controller { get; set; }

        public IEditContainer EditContainer { get; set; }

        private bool _editting;
        public bool Editting 
        {
            get => _editting;
            set
            {
                _editting = value;
                if (!_editting)
                {
                    RefillCombos();
                }
            }
        }

        public void BeforeShow()
        {
            RefillCombos();
            HandleUIUpdate();
        }

        public void CancelChanges()
        {
        

        }

        public void SaveChanges()
        {
           

        }

        private void btnSetAlarm_Click(object sender, EventArgs e)
        {
            // todo - create or uopdate larm log, send to server
            var channel = cbChannels.SelectedItem as Data.Channel;
            if (channel == null)
            {
                btnSetAlarm.Enabled = false;
                return;
            }

            var status = cbStatus.SelectedItem as AlarmStatusOption;
            if (status == null)
            {
                btnSetAlarm.Enabled = false;
                return;
            }

            bool new_log = false;
            var log = Data.AlarmLog.GetLog(Controller.Id, channel.Id, status.Status, status.Label);
            if (log != null)
            {
                if (log.Active == 1)
                {
                    log.EndTS = JSONBuilder.ConvertDatetimeToUnixTimeStamp(DateTime.Now);
                    log.Active = 0;
                    log.SentFlags = 0;
                    Simulator.Simulation.Instance.SendAlarmLog(Controller);
                    btnSetAlarm.Text = "Set Alarm";
                    return;
                }
                else
                {
                    log = new Data.AlarmLog() { ChannelId = channel.Id, ControllerId = Controller.Id };
                    new_log = true;
                }
            }
            else
            {
                log = new Data.AlarmLog() { ChannelId = channel.Id, ControllerId = Controller.Id };
                new_log = true;
            }

            /*
            ILogger logger;
            if (channel.Number >= 60)
            {
                logger = Simulator.LoggerFactory.Instance.GetLogger<Simulator.DigitalGenerator>(Controller);
            }
            else
            {

                logger = Simulator.LoggerFactory.Instance.GetLogger<PulseGenerator>(Controller);
            }
            var logger_channel = logger.GetChannel(channel.Id);
            */

            ushort value;
            ushort limit;
            if (status.DataType == 5)
            {
        
                if (!ushort.TryParse(tbLimit.Text, out limit) || !ushort.TryParse(tbValue.Text, out value))
                {
                    btnSetAlarm.Enabled = false;
                    return;
                }

            }
            else if (status.DataType == 0)
            {
                value = 0;
                limit = 0;
            }
            else
            {
                value = 0;
        
                if (!ushort.TryParse(tbValue.Text, out limit))
                {
                    btnSetAlarm.Enabled = false;
                    return;
                }
            }


            log.ChannelNumber = channel.Number;
            log.Label = status.Label;
            log.StartTS = log.EndTS = JSONBuilder.ConvertDatetimeToUnixTimeStamp(DateTime.Now);
            log.SerialNumber = Controller.SerialNumber;
            log.SentFlags = 0;
            log.Active = 1;
            log.AlarmType = status.DataType;
            log.Status = status.Status;
            log.Value = (uint)((value << 16) | limit);
            if (new_log)
                Data.AlarmLog.AddLog(log);
            Simulator.Simulation.Instance.SendAlarmLog(Controller);
            btnSetAlarm.Text = "Clear Alarm";
        }

        void RefillCombos()
        {
          
            cbChannels.DataSource = null;
            if (Controller != null)
            {
                cbChannels.DataSource = Data.Channel.GetChannelsInGroup(Controller.Id);
                cbChannels.DisplayMember = "Name";
            }

        }


        void FillStatusCombo()
        {
            BuildAlarmOptions();
            cbStatus.DataSource = _options;
            cbStatus.DisplayMember = "Label";
           
        }

        private void cbChannels_SelectedIndexChanged(object sender, EventArgs e)
        {
            HandleUIUpdate();
        }

        void HandleUIUpdate()
        { 
            // todo - see if alarm is active, if so change button text and read alarm log
            var channel = cbChannels.SelectedItem as Data.Channel;
            if (channel == null)
            {
                btnSetAlarm.Enabled = false;
                return;
            }

            var status = cbStatus.SelectedItem as AlarmStatusOption;
            if (status == null)
            {
                btnSetAlarm.Enabled = false;
                return;
            }

            btnSetAlarm.Enabled = Simulator.LoggerFactory.Instance.IsControllerEnabled(Controller);
            if (status.DataType == 5)
            {
                label5.Visible = true;
                tbLimit.Visible = true;
                label4.Visible = true;
                tbValue.Visible = true;
                ushort i;
                if (!ushort.TryParse(tbLimit.Text, out i) || !ushort.TryParse(tbValue.Text, out i))
                    btnSetAlarm.Enabled = false;
         
            }
            else if (status.DataType == 0)
            {
                label5.Visible = false;
                tbLimit.Visible = false;
                label4.Visible = false;
                tbValue.Visible = false;
            }
            else
            {
                label5.Visible = false;
                tbLimit.Visible = false;
                label4.Visible = true;
                tbValue.Visible = true;
                ushort i;
                if (!ushort.TryParse(tbValue.Text, out i))
                    btnSetAlarm.Enabled = false;
            }

           

           

            var log = Data.AlarmLog.GetLog(Controller.Id, channel.Id, status.Status, status.Label);
            if (log != null)
            {
                if (log.Active == 1)
                {
                    btnSetAlarm.Text = "Clear Alarm";
                }
                else
                {
                    btnSetAlarm.Text = "Set Alarm";
                }
            }
            else
            {
                btnSetAlarm.Text = "Set Alarm";
            }
        }


        List<AlarmStatusOption> _options;
        void BuildAlarmOptions()
        {
            _options = new List<AlarmStatusOption>();
            _options.Add(new AlarmStatusOption() { DataType = 5, Label = "STAT_AL_HIGH", Status = 1 });
            _options.Add(new AlarmStatusOption() { DataType = 5, Label = "STAT_AL_HIGH_HIGH", Status = 2 });
            _options.Add(new AlarmStatusOption() { DataType = 5, Label = "STAT_AL_AVE_HIGH", Status = 4 });
            _options.Add(new AlarmStatusOption() { DataType = 5, Label = "STAT_AL_AVE_HIHI", Status = 8 });
            _options.Add(new AlarmStatusOption() { DataType = 0, Label = "STAT_NO_RESPONSE", Status = 16 });
            _options.Add(new AlarmStatusOption() { DataType = 0, Label = "STAT_BAD_VAL", Status = 32 });
            _options.Add(new AlarmStatusOption() { DataType = 0, Label = "STAT_SELF_TEST", Status = 64 });
            _options.Add(new AlarmStatusOption() { DataType = 0, Label = "STAT_POWER_INTERRUPTION", Status = 128 });
            _options.Add(new AlarmStatusOption() { DataType = 5, Label = "STAT_AL_LOW", Status = 256 });
            _options.Add(new AlarmStatusOption() { DataType = 5, Label = "STAT_AL_LOW_LOW", Status = 512 });
            _options.Add(new AlarmStatusOption() { DataType = 5, Label = "STAT_AL_AVE_LOW", Status = 1024 });
            _options.Add(new AlarmStatusOption() { DataType = 5, Label = "STAT_AL_AVE_LOLO", Status = 2048 });
            _options.Add(new AlarmStatusOption() { DataType = 5, Label = "STAT_USER_1", Status = 4096 });
            _options.Add(new AlarmStatusOption() { DataType = 5, Label = "STAT_USER_2", Status = 8192 });
            _options.Add(new AlarmStatusOption() { DataType = 5, Label = "STAT_USER_3", Status = 16384 });
            _options.Add(new AlarmStatusOption() { DataType = 5, Label = "STAT_USER_4", Status = 32768 });
            _options.Add(new AlarmStatusOption() { DataType = 1, Label = "STAT_ZERO_1", Status = 65536 });
            _options.Add(new AlarmStatusOption() { DataType = 1, Label = "STAT_ZERO_2", Status = 131072 });
            _options.Add(new AlarmStatusOption() { DataType = 1, Label = "STAT_ZERO_MEASURE", Status = 262144 });
            _options.Add(new AlarmStatusOption() { DataType = 1, Label = "STAT_USER_FAULT_6", Status = 262144 });
            _options.Add(new AlarmStatusOption() { DataType = 1, Label = "STAT_SPAN_1", Status = 524288 });
            _options.Add(new AlarmStatusOption() { DataType = 1, Label = "STAT_SPAN_2", Status = 1048576 });
            _options.Add(new AlarmStatusOption() { DataType = 1, Label = "STAT_UPSCALE_MEASURE", Status = 2097152 });
            _options.Add(new AlarmStatusOption() { DataType = 1, Label = "STAT_USER_FAULT_7", Status = 2097152 });
            _options.Add(new AlarmStatusOption() { DataType = 1, Label = "STAT_CONTAM_1", Status = 4194304 });
            _options.Add(new AlarmStatusOption() { DataType = 1, Label = "STAT_CONTAM_2", Status = 8388608 });
            _options.Add(new AlarmStatusOption() { DataType = 1, Label = "STAT_USER_FAULT_1", Status = 16777216 });
            _options.Add(new AlarmStatusOption() { DataType = 1, Label = "STAT_USER_FAULT_2", Status = 33554432 });
            _options.Add(new AlarmStatusOption() { DataType = 1, Label = "STAT_USER_FAULT_3", Status = 67108864 });
            _options.Add(new AlarmStatusOption() { DataType = 1, Label = "STAT_USER_FAULT_4", Status = 134217728 });
            _options.Add(new AlarmStatusOption() { DataType = 1, Label = "STAT_USER_FAULT_5", Status = 268435456 });
            _options.Add(new AlarmStatusOption() { DataType = 0, Label = "STAT_IN_CAL", Status = 536870912 });
            _options.Add(new AlarmStatusOption() { DataType = 0, Label = "STAT_MAINTENANCE", Status = 1073741824 });




        }

        private void cbStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            HandleUIUpdate();
        }

        private void tbValue_TextChanged(object sender, EventArgs e)
        {
            HandleUIUpdate();
        }

        private void ctlAlarmSimulation_Load(object sender, EventArgs e)
        {

        }

        private void tbLimit_TextChanged(object sender, EventArgs e)
        {
            HandleUIUpdate();
        }
    }


    public class AlarmStatusOption
    {
        public string Label { get; set; }
        public int Status { get; set; }
        public int DataType { get; set; } 
    }
}
