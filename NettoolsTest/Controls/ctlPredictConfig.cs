﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Interfaces;

namespace NettoolsTest.Controls
{
    public partial class ctlPredictConfig : UserControl, IEditControl
    {
        public ctlPredictConfig()
        {
            InitializeComponent();
        }

        public IEditContainer EditContainer { get; set; }

        private bool _editting;
        public bool Editting 
        {
            get => _editting;
            set
            {
                _editting = value;
                tbDigitalInput.Enabled = _editting;
                tbFrequency.Enabled = _editting;
                tbHighPeakCycle.Enabled = _editting;
                tbHighPeakValue.Enabled = _editting;
                tbMissingCycle.Enabled = _editting;
                tbNoCycles.Enabled = _editting;
                tbPeakToPeak.Enabled = _editting;
                tbInterval.Enabled = _editting;
                cbEnabled.Enabled = _editting;
                tbLagTime.Enabled = _editting;
            }
        }

        public Data.PredictConfig Config { get; set; }
        public Data.Controller Controller { get; set; }

        public void CancelChanges()
        {
            BeforeShow();
        }

        public void SaveChanges()
        {
            int i;
            if (int.TryParse(tbDigitalInput.Text, out i))
                Config.DigitalInput = i;
            if (int.TryParse(tbHighPeakCycle.Text, out i))
                Config.HighPeakCycle = i;
            if (int.TryParse(tbHighPeakValue.Text, out i))
                Config.HighPeakValue = i;
            if (int.TryParse(tbMissingCycle.Text, out i))
                Config.MissingCycle = i;
            if (int.TryParse(tbNoCycles.Text, out i))
                Config.NumberOfCycles = i;
            if (int.TryParse(tbInterval.Text, out i))
                Config.Interval = i;
            if (int.TryParse(tbLagTime.Text, out i))
                Config.LagTime = i;

            double d;
            if (double.TryParse(tbFrequency.Text, out d))
                Config.Frequency = d;
            if (double.TryParse(tbPeakToPeak.Text, out d))
                Config.PeakToPeak = d;

            Config.Enabled = cbEnabled.Checked;

            Config.Save();
            BeforeShow();
        }

        public void BeforeShow()
        {
            Config = Data.PredictConfig.Get(Controller.Id);
            if (Config == null)
            {
                Config = new Data.PredictConfig();
                Config.ControllerId = Controller.Id;
            }
            _updating = true;
           
            tbDigitalInput.Text = Config.DigitalInput.ToString();
            tbFrequency.Text = Config.Frequency.ToString();
            tbHighPeakCycle.Text = Config.HighPeakCycle.ToString();
            tbHighPeakValue.Text = Config.HighPeakValue.ToString();
            tbMissingCycle.Text = Config.MissingCycle.ToString();
            tbNoCycles.Text = Config.NumberOfCycles.ToString();
            tbPeakToPeak.Text = Config.PeakToPeak.ToString();
            tbInterval.Text = Config.Interval.ToString();
            cbEnabled.Checked = Config.Enabled;
            tbLagTime.Text = Config.LagTime.ToString();

            _updating = false;

        }

        bool _updating;

        private void ctlPredictConfig_Load(object sender, EventArgs e)
        {

        }

        private void tbPeakToPeak_TextChanged(object sender, EventArgs e)
        {
            if (!_updating)
            EditContainer.SetItemChanged(true);
        }

        private void tbFrequency_TextChanged(object sender, EventArgs e)
        {
            if (!_updating)
                EditContainer.SetItemChanged(true);
        }

        private void tbNoCycles_TextChanged(object sender, EventArgs e)
        {
            if (!_updating)
                EditContainer.SetItemChanged(true);
        }

        private void tbMissingCycle_TextChanged(object sender, EventArgs e)
        {
            if (!_updating)
                EditContainer.SetItemChanged(true);
        }

        private void tbHighPeakCycle_TextChanged(object sender, EventArgs e)
        {
            if (!_updating)
                EditContainer.SetItemChanged(true);
        }

        private void tbHighPeakValue_TextChanged(object sender, EventArgs e)
        {
            if (!_updating)
                EditContainer.SetItemChanged(true);
        }

        private void tbDigitalInput_TextChanged(object sender, EventArgs e)
        {
            if (!_updating)
                EditContainer.SetItemChanged(true);
        }

        private void tbInterval_TextChanged(object sender, EventArgs e)
        {
            if (!_updating)
                EditContainer.SetItemChanged(true);
        }

        private void cbEnabled_CheckedChanged(object sender, EventArgs e)
        {
            if (!_updating)
                EditContainer.SetItemChanged(true);
        }

        private void tbLagTime_TextChanged(object sender, EventArgs e)
        {
            if (!_updating)
                EditContainer.SetItemChanged(true);
        }
    }
}
