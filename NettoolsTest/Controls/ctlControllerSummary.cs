﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Interfaces;
using log4net;

namespace NettoolsTest.Controls
{
    public partial class ctlControllerSummary : UserControl
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(ctlControllerSummary));

        public ctlControllerSummary()
        {
            InitializeComponent();
            Controllers = new List<Data.Controller>();

        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public List<Data.Controller> Controllers { get; set; }

        public string ControllerNumber
        {
            get { return btnController.Text; }
            set { btnController.Text = value; }
        }

        public bool ControllerEnabled
        {
            get { return cbEnabled.Checked; }
            set { cbEnabled.Checked = value; }
        }

        public string Status
        {
            get { return lblStatus.Text; }
            set { lblStatus.Text = value; }
        }

        public void BeforeShow()
        {
            if (Controllers.Count > 0)
            Instance_DataStatus(Controllers[0].CommsStatus);
            foreach (var c in Controllers)
            {
                c.StatusChanged += C_StatusChanged;
            }
        }

        private void C_StatusChanged(Data.Controller c, ICloudDataStatus status)
        {
            Instance_DataStatus(status);
        }

        public void BeforeHide()
        {
            foreach (var c in Controllers)
            {
                c.StatusChanged -= C_StatusChanged;
            }
        }
      


        private void cbEnabled_CheckedChanged(object sender, EventArgs e)
        {
            if (cbEnabled.Checked)
                cbEnabled.ImageIndex = 1;
            else
                cbEnabled.ImageIndex = 0;

            try
            {
                if (cbEnabled.Checked)
                {

                    bool canstart = true;
                    bool first = true;
                    foreach (var c in Controllers)
                    {
                        string why = "";
                        if (!c.CanEnable(first, out why))
                        {
                            MessageBox.Show("Unable to enable Controller " + c.Number + " - " + why);
                            cbEnabled.Checked = false;
                            canstart = false;
                        }
                        first = false;
                    }
                    if (canstart)
                    {
                        var c = Controllers[0];

                        Simulator.LoggerFactory.Instance.GetLogger<Simulator.PulseGenerator>(c).Controllers = Controllers;
                        Simulator.LoggerFactory.Instance.GetLogger<Simulator.PulseGenerator>(c).Enabled = true;

                        if (Data.Channel.GetDigitalChannels(c.Id).Count > 0)
                        {
                            var dig_logger = Simulator.LoggerFactory.Instance.GetLogger<Simulator.DigitalGenerator>(c);
                            dig_logger.Controllers = Controllers;
                            dig_logger.Enabled = true;
                            dig_logger.SetValue(new Simulator.DigitalData() { Channel = 0, Value = (c.Digital1 ? 1 : 0) });
                            dig_logger.SetValue(new Simulator.DigitalData() { Channel = 1, Value = (c.Digital2 ? 1 : 0) });
                            dig_logger.SetValue(new Simulator.DigitalData() { Channel = 2, Value = (c.Digital3 ? 1 : 0) });
                            dig_logger.SetValue(new Simulator.DigitalData() { Channel = 3, Value = (c.Digital4 ? 1 : 0) });
                        }
                        
                    }
         
                }
                else
                {
                    var c = Controllers[0];
                    Simulator.LoggerFactory.Instance.GetLogger<Simulator.PulseGenerator>(c).Enabled = false;
                    Simulator.LoggerFactory.Instance.GetLogger<Simulator.DigitalGenerator>(c).Enabled = false;
                      
                }
            }
            catch (Exception ee)
            {
                Log.Error("JSON error: " + ee.Message, ee);
            }
        
            
        }


        void BuildMultipleStatus()
        {
            StringBuilder sb = new StringBuilder();
            List<int> Failed = new List<int>();
            DateTime latest = DateTime.MinValue;
            int total = 0;
            ICloudDataStatus lastStatus = Controllers[0].CommsStatus;
          

            foreach(var c in Controllers)
            {
                if (c.CommsStatus == null)
                    continue;

                if (c.CommsStatus.Failed)
                {
                    Failed.Add(c.Number);
                }
                else
                {
                    total += c.CommsStatus.Count;
                }
                if (c.CommsStatus.TimeStamp > latest)
                {
                    latest = c.CommsStatus.TimeStamp;
                    lastStatus = c.CommsStatus;
                }
            }

            sb.Append(latest.ToString("dd/MM/yyyy HH:mm:ss"));
            sb.Append(" ");
            if (Failed.Count > 0)
            {
                sb.Append("Failed: ");
                foreach (var f in Failed)
                {
                    if (f != Failed[0])
                        sb.Append(",");
                    sb.Append(f);
                }
            }
            else
            {
                if (total > 0)
                {
                    sb.Append(total.ToString());
                    sb.Append(" logs sent OK");
                }
                else
                {
                    if (lastStatus != null)
                    sb.Append(lastStatus.Response);
                }
            }
            lblStatus.Text = sb.ToString();

        }

        private void Instance_DataStatus(ICloudDataStatus value)
        {
            if (value == null)
                return;

            if (Controllers.FirstOrDefault(f => f.Id == value.ControllerId) == null)
                return;

            if (this.InvokeRequired)
            {
                this.BeginInvoke(new MethodInvoker(() => Instance_DataStatus(value)));
            }
            else
            {
                if (Controllers.Count > 1)
                {
                    BuildMultipleStatus();
                  
                }
                else
                {
                    if (value.Failed)
                        lblStatus.Text = value.TimeStamp.ToString("dd/MM/yyyy HH:mm:ss") + " Fail " + value.Response;
                    else
                        lblStatus.Text = value.TimeStamp.ToString("dd/MM/yyyy HH:mm:ss") + " " + value.Response;
                }
            }
        }

        private void label1_MouseClick(object sender, MouseEventArgs e)
        {
            if (cbEnabled.Checked)
            {
                MessageBox.Show("Disable controller to change settings");
            }
            else
            {
                Log.Debug("Clicked on Controller " + ControllerNumber);
                if (Controllers.Count == 1)
                    Globals.Container.LoadPage(typeof(Pages.DashBoard1), Controllers[0]);
                else
                    Globals.Container.LoadPage(typeof(Pages.DashBoard10), Controllers);
            }
        }

        private void btnController_Click(object sender, EventArgs e)
        {

            Log.Debug("Clicked on Controller " + ControllerNumber);
            if (Controllers.Count == 1)
                Globals.Container.LoadPage(typeof(Pages.DashBoard1), Controllers[0]);
            else
                Globals.Container.LoadPage(typeof(Pages.DashBoard10), Controllers);
            
        }
    }
}
