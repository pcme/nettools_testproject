﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using log4net;

namespace NettoolsTest.Controls
{
    public partial class ctlConnectivityControls : UserControl
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(ctlConnectivityControls));
        public ctlConnectivityControls()
        {
            InitializeComponent();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            ToggleStart();
        }


        void PreStart()
        {
            var controllers = Data.Controller.GetControllers();
            var c = controllers.FirstOrDefault(ff => ff.Number == 1);

            {
                if (c.ConfigChanged)
                {
                    string hash;
                    DateTime utc = Simulator.ClockMode.GetTime;
                    var json = new Simulator.JSONBuilder();
                    var json_data = json.GetConfigHash(c, utc, out hash);
                    c.UpdateConfig(json_data, hash, utc);
                }
            }
            // a34ko8n2b19x21-ats.iot.eu-west-2.amazonaws.com
            Communications.Globals.URL = Properties.Settings.Default.Prefix + "-ats.iot." + Properties.Settings.Default.Region + ".amazonaws.com";
            Log.Debug("URL set to " + Communications.Globals.URL);
        }

        public void BeforeShow()
        {
            UpdateStatus();
        }

        private void ToggleStart()
        {
            var controllers = Data.Controller.GetControllers();
            var c = controllers.FirstOrDefault(ff => ff.Number == 1);

            try
            {
                if (Simulator.LoggerFactory.Instance.GetLogger<Simulator.PulseGenerator>(c).Enabled)
                {
                    Simulator.LoggerFactory.Instance.GetLogger<Simulator.PulseGenerator>(c).Enabled = false;
                    Simulator.LoggerFactory.Instance.GetLogger<Simulator.DigitalGenerator>(c).Enabled = false;
                    btnStart.Text = "Start";
                }
                else
                {
                    PreStart();
                    bool canstart = true;
                    bool first = true;
       
                    string why = "";
                    if (!c.CanEnable(first, out why))
                    {
                        MessageBox.Show("Unable to enable Controller " + c.Number + " - " + why);
                        canstart = false;
                    }
                    first = false;
                    
                    if (canstart)
                    {
                        List<Data.Controller> Controllers = new List<Data.Controller>();
                        Controllers.Add(c);
                        Simulator.LoggerFactory.Instance.GetLogger<Simulator.PulseGenerator>(c).Controllers = Controllers;
                        Simulator.LoggerFactory.Instance.GetLogger<Simulator.PulseGenerator>(c).Enabled = true;

                        if (Data.Channel.GetDigitalChannels(c.Id).Count > 0)
                        {
                            var dig_logger = Simulator.LoggerFactory.Instance.GetLogger<Simulator.DigitalGenerator>(c);
                            dig_logger.Controllers = Controllers;
                            dig_logger.Enabled = true;
                            dig_logger.SetValue(new Simulator.DigitalData() { Channel = 0, Value = (c.Digital1 ? 1 : 0) });
                            dig_logger.SetValue(new Simulator.DigitalData() { Channel = 1, Value = (c.Digital2 ? 1 : 0) });
                            dig_logger.SetValue(new Simulator.DigitalData() { Channel = 2, Value = (c.Digital3 ? 1 : 0) });
                            dig_logger.SetValue(new Simulator.DigitalData() { Channel = 3, Value = (c.Digital4 ? 1 : 0) });
                        }

                        btnStart.Text = "Stop";

                    }

                }

                UpdateStatus();
              
            }
            catch (Exception ee)
            {
                Log.Error("JSON error: " + ee.Message, ee);
            }


        }


        void UpdateStatus()
        {
            var controllers = Data.Controller.GetControllers();
            var c = controllers.FirstOrDefault(ff => ff.Number == 1);

            if (Simulator.LoggerFactory.Instance.GetLogger<Simulator.PulseGenerator>(c).Enabled)
                lblStatus.Text = "Test running";
            else
                lblStatus.Text = "Test not running";
        }

        private void ctlConnectivityControls_Load(object sender, EventArgs e)
        {
            if (!DesignMode)
            {
                UpdateStatus();


            }
        }


    }
}
