﻿
namespace NettoolsTest.Controls
{
    partial class ctlControllerSummary
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ctlControllerSummary));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblStatus = new System.Windows.Forms.Label();
            this.cbEnabled = new System.Windows.Forms.CheckBox();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.btnController = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel1.Controls.Add(this.lblStatus, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.cbEnabled, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnController, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(589, 50);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblStatus.Location = new System.Drawing.Point(237, 0);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(349, 50);
            this.lblStatus.TabIndex = 2;
            this.lblStatus.Text = "-------";
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbEnabled
            // 
            this.cbEnabled.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbEnabled.AutoSize = true;
            this.cbEnabled.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbEnabled.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.cbEnabled.FlatAppearance.BorderSize = 0;
            this.cbEnabled.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
            this.cbEnabled.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.cbEnabled.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.cbEnabled.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbEnabled.ImageIndex = 0;
            this.cbEnabled.ImageList = this.imageList1;
            this.cbEnabled.Location = new System.Drawing.Point(120, 3);
            this.cbEnabled.Name = "cbEnabled";
            this.cbEnabled.Size = new System.Drawing.Size(111, 44);
            this.cbEnabled.TabIndex = 3;
            this.cbEnabled.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbEnabled.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.cbEnabled.UseVisualStyleBackColor = true;
            this.cbEnabled.CheckedChanged += new System.EventHandler(this.cbEnabled_CheckedChanged);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "cross.png");
            this.imageList1.Images.SetKeyName(1, "tick.png");
            // 
            // btnController
            // 
            this.btnController.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnController.Location = new System.Drawing.Point(3, 3);
            this.btnController.Name = "btnController";
            this.btnController.Size = new System.Drawing.Size(111, 44);
            this.btnController.TabIndex = 4;
            this.btnController.Text = "button1";
            this.btnController.UseVisualStyleBackColor = true;
            this.btnController.Click += new System.EventHandler(this.btnController_Click);
            // 
            // ctlControllerSummary
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "ctlControllerSummary";
            this.Size = new System.Drawing.Size(589, 50);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.CheckBox cbEnabled;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Button btnController;
    }
}
