﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Interfaces;

namespace NettoolsTest.Controls
{
    public partial class ctlSpanZero : UserControl, IEditControl
    {
        public ctlSpanZero()
        {
            InitializeComponent();
        }

        public Data.Controller Controller { get; set; }
        public IEditContainer EditContainer { get; set; }

        private bool _editting;
        public bool Editting
        {
            get => _editting;
            set
            {
                _editting = value;
                tbSpanAlarm.Enabled = value;
                tbSpanPercent.Enabled = value;
                tbZeroAlarm.Enabled = value;
                tbZeroPercent.Enabled = value;
                tbDuration.Enabled = value;
            }
        }

        public void CancelChanges()
        {
            BeforeShow();
        }

        public void SaveChanges()
        {
            double d;
            if (double.TryParse(tbZeroPercent.Text, out d))
                Controller.ZeroPercent = d;
            if (double.TryParse(tbZeroAlarm.Text, out d))
                Controller.ZeroAlarm = d;
            if (double.TryParse(tbSpanPercent.Text, out d))
                Controller.SpanPercent = d;
            if (double.TryParse(tbSpanAlarm.Text, out d))
                Controller.SpanAlarm = d;

            int t;
            if (int.TryParse(tbDuration.Text, out t))
                Controller.TestDuration = t;

            Data.Controller.Update(Controller);
            BeforeShow();

        }

        public void BeforeShow()
        {
            _updating = true;
            tbSpanAlarm.Text = Controller.SpanAlarm.ToString();
            tbSpanPercent.Text = Controller.SpanPercent.ToString();
            tbZeroAlarm.Text = Controller.ZeroAlarm.ToString();
            tbZeroPercent.Text = Controller.ZeroPercent.ToString();
            tbDuration.Text = Controller.TestDuration.ToString();
            _updating = false;
        }

        bool _updating;
        private void tbZeroPercent_TextChanged(object sender, EventArgs e)
        {
            if (!_updating)
            EditContainer.SetItemChanged(true);
        }

        private void tbZeroAlarm_TextChanged(object sender, EventArgs e)
        {
            if (!_updating)
                EditContainer.SetItemChanged(true);
        }

        private void tbSpanPercent_TextChanged(object sender, EventArgs e)
        {
            if (!_updating)
                EditContainer.SetItemChanged(true);
        }

        private void tbSpanAlarm_TextChanged(object sender, EventArgs e)
        {
            if (!_updating)
                EditContainer.SetItemChanged(true);
        }

        private void tbDuration_TextChanged(object sender, EventArgs e)
        {
            if (!_updating)
                EditContainer.SetItemChanged(true);
        }
    }
}
