﻿using CustomControls.Rule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NettoolsTest.Controls
{
    public class ReadOnlyPropertyGrid : PropertyGrid
    {
        public ReadOnlyPropertyGrid()
        {
            this.ToolbarVisible = false;
			InitializeComponent();

		}

		private void InitializeComponent()
		{
			this.SuspendLayout();
			// 
			// PropertyGridControl
			// 
			this.PropertyValueChanged += PropertyGridControl_PropertyValueChanged;
			this.ResumeLayout(false);

		}

		public bool Readonly { get; set; }

        protected override void OnSelectedGridItemChanged(SelectedGridItemChangedEventArgs e)
        {
            if (Readonly)
            {
                if (e.NewSelection.GridItemType == GridItemType.Property)
                {
                    if (e.NewSelection.Parent != null && e.NewSelection.Parent.GridItemType == GridItemType.Category)
                    {
                        this.SelectedGridItem = e.NewSelection.Parent;
                        return;
                    }
                }
            }
            base.OnSelectedGridItemChanged(e);
        }

		#region Event Handlers

		/// <summary>
		/// 
		/// </summary>
		/// <param name="s"></param>
		/// <param name="e"></param>
		private static void PropertyGridControl_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
		{
			RuleBaseAttribute rule;
			Type classType;
			string propertyName;
			PropertyInfo propertyInfo;
			object[] attributes;

			classType = e.ChangedItem.PropertyDescriptor.ComponentType;
			propertyName = e.ChangedItem.PropertyDescriptor.Name;
			propertyInfo = classType.GetProperty(propertyName);
			attributes = propertyInfo.GetCustomAttributes(true);

			if ((attributes != null) && (attributes.Length > 0))
			{
				foreach (object attribute in attributes)
				{
					// Is this Attribute a RuleBaseAttribute
					rule = attribute as RuleBaseAttribute;
					if (rule != null)
					{
						// Validate the data using the rule
						if (rule.IsValid(e.ChangedItem.Value) == false)
						{
							// Data was invalid - show the error
							MessageBox.Show(rule.ErrorMessage, "Data Entry Error",
								MessageBoxButtons.OK, MessageBoxIcon.Error);
							
						}
					}
				}
			}
		}

		#endregion
	}
}
