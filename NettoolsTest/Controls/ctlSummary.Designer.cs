﻿
namespace NettoolsTest.Controls
{
    partial class ctlSummary
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ctlControllerSummary19 = new NettoolsTest.Controls.ctlControllerSummary();
            this.ctlControllerSummary18 = new NettoolsTest.Controls.ctlControllerSummary();
            this.ctlControllerSummary17 = new NettoolsTest.Controls.ctlControllerSummary();
            this.ctlControllerSummary16 = new NettoolsTest.Controls.ctlControllerSummary();
            this.ctlControllerSummary15 = new NettoolsTest.Controls.ctlControllerSummary();
            this.ctlControllerSummary14 = new NettoolsTest.Controls.ctlControllerSummary();
            this.ctlControllerSummary13 = new NettoolsTest.Controls.ctlControllerSummary();
            this.ctlControllerSummary12 = new NettoolsTest.Controls.ctlControllerSummary();
            this.ctlControllerSummary11 = new NettoolsTest.Controls.ctlControllerSummary();
            this.ctlControllerSummary1 = new NettoolsTest.Controls.ctlControllerSummary();
            this.ctlControllerSummary2 = new NettoolsTest.Controls.ctlControllerSummary();
            this.ctlControllerSummary3 = new NettoolsTest.Controls.ctlControllerSummary();
            this.ctlControllerSummary4 = new NettoolsTest.Controls.ctlControllerSummary();
            this.ctlControllerSummary5 = new NettoolsTest.Controls.ctlControllerSummary();
            this.ctlControllerSummary6 = new NettoolsTest.Controls.ctlControllerSummary();
            this.ctlControllerSummary7 = new NettoolsTest.Controls.ctlControllerSummary();
            this.ctlControllerSummary8 = new NettoolsTest.Controls.ctlControllerSummary();
            this.ctlControllerSummary9 = new NettoolsTest.Controls.ctlControllerSummary();
            this.ctlControllerSummary10 = new NettoolsTest.Controls.ctlControllerSummary();
            this.ctlConnectionStatus1 = new NettoolsTest.Controls.ctlConnectionStatus();
            this.ctlDateMode1 = new NettoolsTest.Controls.ctlDateMode();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.ctlControllerSummary19, 1, 10);
            this.tableLayoutPanel1.Controls.Add(this.ctlControllerSummary18, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.ctlControllerSummary17, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.ctlControllerSummary16, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.ctlControllerSummary15, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.ctlControllerSummary14, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.ctlControllerSummary13, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.ctlControllerSummary12, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.ctlControllerSummary11, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.ctlControllerSummary1, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.ctlControllerSummary2, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.ctlControllerSummary3, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.ctlControllerSummary4, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.ctlControllerSummary5, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.ctlControllerSummary6, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.ctlControllerSummary7, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.ctlControllerSummary8, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.ctlControllerSummary9, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.ctlControllerSummary10, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.ctlConnectionStatus1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.ctlDateMode1, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 13;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 2F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1013, 611);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 3;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel3.Controls.Add(this.label6, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.label4, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(509, 64);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(501, 42);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Location = new System.Drawing.Point(203, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(295, 42);
            this.label6.TabIndex = 3;
            this.label6.Text = "Status";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Location = new System.Drawing.Point(103, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 42);
            this.label5.TabIndex = 2;
            this.label5.Text = "Enabled";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Location = new System.Drawing.Point(3, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 42);
            this.label4.TabIndex = 1;
            this.label4.Text = "Controller number";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel2.Controls.Add(this.label3, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.label2, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 64);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(500, 42);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(203, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(294, 42);
            this.label3.TabIndex = 2;
            this.label3.Text = "Status";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(103, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 42);
            this.label2.TabIndex = 1;
            this.label2.Text = "Enabled";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 42);
            this.label1.TabIndex = 0;
            this.label1.Text = "Controller number";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ctlControllerSummary19
            // 
            this.ctlControllerSummary19.ControllerEnabled = false;
            this.ctlControllerSummary19.ControllerNumber = "91-100";
            this.ctlControllerSummary19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlControllerSummary19.Location = new System.Drawing.Point(509, 496);
            this.ctlControllerSummary19.Name = "ctlControllerSummary19";
            this.ctlControllerSummary19.Size = new System.Drawing.Size(501, 42);
            this.ctlControllerSummary19.Status = "-------";
            this.ctlControllerSummary19.TabIndex = 20;
            // 
            // ctlControllerSummary18
            // 
            this.ctlControllerSummary18.ControllerEnabled = false;
            this.ctlControllerSummary18.ControllerNumber = "81-90";
            this.ctlControllerSummary18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlControllerSummary18.Location = new System.Drawing.Point(509, 448);
            this.ctlControllerSummary18.Name = "ctlControllerSummary18";
            this.ctlControllerSummary18.Size = new System.Drawing.Size(501, 42);
            this.ctlControllerSummary18.Status = "-------";
            this.ctlControllerSummary18.TabIndex = 19;
            // 
            // ctlControllerSummary17
            // 
            this.ctlControllerSummary17.ControllerEnabled = false;
            this.ctlControllerSummary17.ControllerNumber = "71-80";
            this.ctlControllerSummary17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlControllerSummary17.Location = new System.Drawing.Point(509, 400);
            this.ctlControllerSummary17.Name = "ctlControllerSummary17";
            this.ctlControllerSummary17.Size = new System.Drawing.Size(501, 42);
            this.ctlControllerSummary17.Status = "-------";
            this.ctlControllerSummary17.TabIndex = 18;
            // 
            // ctlControllerSummary16
            // 
            this.ctlControllerSummary16.ControllerEnabled = false;
            this.ctlControllerSummary16.ControllerNumber = "61-70";
            this.ctlControllerSummary16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlControllerSummary16.Location = new System.Drawing.Point(509, 352);
            this.ctlControllerSummary16.Name = "ctlControllerSummary16";
            this.ctlControllerSummary16.Size = new System.Drawing.Size(501, 42);
            this.ctlControllerSummary16.Status = "-------";
            this.ctlControllerSummary16.TabIndex = 17;
            // 
            // ctlControllerSummary15
            // 
            this.ctlControllerSummary15.ControllerEnabled = false;
            this.ctlControllerSummary15.ControllerNumber = "51-60";
            this.ctlControllerSummary15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlControllerSummary15.Location = new System.Drawing.Point(509, 304);
            this.ctlControllerSummary15.Name = "ctlControllerSummary15";
            this.ctlControllerSummary15.Size = new System.Drawing.Size(501, 42);
            this.ctlControllerSummary15.Status = "-------";
            this.ctlControllerSummary15.TabIndex = 16;
            // 
            // ctlControllerSummary14
            // 
            this.ctlControllerSummary14.ControllerEnabled = false;
            this.ctlControllerSummary14.ControllerNumber = "41-50";
            this.ctlControllerSummary14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlControllerSummary14.Location = new System.Drawing.Point(509, 256);
            this.ctlControllerSummary14.Name = "ctlControllerSummary14";
            this.ctlControllerSummary14.Size = new System.Drawing.Size(501, 42);
            this.ctlControllerSummary14.Status = "-------";
            this.ctlControllerSummary14.TabIndex = 15;
            // 
            // ctlControllerSummary13
            // 
            this.ctlControllerSummary13.ControllerEnabled = false;
            this.ctlControllerSummary13.ControllerNumber = "31-40";
            this.ctlControllerSummary13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlControllerSummary13.Location = new System.Drawing.Point(509, 208);
            this.ctlControllerSummary13.Name = "ctlControllerSummary13";
            this.ctlControllerSummary13.Size = new System.Drawing.Size(501, 42);
            this.ctlControllerSummary13.Status = "-------";
            this.ctlControllerSummary13.TabIndex = 14;
            // 
            // ctlControllerSummary12
            // 
            this.ctlControllerSummary12.ControllerEnabled = false;
            this.ctlControllerSummary12.ControllerNumber = "21-30";
            this.ctlControllerSummary12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlControllerSummary12.Location = new System.Drawing.Point(509, 160);
            this.ctlControllerSummary12.Name = "ctlControllerSummary12";
            this.ctlControllerSummary12.Size = new System.Drawing.Size(501, 42);
            this.ctlControllerSummary12.Status = "-------";
            this.ctlControllerSummary12.TabIndex = 13;
            // 
            // ctlControllerSummary11
            // 
            this.ctlControllerSummary11.ControllerEnabled = false;
            this.ctlControllerSummary11.ControllerNumber = "11-20";
            this.ctlControllerSummary11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlControllerSummary11.Location = new System.Drawing.Point(509, 112);
            this.ctlControllerSummary11.Name = "ctlControllerSummary11";
            this.ctlControllerSummary11.Size = new System.Drawing.Size(501, 42);
            this.ctlControllerSummary11.Status = "-------";
            this.ctlControllerSummary11.TabIndex = 12;
            // 
            // ctlControllerSummary1
            // 
            this.ctlControllerSummary1.ControllerEnabled = false;
            this.ctlControllerSummary1.ControllerNumber = "1";
            this.ctlControllerSummary1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlControllerSummary1.Location = new System.Drawing.Point(3, 112);
            this.ctlControllerSummary1.Name = "ctlControllerSummary1";
            this.ctlControllerSummary1.Size = new System.Drawing.Size(500, 42);
            this.ctlControllerSummary1.Status = "-------";
            this.ctlControllerSummary1.TabIndex = 2;
            // 
            // ctlControllerSummary2
            // 
            this.ctlControllerSummary2.ControllerEnabled = false;
            this.ctlControllerSummary2.ControllerNumber = "2";
            this.ctlControllerSummary2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlControllerSummary2.Location = new System.Drawing.Point(3, 160);
            this.ctlControllerSummary2.Name = "ctlControllerSummary2";
            this.ctlControllerSummary2.Size = new System.Drawing.Size(500, 42);
            this.ctlControllerSummary2.Status = "-------";
            this.ctlControllerSummary2.TabIndex = 3;
            // 
            // ctlControllerSummary3
            // 
            this.ctlControllerSummary3.ControllerEnabled = false;
            this.ctlControllerSummary3.ControllerNumber = "3";
            this.ctlControllerSummary3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlControllerSummary3.Location = new System.Drawing.Point(3, 208);
            this.ctlControllerSummary3.Name = "ctlControllerSummary3";
            this.ctlControllerSummary3.Size = new System.Drawing.Size(500, 42);
            this.ctlControllerSummary3.Status = "-------";
            this.ctlControllerSummary3.TabIndex = 4;
            // 
            // ctlControllerSummary4
            // 
            this.ctlControllerSummary4.ControllerEnabled = false;
            this.ctlControllerSummary4.ControllerNumber = "4";
            this.ctlControllerSummary4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlControllerSummary4.Location = new System.Drawing.Point(3, 256);
            this.ctlControllerSummary4.Name = "ctlControllerSummary4";
            this.ctlControllerSummary4.Size = new System.Drawing.Size(500, 42);
            this.ctlControllerSummary4.Status = "-------";
            this.ctlControllerSummary4.TabIndex = 5;
            // 
            // ctlControllerSummary5
            // 
            this.ctlControllerSummary5.ControllerEnabled = false;
            this.ctlControllerSummary5.ControllerNumber = "5";
            this.ctlControllerSummary5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlControllerSummary5.Location = new System.Drawing.Point(3, 304);
            this.ctlControllerSummary5.Name = "ctlControllerSummary5";
            this.ctlControllerSummary5.Size = new System.Drawing.Size(500, 42);
            this.ctlControllerSummary5.Status = "-------";
            this.ctlControllerSummary5.TabIndex = 6;
            // 
            // ctlControllerSummary6
            // 
            this.ctlControllerSummary6.ControllerEnabled = false;
            this.ctlControllerSummary6.ControllerNumber = "6";
            this.ctlControllerSummary6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlControllerSummary6.Location = new System.Drawing.Point(3, 352);
            this.ctlControllerSummary6.Name = "ctlControllerSummary6";
            this.ctlControllerSummary6.Size = new System.Drawing.Size(500, 42);
            this.ctlControllerSummary6.Status = "-------";
            this.ctlControllerSummary6.TabIndex = 7;
            // 
            // ctlControllerSummary7
            // 
            this.ctlControllerSummary7.ControllerEnabled = false;
            this.ctlControllerSummary7.ControllerNumber = "7";
            this.ctlControllerSummary7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlControllerSummary7.Location = new System.Drawing.Point(3, 400);
            this.ctlControllerSummary7.Name = "ctlControllerSummary7";
            this.ctlControllerSummary7.Size = new System.Drawing.Size(500, 42);
            this.ctlControllerSummary7.Status = "-------";
            this.ctlControllerSummary7.TabIndex = 8;
            // 
            // ctlControllerSummary8
            // 
            this.ctlControllerSummary8.ControllerEnabled = false;
            this.ctlControllerSummary8.ControllerNumber = "8";
            this.ctlControllerSummary8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlControllerSummary8.Location = new System.Drawing.Point(3, 448);
            this.ctlControllerSummary8.Name = "ctlControllerSummary8";
            this.ctlControllerSummary8.Size = new System.Drawing.Size(500, 42);
            this.ctlControllerSummary8.Status = "-------";
            this.ctlControllerSummary8.TabIndex = 9;
            // 
            // ctlControllerSummary9
            // 
            this.ctlControllerSummary9.ControllerEnabled = false;
            this.ctlControllerSummary9.ControllerNumber = "9";
            this.ctlControllerSummary9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlControllerSummary9.Location = new System.Drawing.Point(3, 496);
            this.ctlControllerSummary9.Name = "ctlControllerSummary9";
            this.ctlControllerSummary9.Size = new System.Drawing.Size(500, 42);
            this.ctlControllerSummary9.Status = "-------";
            this.ctlControllerSummary9.TabIndex = 10;
            // 
            // ctlControllerSummary10
            // 
            this.ctlControllerSummary10.ControllerEnabled = false;
            this.ctlControllerSummary10.ControllerNumber = "10";
            this.ctlControllerSummary10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlControllerSummary10.Location = new System.Drawing.Point(3, 544);
            this.ctlControllerSummary10.Name = "ctlControllerSummary10";
            this.ctlControllerSummary10.Size = new System.Drawing.Size(500, 42);
            this.ctlControllerSummary10.Status = "-------";
            this.ctlControllerSummary10.TabIndex = 11;
            // 
            // ctlConnectionStatus1
            // 
            this.ctlConnectionStatus1.Dock = System.Windows.Forms.DockStyle.Left;
            this.ctlConnectionStatus1.Location = new System.Drawing.Point(3, 3);
            this.ctlConnectionStatus1.Name = "ctlConnectionStatus1";
            this.ctlConnectionStatus1.Size = new System.Drawing.Size(229, 55);
            this.ctlConnectionStatus1.TabIndex = 21;
            // 
            // ctlDateMode1
            // 
            this.ctlDateMode1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlDateMode1.Location = new System.Drawing.Point(509, 3);
            this.ctlDateMode1.Name = "ctlDateMode1";
            this.ctlDateMode1.Size = new System.Drawing.Size(501, 55);
            this.ctlDateMode1.TabIndex = 22;
            // 
            // ctlSummary
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "ctlSummary";
            this.Size = new System.Drawing.Size(1013, 611);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private ctlControllerSummary ctlControllerSummary1;
        private ctlControllerSummary ctlControllerSummary2;
        private ctlControllerSummary ctlControllerSummary3;
        private ctlControllerSummary ctlControllerSummary4;
        private ctlControllerSummary ctlControllerSummary5;
        private ctlControllerSummary ctlControllerSummary6;
        private ctlControllerSummary ctlControllerSummary7;
        private ctlControllerSummary ctlControllerSummary8;
        private ctlControllerSummary ctlControllerSummary9;
        private ctlControllerSummary ctlControllerSummary10;
        private ctlControllerSummary ctlControllerSummary19;
        private ctlControllerSummary ctlControllerSummary18;
        private ctlControllerSummary ctlControllerSummary17;
        private ctlControllerSummary ctlControllerSummary16;
        private ctlControllerSummary ctlControllerSummary15;
        private ctlControllerSummary ctlControllerSummary14;
        private ctlControllerSummary ctlControllerSummary13;
        private ctlControllerSummary ctlControllerSummary12;
        private ctlControllerSummary ctlControllerSummary11;
        private ctlConnectionStatus ctlConnectionStatus1;
        private ctlDateMode ctlDateMode1;
    }
}
