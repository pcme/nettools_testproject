﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NettoolsTest.Controls
{
    public partial class ctlDigital : UserControl
    {
        public ctlDigital()
        {
            InitializeComponent();
        }

        public Data.Controller Controller { get; set; }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (_updating)
                return;
            if (Controller != null)
                Controller.Digital1 = checkBox1.Checked;
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (_updating)
                return;
            if (Controller != null)
                Controller.Digital2 = checkBox2.Checked;
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            if (_updating)
                return;
            if (Controller != null)
                Controller.Digital3 = checkBox3.Checked;
        }

        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {
            if (_updating)
                return;
            if (Controller != null)
                Controller.Digital4 = checkBox4.Checked;
        }

        bool _updating;
        private void ctlDigital_Load(object sender, EventArgs e)
        {
          
        }

        public void BeforeShow()
        {
            _updating = true;
            if (Controller != null)
            {
                checkBox1.Checked = Controller.Digital1;
                checkBox2.Checked = Controller.Digital2;
                checkBox3.Checked = Controller.Digital3;
                checkBox3.Checked = Controller.Digital4;
                Controller.DigitalChanged += Controller_DigitalChanged;
            }
            _updating = false;
        }

        public void BeforeHide()
        {
            Controller.DigitalChanged -= Controller_DigitalChanged;
        }

        private void Controller_DigitalChanged(int channel, bool value)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new MethodInvoker(() => Controller_DigitalChanged(channel, value)));
            }
            else
            {
                _updating = true;
                switch (channel)
                {
                    case 0:
                        checkBox1.Checked = value;
                        break;
                    case 1:
                        checkBox2.Checked = value;
                        break;
                    case 2:
                        checkBox3.Checked = value;
                        break;
                    case 3:
                        checkBox4.Checked = value;
                        break;
                }
                _updating = false;
            }
        }
    }
}
