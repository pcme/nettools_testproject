﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Interfaces;
using Syncfusion.Windows.Forms.Tools.MultiColumnTreeView;

namespace NettoolsTest.Controls
{
    public partial class ctlGroupNode : UserControl, IEditControl, IEditContainer
    {
        public ctlGroupNode()
        {
            InitializeComponent();
        }

        public IEditContainer EditContainer { get; set; }


        public TreeNodeAdv Node { get; set; }
        public Data.Group Group { get; set; }
        public string GroupName
        {
            set { label1.Text = "Name: " + value; }
        }

        public void BeforeShow()
        {
            Group = Data.Group.GetGroup(Group.Id);
            btnAdd.Visible = false;
            btnDelete.Visible = false;
            AddChannelNodes();
        }

        private bool _Editting;
        public bool Editting
        {
            get => _Editting;
            set
            {
                _Editting = value;
                btnAdd.Visible = _Editting;
                btnDelete.Visible = _Editting;
                if (_Editting)
                {
    
                }
                foreach(var node in Node.Nodes)
                {
                    ((node as TreeNodeAdv).CustomControl as ctlChannelNode).Editting = _Editting;
                }
            }
        }

        public void SaveChanges()
        {

        }
        public void CancelChanges()
        {
   
            Group = Data.Group.GetGroup(Group.Id);
            AddChannelNodes();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            EditContainer.DeleteItem(this);
        }

   
        private void btnAdd_Click(object sender, EventArgs e)
        {
            bool digital_only = false;
            if (Data.Channel.CountNonDigitalChannels(Group.ControllerId) == 10)
            {
                digital_only = true;
            }


            Forms.frmAddChannel f = new Forms.frmAddChannel();

            f.DigitalChannelOptions = new List<Forms.DigitalChannelOptions>();
            // todo - add channels whicha re not already in this controller
            var existing = Data.Channel.GetChannelsInGroup(Group.ControllerId).Where(g => g.Number >= 60 && g.Number <= 63).ToList();
            for(int i=0; i < 4; i++)
            {
                if (existing.FirstOrDefault(g => g.Number == 60 + i) != null)
                    continue;

                f.DigitalChannelOptions.Add(new Forms.DigitalChannelOptions() { Id = i + 60, Name = "Digital Input " + (i + 1).ToString() });

            }

            if (digital_only && f.DigitalChannelOptions.Count == 0)
            {
                MessageBox.Show("Channel limit (10 plus 4 digital) reached for this controller");
                return;
            }

            f.DigitalOnly = digital_only;
            if (DialogResult.OK == f.ShowDialog(this))
            {
                
                var ch = new Data.Channel();
                ch.ChannelType = f.DeviceType.ChannelTypeId;
                ch.ChannelTypeName = f.DeviceType.Device;
                ch.GroupId = Group.Id;
              
                ch.ModbusAddress = f.ModbusAddress;
                if (f.DeviceType.FixNameAndId)
                {
                    ch.Name = f.DigitalChannel.Name;
                    ch.Number = f.DigitalChannel.Id;
                }
                else
                {
                    ch.Name = f.ChannelName;
                    ch.Number = Data.Channel.CountNonDigitalChannels(Group.ControllerId) + 1;
                }
                Data.Channel.AddChannel(ch);
                Group.Channels.Add(ch);
                EditContainer.SetItemChanged(true);
                AddChannelNode(ch);
            
                Simulator.ChannelHelper.AddChannelConfigs(ch);
            }
        }


       


        void AddChannelNode(Data.Channel ch)
        {
            TreeNodeAdv node = new TreeNodeAdv();
            ctlChannelNode channel_node = new ctlChannelNode();
            channel_node.ChannelName = ch.Name;
            channel_node.ChannelTypeName = ch.ChannelTypeName;
            channel_node.Channel = ch;
            channel_node.Node = node;
            node.CustomControl = channel_node;
            node.Height = 40;
            node.EnsureDefaultOptionedChild = true;
            node.Optioned = true;
            node.ChildStyle.EnsureDefaultOptionedChild = true;
            if (ch.Number >= 60)
                node.Text = ch.Number.ToString();
            else
                node.Text = (ch.Number - 1).ToString();
            // node.SubItems.Add(new TreeNodeAdvSubItem() { Text = g.Name });
            // var button_node = new TreeNodeAdvSubItem();
            // button_node.Cus
            Node.Nodes.Add(node);

            channel_node.BeforeShow();
            channel_node.EditContainer = this;
            channel_node.Editting = Editting;

        }

        void RemoveChannelFromTree(ctlChannelNode ch)
        {
           
            Node.Nodes.Remove(ch.Node);
        }

        void AddChannelNodes()
        {
            Node.Nodes.Clear();
            if (Group != null)
            {
                foreach (var ch in Group.Channels)
                {
                    AddChannelNode(ch);
                }
            }
        }

        public void SetItemChanged(bool changed)
        {
            EditContainer.SetItemChanged(changed);
        }

      

        public void DeleteItem(object item)
        {
            var gn = (item as ctlChannelNode);
            Data.Channel.DeleteChannel(gn.Channel);
            RemoveChannelFromTree(gn);
            EditContainer.SetItemChanged(true);
        }
    }
}
