﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Interfaces;

namespace NettoolsTest.Controls
{
    public partial class ctlPulseGeneratorSetup : UserControl, IEditControl
    {
        public ctlPulseGeneratorSetup()
        {
            InitializeComponent();
        }

        public Data.Controller Controller { get; set; }

        public IEditContainer EditContainer { get; set; }

        private bool _Editting;
        public bool Editting 
        {
            get => _Editting;
            set
            {
                _Editting = value;
                tbFrequency.Enabled = value;
                tbPeakToPeak.Enabled = value;
            }
        }

        public void BeforeShow()
        {
            CancelChanges();
        }

        public void CancelChanges()
        {
            _updating = true;
            tbFrequency.Text = Controller.Frequency.ToString();
            tbPeakToPeak.Text = Controller.PeakToPeak.ToString();
            _updating = false;
        }

        public void SaveChanges()
        {
            double d;
            if (double.TryParse(tbFrequency.Text, out d))
                Controller.Frequency = d;
            if (double.TryParse(tbPeakToPeak.Text, out d))
                Controller.PeakToPeak = d;
            Data.Controller.Update(Controller);
            Simulator.LoggerFactory.Instance.GetLogger<Simulator.PulseGenerator>(Controller)?.UpdateSettings();
        }

        bool _updating;
        private void tbPeakToPeak_TextChanged(object sender, EventArgs e)
        {
            if (!_updating)
                EditContainer.SetItemChanged(true);
        }

        private void tbFrequency_TextChanged(object sender, EventArgs e)
        {

            if (!_updating)
                EditContainer.SetItemChanged(true);
        }
    }
}
