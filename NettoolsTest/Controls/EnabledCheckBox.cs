﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using log4net;

namespace NettoolsTest.Controls
{
    public class EnabledCheckBox : CheckBox
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(EnabledCheckBox));
        public EnabledCheckBox()
        {
            this.Text = "";
        }

        protected override void OnPaint(PaintEventArgs pevent)
        {
            // base.OnPaint(pevent);
            // int h = this.ClientSize.Height - 2;
            pevent.Graphics.FillRectangle(new SolidBrush(this.BackColor), this.ClientRectangle);
            int h = (this.Font.Height * 3)/2;
            var rc = new Rectangle(new Point(1, this.ClientRectangle.Height / 2 - h / 2), new Size(h, h));

            Log.Debug("ClientHeight: " + this.ClientRectangle.Height + " rc height: " + rc.Height + " rc.top " + rc.Top);

            try
            {
                string imagename;
                if (this.Checked)
                {
                    imagename = "NettoolsTest.Assets.tick.png";
                }
                else
                {
                    imagename = "NettoolsTest.Assets.cross.png";
                }
                Assembly assembly = Assembly.GetExecutingAssembly();
                Stream s = assembly.GetManifestResourceStream(imagename);
                Bitmap bm = new Bitmap(s);
                pevent.Graphics.DrawImage(bm, rc);
            }
            catch (Exception ee)
            {
                System.Diagnostics.Debug.WriteLine("Argh! " + ee.Message);
            }

        }
    }
}
