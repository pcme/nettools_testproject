﻿using Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NettoolsTest.Controls
{
    public partial class ctlPlantStart : UserControl, IEditControl
    {
        public ctlPlantStart()
        {
            InitializeComponent();
        }

        public IEditContainer EditContainer { get; set; }

        public Data.Controller Controller { get; set; }

        public Data.PlantConfig Config { get; set; }
        private bool _editting;
        public bool Editting
        {
            get => _editting;
            set
            {
                _editting = value;

                cbEnabled.Enabled = _editting;
                dtStart.Enabled = _editting;
                dtStop.Enabled = _editting;
                tbDigital.Enabled = _editting;
            }
        }


        private void cbEnable_CheckedChanged(object sender, EventArgs e)
        {

           
            if (!_updating)
                EditContainer.SetItemChanged(true);
        }


        bool _updating;
        public void BeforeShow()
        {
            Config = Data.PlantConfig.Get(Controller.Id);
            if (Config == null)
            {
                Config = new Data.PlantConfig();
                Config.ControllerId = Controller.Id;
            }
            _updating = true;

            tbDigital.Text = Config.DigitalInput.ToString();
            dtStart.Value = Config.Start;
            dtStop.Value = Config.Stop;

            cbEnabled.Checked = Config.Enabled;

            _updating = false;

        }

        public void CancelChanges()
        {
            BeforeShow();
        }

        private void dtStart_ValueChanged(object sender, EventArgs e)
        {
            if (!_updating)
                EditContainer.SetItemChanged(true);
        }

        private void dtStop_ValueChanged(object sender, EventArgs e)
        {
            if (!_updating)
                EditContainer.SetItemChanged(true);
        }

        private void ctlPlantStart_Load(object sender, EventArgs e)
        {
            if (!_updating && (EditContainer != null))
                EditContainer.SetItemChanged(true);
        }

        public void SaveChanges()
        {
            int i;
            if (int.TryParse(tbDigital.Text, out i))
                Config.DigitalInput = i;

            Config.Start = dtStart.Value;
            Config.Stop = dtStop.Value;



            Config.Enabled = cbEnabled.Checked;

            Config.Save();
            BeforeShow();
        }

    }
}
