﻿using Data;
using Interfaces;
using Syncfusion.Windows.Forms.Tools.MultiColumnTreeView;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NettoolsTest.Controls
{
    public partial class ctlGroups : UserControl, IEditControl, IEditContainer
    {
        public ctlGroups()
        {
            InitializeComponent();
        }

        public Data.Controller Controller 
        { 
            get; 
            set; 
        }

        List<Group> _groups;

        public IEditContainer EditContainer { get; set; }

        private bool _Editting;
        public bool Editting 
        {
            get => _Editting;
            set
            {
                _Editting = value;
                if (_Editting)
                {
                  
                    btnAddGroup.Enabled = true;
                }
                else
                {
                    btnAddGroup.Enabled = false;
                }
                if (GroupNodes != null)
                {
                    foreach (var group in GroupNodes)
                    {
                        group.Editting = _Editting;
                    }
                }
            }
        }

        public void SaveChanges()
        {
            foreach (var group in GroupNodes)
            {
                group.SaveChanges();
            }
        

            _groups = Data.Group.GetGroups(Controller.Id);
            Controller.ConfigChanged = true;
            SetupTreeView();
        }
        public void CancelChanges()
        {
            
            foreach (var group in GroupNodes)
            {
                group.CancelChanges();
            }
      

            _groups = Data.Group.GetGroups(Controller.Id);
            SetupTreeView();
        }

        public void DeleteItem(object item)
        {
            var gn = (item as ctlGroupNode);
            Data.Group.DeleteGroup(gn.Group);
         
            RemoveGroupFromTree(gn);
            EditContainer.SetItemChanged(true);
        }

        public void SetItemChanged(bool changed)
        {
            EditContainer.SetItemChanged(changed);
        }

        public void BeforeShow()
        {
           
            _groups = Data.Group.GetGroups(Controller.Id);
            SetupTreeView();
            btnAddGroup.Enabled = false;
            Editting = false;
#if Connectivity
            btnAddGroup.Visible = false;
#endif
        }



        private void btnAddGroup_Click(object sender, EventArgs e)
        {
            Forms.frmAddGroup f = new Forms.frmAddGroup();
            if (DialogResult.OK == f.ShowDialog(this))
            {
                Group g = new Group();
                g.Number = _groups.Count + 1;
                g.Name = f.GroupName;
                g.ControllerId = Controller.Id;
                
                Group.AddGroup(g);
          
                _groups.Add(g);
                AddGroupToTree(g);
                EditContainer.SetItemChanged(true);
            }
        }

        List <ctlGroupNode> GroupNodes { get; set; }

        void RemoveGroupFromTree(ctlGroupNode g)
        {
            multiColumnTreeView1.Nodes.Remove(g.Node);
        }

        void AddGroupToTree(Group g)
        {
            TreeNodeAdv node = new TreeNodeAdv();
            ctlGroupNode group_node = new ctlGroupNode();
            group_node.GroupName = g.Name;
            group_node.Group = g;
            group_node.Node = node;
            node.CustomControl = group_node;
            node.Height = 40;
            node.EnsureDefaultOptionedChild = true;
            node.Optioned = true;
            node.ChildStyle.EnsureDefaultOptionedChild = true;
            node.Text = g.Number.ToString();
           // node.SubItems.Add(new TreeNodeAdvSubItem() { Text = g.Name });
           // var button_node = new TreeNodeAdvSubItem();
           // button_node.Cus
            multiColumnTreeView1.Nodes.Add(node);
            GroupNodes.Add(group_node);
            group_node.BeforeShow();
            group_node.EditContainer = this;
            group_node.Editting = Editting;

#if Connectivity
            node.Expand();
#endif
        }

        void SetupTreeView()
        {
            TreeColumnAdv groupCol = new TreeColumnAdv();
            groupCol.Text = "Groups";
        
            groupCol.Width = this.ClientRectangle.Width - 10;

            multiColumnTreeView1.Columns.Clear();
            multiColumnTreeView1.Columns.AddRange(new TreeColumnAdv[] { groupCol });

            multiColumnTreeView1.Nodes.Clear();

            GroupNodes = new List<ctlGroupNode>();

            foreach (var g in _groups)
            {
                AddGroupToTree(g);         
            }
        }

        private void ctlGroups_SizeChanged(object sender, EventArgs e)
        {
            if (multiColumnTreeView1.Columns.Count > 0)
                multiColumnTreeView1.Columns[0].Width = this.ClientRectangle.Width - 10;
        }

        private void multiColumnTreeView1_BeforeNodePaint(object sender, TreeNodeAdvPaintEventArgs e)
        {
            e.Active = false;
        }
    }
}
