﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Interfaces;
using Syncfusion.Windows.Forms.Tools.MultiColumnTreeView;

namespace NettoolsTest.Controls
{
    public partial class ctlChannelNode : UserControl, IEditControl
    {
        public ctlChannelNode()
        {
            InitializeComponent();
        }

        public IEditContainer EditContainer { get; set; }


        public TreeNodeAdv Node { get; set; }

        public Data.Channel Channel { get; set; }

        public string ChannelName
        {
            get => lblName.Text;
            set { lblName.Text =  value; }
        }

        public string ChannelTypeName
        {
            get => lblTypeName.Text;
            set { lblTypeName.Text = "Type: " + value; }
        }

        public void BeforeShow()
        {
            btnDelete.Visible = false;
        }

        private bool _Editting;
        public bool Editting
        {
            get => _Editting;
            set
            {
                _Editting = value;
                btnDelete.Visible = _Editting;
            }
        }

        public void SaveChanges()
        {

        }
        public void CancelChanges()
        {

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            EditContainer.DeleteItem(this);
        }

        private void lblTypeName_Click(object sender, EventArgs e)
        {
            ShowChannelConfig();
        }

        private void lblName_Click(object sender, EventArgs e)
        {
            ShowChannelConfig();
        }

        void ShowChannelConfig()
        {
            var dt = Communications.DeviceTypes.Instance.deviceTypes.FirstOrDefault(f => f.ChannelTypeId == Channel.ChannelType);
            if (dt == null || dt.Config == null)
            {
                MessageBox.Show("Configuration not defined for " + Channel.ChannelTypeName);
            }
            else
            {
                Forms.frmEditChannelConfig f = new Forms.frmEditChannelConfig();
                f.Edittable = Editting;
                f.Channel = Channel;
                f.ShowDialog(this);

            }
        }
    }
}
