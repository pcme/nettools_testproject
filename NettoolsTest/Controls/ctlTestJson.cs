﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NettoolsTest.Controls
{
    public partial class ctlTestJson : UserControl
    {
        public ctlTestJson()
        {
            InitializeComponent();
            label1.Text = "";
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            label1.Text = "";
            SaveFileDialog d = new SaveFileDialog();
            if (DialogResult.OK == d.ShowDialog())
            {
                string s = Communications.Helper.GenerateTestJson();
                File.WriteAllText(d.FileName, s);
                label1.Text = "Written to " + d.FileName;
            }
        }
    }
}
