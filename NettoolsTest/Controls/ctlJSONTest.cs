﻿using Communications;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NettoolsTest.Controls
{
    public partial class ctlJSONTest : UserControl
    {
        public ctlJSONTest()
        {
            InitializeComponent();
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog d = new OpenFileDialog();
            if (DialogResult.OK == d.ShowDialog())
            {
                try
                {
                    tbJSON.Text = File.ReadAllText(d.FileName);
                }
                catch (Exception ee)
                {
                    tbJSON.Text = "Red failed: " + ee.Message;
                }
            }
        }

        private async void btnSend_Click(object sender, EventArgs e)
        {
            tbLoginResponse.Text = "";
            Cloud cloud = new Cloud();
            string url = tbURL.Text + "/dropbox";
            var resp = await cloud.SendAsync<payloadresponse>(url, Communications.Globals.jwt, tbJSON.Text);
            if (resp == null)
                tbLoginResponse.Text = "null resp";
            else
            {
                if (!resp.status)
                    tbLoginResponse.Text = resp.error;
                else
                    tbLoginResponse.Text = "OK";
            }
           
        }

        private void ctlJSONTest_Load(object sender, EventArgs e)
        {
            tbURL.Text = Properties.Settings.Default.CloudURL;
        }
    }
}
