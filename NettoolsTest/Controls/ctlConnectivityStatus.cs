﻿using Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using log4net;
using System.Threading;

namespace NettoolsTest.Controls
{
    public partial class ctlConnectivityStatus : UserControl
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(ctlConnectivityStatus));

        public ctlConnectivityStatus()
        {
            InitializeComponent();
        }

        public event HandleConnectionStatus ConnectionStatus;

        private void ctlConnectivityStatus_Load(object sender, EventArgs e)
        {

        }

        public void BeforeShow()
        {
            var controllers = Data.Controller.GetControllers();
            var c = controllers.FirstOrDefault(ff => ff.Number == 1);

            Simulator.LoggerFactory.Instance.GetLogger<Simulator.PulseGenerator>(c).EnabledChanged += CtlConnectivityStatus_EnabledChanged;
            payload = new PayloadChecker(c);
            cert = new CertChecker(c);
            dns = new DNSChecker();
            pinger = new PingChecker();

            ctlConnectionStatus1.StatusProvider = pinger;
            ctlConnectionStatus1.BeforeShow();
            ctlConnectionStatus1.Enabled = false;

            ctlConnectionStatus2.StatusProvider = payload;
            ctlConnectionStatus2.BeforeShow();
            ctlConnectionStatus2.Enabled = false;

            ctlConnectionStatus3.StatusProvider = cert;
            ctlConnectionStatus3.BeforeShow();
            ctlConnectionStatus3.Enabled = false;

            ctlConnectionStatus4.StatusProvider = dns;
            ctlConnectionStatus4.BeforeShow();
            ctlConnectionStatus4.Enabled = false;
        }

        private void CtlConnectivityStatus_EnabledChanged(Simulator.ILogger logger)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(() => CtlConnectivityStatus_EnabledChanged(logger)));
            }
            else
            {
                if (logger.Enabled)
                {
                    ctlConnectionStatus1.Enabled = true;
                    ctlConnectionStatus2.Enabled = true;
                    ctlConnectionStatus3.Enabled = true;
                    ctlConnectionStatus4.Enabled = true;
                    dns.CheckNow();
                    enabled = true;
                    Pinger();

                }
                else
                {
                    enabled = false;
                    ctlConnectionStatus1.Enabled = false;
                    ctlConnectionStatus2.Enabled = false;
                    ctlConnectionStatus3.Enabled = false;
                    ctlConnectionStatus4.Enabled = false;
                }
            }
        }

        bool enabled;


        void Pinger()
        {
            Task.Run(async () =>
            {
                while (enabled)
                {

                    bool r = await pinger.CheckNow();
                    if (!r)
                        Thread.Sleep(10000);
                    else
                        Thread.Sleep(30000);
                }
            });
        }

        PayloadChecker payload;
        CertChecker cert;
        DNSChecker dns;
        PingChecker pinger;
    }

    public class CertChecker : ICloudConnectionStatus, Interfaces.IStatusProvider
    {
        Data.Controller _Controller;
        public CertChecker(Data.Controller c)
        {
            _Controller = c;
            c.StatusChanged += C_StatusChanged1;
            Status = this;

        }


        private void C_StatusChanged1(Data.Controller c, ICloudDataStatus status)
        {

            Connected = !status.Failed && (status.LastError != IOTErrors.CertificateError);

            ConnectionStatus?.Invoke(this);
        }

        public bool Connected { get; set; }
        public ICloudConnectionStatus Status { get; set; }
        public event HandleConnectionStatus ConnectionStatus;



    }

    public class DNSChecker : IStatusProvider, ICloudConnectionStatus
    {
        public DNSChecker()
        {
            Status = this;
        }

        public void CheckNow()
        {

            Connected = Communications.dnscheck.CheckDNS(Communications.Globals.URL).Connected;

            ConnectionStatus?.Invoke(this);
        }
        public ICloudConnectionStatus Status { get; set; }

        public bool Connected { get; set; }

        public event HandleConnectionStatus ConnectionStatus;
    }


    public class PingChecker : IStatusProvider, ICloudConnectionStatus
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(PingChecker));

        public PingChecker()
        {
            Status = this;
        }

        public async Task<bool> CheckNow()
        {
            bool result = true;

            string url = Properties.Settings.Default.Prefix + "-ats.iot." + Properties.Settings.Default.Region + ".amazonaws.com";

            var r = await Communications.utc.PingURL(url);
            if (!r.Connected)
            {
                Log.Debug("Ping " + url + " failed");
                result = false;
            }
            url =  "greengrass-ats.iot." + Properties.Settings.Default.Region + ".amazonaws.com";
            r = await Communications.utc.PingURL(url);
            if (!r.Connected)
            {
                Log.Debug("Ping " + url + " failed");
                result = false;
            }

            url = Properties.Settings.Default.Prefix + ".credentials.iot." + Properties.Settings.Default.Region + ".amazonaws.com";
            r = await Communications.utc.PingURL(url);
            if (!r.Connected)
            {
                Log.Debug("Ping " + url + " failed");
                result = false;
            }

            url = Properties.Settings.Default.Prefix + ".s3.amazonaws.com";
            r = await Communications.utc.PingURL(url);
            if (!r.Connected)
            {
                Log.Debug("Ping " + url + " failed");
                result = false;
            }

            url = Properties.Settings.Default.Prefix + ".s3." + Properties.Settings.Default.Region +".amazonaws.com";
            r = await Communications.utc.PingURL(url);
            if (!r.Connected)
            {
                Log.Debug("Ping " + url + " failed");
                result = false;
            }
            Connected = result;

            ConnectionStatus?.Invoke(this);

            return result;
        }
        public ICloudConnectionStatus Status { get; set; }

        public bool Connected { get; set; }

        public event HandleConnectionStatus ConnectionStatus;
    }

    public class PayloadChecker : ICloudConnectionStatus, Interfaces.IStatusProvider
    {
        Data.Controller _Controller;
        public PayloadChecker(Data.Controller c)
        {
            _Controller = c;
            c.StatusChanged += C_StatusChanged1;
            Status = this;

        }


        private void C_StatusChanged1(Data.Controller c, ICloudDataStatus status)
        {

            Connected = !status.Failed;

            ConnectionStatus?.Invoke(this);
        }

        public bool Connected { get; set; }
        public ICloudConnectionStatus Status { get; set; }
        public event HandleConnectionStatus ConnectionStatus;



    }

    
}
