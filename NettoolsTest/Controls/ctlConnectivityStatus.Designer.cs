﻿
namespace NettoolsTest.Controls
{
    partial class ctlConnectivityStatus
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.ctlConnectionStatus1 = new NettoolsTest.Controls.ctlConnectionStatus();
            this.ctlConnectionStatus2 = new NettoolsTest.Controls.ctlConnectionStatus();
            this.ctlConnectionStatus3 = new NettoolsTest.Controls.ctlConnectionStatus();
            this.ctlConnectionStatus4 = new NettoolsTest.Controls.ctlConnectionStatus();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.ctlConnectionStatus1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.ctlConnectionStatus2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.ctlConnectionStatus3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.ctlConnectionStatus4, 0, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(608, 675);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // ctlConnectionStatus1
            // 
            this.ctlConnectionStatus1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlConnectionStatus1.Enabled = false;
            this.ctlConnectionStatus1.Label = "Connection Status";
            this.ctlConnectionStatus1.Location = new System.Drawing.Point(8, 7);
            this.ctlConnectionStatus1.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.ctlConnectionStatus1.Name = "ctlConnectionStatus1";
            this.ctlConnectionStatus1.Size = new System.Drawing.Size(592, 121);
            this.ctlConnectionStatus1.StatusProvider = null;
            this.ctlConnectionStatus1.TabIndex = 0;
            // 
            // ctlConnectionStatus2
            // 
            this.ctlConnectionStatus2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlConnectionStatus2.Enabled = false;
            this.ctlConnectionStatus2.Label = "Payload Transferred";
            this.ctlConnectionStatus2.Location = new System.Drawing.Point(8, 142);
            this.ctlConnectionStatus2.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.ctlConnectionStatus2.Name = "ctlConnectionStatus2";
            this.ctlConnectionStatus2.Size = new System.Drawing.Size(592, 121);
            this.ctlConnectionStatus2.StatusProvider = null;
            this.ctlConnectionStatus2.TabIndex = 1;
            // 
            // ctlConnectionStatus3
            // 
            this.ctlConnectionStatus3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlConnectionStatus3.Enabled = false;
            this.ctlConnectionStatus3.Label = "Security validated";
            this.ctlConnectionStatus3.Location = new System.Drawing.Point(8, 277);
            this.ctlConnectionStatus3.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.ctlConnectionStatus3.Name = "ctlConnectionStatus3";
            this.ctlConnectionStatus3.Size = new System.Drawing.Size(592, 121);
            this.ctlConnectionStatus3.StatusProvider = null;
            this.ctlConnectionStatus3.TabIndex = 2;
            // 
            // ctlConnectionStatus4
            // 
            this.ctlConnectionStatus4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlConnectionStatus4.Enabled = false;
            this.ctlConnectionStatus4.Label = "DNS";
            this.ctlConnectionStatus4.Location = new System.Drawing.Point(8, 412);
            this.ctlConnectionStatus4.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.ctlConnectionStatus4.Name = "ctlConnectionStatus4";
            this.ctlConnectionStatus4.Size = new System.Drawing.Size(592, 121);
            this.ctlConnectionStatus4.StatusProvider = null;
            this.ctlConnectionStatus4.TabIndex = 3;
            // 
            // ctlConnectivityStatus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "ctlConnectivityStatus";
            this.Size = new System.Drawing.Size(608, 675);
            this.Load += new System.EventHandler(this.ctlConnectivityStatus_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private ctlConnectionStatus ctlConnectionStatus1;
        private ctlConnectionStatus ctlConnectionStatus2;
        private ctlConnectionStatus ctlConnectionStatus3;
        private ctlConnectionStatus ctlConnectionStatus4;
    }
}
