﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using log4net;

namespace NettoolsTest.Controls
{
    public partial class ctlSummary : UserControl
    {
        public ctlSummary()
        {
            InitializeComponent();
            _first_run = true;
        }

        bool _first_run;

        public void BeforeShow()
        {
            var controllers = Data.Controller.GetControllers();
            if (_first_run)
            {
                _first_run = false;
                ctlControllerSummary1.Controllers.Add(controllers.FirstOrDefault(f => f.Number == 1));
                ctlControllerSummary2.Controllers.Add(controllers.FirstOrDefault(f => f.Number == 2));
                ctlControllerSummary3.Controllers.Add(controllers.FirstOrDefault(f => f.Number == 3));
                ctlControllerSummary4.Controllers.Add(controllers.FirstOrDefault(f => f.Number == 4));
                ctlControllerSummary5.Controllers.Add(controllers.FirstOrDefault(f => f.Number == 5));
                ctlControllerSummary6.Controllers.Add(controllers.FirstOrDefault(f => f.Number == 6));
                ctlControllerSummary7.Controllers.Add(controllers.FirstOrDefault(f => f.Number == 7));
                ctlControllerSummary8.Controllers.Add(controllers.FirstOrDefault(f => f.Number == 8));
                ctlControllerSummary9.Controllers.Add(controllers.FirstOrDefault(f => f.Number == 9));
                ctlControllerSummary10.Controllers.Add(controllers.FirstOrDefault(f => f.Number == 10));

                ctlControllerSummary11.Controllers.AddRange(controllers.Where(f => f.Number > 10 && f.Number <= 20));
                ctlControllerSummary12.Controllers.AddRange(controllers.Where(f => f.Number > 20 && f.Number <= 30));
                ctlControllerSummary13.Controllers.AddRange(controllers.Where(f => f.Number > 30 && f.Number <= 40));
                ctlControllerSummary14.Controllers.AddRange(controllers.Where(f => f.Number > 40 && f.Number <= 50));
                ctlControllerSummary15.Controllers.AddRange(controllers.Where(f => f.Number > 50 && f.Number <= 60));
                ctlControllerSummary16.Controllers.AddRange(controllers.Where(f => f.Number > 60 && f.Number <= 70));
                ctlControllerSummary17.Controllers.AddRange(controllers.Where(f => f.Number > 70 && f.Number <= 80));
                ctlControllerSummary18.Controllers.AddRange(controllers.Where(f => f.Number > 80 && f.Number <= 90));
                ctlControllerSummary19.Controllers.AddRange(controllers.Where(f => f.Number > 90 && f.Number <= 100));
            }
            ctlConnectionStatus1.BeforeShow();

            ctlControllerSummary1.BeforeShow();
            ctlControllerSummary2.BeforeShow();
            ctlControllerSummary3.BeforeShow();
            ctlControllerSummary4.BeforeShow();
            ctlControllerSummary5.BeforeShow();
            ctlControllerSummary6.BeforeShow();
            ctlControllerSummary7.BeforeShow();
            ctlControllerSummary8.BeforeShow();
            ctlControllerSummary9.BeforeShow();
            ctlControllerSummary10.BeforeShow();
            ctlControllerSummary11.BeforeShow();
            ctlControllerSummary12.BeforeShow();
            ctlControllerSummary13.BeforeShow();
            ctlControllerSummary14.BeforeShow();
            ctlControllerSummary15.BeforeShow();
            ctlControllerSummary16.BeforeShow();
            ctlControllerSummary17.BeforeShow();
            ctlControllerSummary18.BeforeShow();
            ctlControllerSummary19.BeforeShow();
        }

        public void BeforeHide()
        {
            ctlConnectionStatus1.BeforeHide();

            ctlControllerSummary1.BeforeHide();
            ctlControllerSummary2.BeforeHide();
            ctlControllerSummary3.BeforeHide();
            ctlControllerSummary4.BeforeHide();
            ctlControllerSummary5.BeforeHide();
            ctlControllerSummary6.BeforeHide();
            ctlControllerSummary7.BeforeHide();
            ctlControllerSummary8.BeforeHide();
            ctlControllerSummary9.BeforeHide();
            ctlControllerSummary10.BeforeHide();
            ctlControllerSummary11.BeforeHide();
            ctlControllerSummary12.BeforeHide();
            ctlControllerSummary13.BeforeHide();
            ctlControllerSummary14.BeforeHide();
            ctlControllerSummary15.BeforeHide();
            ctlControllerSummary16.BeforeHide();
            ctlControllerSummary17.BeforeHide();
            ctlControllerSummary18.BeforeHide();
            ctlControllerSummary19.BeforeHide();
        }

    
    }
}
