﻿
namespace NettoolsTest.Controls
{
    partial class ctlDateMode
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.rbPCClock = new System.Windows.Forms.RadioButton();
            this.rbFixedClock = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.dtDate = new System.Windows.Forms.DateTimePicker();
            this.dtTime = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Clock Mode";
            // 
            // rbPCClock
            // 
            this.rbPCClock.AutoSize = true;
            this.rbPCClock.Location = new System.Drawing.Point(73, 11);
            this.rbPCClock.Name = "rbPCClock";
            this.rbPCClock.Size = new System.Drawing.Size(47, 17);
            this.rbPCClock.TabIndex = 1;
            this.rbPCClock.TabStop = true;
            this.rbPCClock.Text = "UTC";
            this.rbPCClock.UseVisualStyleBackColor = true;
            this.rbPCClock.CheckedChanged += new System.EventHandler(this.rbPCClock_CheckedChanged);
            // 
            // rbFixedClock
            // 
            this.rbFixedClock.AutoSize = true;
            this.rbFixedClock.Location = new System.Drawing.Point(73, 34);
            this.rbFixedClock.Name = "rbFixedClock";
            this.rbFixedClock.Size = new System.Drawing.Size(80, 17);
            this.rbFixedClock.TabIndex = 2;
            this.rbFixedClock.TabStop = true;
            this.rbFixedClock.Text = "Fixed Clock";
            this.rbFixedClock.UseVisualStyleBackColor = true;
            this.rbFixedClock.CheckedChanged += new System.EventHandler(this.rbFixedClock_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(159, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Start Time";
            // 
            // dtDate
            // 
            this.dtDate.Location = new System.Drawing.Point(211, 34);
            this.dtDate.Name = "dtDate";
            this.dtDate.Size = new System.Drawing.Size(146, 20);
            this.dtDate.TabIndex = 4;
            this.dtDate.ValueChanged += new System.EventHandler(this.dtDate_ValueChanged);
            // 
            // dtTime
            // 
            this.dtTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtTime.Location = new System.Drawing.Point(363, 34);
            this.dtTime.Name = "dtTime";
            this.dtTime.ShowUpDown = true;
            this.dtTime.Size = new System.Drawing.Size(86, 20);
            this.dtTime.TabIndex = 5;
            this.dtTime.ValueChanged += new System.EventHandler(this.dtTime_ValueChanged);
            // 
            // ctlDateMode
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dtTime);
            this.Controls.Add(this.dtDate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.rbFixedClock);
            this.Controls.Add(this.rbPCClock);
            this.Controls.Add(this.label1);
            this.Name = "ctlDateMode";
            this.Size = new System.Drawing.Size(478, 65);
            this.Load += new System.EventHandler(this.ctlDateMode_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rbPCClock;
        private System.Windows.Forms.RadioButton rbFixedClock;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtDate;
        private System.Windows.Forms.DateTimePicker dtTime;
    }
}
