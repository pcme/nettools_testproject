﻿
namespace NettoolsTest
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.ctlTestComms1 = new NettoolsTest.Controls.ctlTestComms();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.ctlJSONTest1 = new NettoolsTest.Controls.ctlJSONTest();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.ctlTestJson1 = new NettoolsTest.Controls.ctlTestJson();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.ctlSummary1 = new NettoolsTest.Controls.ctlSummary();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(940, 537);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.ctlTestComms1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(932, 511);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Login";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // ctlTestComms1
            // 
            this.ctlTestComms1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlTestComms1.Location = new System.Drawing.Point(3, 3);
            this.ctlTestComms1.Name = "ctlTestComms1";
            this.ctlTestComms1.Size = new System.Drawing.Size(926, 505);
            this.ctlTestComms1.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.ctlJSONTest1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(932, 511);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "JSON Test";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // ctlJSONTest1
            // 
            this.ctlJSONTest1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlJSONTest1.Location = new System.Drawing.Point(3, 3);
            this.ctlJSONTest1.Name = "ctlJSONTest1";
            this.ctlJSONTest1.Size = new System.Drawing.Size(926, 505);
            this.ctlJSONTest1.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.ctlTestJson1);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(932, 511);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "JSON Generate";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // ctlTestJson1
            // 
            this.ctlTestJson1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlTestJson1.Location = new System.Drawing.Point(3, 3);
            this.ctlTestJson1.Name = "ctlTestJson1";
            this.ctlTestJson1.Size = new System.Drawing.Size(926, 505);
            this.ctlTestJson1.TabIndex = 0;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.ctlSummary1);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(932, 511);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "DashBoard";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // ctlSummary1
            // 
            this.ctlSummary1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlSummary1.Location = new System.Drawing.Point(3, 3);
            this.ctlSummary1.Name = "ctlSummary1";
            this.ctlSummary1.Size = new System.Drawing.Size(926, 505);
            this.ctlSummary1.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(940, 537);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Envea NETtools Test";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private Controls.ctlTestComms ctlTestComms1;
        private System.Windows.Forms.TabPage tabPage2;
        private Controls.ctlJSONTest ctlJSONTest1;
        private System.Windows.Forms.TabPage tabPage3;
        private Controls.ctlTestJson ctlTestJson1;
        private System.Windows.Forms.TabPage tabPage4;
        private Controls.ctlSummary ctlSummary1;
    }
}

