﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using log4net;

namespace NettoolsTest.Pages
{
    public partial class Summary : BasePage
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(Summary));

        public Summary()
        {
            InitializeComponent();
        }

        public override void BeforeShow()
        {
            base.BeforeShow();
          
            ctlSummary1.BeforeShow();
        }

        public override void BeforeHide()
        {
            base.BeforeHide();
            ctlSummary1.BeforeHide();
        }
    }
}
