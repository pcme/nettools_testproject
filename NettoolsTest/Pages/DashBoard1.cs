﻿using Interfaces;
using Syncfusion.WinForms.DataGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using log4net;

namespace NettoolsTest.Pages
{
    public partial class DashBoard1 : BasePage, IEditContainer
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(DashBoard1));

        public DashBoard1()
        {
            InitializeComponent();
            grid1.CurrentCellEndEdit += Grid1_CurrentCellEndEdit;
            ctlGroups1.EditContainer = this;
            ctlPulseGeneratorSetup1.EditContainer = this;
            grid1.CellButtonClick += Grid1_CellButtonClick;
            ctlPredictConfig1.EditContainer = this;
            ctlPlantStart1.EditContainer = this;
            ctlSpanZero1.EditContainer = this;

#if Connectivity
            AddConnectivityControls();
#endif
        }

        private void Grid1_CellButtonClick(object sender, Syncfusion.WinForms.DataGrid.Events.CellButtonClickEventArgs e)
        {

            var r = ((e.Record as Syncfusion.WinForms.DataGrid.DataRow).RowData as Data.Controller);
            Forms.frmEditSecurityKey f = new Forms.frmEditSecurityKey();
            f.Controller = r;
            f.Editting = _editting;
            f.ShowDialog();
        }

        bool _setting_changed;
        private void Grid1_CurrentCellEndEdit(object sender, Syncfusion.WinForms.DataGrid.Events.CurrentCellEndEditEventArgs e)
        {
            var data = (Syncfusion.Data.RecordEntry)grid1.GetRecordEntryAtRowIndex(grid1.CurrentCell.RowIndex);
            var setting = (data.Data as Data.Controller);
            _setting_changed = true;
            SetSaveButton();
        }

        public bool MultipleControllers { get; set; }

        Data.Controller Controller;
        List<Data.Controller> Controllers { get; set; }
        public override void BeforeShow()
        {
            _editting = false;
            btnBack.Enabled = true;
            btnSave.Visible = false;
            btnEdit.Text = "Edit";

            base.BeforeShow();

            if (ScreenData is Data.Controller)
                MultipleControllers = false;
            else
                MultipleControllers = true;

            if (MultipleControllers)
            {
                Controllers = (List<Data.Controller>)ScreenData;
                lblTitle.Text = "Configuration " + Controllers.First().Number.ToString() + " - " + Controllers.Last().Number.ToString();
                Controller = Controllers[0];
                grid1.Visible = false;
                ctlAlarmSimulation1.Visible = false;
            }
            else
            {
                Controller = (Data.Controller)ScreenData;
                Controllers = new List<Data.Controller>();
                Controllers.Add(Controller);
                ctlAlarmSimulation1.Visible = true;

                lblTitle.Text = "Dashboard " + Controllers.First().Number.ToString();
                grid1.Visible = true;
                FillGrid();
            }

            ctlGroups1.Controller = Controller;
            ctlPulseGeneratorSetup1.Controller = Controller;
            ctlPredictConfig1.Controller = Controller;
            ctlSpanZero1.Controller = Controller;
            ctlDigital1.Controller = Controller;
            ctlPlantStart1.Controller = Controller;
            ctlAlarmSimulation1.Controller = Controller;
         
            ctlGroups1.BeforeShow();
            ctlConnectionStatus1.BeforeShow();
            ctlPulseGeneratorSetup1.BeforeShow();
            ctlPredictConfig1.BeforeShow();
            ctlSpanZero1.BeforeShow();
            ctlDigital1.BeforeShow();
            ctlPlantStart1.BeforeShow();
            ctlAlarmSimulation1.BeforeShow();

            ctlSpanZero1.Editting = false;
            ctlPredictConfig1.Editting = false;
            ctlPulseGeneratorSetup1.Editting = false;
            ctlPlantStart1.Editting = false;
            ctlAlarmSimulation1.Editting = false;

#if Connectivity
            ctlSpanZero1.Visible = false;
            ctlPredictConfig1.Visible = false;
            ctlPulseGeneratorSetup1.Visible = false;
            ctlPlantStart1.Visible = false;
            ctlAlarmSimulation1.Visible = false;

            btnBack.Visible = false;
            ctlConnectionStatus1.Visible = false;
            ctlDigital1.Visible = false;


            ctlConnectivityStatus1.BeforeShow();
            ctlConnectivityControls1.BeforeShow();
           
#endif
        }

        public override void BeforeHide()
        {
            base.BeforeHide();
            ctlConnectionStatus1.BeforeHide();
            ctlDigital1.BeforeHide();
            if (MultipleControllers)
            {
                if (Controller.ConfigChanged)
                {
                    var json = new Simulator.JSONBuilder();
                    string hash;
                    DateTime utc = Simulator.ClockMode.GetTime;
                    foreach (var c in Controllers)
                    {
                       
                        var json_data = json.GetConfigHash(c, utc, Controller, out hash);
                        c.UpdateConfig(json_data, hash, utc);
                    }
                }
            }
            else
            {
                if (Controller.ConfigChanged)
                {
                    string hash;
                    DateTime utc = Simulator.ClockMode.GetTime;
                    var json = new Simulator.JSONBuilder();
                    var json_data = json.GetConfigHash(Controller, utc, out hash);
                    Controller.UpdateConfig(json_data, hash, utc);
                }
            }
        }


        private void FillGrid()
        {

            grid1.AutoSizeColumnsMode = Syncfusion.WinForms.DataGrid.Enums.AutoSizeColumnsMode.Fill;
            grid1.AutoGenerateColumns = false;

            grid1.DataSource = Controllers;

            grid1.AllowEditing = false;

            grid1.Columns.Clear();
            grid1.Columns.Add(new GridTextColumn() { MappingName = "Id", HeaderText = "ID", Visible = false });
            grid1.Columns.Add(new GridTextColumn() { MappingName = "Number", HeaderText = "Number", Visible = false });
            grid1.Columns.Add(new GridTextColumn() { MappingName = "Name", HeaderText = "Name" });
            grid1.Columns.Add(new GridTextColumn() { MappingName = "ControllerType", HeaderText = "Type" });
            grid1.Columns.Add(new GridTextColumn() { MappingName = "SerialNumber", HeaderText = "Serial number" });
            grid1.Columns.Add(new GridTextColumn() { MappingName = "MacAddress", HeaderText = "Mac Address" });
            grid1.Columns.Add(new GridTextColumn() { MappingName = "FirmwareVersion", HeaderText = "Firmware version" });
            grid1.Columns.Add(new GridTextColumn() { MappingName = "Date", HeaderText = "Date" });
            grid1.Columns.Add(new GridTextColumn() { MappingName = "GitId", HeaderText = "Git Id" });
            grid1.Columns.Add(new GridTextColumn() { MappingName = "ProtocolVersion", HeaderText = "Protocol version" });

            grid1.Columns.Add(new GridButtonColumn() { MappingName = "SecurityCertificate", HeaderText="Device Security", AllowDefaultButtonText = true, DefaultButtonText = "Edit..." });

           // grid1.Columns.Add(new GridTextColumn() { MappingName = "SecurityCertificate", HeaderText = "Security Certificate" });

            for (int i = 2; i < 10; i++)
            {
                grid1.Columns[i].AllowEditing = true;
            }


            int row0Height;
            grid1.AutoSizeController.GetAutoRowHeight(0, new RowAutoFitOptions() { AutoSizeCalculationMode = Syncfusion.WinForms.DataGrid.Enums.AutoSizeCalculationMode.Default }, out row0Height);
            int rowHeight;
            grid1.AutoSizeController.GetAutoRowHeight(1, new RowAutoFitOptions() { AutoSizeCalculationMode = Syncfusion.WinForms.DataGrid.Enums.AutoSizeCalculationMode.Default }, out rowHeight);
            grid1.Height = row0Height + row0Height + 30;

            Log.Debug("Set height " + row0Height.ToString() + "," + rowHeight.ToString());

            for (int i = 2; i < 11; i++)
            {
                grid1.Columns[i].AllowEditing = _editting;
            }
        }




        private void btnBack_Click(object sender, EventArgs e)
        {
            
            Globals.Container.BackPage();
        }


        void Reload()
        {
            if (MultipleControllers)
            {

            }
            else
            {
                Controller = Data.Controller.GetController(Controller.Id);
                Controllers = new List<Data.Controller>();
                Controllers.Add(Controller);
                FillGrid();
            }

        }




        bool _editting;
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (_editting)
            {
                _editting = false;
                if (_setting_changed)
                {
                    Reload();
                    ctlGroups1.CancelChanges();
                    ctlPulseGeneratorSetup1.CancelChanges();
                    ctlPredictConfig1.CancelChanges();
                    ctlSpanZero1.CancelChanges();
                    ctlPlantStart1.CancelChanges();
                }

                btnBack.Enabled = true;
                btnSave.Visible = false;
                btnEdit.Text = "Edit";
            }
            else
            {
                if (!Simulator.LoggerFactory.Instance.GetLogger<Simulator.PulseGenerator>(Controller).Enabled)
                {
                    _editting = true;
                    btnSave.Visible = true;
                    btnSave.Enabled = false;
                    btnEdit.Text = "Cancel";
                }
                else
                {
                    MessageBox.Show("Cannot edit setting while controller enabled");
                }
            }

#if Connectivity
#else
            ctlPredictConfig1.Editting = _editting;
            ctlSpanZero1.Editting = _editting;
            ctlGroups1.Editting = _editting;
            ctlPulseGeneratorSetup1.Editting = _editting;
            ctlPlantStart1.Editting = _editting;
            ctlAlarmSimulation1.Editting = _editting;
#endif

            if (!MultipleControllers)
            {
                for (int i = 2; i < 11; i++)
                {
                    grid1.Columns[i].AllowEditing = _editting;
                }
            }

        }

#if Connectivity
        private Controls.ctlConnectivityControls ctlConnectivityControls1;
        private Controls.ctlConnectivityStatus ctlConnectivityStatus1;

        void AddConnectivityControls()
        {


            this.ctlConnectivityControls1 = new NettoolsTest.Controls.ctlConnectivityControls();
            this.ctlConnectivityStatus1 = new NettoolsTest.Controls.ctlConnectivityStatus();

            this.ctlConnectivityControls1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ctlConnectivityControls1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlConnectivityControls1.Location = new System.Drawing.Point(3, 228);
            this.ctlConnectivityControls1.Name = "ctlConnectivityControls1";
            this.ctlConnectivityControls1.Size = new System.Drawing.Size(354, 219);
            this.ctlConnectivityControls1.TabIndex = 4;

            this.ctlConnectivityStatus1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ctlConnectivityStatus1.Location = new System.Drawing.Point(8, 457);
            this.ctlConnectivityStatus1.Dock = System.Windows.Forms.DockStyle.Fill;

            this.ctlConnectivityStatus1.Name = "ctlConnectivityStatus1";
            this.ctlConnectivityStatus1.Size = new System.Drawing.Size(344, 93);
            this.ctlConnectivityStatus1.TabIndex = 5;



            this.tableLayoutPanel2.SetColumnSpan(this.ctlConnectivityControls1, 2);
            this.tableLayoutPanel2.SetColumnSpan(this.ctlConnectivityStatus1, 2);
            this.tableLayoutPanel2.SetRowSpan(this.ctlConnectivityStatus1, 2);

            this.tableLayoutPanel2.Controls.Add(this.ctlConnectivityControls1, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.ctlConnectivityStatus1, 1, 1);
        }
#endif
        void SetSaveButton()
        {
            if (_setting_changed)
            {
                btnSave.Enabled = true;
                btnBack.Enabled = false;
            }
            else
            {
                btnSave.Enabled = false;
                btnBack.Enabled = true;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (_setting_changed)
            {

                Data.Controller.Update(Controller);
                ctlGroups1.SaveChanges();
                ctlPulseGeneratorSetup1.SaveChanges();
                ctlPredictConfig1.SaveChanges();
                ctlSpanZero1.SaveChanges();
                ctlPlantStart1.SaveChanges();

                Controller.ConfigChanged = true;
                _setting_changed = false;
                SetSaveButton();
                Reload();
            }
        }

        public void SetItemChanged(bool changed)
        {
            if (changed)
            {
                _setting_changed = true;
                SetSaveButton();
            }
        }

        public void DeleteItem(object item)
        {
            _setting_changed = true;
        }
    }
}
