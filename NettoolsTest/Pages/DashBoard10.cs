﻿using Syncfusion.WinForms.DataGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NettoolsTest.Pages
{
    public partial class DashBoard10 : BasePage
    {
        public DashBoard10()
        {
            InitializeComponent();

            grid1.CurrentCellEndEdit += Grid1_CurrentCellEndEdit;
            grid1.CellButtonClick += Grid1_CellButtonClick;
        }

        private void Grid1_CellButtonClick(object sender, Syncfusion.WinForms.DataGrid.Events.CellButtonClickEventArgs e)
        {

            var r = ((e.Record as Syncfusion.WinForms.DataGrid.DataRow).RowData as Data.Controller);
            Forms.frmEditSecurityKey f = new Forms.frmEditSecurityKey();
            f.Controller = r;
            f.Editting = true;
            f.ShowDialog();
        }

        private void Grid1_CurrentCellEndEdit(object sender, Syncfusion.WinForms.DataGrid.Events.CurrentCellEndEditEventArgs e)
        {
            var data = (Syncfusion.Data.RecordEntry)grid1.GetRecordEntryAtRowIndex(grid1.CurrentCell.RowIndex);
            var setting = (data.Data as Data.Controller);

            Data.Controller.Update(setting);
        }

        List<Data.Controller> Controllers { get; set; }
        public override void BeforeShow()
        {
            base.BeforeShow();
            if (ScreenData != null)
                Controllers = (List<Data.Controller>)ScreenData;
            lblTitle.Text = "Dashboard " + Controllers.First().Number.ToString() + "-" + Controllers.Last().Number.ToString();
            FillGrid();
        }


        private void FillGrid()
        { 

            grid1.AutoSizeColumnsMode = Syncfusion.WinForms.DataGrid.Enums.AutoSizeColumnsMode.Fill;
            grid1.AutoGenerateColumns = false;
            grid1.DataSource = Controllers;
            
            grid1.AllowEditing = false;

            grid1.Columns.Clear();
            grid1.Columns.Add(new GridTextColumn() { MappingName = "Id", HeaderText = "ID", Visible = false });
            grid1.Columns.Add(new GridTextColumn() { MappingName = "Number", HeaderText = "Number" });
            grid1.Columns.Add(new GridTextColumn() { MappingName = "Name", HeaderText = "Name" });
            grid1.Columns.Add(new GridTextColumn() { MappingName = "ControllerType", HeaderText = "Type" });
            grid1.Columns.Add(new GridTextColumn() { MappingName = "SerialNumber", HeaderText = "Serial number" });
            grid1.Columns.Add(new GridTextColumn() { MappingName = "MacAddress", HeaderText = "Mac Address" });
            grid1.Columns.Add(new GridTextColumn() { MappingName = "FirmwareVersion", HeaderText = "Firmware version" });
            grid1.Columns.Add(new GridTextColumn() { MappingName = "Date", HeaderText = "Date" });
            grid1.Columns.Add(new GridTextColumn() { MappingName = "GitId", HeaderText = "Git Id" });
            grid1.Columns.Add(new GridTextColumn() { MappingName = "ProtocolVersion", HeaderText = "Protocol version" });
            grid1.Columns.Add(new GridButtonColumn() { MappingName = "SecurityCertificate", HeaderText = "Security Certificate", AllowDefaultButtonText = true, DefaultButtonText = "Edit..." });
         

            for (int i = 2; i < 10; i++)
            {
                grid1.Columns[i].AllowEditing = true;
            }

        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            Globals.Container.BackPage();
        }

        private void btnEditChannels_Click(object sender, EventArgs e)
        {
            Globals.Container.LoadPage(typeof(Pages.DashBoard1), Controllers);
        }
    }
}
