﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NettoolsTest.Pages
{
    public partial class BasePage : UserControl
    {
        public object ScreenData { get; set; }
        public BasePage()
        {
            InitializeComponent();
        }

        public virtual void BeforeHide()
        {
        }

        public virtual void AfterHide()
        {
            // clear the access enable control so that next timeit is shown it defaults to enabled

        }


        private void BeforeShowControl(ControlCollection container)
        {
            foreach (Control c in container)
            {
                /*

                if (c.GetType() == typeof(Controls.HMIButton))
                {
                    (c as Controls.HMIButton).BeforeShow();
                }
                if (c.GetType() == typeof(Controls.HMICenterButton))
                {
                    (c as Controls.HMICenterButton).BeforeShow();
                }
                if (c.GetType() == typeof(Controls.BackButton))
                {
                    (c as Controls.BackButton).BeforeShow();
                }
                */

                BeforeShowControl(c.Controls);
            }
        }

        public virtual void BeforeShow()
        {
            BeforeShowControl(this.Controls);
        }



        public virtual void AfterShow()
        {

        }
    }
}
