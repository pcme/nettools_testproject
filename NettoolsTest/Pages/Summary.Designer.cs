﻿
namespace NettoolsTest.Pages
{
    partial class Summary
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ctlSummary1 = new NettoolsTest.Controls.ctlSummary();
            this.SuspendLayout();
            // 
            // ctlSummary1
            // 
            this.ctlSummary1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlSummary1.Location = new System.Drawing.Point(0, 0);
            this.ctlSummary1.Name = "ctlSummary1";
            this.ctlSummary1.Size = new System.Drawing.Size(570, 379);
            this.ctlSummary1.TabIndex = 0;
            // 
            // Summary
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ctlSummary1);
            this.Name = "Summary";
            this.Size = new System.Drawing.Size(570, 379);
            this.ResumeLayout(false);

        }

        #endregion

        private Controls.ctlSummary ctlSummary1;
    }
}
