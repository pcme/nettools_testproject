﻿
namespace NettoolsTest.Pages
{
    partial class DashBoard1
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.lblTitle = new System.Windows.Forms.Label();
            this.grid1 = new Syncfusion.WinForms.DataGrid.SfDataGrid();
            this.btnBack = new System.Windows.Forms.Button();
            this.ctlConnectionStatus1 = new NettoolsTest.Controls.ctlConnectionStatus();
            this.ctlGroups1 = new NettoolsTest.Controls.ctlGroups();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.ctlPulseGeneratorSetup1 = new NettoolsTest.Controls.ctlPulseGeneratorSetup();
            this.ctlPredictConfig1 = new NettoolsTest.Controls.ctlPredictConfig();
            this.ctlSpanZero1 = new NettoolsTest.Controls.ctlSpanZero();
            this.ctlDigital1 = new NettoolsTest.Controls.ctlDigital();
            this.ctlPlantStart1 = new NettoolsTest.Controls.ctlPlantStart();
            this.ctlAlarmSimulation1 = new NettoolsTest.Controls.ctlAlarmSimulation();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grid1)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 6;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.Controls.Add(this.btnSave, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnEdit, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblTitle, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.grid1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnBack, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.ctlConnectionStatus1, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.ctlGroups1, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 2, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(2243, 1245);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // btnSave
            // 
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSave.Location = new System.Drawing.Point(1585, 7);
            this.btnSave.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(200, 113);
            this.btnSave.TabIndex = 9;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnEdit.Location = new System.Drawing.Point(1809, 7);
            this.btnEdit.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(200, 113);
            this.btnEdit.TabIndex = 8;
            this.btnEdit.Text = "Edit";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTitle.Location = new System.Drawing.Point(8, 0);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(656, 127);
            this.lblTitle.TabIndex = 4;
            this.lblTitle.Text = "label1";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // grid1
            // 
            this.grid1.AccessibleName = "Table";
            this.tableLayoutPanel1.SetColumnSpan(this.grid1, 6);
            this.grid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grid1.Location = new System.Drawing.Point(8, 134);
            this.grid1.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.grid1.Name = "grid1";
            this.grid1.PreviewRowHeight = 70;
            this.grid1.SelectionUnit = Syncfusion.WinForms.DataGrid.Enums.SelectionUnit.Cell;
            this.grid1.Size = new System.Drawing.Size(2227, 188);
            this.grid1.TabIndex = 5;
            this.grid1.Text = "sfDataGrid1";
            // 
            // btnBack
            // 
            this.btnBack.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnBack.Location = new System.Drawing.Point(2035, 7);
            this.btnBack.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(200, 113);
            this.btnBack.TabIndex = 3;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // ctlConnectionStatus1
            // 
            this.ctlConnectionStatus1.Location = new System.Drawing.Point(805, 17);
            this.ctlConnectionStatus1.Margin = new System.Windows.Forms.Padding(21, 17, 21, 17);
            this.ctlConnectionStatus1.Name = "ctlConnectionStatus1";
            this.ctlConnectionStatus1.Size = new System.Drawing.Size(432, 93);
            this.ctlConnectionStatus1.TabIndex = 6;
            // 
            // ctlGroups1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.ctlGroups1, 2);
            this.ctlGroups1.Controller = null;
            this.ctlGroups1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlGroups1.EditContainer = null;
            this.ctlGroups1.Editting = false;
            this.ctlGroups1.Location = new System.Drawing.Point(21, 346);
            this.ctlGroups1.Margin = new System.Windows.Forms.Padding(21, 17, 21, 17);
            this.ctlGroups1.Name = "ctlGroups1";
            this.ctlGroups1.Size = new System.Drawing.Size(742, 882);
            this.ctlGroups1.TabIndex = 7;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel1.SetColumnSpan(this.tableLayoutPanel2, 4);
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.Controls.Add(this.ctlPulseGeneratorSetup1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.ctlPredictConfig1, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.ctlSpanZero1, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.ctlDigital1, 3, 3);
            this.tableLayoutPanel2.Controls.Add(this.ctlPlantStart1, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.ctlAlarmSimulation1, 1, 3);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(792, 336);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 4;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1443, 902);
            this.tableLayoutPanel2.TabIndex = 10;
            // 
            // ctlPulseGeneratorSetup1
            // 
            this.ctlPulseGeneratorSetup1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ctlPulseGeneratorSetup1.Controller = null;
            this.ctlPulseGeneratorSetup1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlPulseGeneratorSetup1.EditContainer = null;
            this.ctlPulseGeneratorSetup1.Editting = false;
            this.ctlPulseGeneratorSetup1.Location = new System.Drawing.Point(21, 17);
            this.ctlPulseGeneratorSetup1.Margin = new System.Windows.Forms.Padding(21, 17, 21, 17);
            this.ctlPulseGeneratorSetup1.Name = "ctlPulseGeneratorSetup1";
            this.ctlPulseGeneratorSetup1.Size = new System.Drawing.Size(318, 191);
            this.ctlPulseGeneratorSetup1.TabIndex = 0;
            // 
            // ctlPredictConfig1
            // 
            this.ctlPredictConfig1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ctlPredictConfig1.Config = null;
            this.ctlPredictConfig1.Controller = null;
            this.ctlPredictConfig1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlPredictConfig1.EditContainer = null;
            this.ctlPredictConfig1.Editting = false;
            this.ctlPredictConfig1.Location = new System.Drawing.Point(1101, 17);
            this.ctlPredictConfig1.Margin = new System.Windows.Forms.Padding(21, 17, 21, 17);
            this.ctlPredictConfig1.Name = "ctlPredictConfig1";
            this.tableLayoutPanel2.SetRowSpan(this.ctlPredictConfig1, 3);
            this.ctlPredictConfig1.Size = new System.Drawing.Size(321, 641);
            this.ctlPredictConfig1.TabIndex = 0;
            // 
            // ctlSpanZero1
            // 
            this.ctlSpanZero1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel2.SetColumnSpan(this.ctlSpanZero1, 2);
            this.ctlSpanZero1.Controller = null;
            this.ctlSpanZero1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlSpanZero1.EditContainer = null;
            this.ctlSpanZero1.Editting = false;
            this.ctlSpanZero1.Location = new System.Drawing.Point(381, 17);
            this.ctlSpanZero1.Margin = new System.Windows.Forms.Padding(21, 17, 21, 17);
            this.ctlSpanZero1.Name = "ctlSpanZero1";
            this.ctlSpanZero1.Size = new System.Drawing.Size(678, 191);
            this.ctlSpanZero1.TabIndex = 0;
            // 
            // ctlDigital1
            // 
            this.ctlDigital1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ctlDigital1.Controller = null;
            this.ctlDigital1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlDigital1.Location = new System.Drawing.Point(1101, 692);
            this.ctlDigital1.Margin = new System.Windows.Forms.Padding(21, 17, 21, 17);
            this.ctlDigital1.Name = "ctlDigital1";
            this.ctlDigital1.Size = new System.Drawing.Size(321, 193);
            this.ctlDigital1.TabIndex = 1;
            // 
            // ctlPlantStart1
            // 
            this.ctlPlantStart1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel2.SetColumnSpan(this.ctlPlantStart1, 2);
            this.ctlPlantStart1.Config = null;
            this.ctlPlantStart1.Controller = null;
            this.ctlPlantStart1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlPlantStart1.EditContainer = null;
            this.ctlPlantStart1.Editting = false;
            this.ctlPlantStart1.Location = new System.Drawing.Point(381, 242);
            this.ctlPlantStart1.Margin = new System.Windows.Forms.Padding(21, 17, 21, 17);
            this.ctlPlantStart1.Name = "ctlPlantStart1";
            this.tableLayoutPanel2.SetRowSpan(this.ctlPlantStart1, 2);
            this.ctlPlantStart1.Size = new System.Drawing.Size(678, 416);
            this.ctlPlantStart1.TabIndex = 2;
            // 
            // ctlAlarmSimulation1
            // 
            this.ctlAlarmSimulation1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel2.SetColumnSpan(this.ctlAlarmSimulation1, 2);
            this.ctlAlarmSimulation1.Controller = null;
            this.ctlAlarmSimulation1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlAlarmSimulation1.EditContainer = null;
            this.ctlAlarmSimulation1.Editting = false;
            this.ctlAlarmSimulation1.Location = new System.Drawing.Point(381, 692);
            this.ctlAlarmSimulation1.Margin = new System.Windows.Forms.Padding(21, 17, 21, 17);
            this.ctlAlarmSimulation1.Name = "ctlAlarmSimulation1";
            this.ctlAlarmSimulation1.Size = new System.Drawing.Size(678, 193);
            this.ctlAlarmSimulation1.TabIndex = 3;
            // 
            // DashBoard1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.Name = "DashBoard1";
            this.Size = new System.Drawing.Size(2243, 1245);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grid1)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Label lblTitle;
        private Syncfusion.WinForms.DataGrid.SfDataGrid grid1;
        private Controls.ctlConnectionStatus ctlConnectionStatus1;
        private Controls.ctlGroups ctlGroups1;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Controls.ctlPulseGeneratorSetup ctlPulseGeneratorSetup1;
        private Controls.ctlPredictConfig ctlPredictConfig1;
        private Controls.ctlSpanZero ctlSpanZero1;
        private Controls.ctlDigital ctlDigital1;
        private Controls.ctlPlantStart ctlPlantStart1;
        private Controls.ctlAlarmSimulation ctlAlarmSimulation1;

    }
}
