﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NettoolsTest
{
    public partial class MainForm : Form, Interfaces.IHMIContainer
    {
        private Stack<Pages.BasePage> _pageStack;

        public MainForm()
        {
            InitializeComponent();
            _pageStack = new Stack<Pages.BasePage>();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            Globals.Container = this;
#if Connectivity
            this.Text = "Connectivity Test V" + Assembly.GetExecutingAssembly().GetName().Version.ToString();
#else
            this.Text = "Nettools Test V" + Assembly.GetExecutingAssembly().GetName().Version.ToString();
#endif
        }

        public Pages.BasePage GetPage(Type page)
        {
            foreach (Control c in this.panel1.Controls)
            {
                if (c.GetType() == page)
                {
                    return c as Pages.BasePage;
                }
            }
            Pages.BasePage screen = (Pages.BasePage)Activator.CreateInstance(page);
            this.panel1.Controls.Add(screen);
            screen.Dock = DockStyle.Fill;
            return screen;
        }

        private Pages.BasePage _currentPage;
        private Pages.BasePage _previousPage;

        public void LoadPage(Type page)
        {
            LoadPage(page, null);
        }

        public void LoadPage(Type page, object data)
        {
            Pages.BasePage screen = GetPage(page);
            if (screen == null)
                return;

            DoLoadPage(screen, false, data);
        }


        private void DoLoadPage(Pages.BasePage screen, bool popping, object data)
        {
            if (_currentPage != null && (_currentPage != screen))
            {
                _currentPage.BeforeHide();
            }

            screen.ScreenData = data;
            screen.BeforeShow();
            screen.Visible = true;
            screen.AfterShow();
            if (_currentPage != null && (_currentPage != screen))
            {
                if (!popping)
                    _pageStack.Push(_currentPage);

                _currentPage.Visible = false;
                _currentPage.AfterHide();
            }
            _currentPage = screen;
            this.PerformLayout();
        }

        public void BackPage()
        {
            if (_pageStack.Count > 0)
            {
                _previousPage = _pageStack.Pop();

                DoLoadPage(_previousPage, true, null);
            }
        }



        public void LockControls(Type ControlType)
        {
           
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Simulator.Simulation.Instance.StopRunner();
            Simulator.LoggerFactory.Instance.CloseFactory();
        }
    }
}
