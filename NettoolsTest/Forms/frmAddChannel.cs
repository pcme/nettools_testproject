﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NettoolsTest.Forms
{
    public partial class frmAddChannel : Form
    {
        public frmAddChannel()
        {
            InitializeComponent();

           
           
        }

        public bool DigitalOnly { get; set; }

        public List<DigitalChannelOptions> DigitalChannelOptions
        {
            get; set;
        }


        private void btnAdd_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            this.Close();
        }

        public string ChannelName
        {
            get { return tbChannelName.Text; }
        }

        public string ModbusAddress
        {
            get
            {
                return tbModbusAddress.Text;
            }
        }

        public Communications.DeviceType DeviceType
        {
            get
            {
                return comboBox1.SelectedItem as Communications.DeviceType;
            }
        }

        public DigitalChannelOptions DigitalChannel
        {
            get
            {
                return cbDigitalChannel.SelectedItem as DigitalChannelOptions;
            }
        }

        void EnableAdd()
        {
            if ((DeviceType != null) && DeviceType.FixNameAndId)  // digital
            {
                if (cbDigitalChannel.SelectedItem == null)
                    btnAdd.Enabled = false;
                else
                    btnAdd.Enabled = true;
            }
            else
            {

                if ((comboBox1.SelectedItem == null) ||
                (String.IsNullOrEmpty(tbModbusAddress.Text)) ||
                (String.IsNullOrEmpty(tbChannelName.Text)))
                {
                    btnAdd.Enabled = false;
                }
                else
                {
                    btnAdd.Enabled = true;
                }
            }
        }


        private void frmAddChannel_Load(object sender, EventArgs e)
        {

            if (DigitalChannelOptions.Count == 0)
            {   // no more digitals
                comboBox1.DataSource = Communications.DeviceTypes.Instance.deviceTypes.Where(f => (f.Config != null) && (f.ChannelTypeId > 0) && (f.ChannelTypeId != 26)).ToList();
            }
            else
            {
                comboBox1.DataSource = Communications.DeviceTypes.Instance.deviceTypes.Where(f => (f.Config != null) && (f.ChannelTypeId > 0)).ToList();
            }
            comboBox1.DisplayMember = "Device";
            comboBox1.ValueMember = "ChannelTypeId";


            cbDigitalChannel.DataSource = DigitalChannelOptions;
            cbDigitalChannel.DisplayMember = "Name";
            cbDigitalChannel.ValueMember = "Id";

            cbDigitalChannel.Visible = false;
            tbChannelName.Visible = true;

            if (DigitalOnly)
            {
                comboBox1.SelectedItem = Communications.DeviceTypes.Instance.deviceTypes.FirstOrDefault(f => f.ChannelTypeId == 26);
                comboBox1.Enabled = false;
                tbModbusAddress.Text = "0";
                tbModbusAddress.Enabled = false;
                cbDigitalChannel.Visible = true;
                tbChannelName.Visible = false;
            }

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnableAdd();
            if ((DeviceType != null) && DeviceType.FixNameAndId)
            {
                tbChannelName.Visible = false;
                tbModbusAddress.Text = "0";
                tbModbusAddress.Enabled = false;
                cbDigitalChannel.Visible = true;
            }
            else
            {
                tbChannelName.Visible = true;
                tbModbusAddress.Enabled = true;
                cbDigitalChannel.Visible = false;
            }
        }

        private void tbChannelName_TextChanged(object sender, EventArgs e)
        {
            EnableAdd();
        }

        private void tbModbusAddress_TextChanged(object sender, EventArgs e)
        {
            EnableAdd();
        }

        private void cbDigitalChannel_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnableAdd();
        }
    }


    public class DigitalChannelOptions
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
