﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;

namespace NettoolsTest.Forms
{
    public partial class frmEditChannelConfig : Form
    {
        public frmEditChannelConfig()
        {
            InitializeComponent();
        }

        private bool _Edittable;
        public bool Edittable
        {
            get => _Edittable;
            set
            {
                _Edittable = value;
                if (_Edittable)
                {
                  
                }
                btnSave.Enabled = _Edittable;
                
            }
        }

        public Data.Channel Channel { get; set; }

        private Communications.channel_config Config;

        private List<Data.ChannelConfig> ChannelConfigs;

        private void frmEditChannelConfig_Load(object sender, EventArgs e)
        {
            var dt = Communications.DeviceTypes.Instance.deviceTypes.FirstOrDefault(f => f.ChannelTypeId == Channel.ChannelType);
            ChannelConfigs = Data.ChannelConfig.GetConfigs(Channel.Id);

            var fields = dt.Config.GetType().GetProperties().Select(x => x.GetCustomAttribute<DisplayNameAttribute>()).Where(x => x != null).Select(x => x.DisplayName);

            Config = Activator.CreateInstance(dt.Config.GetType()) as Communications.channel_config;

            int i = 0;
            while (ChannelConfigs.Count < Config.NumberOfConfigs)
            {
                Data.ChannelConfig ch = new Data.ChannelConfig();
                ch.ChannelId = Channel.Id;      
                ch.Value = Config.config[i];
                ch.Index = i++;
                ChannelConfigs.Add(ch);
                ch.Update();
            }
            for(i=0; i<Config.NumberOfConfigs; i++)
            {
                Config.config[i] = ChannelConfigs.FirstOrDefault(f => f.Index == i)?.Value ?? "";
            }

            propertyGrid1.SelectedObject = Config;
            propertyGrid1.Readonly = !Edittable;

         

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < Config.NumberOfConfigs; i++)
            {
                var ch = ChannelConfigs.FirstOrDefault(f => f.Index == i);
                if (ch != null)
                {
                   ch.Value = Config.config[i];
                   ch.Update();
                }
            }

            
           
        }
    }
}
