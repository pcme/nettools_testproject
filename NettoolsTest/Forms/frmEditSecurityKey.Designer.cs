﻿
namespace NettoolsTest.Forms
{
    partial class frmEditSecurityKey
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.tbRootCertificate = new System.Windows.Forms.TextBox();
            this.btnBrowseRootCert = new System.Windows.Forms.Button();
            this.btnBrowseThing = new System.Windows.Forms.Button();
            this.tbThingCertificate = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnBrowsePrivateKey = new System.Windows.Forms.Button();
            this.tbPrivateKey = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbClientId = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbRegion = new System.Windows.Forms.TextBox();
            this.lblRegion = new System.Windows.Forms.Label();
            this.tbPrefix = new System.Windows.Forms.TextBox();
            this.lblPrefix = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(56, 119);
            this.label1.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(257, 32);
            this.label1.TabIndex = 0;
            this.label1.Text = "CA Root Certificate";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(1111, 675);
            this.btnSave.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(200, 79);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(719, 675);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(200, 79);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // tbRootCertificate
            // 
            this.tbRootCertificate.Location = new System.Drawing.Point(371, 112);
            this.tbRootCertificate.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tbRootCertificate.Name = "tbRootCertificate";
            this.tbRootCertificate.Size = new System.Drawing.Size(1465, 38);
            this.tbRootCertificate.TabIndex = 5;
            // 
            // btnBrowseRootCert
            // 
            this.btnBrowseRootCert.Location = new System.Drawing.Point(1901, 107);
            this.btnBrowseRootCert.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.btnBrowseRootCert.Name = "btnBrowseRootCert";
            this.btnBrowseRootCert.Size = new System.Drawing.Size(200, 55);
            this.btnBrowseRootCert.TabIndex = 6;
            this.btnBrowseRootCert.Text = "Browse...";
            this.btnBrowseRootCert.UseVisualStyleBackColor = true;
            this.btnBrowseRootCert.Click += new System.EventHandler(this.btnBrowseRootCert_Click);
            // 
            // btnBrowseThing
            // 
            this.btnBrowseThing.Location = new System.Drawing.Point(1901, 200);
            this.btnBrowseThing.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.btnBrowseThing.Name = "btnBrowseThing";
            this.btnBrowseThing.Size = new System.Drawing.Size(200, 55);
            this.btnBrowseThing.TabIndex = 9;
            this.btnBrowseThing.Text = "Browse...";
            this.btnBrowseThing.UseVisualStyleBackColor = true;
            this.btnBrowseThing.Click += new System.EventHandler(this.btnBrowseThing_Click);
            // 
            // tbThingCertificate
            // 
            this.tbThingCertificate.Location = new System.Drawing.Point(371, 205);
            this.tbThingCertificate.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tbThingCertificate.Name = "tbThingCertificate";
            this.tbThingCertificate.Size = new System.Drawing.Size(1465, 38);
            this.tbThingCertificate.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(56, 212);
            this.label2.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(223, 32);
            this.label2.TabIndex = 7;
            this.label2.Text = "Thing Certificate";
            // 
            // btnBrowsePrivateKey
            // 
            this.btnBrowsePrivateKey.Location = new System.Drawing.Point(1901, 298);
            this.btnBrowsePrivateKey.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.btnBrowsePrivateKey.Name = "btnBrowsePrivateKey";
            this.btnBrowsePrivateKey.Size = new System.Drawing.Size(200, 55);
            this.btnBrowsePrivateKey.TabIndex = 12;
            this.btnBrowsePrivateKey.Text = "Browse...";
            this.btnBrowsePrivateKey.UseVisualStyleBackColor = true;
            this.btnBrowsePrivateKey.Click += new System.EventHandler(this.btnBrowsePrivateKey_Click);
            // 
            // tbPrivateKey
            // 
            this.tbPrivateKey.Location = new System.Drawing.Point(371, 303);
            this.tbPrivateKey.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tbPrivateKey.Name = "tbPrivateKey";
            this.tbPrivateKey.Size = new System.Drawing.Size(1465, 38);
            this.tbPrivateKey.TabIndex = 11;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(56, 310);
            this.label3.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(214, 32);
            this.label3.TabIndex = 10;
            this.label3.Text = "Private Key File";
            // 
            // tbClientId
            // 
            this.tbClientId.Location = new System.Drawing.Point(371, 396);
            this.tbClientId.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tbClientId.Name = "tbClientId";
            this.tbClientId.Size = new System.Drawing.Size(612, 38);
            this.tbClientId.TabIndex = 14;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(56, 403);
            this.label4.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(119, 32);
            this.label4.TabIndex = 13;
            this.label4.Text = "Client Id";
            // 
            // tbRegion
            // 
            this.tbRegion.Location = new System.Drawing.Point(371, 489);
            this.tbRegion.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tbRegion.Name = "tbRegion";
            this.tbRegion.Size = new System.Drawing.Size(612, 38);
            this.tbRegion.TabIndex = 16;
            // 
            // lblRegion
            // 
            this.lblRegion.AutoSize = true;
            this.lblRegion.Location = new System.Drawing.Point(56, 496);
            this.lblRegion.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lblRegion.Name = "lblRegion";
            this.lblRegion.Size = new System.Drawing.Size(106, 32);
            this.lblRegion.TabIndex = 15;
            this.lblRegion.Text = "Region";
            // 
            // tbPrefix
            // 
            this.tbPrefix.Location = new System.Drawing.Point(371, 575);
            this.tbPrefix.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tbPrefix.Name = "tbPrefix";
            this.tbPrefix.Size = new System.Drawing.Size(612, 38);
            this.tbPrefix.TabIndex = 18;
            // 
            // lblPrefix
            // 
            this.lblPrefix.AutoSize = true;
            this.lblPrefix.Location = new System.Drawing.Point(56, 582);
            this.lblPrefix.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lblPrefix.Name = "lblPrefix";
            this.lblPrefix.Size = new System.Drawing.Size(88, 32);
            this.lblPrefix.TabIndex = 17;
            this.lblPrefix.Text = "Prefix";
            // 
            // frmEditSecurityKey
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(2162, 822);
            this.ControlBox = false;
            this.Controls.Add(this.tbPrefix);
            this.Controls.Add(this.lblPrefix);
            this.Controls.Add(this.tbRegion);
            this.Controls.Add(this.lblRegion);
            this.Controls.Add(this.tbClientId);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnBrowsePrivateKey);
            this.Controls.Add(this.tbPrivateKey);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnBrowseThing);
            this.Controls.Add(this.tbThingCertificate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnBrowseRootCert);
            this.Controls.Add(this.tbRootCertificate);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEditSecurityKey";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Edit Security Key";
            this.Load += new System.EventHandler(this.frmEditSecurityKey_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TextBox tbRootCertificate;
        private System.Windows.Forms.Button btnBrowseRootCert;
        private System.Windows.Forms.Button btnBrowseThing;
        private System.Windows.Forms.TextBox tbThingCertificate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnBrowsePrivateKey;
        private System.Windows.Forms.TextBox tbPrivateKey;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbClientId;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbRegion;
        private System.Windows.Forms.Label lblRegion;
        private System.Windows.Forms.TextBox tbPrefix;
        private System.Windows.Forms.Label lblPrefix;
    }
}