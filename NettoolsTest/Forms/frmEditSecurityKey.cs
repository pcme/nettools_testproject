﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NettoolsTest.Forms
{
    public partial class frmEditSecurityKey : Form
    {
        public frmEditSecurityKey()
        {
            InitializeComponent();
        }

        public bool Editting { get; set; }
        public Data.Controller Controller { get; set; }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Controller.SecurityCertificate = tbRootCertificate.Text;
            Controller.KeyPath = tbPrivateKey.Text;
            Controller.ThingCertificate = tbThingCertificate.Text;
            Controller.ClientId = tbClientId.Text;

            Properties.Settings.Default.Prefix = tbPrefix.Text;
            Properties.Settings.Default.Region = tbRegion.Text;
            Properties.Settings.Default.Save();

            Data.Controller.Update(Controller);
            DialogResult = DialogResult.OK;
            this.Close();
        }

        private void frmEditSecurityKey_Load(object sender, EventArgs e)
        {
            tbRootCertificate.Text = Controller.SecurityCertificate;
            tbPrivateKey.Text = Controller.KeyPath;
            tbThingCertificate.Text = Controller.ThingCertificate;
            tbClientId.Text = Controller.ClientId;

#if Connectivity
            tbRegion.Text = Properties.Settings.Default.Region;
            tbPrefix.Text = Properties.Settings.Default.Prefix;
#else
            lblRegion.Visible = false;
            tbRegion.Visible = false;
            tbPrefix.Visible = false;
            lblPrefix.Visible = false;
#endif
            if (!Editting)
            {
                btnSave.Enabled = false;
                tbRootCertificate.Enabled = false;
                tbClientId.Enabled = false;
                tbPrivateKey.Enabled = false;
                tbThingCertificate.Enabled = false;
                btnBrowsePrivateKey.Enabled = false;
                btnBrowseRootCert.Enabled = false;
                btnBrowseThing.Enabled = false;

                tbPrefix.Enabled = false;
                tbRegion.Enabled = false;
            }
            else
            {
                btnSave.Enabled = true;
                tbRootCertificate.Enabled = true;
                tbClientId.Enabled = true;
                tbPrivateKey.Enabled = true;
                tbThingCertificate.Enabled = true;
                btnBrowsePrivateKey.Enabled = true;
                btnBrowseRootCert.Enabled = true;
                btnBrowseThing.Enabled = true;


                tbPrefix.Enabled = true;
                tbRegion.Enabled = true;
            }
        }

        private void btnBrowseRootCert_Click(object sender, EventArgs e)
        {
            OpenFileDialog d = new OpenFileDialog();
            d.Filter = "Root Certificate (*.pem)|*.pem";
            d.Title = "Root Certificate";
            if (DialogResult.OK == d.ShowDialog())
            {
                tbRootCertificate.Text = d.FileName;

            }
        }

        private void btnBrowseThing_Click(object sender, EventArgs e)
        {
            OpenFileDialog d = new OpenFileDialog();
            d.Filter = "Certificate (*.pem.crt)|*.pem.crt";
            d.Title = "Thing Certificate";
            if (DialogResult.OK == d.ShowDialog())
            {
                tbThingCertificate.Text = d.FileName;

            }
        }

        private void btnBrowsePrivateKey_Click(object sender, EventArgs e)
        {
            OpenFileDialog d = new OpenFileDialog();
            d.Filter = "Private Key (*.pem.key)|*.pem.key";
            d.Title = "Private Key File";
            if (DialogResult.OK == d.ShowDialog())
            {
                tbPrivateKey.Text = d.FileName;

            }
        }
    }
}
