﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using log4net;
using log4net.Config;
using System.Reflection;
namespace NettoolsTest
{
    static class Program
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(Program));
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("NTQxMTE0QDMxMzkyZTMzMmUzMEZoU3RjWHkyUWU5MmhqVWVUTUZrK2NjZUoyNlcvUGhKelphZW5qNWRDcDA9");

            XmlConfigurator.Configure();
            log4net.Util.LogLog.QuietMode = true;

            Log.Debug("Starting V " + Assembly.GetExecutingAssembly().GetName().Version.ToString());

            Communications.Globals.URL = Properties.Settings.Default.CloudURL;
            Communications.Globals.Port = Properties.Settings.Default.Port;

            Data.Controller.CreateDefaultControllers();

            Simulator.LoggerFactory.Instance.NoiseAmplitude = Properties.Settings.Default.NoiseAmplitude;

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            // for test tabs:

            if (Properties.Settings.Default.TestTabs)
            {
                Application.Run(new Form1());
            }
            else
            {
                var f = new MainForm();
#if Connectivity

                Communications.Globals.URL = Properties.Settings.Default.Prefix + "-ats.iot." + Properties.Settings.Default.Region + ".amazonaws.com";


                var controllers = Data.Controller.GetControllers();
                f.LoadPage(typeof(Pages.DashBoard1), controllers.FirstOrDefault(ff => ff.Number == 1));
#else
                f.LoadPage(typeof(Pages.Summary));
#endif
                Application.Run(f);
            }

        }
    }
}
