﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    public interface IHMIContainer
    {
        // void LoadPage(UserControl c);
        void LoadPage(Type page);
        void LoadPage(Type page, object data);
        // go back one page - stack is only 1 page deep!
        void BackPage();
        void LockControls(Type ControlType);
    }


    public interface IEditContainer
    {
        void SetItemChanged(bool changed);
        void DeleteItem(object item);
      
    }

    public interface IEditControl
    {
        IEditContainer EditContainer { get; set; }
        bool Editting { get; set; }
        void SaveChanges();
        void CancelChanges();
    }
}
