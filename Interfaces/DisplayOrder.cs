﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    public class DisplayOrder : Attribute
    {
        public DisplayOrder(int order)
        {
            Order = order;
        }

        public int Order { get; set; }
    }

    public class DataType : Attribute
    {
        public DataType(int t)
        {
            Value = t;
        }

        public int Value { get; set; }
    }
}
