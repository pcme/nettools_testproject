﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    public interface IGeneratorData
    {
        double Value { get; }
        DateTime TimeStamp { get; }
    }

   
}
