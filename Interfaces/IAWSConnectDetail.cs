﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    public interface IAWSConnectDetail
    {
        string SecurityCertificate { get; set; }

        string KeyPath { get; set; }
        string ThingCertificate { get; set; }

        string ClientId { get; set; }

        string SerialNumber { get; set; }
    }
}
