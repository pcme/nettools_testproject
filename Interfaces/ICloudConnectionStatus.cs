﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    public enum IOTErrors
    {
        None,
        NoConnection,
        PublishFail,
        CertificateError
    }

    public interface ICloudConnectionStatus
    {
        bool Connected { get; }
    }

    public delegate void HandleConnectionStatus(ICloudConnectionStatus status);


    public interface IStatusProvider
    {
        ICloudConnectionStatus Status { get; set; }
        event HandleConnectionStatus ConnectionStatus;
    }


    public interface ICloudDataStatus
    {
        int ControllerId { get; }
        string Response { get; }
        bool Failed { get; }
        DateTime TimeStamp { get; }
        int Count { get; set; }

        IOTErrors LastError { get; set; }
    }
}
