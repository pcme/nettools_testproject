﻿using System;

using System.Text;
using System.Threading.Tasks;

using System.Net;
using Newtonsoft.Json;
using System.IO;


using log4net;

namespace Communications
{
    public class Cloud
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(Cloud));

        private string SanitizeResult(string r)
        {
            r = r.Trim(new char[] { '"' });
            r = r.Replace("\\\"", "\"");
            return r;
        }

        public Task<T> SendAsync<T>(string url, object data)
        {
            Task<T> t = Task<T>.Run(() =>
            {
                try
                {
                    T result = Send<T>(url, data);
                    return result;
                }
                catch (AggregateException ae)
                {
                    ae.Handle(ex =>
                    {
                        return true;
                    });
                    foreach (Exception eee in ae.InnerExceptions)
                    {
                        Log.Error(eee);
                    }
                }
                catch (Exception ee)
                {
                    Log.Error(ee);
                }
                return default(T);
            });
            return t;
        }


      

        public Task<T> GetAsync<T>(string url)
        {
            Task<T> t = Task<T>.Run(() =>
            {
                try
                {
                    T result = Get<T>(url);
                    return result;
                }
                catch (AggregateException ae)
                {
                    ae.Handle(ex =>
                    {
                        return true;
                    });
                    foreach (Exception eee in ae.InnerExceptions)
                    {
                        Log.Error(eee);
                    }
                }
                catch (Exception ee)
                {
                    Log.Error(ee);
                }
                return default(T);
            });
            return t;
        }

        public Task<string> SendAsync(string url, string data)
        {
            Task<string> t = Task<string>.Run(() =>
            {
                try
                {
                    string result = Send(url, "", data);
                    return result;
                }
                catch (AggregateException ae)
                {
                    ae.Handle(ex =>
                    {
                        return true;
                    });
                    foreach (Exception eee in ae.InnerExceptions)
                    {
                        Log.Error(eee);
                    }
                }
                catch (Exception ee)
                {
                    Log.Error(ee);
                }
                return "";
            });
            return t;
        }

        public Task<string> SendAsync(string url, string jwt, string data)
        {
            Task<string> t = Task<string>.Run(() =>
            {
                try
                {
                    string result = Send(url, jwt, data);
                    return result;
                }
                catch (AggregateException ae)
                {
                    ae.Handle(ex =>
                    {
                        return true;
                    });
                    foreach (Exception eee in ae.InnerExceptions)
                    {
                        Log.Error(eee);
                    }
                }
                catch (Exception ee)
                {
                    Log.Error(ee);
                    return ee.Message;
                }
                return "";
            });
            return t;
        }


        public Task<T> SendAsync<T>(string url, string jwt, string data)
        {
            Task<T> t = Task<T>.Run(() =>
            {
                try
                {
                    T result = Send<T>(url, jwt, data);
                    return result;
                }
                catch (AggregateException ae)
                {
                    ae.Handle(ex =>
                    {
                        return true;
                    });
                    foreach (Exception eee in ae.InnerExceptions)
                    {
                        Log.Error(eee);
                    }
                }
                catch (Exception ee)
                {
                    Log.Error(ee);
    
                }
                return default(T);
            });
            return t;
        }



        public T Send<T>(string url, string jwt, string data)
        {

            try
            {
                string path = url;
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(path);

                request.KeepAlive = false;
                request.ProtocolVersion = HttpVersion.Version11;
                request.Method = "POST";



                Log.Debug("Post to " + path);
                // Log.Debug(data);
                byte[] postdata = Encoding.UTF8.GetBytes(data);

                request.ContentType = "application/json";
                request.Accept = "application/json";
                request.ContentLength = postdata.Length;
                if (!String.IsNullOrEmpty(jwt))
                    request.Headers.Add("authorization", jwt);
                Stream s = request.GetRequestStream();
                s.Write(postdata, 0, postdata.Length);
                s.Close();

                HttpWebResponse resp = (HttpWebResponse)request.GetResponse();
                string result = "";
                using (StreamReader rdr = new StreamReader(resp.GetResponseStream()))
                {
                    result = rdr.ReadToEnd();
                }


                Log.Debug("Result: " + result);

               // result = SanitizeResult(result);

               // Log.Debug("Result: " + result);

                T input = (T)JsonConvert.DeserializeObject(result, typeof(T));


                return input;
            }
            catch (Exception ee)
            {
                Log.Error("Exception", ee);
                throw ee;
            }

        }


        public string Send(string url, string jwt, string data)
        {

            try
            {
                string path = url;
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(path);

                request.KeepAlive = false;
                request.ProtocolVersion = HttpVersion.Version11;
                request.Method = "POST";

         

                Log.Debug("Post to " + path);
                Log.Debug(data);
                byte[] postdata = Encoding.UTF8.GetBytes(data);

                request.ContentType = "application/json";
                request.Accept = "application/json";
                request.ContentLength = postdata.Length;
                if (!String.IsNullOrEmpty(jwt))
                    request.Headers.Add("authorization", jwt);
                Stream s = request.GetRequestStream();
                s.Write(postdata, 0, postdata.Length);
                s.Close();

                HttpWebResponse resp = (HttpWebResponse)request.GetResponse();
                string result = "";
                using (StreamReader rdr = new StreamReader(resp.GetResponseStream()))
                {
                    result = rdr.ReadToEnd();
                }
                result = SanitizeResult(result);

                Log.Debug("Result: " + result);

               

                return result;
            }
            catch (Exception ee)
            {
                Log.Error("Exception", ee);
                throw ee;
            }

        }

        public T Send<T>(string url, object data)
        {

            try
            {
                string path = url;
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(path);

                request.KeepAlive = false;
                request.ProtocolVersion = HttpVersion.Version11;
                request.Method = "POST";

                string json = JsonConvert.SerializeObject(data);

                Log.Debug("Post to " + path);
                // Log.Debug(json);
                byte[] postdata = Encoding.UTF8.GetBytes(json);

                request.ContentType = "application/json";
                request.Accept = "application/json";
                request.ContentLength = postdata.Length;
                Stream s = request.GetRequestStream();
                s.Write(postdata, 0, postdata.Length);
                s.Close();

              


                HttpWebResponse resp = (HttpWebResponse)request.GetResponse();
                string result = "";
                using (StreamReader rdr = new StreamReader(resp.GetResponseStream()))
                {
                    result = rdr.ReadToEnd();
                }
                result = SanitizeResult(result);

                Log.Debug("Result: " + result);

                T input = (T)JsonConvert.DeserializeObject(result, typeof(T));

                return input;
            }
            catch (Exception ee)
            {
                Log.Error("Exception", ee);
                throw ee;
            }

        }


        public T Get<T>(string url)
        {

            try
            {
                string path = url;
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(path);

                request.KeepAlive = false;
                request.ProtocolVersion = HttpVersion.Version11;
                request.Method = "GET";

              

                Log.Debug("Get to " + path);
               

                request.ContentType = "application/json";
                request.Accept = "application/json";
                request.ContentLength = 0;
               

                HttpWebResponse resp = (HttpWebResponse)request.GetResponse();
                string result = "";
                using (StreamReader rdr = new StreamReader(resp.GetResponseStream()))
                {
                    result = rdr.ReadToEnd();
                }
                result = SanitizeResult(result);

                Log.Debug("Result: " + result);

                T input = (T)JsonConvert.DeserializeObject(result, typeof(T));

                return input;
            }
            catch (Exception ee)
            {
                Log.Error("Exception", ee);
                throw ee;
            }

        }

    }
}
