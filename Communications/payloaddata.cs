﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Security.Cryptography;

using System.ComponentModel;
using CustomControls.ComboBox;
using System.Drawing.Design;
using CustomControls.Data;
using Communications.Controls;
using CustomControls.Rule;
using CustomControls.Forms;
using Newtonsoft.Json.Linq;
using System.Globalization;

namespace Communications
{

    public class payloadresponse
    {
        public bool status { get; set; }
        public string error { get; set; }
    }
    
    public class payloaddata
    {

        public string payload { get; set; }
        public string tags { get; set; }
        public string format { get; set; }


        public payloaddata()
        {
            tags = "[]";
            format = JsonConvert.SerializeObject(formatwrapper.GenerateTestData(), Formatting.None);
        }

        public static payloaddata GenerateTestData()
        {
            payloaddata p = new payloaddata();
            p.tags = "[]";
            p.format = JsonConvert.SerializeObject(formatwrapper.GenerateTestData(), Formatting.None);

            var pp = cpayload.GenerateTestData();
            p.payload = pp.ConvertToJson();


            return p;
        }
    }

    public class formatwrapper
    {
        public string [] format { get; set; }

        public static string [] GenerateTestData()
        {
            var r = new formatwrapper();
            r.format = new string[] { "json", "none", "none" };
            return r.format;
        }
    }

    public class cfw_version
    {
        public string date { get; set; }
        public string fw_ver { get; set; }
        public string git_id { get; set; }
        public string protocol_ver { get; set; }

        public static cfw_version GenerateTestData()
        {
            cfw_version c = new cfw_version();
            c.date = "2019-07-10";
            c.fw_ver = "9.99";
            c.git_id = "3d6a323";
            c.protocol_ver = "1";
            return c;
        }
    }

    public class cconfig : hash_base
    {
        public string config_id { get; set; }
        public string processed { get; set; }
        public string serialnumber { get; set; }

        public string utc { get; set; }
        public string json_data { get; set; }

        public cconfig()
        {
            hash = "f1a2172b-9630-4e6d-b698-a19157417ced";
        }
        public static cconfig GenerateTestData()
        {
            cconfig c = new cconfig();
            c.config_id = "1";
            c.processed = "0";
            c.serialnumber = "000019AAA481";
            c.utc = "2021-11-04 10:05:00";
            return c;
        }
    }

    public class cproc
    {
        public string mac_address { get; set; }
        public string serialnumber { get; set; }

        public static cproc GenerateTestData()
        {
            var c = new cproc();
            c.mac_address = "70B3D5FE8289";
            c.serialnumber = "000019AAA481";
            return c;
        }

    }

    public class cgroup
    {
        public string group_id { get; set; }
        public string group_name { get; set; }
    }


    public class cgroups
    {

        [JsonIgnore]
        public List<cgroup> groups { get; set; }


        public cgroups()
        {
            groups = new List<cgroup>();
        }

    }

    public class GroupsConvertor : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return (objectType == typeof(cgroups));
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            cgroups tt = value as cgroups;

            JToken token = JToken.FromObject(value);

            if (token.Type != JTokenType.Object)
            {
                token.WriteTo(writer);
            }
            else
            {
                JObject j = (JObject)token;
                foreach (var t in tt.groups)
                {

                    j.Add(new JProperty(t.group_id, t.group_name));

                }
                j.WriteTo(writer);
            }
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }


    public class DecimalFormatConvertor : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return (
                (objectType == typeof(decimal))
                || (objectType == typeof(float))
                || (objectType == typeof(double))

                );
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteRawValue($"{value:0.000}");
           
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }


    public class calarm_threshold
    {
        public string max { get; set; }
        public string min { get; set; }
    }


 
    public class cchannels
    {
        [JsonProperty("1")]
        public channel_config channel1 { get; set; }

        [JsonProperty("2")]
        public channel_config channel2 { get; set; }

        [JsonProperty("3")]
        public channel_config channel3 { get; set; }

        [JsonProperty("4")]
        public channel_config channel4 { get; set; }
        [JsonProperty("5")]
        public channel_config channel5 { get; set; }
        [JsonProperty("6")]
        public channel_config channel6 { get; set; }
        [JsonProperty("7")]
        public channel_config channel7 { get; set; }
        [JsonProperty("8")]
        public channel_config channel8 { get; set; }
        [JsonProperty("9")]
        public channel_config channel9 { get; set; }
        [JsonProperty("10")]
        public channel_config channel10 { get; set; }
        [JsonProperty("11")]
        public channel_config channel11 { get; set; }
        [JsonProperty("12")]
        public channel_config channel12 { get; set; }
        [JsonProperty("13")]
        public channel_config channel13 { get; set; }
        [JsonProperty("14")]
        public channel_config channel14 { get; set; }
        [JsonProperty("15")]
        public channel_config channel15 { get; set; }
        [JsonProperty("16")]
        public channel_config channel16 { get; set; }
        [JsonProperty("17")]
        public channel_config channel17 { get; set; }
        [JsonProperty("18")]
        public channel_config channel18 { get; set; }
        [JsonProperty("19")]
        public channel_config channel19 { get; set; }
        [JsonProperty("20")]
        public channel_config channel20 { get; set; }


        // special channel numbers for digital inputs
        [JsonProperty("60")]
        public channel_config channel60 { get; set; }
        [JsonProperty("61")]
        public channel_config channel61 { get; set; }
        [JsonProperty("62")]
        public channel_config channel62 { get; set; }
        [JsonProperty("63")]
        public channel_config channel63 { get; set; }

        public static cchannels GenerateTestData()
        {
            var c = new cchannels();

            c.channel1 = channel_config.GenerateTestData("0");
            c.channel2 = channel_config.GenerateTestData("1");
            c.channel3 = channel_config.GenerateTestData("2");
            c.channel4 = channel_config.GenerateTestData("3");

            return c;
        }
    }

    public class cchannel_type
    {
        public string channel_type_id { get; set; }
        public string channel_type_name { get; set; }
    }

    public class ChannelTypeConvertor : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return (objectType == typeof(cchannel_types));
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            cchannel_types tt = value as cchannel_types;

            JToken token = JToken.FromObject(value);

            if (token.Type != JTokenType.Object)
            {
                token.WriteTo(writer);
            }
            else
            {
                JObject j = (JObject)token;
                foreach (var t in tt.channel_type)
                {
                   
                    j.Add(new JProperty(t.channel_type_id, t.channel_type_name));
                  
                }
                j.WriteTo(writer);
            }
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
    public class cchannel_types
    {
        
        [JsonIgnore]
        public List<cchannel_type> channel_type { get; set; }


        public cchannel_types()
        {
            channel_type = new List<cchannel_type>();
        }
        
    }


   
    public class cpredict_config
    {
        public string max { get; set; }
        public string channel { get; set; }
        public string pulse_delay { get; set; }
        public string[] cleaning_order { get; set; }
        public string[] actuator_channels { get; set; }
        public string sequential_cleaning { get; set; }
        public string seconds_between_pulses { get; set; }

        public cpredict_config()
        {
            cleaning_order = new string[1];
            actuator_channels = new string[1];
        }

        public static cpredict_config GenerateTestData()
        {
            var c = new cpredict_config();
            c.max = "2";
            c.channel = "1";
            c.pulse_delay = "1";
            c.cleaning_order = new string[] { "p1", "p2", "p3", "p4", "p5", "p6" };
            c.actuator_channels = new string[] { "3" };
            c.sequential_cleaning = "1";
            c.seconds_between_pulses = "2";
            return c;
        }
    }

    public class channel_types_v2
    {
        public string id { get; set; }
        public string name { get; set; }
        public string code { get; set; }

        public List<string> attribute_names;
        public List<string> attribute_datatypes;

        public channel_types_v2()
        {
            attribute_datatypes = new List<string>();
            attribute_names = new List<string>();
        }
    }

    public class cdata : hash_base
    {
      
        // public string plant_stopped_channel { get; set; }

        public string mac_address { get; set; }
        public string serial_number { get; set; }
        public string fw_version { get; set; }
        public string fw_date { get; set; }
        public string fw_hash { get; set; }

        public cgroups groups { get; set; }

        public List<channel_config> channels { get; set; }

    
        public cchannel_types channel_types { get; set; }

        public List<channel_types_v2> channel_types_v2 { get; set; }
        public override void CalculateHash()
        {
            var settings = new JsonSerializerSettings();

            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.DefaultValueHandling = DefaultValueHandling.Ignore;
            settings.Formatting = Formatting.None;

            

            string s = JsonConvert.SerializeObject(this, settings);
            using (MD5 md5 = MD5.Create())
            {
                byte[] hs = md5.ComputeHash(Encoding.ASCII.GetBytes(s));

                StringBuilder sb = new StringBuilder();
                for (int j = 0; j < hs.Length; j++)
                {
                    sb.Append(hs[j].ToString("x2"));
                }
                this.hash = sb.ToString();
            }

        }

       // public cpredict_config predict_config { get; set; }
        public cdata()
        {
            groups = new cgroups();

            channels = new List<channel_config>();

            channel_types = new cchannel_types();

            channel_types_v2 = new List<channel_types_v2>();

            //  predict_config = new cpredict_config();
            hash = "f1a2172b-9630-4e6d-b698-a19157417ced";
        }
        public static cdata GenerateTestData()
        {
            var c = new cdata();
           
            return c;

        }
    }

    public class ctransfer
    {
        public string tranfer_id { get; set; }  // misspelling intentional
        public string uuid { get; set; }
        public string status { get; set; }
        public string utc_start { get; set; }
        public string utc_end { get; set; }


    }
    public class cpayload
    {
        [JsonProperty("proc_serialnumber")]
        public string proc_sn { get; set; }


        public List<ctransfer> transfer { get; set; }

        public List<cconfig> config { get; set; }

        public cdata data { get; set; }

        public List<clogs> logs { get; set; }

        public List<alogs> alarmlog { get; set; }

        public List<clinked_config> linked_configs { get; set; }



        public cpayload()
        {
            logs = new List<clogs>();
            alarmlog = new List<alogs>();
            linked_configs = new List<clinked_config>();
            transfer = new List<ctransfer>();
            config = new List<cconfig>();
        }




        public string ConvertToJson(Formatting f = Formatting.None)
        {
            var settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.DefaultValueHandling = DefaultValueHandling.Ignore;
            settings.Formatting = f;

            return JsonConvert.SerializeObject(this, f, settings);
        }

        public static cpayload GenerateTestData()
        {
            var p = new cpayload();
            p.proc_sn = "000019AAA481";
            p.logs = new List<clogs>();
            p.alarmlog = new List<alogs>();
            for(int i=0; i<2; i++)
                p.logs.Add(clogs.GenerateTestData(i));
            for (int i = 0; i < 2; i++)
                p.alarmlog.Add(alogs.GenerateTestData(i));

            p.linked_configs.Add(clinked_config.GenerateTestData(0));


            p.config.Add(cconfig.GenerateTestData());
            p.data = cdata.GenerateTestData();

            var settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.DefaultValueHandling = DefaultValueHandling.Ignore;
            settings.Formatting = Formatting.None;
            p.config[0].json_data = JsonConvert.SerializeObject(p.data, settings);
            p.config[0].CalculateHash();


            return p;

        }
    }

    public class alogs
    {
        public string alarmlog_id { get; set; }
        public string config_id { get; set; }
        public string channel_id { get; set; }
        public string alarm_status { get; set; }
        public string start_ts { get; set; }
        public string end_ts { get; set; }
        public string active { get; set; }
        public string alarm_data_type { get; set; }
        public string alarm_data { get; set; }

        public static alogs GenerateTestData(int i)
        {
            alogs l = new alogs();
            l.alarmlog_id = "1";
            l.config_id = "1";
            l.alarm_status = "128";
           
            DateTime now = DateTime.Now.AddSeconds(i);
            l.start_ts = now.ToString("yyyy-MM-dd HH:mm:ss"); // "2021-11-04 10:05:00";
            l.end_ts = now.AddMinutes(10).ToString("yyyy-MM-dd HH:mm:ss"); // "2021-11-04 10:05:00";
            l.channel_id = "1";
            l.active = "0";
            l.alarm_data_type = "0";
            l.alarm_data = "0";

            return l;
        }

    }


    public class hash_base
    {
        public hash_base()
        {
            hash = "f1a2172b-9630-4e6d-b698-a19157417ced";
        }

        [DefaultValue("")]
        public string hash { get; set; }
        public virtual void CalculateHash()
        {
            hash = "f1a2172b-9630-4e6d-b698-a19157417ced";
            /*
            var settings = new JsonSerializerSettings();

            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.DefaultValueHandling = DefaultValueHandling.Ignore;
            settings.Formatting = Formatting.None;

            string s = JsonConvert.SerializeObject(this,settings);
            using (MD5 md5 = MD5.Create())
            {
                byte[] hs = md5.ComputeHash(Encoding.ASCII.GetBytes(s));

                StringBuilder sb = new StringBuilder();
                for (int j = 0; j < hs.Length; j++)
                {
                    sb.Append(hs[j].ToString("x2"));
                }
                this.hash = sb.ToString();
            }
            */
        }
    }


    public class clogs
    {
        public string logs_id { get; set; }
        public string log_type { get; set; }
        public string ts { get; set; }
        public string channel_id { get; set; }
        public string value { get; set; }
        public string status { get; set; }
        public string config_id { get; set; }

        public static clogs GenerateTestData(int i)
        { 
            clogs l = new clogs();
            l.log_type = 1.ToString();
            DateTime now = DateTime.Now.AddSeconds(i);
            l.ts = now.ToString("yyyy-MM-dd HH:mm:ss"); // "2021-11-04 10:05:00";
            l.channel_id = "1";
            l.value = "0.96";
            l.status = "0";
            l.config_id = "1";
            return l;
        }
    }



   

    public class clinked_config : hash_base

    {
        public string config_id { get; set; }
        public string serialnumber { get; set; }
        public string utc { get; set; }


        public clinked_config()
        {
            hash = "f1a2172b-9630-4e6d-b698-a19157417ced";
        }
        public static clinked_config GenerateTestData(int i)
        {
            clinked_config l = new clinked_config();
            l.config_id = "1";
            DateTime now = DateTime.Now.AddSeconds(i);
            l.utc = now.ToString("yyyy-MM-dd HH:mm:ss"); // "2021-11-04 10:05:00";
            l.utc = "2021-11-04 10:05:40";
            l.serialnumber = "000019AAA481";

            l.CalculateHash();

           
        //    \"f1a2172b-9630-4e6d-b698-a19157417ced\"
            return l;
        }

      

    }
}
