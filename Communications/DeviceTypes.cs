﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Communications
{
    public class DeviceTypes
    {

        private static DeviceTypes _instance;
        public static DeviceTypes Instance {  get { if (_instance == null) _instance = new DeviceTypes(); return _instance; } }

        private DeviceTypes()
        {
            deviceTypes = new List<DeviceType>();
            deviceTypes.Add(new DeviceType() { Device = "990/980", ChannelTypeCode = 1, ChannelTypeId=6, Config = new channel_config_990() });
            deviceTypes.Add(new DeviceType() { Device = "550", ChannelTypeCode = 2, Config = new channel_config_550() });
            deviceTypes.Add(new DeviceType() { Device = "991", ChannelTypeCode = 4, ChannelTypeId=1, Config = new channel_config_991() });
            deviceTypes.Add(new DeviceType() { Device = "990X", ChannelTypeCode = 5, ChannelTypeId=7, Config = new channel_config_990x() });
            deviceTypes.Add(new DeviceType() { Device = "980", ChannelTypeCode = 6 });
            deviceTypes.Add(new DeviceType() { Device = "980X", ChannelTypeCode = 7 });
            deviceTypes.Add(new DeviceType() { Device = "220", ChannelTypeCode = 17, ChannelTypeId=15, Config = new channel_config_220() });
            deviceTypes.Add(new DeviceType() { Device = "660", ChannelTypeCode = 18, ChannelTypeId=14, Config = new channel_config_660() });
            deviceTypes.Add(new DeviceType() { Device = "880", ChannelTypeCode = 19 });
            deviceTypes.Add(new DeviceType() { Device = "273", ChannelTypeCode = 20, ChannelTypeId=16, Config = new channel_config_273() });
            deviceTypes.Add(new DeviceType() { Device = "320", ChannelTypeCode = 21, ChannelTypeId=17, Config = new channel_config_320() });
            deviceTypes.Add(new DeviceType() { Device = "330", ChannelTypeCode = 22, ChannelTypeId=18, Config = new channel_config_330() });
            deviceTypes.Add(new DeviceType() { Device = "662", ChannelTypeCode = 23, ChannelTypeId=19, Config = new channel_config_662() });
            deviceTypes.Add(new DeviceType() { Device = "4-20mA Input", ChannelTypeCode = 65, ChannelTypeId=23, Config = new channel_config_AIM() });
            deviceTypes.Add(new DeviceType() { Device = "ROM", ChannelTypeCode = 66, ChannelTypeId=24, Config = new channel_config_ROM() });
            deviceTypes.Add(new DeviceType() { Device = "AOM", ChannelTypeCode = 67, ChannelTypeId=25, Config = new channel_config_AOM() });
            deviceTypes.Add(new DeviceType() { Device = "1090 Dust", ChannelTypeCode = 70, ChannelTypeId=8, Config = new channel_config_1090()});
            deviceTypes.Add(new DeviceType() { Device = "181", ChannelTypeCode = 82, ChannelTypeId=2, Config = new channel_config_181() });
            deviceTypes.Add(new DeviceType() { Device = "Digital Input", ChannelTypeCode = 514, ChannelTypeId=26, FixNameAndId = true, Config = new channel_config_AIMDigital() });
            deviceTypes.Add(new DeviceType() { Device = "260", ChannelTypeCode = 90, ChannelTypeId=3, Config = new channel_config_260() });
            deviceTypes.Add(new DeviceType() { Device = "360", ChannelTypeCode = 91, ChannelTypeId=4, Config = new channel_config_360() });
            deviceTypes.Add(new DeviceType() { Device = "2620", ChannelTypeCode = 92, ChannelTypeId=5, Config = new channel_config_2620() });
            deviceTypes.Add(new DeviceType() { Device = "602_Dust", ChannelTypeCode = 97, ChannelTypeId=9, Config = new channel_config_602Dust() });
            deviceTypes.Add(new DeviceType() { Device = "602_Opacity", ChannelTypeCode = 98, ChannelTypeId=10, Config = new channel_config_602Opacity() });
            deviceTypes.Add(new DeviceType() { Device = "710 Opacity", ChannelTypeCode = 99, ChannelTypeId=78, Config = new channel_config_710Opacity() });
            deviceTypes.Add(new DeviceType() { Device = "710 Dust", ChannelTypeCode = 100, ChannelTypeId=79, Config = new channel_config_710PMDust() });
            deviceTypes.Add(new DeviceType() { Device = "800", ChannelTypeCode = 113, ChannelTypeId=13, Config = new channel_config_800() });
            deviceTypes.Add(new DeviceType() { Device = "CNX800", ChannelTypeCode = 120, Config = new channel_config_CNX800() });
            deviceTypes.Add(new DeviceType() { Device = "370", ChannelTypeCode = 129, ChannelTypeId=11, Config = new channel_config_370() });
            deviceTypes.Add(new DeviceType() { Device = "373", ChannelTypeCode = 131, ChannelTypeId=12, Config = new channel_config_373() });
            deviceTypes.Add(new DeviceType() { Device = "WETSTACK", ChannelTypeCode = 147, ChannelTypeId=20, Config = new channel_config_WS() });
            deviceTypes.Add(new DeviceType() { Device = "StackFlow 400", ChannelTypeCode = 148, ChannelTypeId=21, Config = new channel_config_SF400() });
            deviceTypes.Add(new DeviceType() { Device = "StackFlow 200", ChannelTypeCode = 149, ChannelTypeId=22, Config = new channel_config_SF200() });
            deviceTypes.Add(new DeviceType() { Device = "MODBUS_INPUT", ChannelTypeCode = 253, ChannelTypeId=80, Config = new channel_config_ModbusInput() }); 
            deviceTypes.Add(new DeviceType() { Device = "MODBUS_WRITE", ChannelTypeCode = 254, ChannelTypeId=82, Config = new channel_config_ModbusWrite() });
            deviceTypes.Add(new DeviceType() { Device = "MODBUS_READ", ChannelTypeCode = 255, ChannelTypeId=81, Config = new channel_config_ModbusRead() });
            deviceTypes.Add(new DeviceType() { Device = "NULL", ChannelTypeCode = 256, Config = new channel_config_NULL() }); ;
            deviceTypes.Add(new DeviceType() { Device = "FIXED", ChannelTypeCode = 257, ChannelTypeId=28, Config = new channel_config_FixedValue() });
            deviceTypes.Add(new DeviceType() { Device = "MASS", ChannelTypeCode = 258, ChannelTypeId=29, Config = new channel_config_MASS() });
            deviceTypes.Add(new DeviceType() { Device = "TEMP", ChannelTypeCode = 259,});
            deviceTypes.Add(new DeviceType() { Device = "O2", ChannelTypeCode = 260 });
            deviceTypes.Add(new DeviceType() { Device = "TOTAL", ChannelTypeCode = 261, ChannelTypeId=32, Config = new channel_config_Totaliser() });
            deviceTypes.Add(new DeviceType() { Device = "FILTER", ChannelTypeCode = 262, ChannelTypeId=27, Config = new channel_config_FILTER() });
            deviceTypes.Add(new DeviceType() { Device = "PRESSURE", ChannelTypeCode = 263 });
            deviceTypes.Add(new DeviceType() { Device = "SUM", ChannelTypeCode = 264, ChannelTypeId=30, Config = new channel_config_Sum() });
            deviceTypes.Add(new DeviceType() { Device = "AVERAGE", ChannelTypeCode = 265, ChannelTypeId=33, Config = new channel_config_Averager() });
            deviceTypes.Add(new DeviceType() { Device = "Change Rate", ChannelTypeCode = 266, ChannelTypeId=34, Config = new channel_config_ChangeRate()});
            deviceTypes.Add(new DeviceType() { Device = "Dust at STP O2", ChannelTypeCode = 267, ChannelTypeId=31, Config=new channel_config_DustAtSTP() });
            deviceTypes.Add(new DeviceType() { Device = "MAXMIN", ChannelTypeCode = 268, ChannelTypeId=35, Config = new channel_config_MinMax() });
            deviceTypes.Add(new DeviceType() { Device = "991: QAL3 Zero", ChannelTypeCode = 301, ChannelTypeId=36, Config = new channel_config_991Zero() });
            deviceTypes.Add(new DeviceType() { Device = "991: QAL3 Span", ChannelTypeCode = 302, ChannelTypeId=37, Config = new channel_config_991Span() });
            deviceTypes.Add(new DeviceType() { Device = "991: QAL3 SC", ChannelTypeCode = 303, ChannelTypeId=38, Config = new channel_config_991SC() });
            deviceTypes.Add(new DeviceType() { Device = "990: Contam Ring", ChannelTypeCode = 304, ChannelTypeId=39, Config = new channel_config_990ContamRing() });
            deviceTypes.Add(new DeviceType() { Device = "AMCDig", ChannelTypeCode = 513 });
        //    deviceTypes.Add(new DeviceType() { Device = "AIMDig", ChannelTypeCode = 514, ChannelTypeId=26, Config = new channel_config_AIMDigital() });
            deviceTypes.Add(new DeviceType() { Device = "181: QAL3 Zero", ChannelTypeCode = 255, ChannelTypeId = 40, Config = new channel_config_ModbusRead() });
            deviceTypes.Add(new DeviceType() { Device = "181: QAL3 Span", ChannelTypeCode = 255, ChannelTypeId = 41, Config = new channel_config_ModbusRead() });
            deviceTypes.Add(new DeviceType() { Device = "181: Flow", ChannelTypeCode = 255, ChannelTypeId = 42, Config = new channel_config_ModbusRead() });
            deviceTypes.Add(new DeviceType() { Device = "181: Pcb T", ChannelTypeCode = 255, ChannelTypeId = 43, Config = new channel_config_ModbusRead() });
            deviceTypes.Add(new DeviceType() { Device = "181: Tube T", ChannelTypeCode = 255, ChannelTypeId = 44, Config = new channel_config_ModbusRead() });
            deviceTypes.Add(new DeviceType() { Device = "181: I", ChannelTypeCode = 255, ChannelTypeId = 45, Config = new channel_config_ModbusRead() });
            deviceTypes.Add(new DeviceType() { Device = "1090_:Zero", ChannelTypeCode = 255, ChannelTypeId = 46, Config = new channel_config_ModbusRead() });
            deviceTypes.Add(new DeviceType() { Device = "1090_:Span", ChannelTypeCode = 255, ChannelTypeId = 47, Config = new channel_config_ModbusRead() });
            deviceTypes.Add(new DeviceType() { Device = "1090_:Flow", ChannelTypeCode = 255, ChannelTypeId = 48, Config = new channel_config_ModbusRead() });
            deviceTypes.Add(new DeviceType() { Device = "1090_:Pcb T", ChannelTypeCode = 255, ChannelTypeId = 49, Config = new channel_config_ModbusRead() });
            deviceTypes.Add(new DeviceType() { Device = "1090_:Contam Ring", ChannelTypeCode = 255, ChannelTypeId = 50, Config = new channel_config_ModbusRead() });
            deviceTypes.Add(new DeviceType() { Device = "1090_:I", ChannelTypeCode = 255, ChannelTypeId = 51, Config = new channel_config_ModbusRead() });
            deviceTypes.Add(new DeviceType() { Device = "WSM:Htr1 Tmp", ChannelTypeCode = 255, ChannelTypeId = 52, Config = new channel_config_ModbusRead() });
            deviceTypes.Add(new DeviceType() { Device = "WSM:Htr2 Tmp", ChannelTypeCode = 255, ChannelTypeId = 53, Config = new channel_config_ModbusRead() });
            deviceTypes.Add(new DeviceType() { Device = "WSM:ExG Tmp", ChannelTypeCode = 255, ChannelTypeId = 54, Config = new channel_config_ModbusRead() });
            deviceTypes.Add(new DeviceType() { Device = "WSM:InA Tmp", ChannelTypeCode = 255, ChannelTypeId = 55, Config = new channel_config_ModbusRead() });
            deviceTypes.Add(new DeviceType() { Device = "WSM:ExA Tmp", ChannelTypeCode = 255, ChannelTypeId = 56, Config = new channel_config_ModbusRead() });
            deviceTypes.Add(new DeviceType() { Device = "WSM:Ex Tmp", ChannelTypeCode = 255, ChannelTypeId = 57, Config = new channel_config_ModbusRead() });
            deviceTypes.Add(new DeviceType() { Device = "WSM:Pump Val", ChannelTypeCode = 255, ChannelTypeId = 58, Config = new channel_config_ModbusRead() });
            deviceTypes.Add(new DeviceType() { Device = "WSM:Samp Val", ChannelTypeCode = 255, ChannelTypeId = 59, Config = new channel_config_ModbusRead() });
            deviceTypes.Add(new DeviceType() { Device = "602:I", ChannelTypeCode = 255, ChannelTypeId = 60, Config = new channel_config_ModbusRead() });
            deviceTypes.Add(new DeviceType() { Device = "602:SC", ChannelTypeCode = 255, ChannelTypeId = 61, Config = new channel_config_ModbusRead() });
            deviceTypes.Add(new DeviceType() { Device = "602:TxT", ChannelTypeCode = 255, ChannelTypeId = 62, Config = new channel_config_ModbusRead() });
            deviceTypes.Add(new DeviceType() { Device = "602:RxT", ChannelTypeCode = 255, ChannelTypeId = 63, Config = new channel_config_ModbusRead() });
            deviceTypes.Add(new DeviceType() { Device = "400:Lower Ref", ChannelTypeCode = 255, ChannelTypeId = 64, Config = new channel_config_ModbusRead() });
            deviceTypes.Add(new DeviceType() { Device = "400:Upper Ref", ChannelTypeCode = 255, ChannelTypeId = 65, Config = new channel_config_ModbusRead() });
            deviceTypes.Add(new DeviceType() { Device = "400:Probe Temp", ChannelTypeCode = 255, ChannelTypeId = 66, Config = new channel_config_ModbusRead() });
            deviceTypes.Add(new DeviceType() { Device = "400:Sig Qual 1", ChannelTypeCode = 255, ChannelTypeId = 67, Config = new channel_config_ModbusRead() });
            deviceTypes.Add(new DeviceType() { Device = "400:Sig Qual 2", ChannelTypeCode = 255, ChannelTypeId = 68, Config = new channel_config_ModbusRead() });
            deviceTypes.Add(new DeviceType() { Device = "200:Probe Temp", ChannelTypeCode = 255, ChannelTypeId = 69, Config = new channel_config_ModbusRead() });
            deviceTypes.Add(new DeviceType() { Device = "200:PStat", ChannelTypeCode = 255, ChannelTypeId = 70, Config = new channel_config_ModbusRead() });
            deviceTypes.Add(new DeviceType() { Device = "200:PDiff", ChannelTypeCode = 255, ChannelTypeId = 71, Config = new channel_config_ModbusRead() });
            deviceTypes.Add(new DeviceType() { Device = "260/360:QAL3 Zero", ChannelTypeCode = 255, ChannelTypeId = 72, Config = new channel_config_ModbusRead() });
            deviceTypes.Add(new DeviceType() { Device = "260/360:QAL3 Span", ChannelTypeCode = 255, ChannelTypeId = 73, Config = new channel_config_ModbusRead() });
            deviceTypes.Add(new DeviceType() { Device = "260/360:Flow", ChannelTypeCode = 255, ChannelTypeId = 74, Config = new channel_config_ModbusRead() });
            deviceTypes.Add(new DeviceType() { Device = "2620:Zero", ChannelTypeCode = 255, ChannelTypeId = 75, Config = new channel_config_ModbusRead() });
            deviceTypes.Add(new DeviceType() { Device = "2620:Span", ChannelTypeCode = 255, ChannelTypeId = 76, Config = new channel_config_ModbusRead() });
            deviceTypes.Add(new DeviceType() { Device = "2620:Flow", ChannelTypeCode = 255, ChannelTypeId = 77, Config = new channel_config_ModbusRead() });
        
        }

        public List<DeviceType> deviceTypes { get; set; }

    }


    public class DeviceType
    {
        public int ChannelTypeCode { get; set; }   // this is channel type code
        public string Device { get; set; }

        public int ChannelTypeId { get; set; }

        public channel_config Config { get; set; }

        public bool FixNameAndId { get; set; }
    }
}
