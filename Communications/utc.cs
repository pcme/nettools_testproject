﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reactive.Linq;
using System.Threading;
using System.Reactive.Disposables;
using System.Reactive.Threading.Tasks;
using log4net;
using Interfaces;
using System.Net.NetworkInformation;
using log4net;
using System.Net;
using System.Net.Sockets;

namespace Communications
{

    public class dnscheck
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(utc));

        public static utc_response CheckDNS(string url)
        {
            try
            {
                Log.Debug("DNS check " + url);
                Dns.GetHostEntry(url);
                return new utc_response() { Connected = true };
            }
            catch (Exception ee)
            {
                Log.Error("DNS fail " + ee.Message + " on url " + url);

            }
            return new utc_response() { Connected = false };
        }
    }

    public class utc
    {

        private static readonly ILog Log = LogManager.GetLogger(typeof(utc));


        //      public IObservable<ILogProgress> ExtractLogSession(IDevice device, IDeviceCommunicator communicator, UpgradeMethods method, ILogSession session)
        //         => 


        public static Task<utc_response> PingURL(string url)
        {
            return Task.Run(() =>
            {
                Ping pingSender = new Ping();
                PingOptions options = new PingOptions();
                options.DontFragment = true;
                string data = "12345678901234567890123456789012";
                byte[] buffer = Encoding.ASCII.GetBytes(data);
                int timeout = 120;
                Log.Debug("Pinging " + url);
                var resp = new utc_response();
                try
                {
                    PingReply reply = pingSender.Send(url, timeout, buffer, options);


                    Log.Debug("Ping " + url + " response: " + reply.Status);

                    if (reply.Status != IPStatus.Success)
                    {
                        Log.Debug("Ping " + url + " failed, try tcp connect");
                        TcpClient client = new TcpClient();
                        if (!client.ConnectAsync(url, 443).Wait(5000))
                        {
                            Log.Debug("Socket failed to open to " + url + " port 443");
                            resp.Connected = false;
                        }
                        else
                        {
                            Log.Debug("Socket opened to " + url + " port 443");
                            resp.Connected = true;
                            client.Dispose();
                        }
                    }
                    else
                    resp.Connected = (reply.Status == IPStatus.Success);
                }
                catch (AggregateException ae)
                {
                    resp.Connected = false;
                    ae.Handle(ex =>
                    {
                        return true;
                    });
                    foreach (Exception eee in ae.InnerExceptions)
                    {
                        Log.Error(eee);
                    }
                }
                catch (Exception ee)
                {
                    Log.Error(ee);
                    resp.Connected = false;
                }

                return resp;
            });
        }



        public static Task<utc_response> PingCloud()
        {
            return Task.Run(() =>
            {
                Ping pingSender = new Ping();
                PingOptions options = new PingOptions();
                options.DontFragment = true;
                string data = "12345678901234567890123456789012";
                byte[] buffer = Encoding.ASCII.GetBytes(data);
                int timeout = 120;
                Log.Debug("Pinging " + Communications.Globals.URL);
                var resp = new utc_response();
                try
                {
                    PingReply reply = pingSender.Send(Communications.Globals.URL, timeout, buffer, options);
               

                    Log.Debug("Ping " + Communications.Globals.URL + " response: " + reply.Status);
                    resp.Connected = (reply.Status == IPStatus.Success);
                }
                catch (AggregateException ae)
                {
                    resp.Connected = false;
                    ae.Handle(ex =>
                    {
                        return true;
                    });
                    foreach (Exception eee in ae.InnerExceptions)
                    {
                        Log.Error(eee);
                    }
                }
                catch (Exception ee)
                {
                    Log.Error(ee);
                    resp.Connected = false;
                }

                return resp;
            });
        }

    }

    public class utc_response : ICloudConnectionStatus
    {


        public bool Connected { get; set; }
        
    }

    
}
