﻿using CustomControls.ComboBox;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Communications.Controls
{
    public class EnabledComboBox : GridComboBox
    {
        protected override object GetDataObjectSelected(ITypeDescriptorContext context)
        {
            return (base.ListBox.SelectedItem);
        }

        protected override void RetrieveDataList(ITypeDescriptorContext context)
        {
            List<string> list = new List<string>();
            list.Add("Enabled");
            list.Add("Maintenance");
            list.Add("Disabled");
            base.DataList = list;
        }
    }
}
