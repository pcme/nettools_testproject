﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Communications
{
    public class Helper
    {
        public static string GenerateTestJson()
        {
            var p = payloaddata.GenerateTestData();

            var settings = new JsonSerializerSettings();
    
            string s = JsonConvert.SerializeObject(p, Formatting.Indented, settings);
            return s;
        }
    }
}
