﻿using CustomControls.ComboBox;
using CustomControls.Data;
using CustomControls.Forms;
using CustomControls.Rule;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomControls;
using CustomControls.ComboBox;
using CustomControls.Data;
using CustomControls.Forms;
using CustomControls.Rule;

namespace Communications
{
    public class channel_config_SF400 : channel_config
    {


        public channel_config_SF400() : base(34)
        {
            UnitName = "units";
            DisplayUnits = "Velocity (m/s)";
            InstallationAngle = 45;
            FlowDirection = FlowDirectionOptions.Upstream;
            InstrumentRange = "30";
            StackArea = "1";
            CalA = "0";
            CalB = "1";
            CalC = "0";
            InstantWarningAlarm = "0";
            InstantLimitAlarm = "30";
            AveragingTime = 60;
            AverageWarningAlarm = "0";
            AverageLimitAlarm = "0";
            LowWarningAlarm = "0";
            LowLimitAlarm = "-30";
            AveLowWarningAlarm = "0";
            AveLowLimitAlarm = "0";
            AlarmDelay = 5;
 
            Local4_40mAZero = "0";
            Local4_20mASpan = "0";
            Local4_20mAFilter = "30";
            Relay1 = "Flow Alarm";
            Relay2 = "Fault Alarm";
            SelfTestRate = 1440;
    

            PollRate = 1000;
            LogOptions = "*";
            PlantRunInput = 0;
            PlantStopSensor = YesNoOptions.No;
            DisplayModule = YesNoOptions.No;
            Enabled = EnabledOptions.Enabled;

        }


        [JsonIgnore]
        [Interfaces.DisplayOrder(3)]
        [Interfaces.DataType(23)]
        [DisplayName("Units Name")]
        [Category("Config")]
        [LengthRule(0, 18)]
        public string UnitName { get { return config[3]; } set { config[3] = value; } }


        [JsonIgnore]
        [Interfaces.DisplayOrder(4)]
        [Interfaces.DataType(17)]
        [DisplayName("Display Units")]
        [Category("Config")]
        [Editor(typeof(EnumDescriptionGridComboBox), typeof(UITypeEditor))]
        [DefaultValue("Velocity (m/s)")]
        [EnumList(typeof(DisplayUnitsOptions))]
        public string DisplayUnits
        {
            get
            {
                return
                    (String.IsNullOrEmpty(config[4])) ? DisplayUnitsOptions.Velocity.GetDescription() :
                    config[4];
            }
            set { config[4] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(5)]
        [Interfaces.DataType(0)]
        [DisplayName("Installation Angle")]
        [Category("Config")]
        [DefaultValue(45)]
        [IntRule(0, int.MaxValue)]
        public int InstallationAngle
        {
            get { int d; return int.TryParse(config[5], out d) ? d : 0; }
            set { config[5] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(6)]
        [Interfaces.DataType(16)]
        [DisplayName("Flow Direction")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(FlowDirectionOptions.Upstream)]
        [EnumList(typeof(FlowDirectionOptions))]
        public FlowDirectionOptions FlowDirection
        {
            get
            {
                return (String.IsNullOrEmpty(config[6])) ? FlowDirectionOptions.Upstream : (FlowDirectionOptions)Enum.Parse(typeof(FlowDirectionOptions), config[6]);
            }
            set { config[6] = value.ToString(); }
        }



        [JsonIgnore]
        [Interfaces.DisplayOrder(7)]
        [Interfaces.DataType(2)]
        [DisplayName("Instrument Range")]
        [Description("Instrument Range (m/s)")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string InstrumentRange { get { return config[7]; } set { config[7] = value; } }

        [JsonIgnore]
        [Interfaces.DisplayOrder(8)]
        [Interfaces.DataType(2)]
        [DisplayName("Stack Area")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string StackArea { get { return config[8]; } set { config[8] = value; } }

        [JsonIgnore]
        [Interfaces.DisplayOrder(9)]
        [Interfaces.DataType(2)]
        [DisplayName("Cal a")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string CalA { get { return config[9]; } set { config[9] = value; } }

        [JsonIgnore]
        [Interfaces.DisplayOrder(10)]
        [Interfaces.DataType(2)]
        [DisplayName("Cal b")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string CalB { get { return config[10]; } set { config[10] = value; } }

        [JsonIgnore]
        [Interfaces.DisplayOrder(11)]
        [Interfaces.DataType(2)]
        [DisplayName("Cal c")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string CalC { get { return config[11]; } set { config[11] = value; } }


        [JsonIgnore]
        [Interfaces.DisplayOrder(12)]
        [Interfaces.DataType(2)]
        [DisplayName("Instant Warning Alarm")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string InstantWarningAlarm { get { return config[12]; } set { config[12] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(13)]
        [Interfaces.DataType(2)]
        [DisplayName("Instant Limit Alarm")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string InstantLimitAlarm { get { return config[13]; } set { config[13] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(14)]
        [Interfaces.DataType(0)]
        [DisplayName("Averaging Time")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int AveragingTime
        {
            get { int d; return int.TryParse(config[14], out d) ? d : 0; }
            set { config[14] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(15)]
        [Interfaces.DataType(2)]
        [DisplayName("Average Warning Alarm")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string AverageWarningAlarm { get { return config[15]; } set { config[15] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(16)]
        [Interfaces.DataType(2)]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        [DisplayName("Average Limit Alarm")]
        [Category("Config")]
        public string AverageLimitAlarm { get { return config[16]; } set { config[16] = value; } }

        [JsonIgnore]
        [Interfaces.DisplayOrder(17)]
        [Interfaces.DataType(2)]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        [DisplayName("Low Warning Alarm")]
        [Category("Config")]
        public string LowWarningAlarm { get { return config[17]; } set { config[17] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(18)]
        [Interfaces.DataType(2)]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        [DisplayName("Low Limit Alarm")]
        [Category("Config")]
        public string LowLimitAlarm { get { return config[18]; } set { config[18] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(19)]
        [Interfaces.DataType(2)]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        [DisplayName("Average Low Warning Alarm")]
        [Category("Config")]
        public string AveLowWarningAlarm { get { return config[19]; } set { config[19] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(20)]
        [Interfaces.DataType(2)]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        [DisplayName("Average Low Limit Alarm")]
        [Category("Config")]
        public string AveLowLimitAlarm { get { return config[20]; } set { config[20] = value; } }

        [JsonIgnore]
        [Interfaces.DisplayOrder(21)]
        [Interfaces.DataType(0)]
        [DisplayName("Alarm Delay")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int AlarmDelay
        {
            get { int d; return int.TryParse(config[21], out d) ? d : 0; }
            set { config[21] = value.ToString(); }
        }


        [JsonIgnore]
        [Interfaces.DisplayOrder(22)]
        [Interfaces.DataType(2)]
        [DisplayName("Local 4-20mA Zero")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string Local4_40mAZero { get { return config[22]; } set { config[22] = value; } }

        [JsonIgnore]
        [Interfaces.DisplayOrder(23)]
        [Interfaces.DataType(2)]
        [DisplayName("Local 4-20mA Span")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string Local4_20mASpan { get { return config[23]; } set { config[23] = value; } }



        [JsonIgnore]
        [Interfaces.DisplayOrder(24)]
        [Interfaces.DataType(0)]
        [DisplayName("Local 4-20mA Filter")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string Local4_20mAFilter { get { return config[24]; } set { config[24] = value; } }


        [JsonIgnore]
        [Interfaces.DisplayOrder(25)]
        [Interfaces.DataType(18)]
        [DisplayName("Relay 1")]
        [Category("Config")]
        [Editor(typeof(EnumDescriptionGridComboBox), typeof(UITypeEditor))]
        [DefaultValue("Flow Alarm")]
        [EnumList(typeof(RelayFlowOptions))]
        public string Relay1
        {
            get
            {
                return
                    (String.IsNullOrEmpty(config[25])) ? RelayFlowOptions.FlowAlarm.GetDescription() :
                    config[25];
            }
            set { config[25] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(26)]
        [Interfaces.DataType(18)]
        [DisplayName("Relay 2")]
        [Category("Config")]
        [Editor(typeof(EnumDescriptionGridComboBox), typeof(UITypeEditor))]
        [DefaultValue("Fault Alarm")]
        [EnumList(typeof(RelayFlowOptions))]
        public string Relay2
        {
            get
            {
                return
                    (String.IsNullOrEmpty(config[26])) ? RelayFlowOptions.FaultAlarm.GetDescription() :
                    config[26];
            }
            set { config[26] = value.ToString(); }
        }


        [JsonIgnore]
        [Interfaces.DisplayOrder(27)]
        [Interfaces.DataType(1)]
        [DisplayName("Self test Rate")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int SelfTestRate
        {
            get { int d; return int.TryParse(config[27], out d) ? d : 0; }
            set { config[27] = value.ToString(); }
        }

     
     
        [JsonIgnore]
        [Interfaces.DisplayOrder(28)]
        [Interfaces.DataType(1)]
        [DisplayName("Poll Rate")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int PollRate
        {
            get { int d; return int.TryParse(config[28], out d) ? d : 0; }
            set { config[28] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(29)]
        [Interfaces.DataType(30)]
        [DisplayName("Log Options")]
        [Category("Config")]
        [Editor(typeof(LogOptionsEditor), typeof(UITypeEditor))]
        public string LogOptions { get { return config[29]; } set { config[29] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(30)]
        [Interfaces.DataType(0)]
        [DisplayName("Plant Run Input")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int PlantRunInput
        {
            get { int d; return int.TryParse(config[30], out d) ? d : 0; }
            set { config[30] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(31)]
        [Interfaces.DataType(3)]
        [DisplayName("Plant Stop sensor")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(YesNoOptions.No)]
        [EnumList(typeof(YesNoOptions))]
        public YesNoOptions PlantStopSensor
        {
            get
            {
                return (String.IsNullOrEmpty(config[31])) ? YesNoOptions.No : 
                    (YesNoOptions)Enum.Parse(typeof(YesNoOptions), config[31]);
            }
            set { config[31] = value.ToString(); }
        }


        [JsonIgnore]
        [Interfaces.DisplayOrder(32)]
        [Interfaces.DataType(3)]
        [DisplayName("Display Module")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(YesNoOptions.No)]
        [EnumList(typeof(YesNoOptions))]
        public YesNoOptions DisplayModule
        {
            get
            {
                return (String.IsNullOrEmpty(config[32])) ? YesNoOptions.No : 
                   (YesNoOptions)Enum.Parse(typeof(YesNoOptions), config[32]);
            }
            set { config[32] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(33)]
        [Interfaces.DataType(9)]
        [DisplayName("Enabled")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(EnabledOptions.Enabled)]
        [EnumList(typeof(EnabledOptions))]
        public EnabledOptions Enabled
        {
            get
            {
                return
    (String.IsNullOrEmpty(config[33])) ? EnabledOptions.Enabled :
    (EnabledOptions)Enum.Parse(typeof(EnabledOptions), config[33]);
            }
            set { config[33] = value.ToString(); }
        }

    }
}
