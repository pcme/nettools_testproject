﻿using CustomControls.ComboBox;
using CustomControls.Data;
using CustomControls.Forms;
using CustomControls.Rule;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Communications
{
    public class channel_config_550 : channel_config
    {
        public channel_config_550() : base(11)
        {
            AveragingTime = 60;
            WarningLevel = "80";
            BrokenBagLevel = "500";
            AlarmDelay = 5;
            PollRate = 1000;
            LogOptions = "P";
            PlantRunInput = 0;
            Enabled = EnabledOptions.Enabled;
        }

        [JsonIgnore]
        [DisplayName("Averaging Time")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int AveragingTime
        {
            get { int d; return int.TryParse(config[3], out d) ? d : 0; }
            set { config[3] = value.ToString(); }
        }


        [JsonIgnore]
        [DisplayName("Warning Level")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string WarningLevel { get { return config[4]; } set { config[4] = value; } }
        [JsonIgnore]
        [DisplayName("Broken Bag Level")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string BrokenBagLevel { get { return config[5]; } set { config[5] = value; } }
        [JsonIgnore]
        [DisplayName("Alarm Delay")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int AlarmDelay
        {
            get { int d; return int.TryParse(config[6], out d) ? d : 0; }
            set { config[6] = value.ToString(); }
        }
        [JsonIgnore]
        [DisplayName("Poll Rate")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int PollRate
        {
            get { int d; return int.TryParse(config[7], out d) ? d : 0; }
            set { config[7] = value.ToString(); }
        }

        [JsonIgnore]
        [DisplayName("Log Options")]
        [Category("Config")]
        [Editor(typeof(LogOptionsPEEditor), typeof(UITypeEditor))]
        public string LogOptions { get { return config[8]; } set { config[8] = value; } }

        [JsonIgnore]
        [DisplayName("Plant Run Input")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int PlantRunInput
        {
            get { int d; return int.TryParse(config[9], out d) ? d : 0; }
            set { config[9] = value.ToString(); }
        }

        [JsonIgnore]
        [DisplayName("Enabled")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(EnabledOptions.Enabled)]
        [EnumList(typeof(EnabledOptions))]
        public EnabledOptions Enabled
        {
            get
            {
                return
    (String.IsNullOrEmpty(config[10])) ? EnabledOptions.Enabled :
    (EnabledOptions)Enum.Parse(typeof(EnabledOptions), config[10]);
            }
            set { config[10] = value.ToString(); }
        }

    }
}
