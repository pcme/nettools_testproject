﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Communications
{
    public class channel_config
    {
        [Browsable(false)]
        public string[] config;
        [Browsable(false)]
        public string group_id { get; set; }

        [Browsable(false)]
        public string group_name { get; set; }

        [Browsable(false)]
        public string channel_id { get; set; }
      


        private string _modbus_addr;
        [Browsable(false)]
        [Interfaces.DisplayOrder(0)]
        [Interfaces.DataType(0)]
        [DisplayName("Modbus Address")]
        public string modbus_addr 
        {
            get => _modbus_addr;
            set { _modbus_addr = value; config[0] = value; }
        }


        private string _channel_name;
        [Browsable(false)]
        [Interfaces.DisplayOrder(2)]
        [Interfaces.DataType(24)]
        [DisplayName("Device Name")]
        public string channel_name 
        {
            get => _channel_name;
            set { _channel_name = value; config[2] = value; }
        }
        [Browsable(false)]
        public string channel_type_id { get; set; }


        [Browsable(false)]
        public string channel_type { get; set; }


        [Browsable(false)]
        public string channel_type_code { get; set; }

        [Browsable(false)]
        public string unit { get; set; }

        // [Browsable(false)]
        // public calarm_threshold alarm_thresholds { get; set; }

        [JsonIgnore]
        [Browsable(false)]
        public int NumberOfConfigs { get; set; }

        private string _plant;
        [JsonIgnore]
        [Browsable(false)]
        [Interfaces.DisplayOrder(1)]
        [Interfaces.DataType(29)]
        [DisplayName("Group/Plant/Stack")]
        public string plant
        {
            get => _plant;
            set { _plant = value; config[1] = value; }
        }

        public channel_config(int number_of_configs)
        {
            NumberOfConfigs = number_of_configs;
            config = new string[NumberOfConfigs];
            plant = "0";            // plant id always zero
         //   alarm_thresholds = new calarm_threshold();
        }

        public channel_config()
        {
            config = new string[35];
        //    alarm_thresholds = new calarm_threshold();
        }

        private static string[] default_config =
        {
             "1",
                    "0",
                    "AE3937",
                    "units",
                    "Uncalibrated",
                    "1.0000",
                    "0.0000",
                    "No",
                    "High",
                    "0",
                    "0.0000",
                    "0.0000",
                    "60",
                    "0.0000",
                    "0.0000",
                    "5",
                    "Both",
                    "Limit",
                    "0.0000",
                    "100.0000",
                    "10",
                    "Yes",
                    "30",
                    "Yes",
                    "Yes",
                    "Yes",
                    "100",
                    "1.0000",
                    "No",
                    "1000",
                    "240",
                    "0",
                    "1000.000",
                    "No",
                    "ENABLED"
        };

        public static channel_config GenerateTestData(string id)
        {
            var c = new channel_config();
            c.group_id = "0";
            c.channel_id = id;
            c.modbus_addr = "1";
            c.channel_name = "AE3937";
            c.channel_type_id = "1";
          //  c.alarm_thresholds.max = "2.0";
          //  c.alarm_thresholds.min = "0.0";

            for (int i = 0; i < c.config.Length; i++)
            {
                c.config[i] = default_config[i];
            }
            return c;
        }
    }

}
