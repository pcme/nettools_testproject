﻿using CustomControls.ComboBox;
using CustomControls.Data;
using CustomControls.Forms;
using CustomControls.Rule;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Communications
{
    public class channel_config_602Dust : channel_config
    {
        public channel_config_602Dust() : base(26)
        {
            UnitName = "units";
            CalibrationType = CalibrationTypeOptions.Uncalibrated;
            CalibrationFactor = "1";
            NegativeOffset = "0";

            Sensitivity = SensitivityOptions.High;
            CalRange = 0;
            InstantWarningAlarm = "0";
            InstantLimitAlarm = "0";
            AveragingTime = 60;
            AverageWarningAlarm = "0";
            AverageLimitAlarm = "0";
            AlarmDelay = 5;
            Brightness = "50";
         
            SelfTestsActive = YesNoOptions.Yes;

            ZeroSpanAlarm = YesNoOptions.Yes;
          
            ChecksOn4_20mA = YesNoOptions.No;
            PollRate = 1000;
            LogOptions = "P";
            PlantRunInput = 0;
            ClipLevel = "1000";
            DisplayModule = YesNoOptions.No;
            Enabled = EnabledOptions.Enabled;

        }


        [JsonIgnore]
        [Interfaces.DisplayOrder(3)]
        [Interfaces.DataType(23)]
        [DisplayName("Units Name")]
        [Category("Config")]
        [LengthRule(0, 18)]
        public string UnitName { get { return config[3]; } set { config[3] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(4)]
        [Interfaces.DataType(13)]
        [DisplayName("Calibration Type")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(CalibrationTypeOptions.Uncalibrated)]
        [EnumList(typeof(CalibrationTypeOptions))]
        public CalibrationTypeOptions CalibrationType
        {
            get
            {
                return
    (String.IsNullOrEmpty(config[4])) ? CalibrationTypeOptions.Uncalibrated :
    (CalibrationTypeOptions)Enum.Parse(typeof(CalibrationTypeOptions), config[4]);
            }
            set { config[4] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(5)]
        [Interfaces.DataType(2)]
        [DisplayName("Calibration Factor")]
        [Description("Calibration Factor (4 decimal places)")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string CalibrationFactor { get { return config[5]; } set { config[5] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(6)]
        [Interfaces.DataType(2)]
        [DisplayName("Negative Offset")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string NegativeOffset { get { return config[6]; } set { config[6] = value; } }
        

        [JsonIgnore]
        [Interfaces.DisplayOrder(7)]
        [Interfaces.DataType(4)]
        [DisplayName("Sensitivity")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(SensitivityOptions.High)]
        [EnumList(typeof(SensitivityOptions))]
        public SensitivityOptions Sensitivity
        {
            get
            {
                return (String.IsNullOrEmpty(config[7])) ? SensitivityOptions.High : (SensitivityOptions)Enum.Parse(typeof(SensitivityOptions), config[7]);
            }
            set { config[7] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(8)]
        [Interfaces.DataType(2)]
        [DisplayName("Brightness")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string Brightness { get { return config[8]; } set { config[8] = value; } }

        [JsonIgnore]
        [Interfaces.DisplayOrder(9)]
        [Interfaces.DataType(0)]
        [DisplayName("Cal Range")]
        [Description("Cal Range (0-100)")]
        [Category("Config")]
        [IntRule(0, 100)]
        public int CalRange
        {
            get { int d; return int.TryParse(config[9], out d) ? d : 0; }
            set { config[9] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(10)]
        [Interfaces.DataType(2)]
        [DisplayName("Instant Warning Alarm")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string InstantWarningAlarm { get { return config[10]; } set { config[10] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(11)]
        [Interfaces.DataType(2)]
        [DisplayName("Instant Limit Alarm")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string InstantLimitAlarm { get { return config[11]; } set { config[11] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(12)]
        [Interfaces.DataType(0)]
        [DisplayName("Averaging Time")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int AveragingTime
        {
            get { int d; return int.TryParse(config[12], out d) ? d : 0; }
            set { config[12] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(13)]
        [Interfaces.DataType(2)]
        [DisplayName("Average Warning Alarm")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string AverageWarningAlarm { get { return config[13]; } set { config[13] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(14)]
        [Interfaces.DataType(2)]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        [DisplayName("Average Limit Alarm")]
        [Category("Config")]
        public string AverageLimitAlarm { get { return config[14]; } set { config[14] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(15)]
        [Interfaces.DataType(0)]
        [DisplayName("Alarm Delay")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int AlarmDelay
        {
            get { int d; return int.TryParse(config[15], out d) ? d : 0; }
            set { config[15] = value.ToString(); }
        }
       
       
        [JsonIgnore]
        [Interfaces.DisplayOrder(16)]
        [Interfaces.DataType(3)]
        [DisplayName("Self Tests Active")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(YesNoOptions.Yes)]
        [EnumList(typeof(YesNoOptions))]
        public YesNoOptions SelfTestsActive
        {
            get
            {
                return (String.IsNullOrEmpty(config[16])) ? YesNoOptions.No : (YesNoOptions)Enum.Parse(typeof(YesNoOptions), config[16]);
            }
            set { config[16] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(17)]
        [Interfaces.DataType(3)]
        [DisplayName("Zero Span Alarm")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(YesNoOptions.No)]
        [EnumList(typeof(YesNoOptions))]
        public YesNoOptions ZeroSpanAlarm
        {
            get
            {
                return (String.IsNullOrEmpty(config[17])) ? YesNoOptions.No : (YesNoOptions)Enum.Parse(typeof(YesNoOptions), config[17]);
            }
            set { config[17] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(18)]
        [Interfaces.DataType(2)]
        [DisplayName("Min Intensity")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string MinIntensity { get { return config[18]; } set { config[18] = value; } }



        [JsonIgnore]
        [Interfaces.DisplayOrder(19)]
        [Interfaces.DataType(3)]
        [DisplayName("Checks On 4-20mA")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(YesNoOptions.No)]
        [EnumList(typeof(YesNoOptions))]
        public YesNoOptions ChecksOn4_20mA
        {
            get
            {
                return (String.IsNullOrEmpty(config[19])) ? YesNoOptions.No : (YesNoOptions)Enum.Parse(typeof(YesNoOptions), config[19]);
            }
            set { config[19] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(20)]
        [Interfaces.DataType(1)]
        [DisplayName("Poll Rate")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int PollRate
        {
            get { int d; return int.TryParse(config[20], out d) ? d : 0; }
            set { config[20] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(21)]
        [Interfaces.DataType(30)]
        [DisplayName("Log Options")]
        [Category("Config")]
        [Editor(typeof(LogOptionsEditor), typeof(UITypeEditor))]
        public string LogOptions { get { return config[21]; } set { config[21] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(22)]
        [Interfaces.DataType(0)]
        [DisplayName("Plant Run Input")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int PlantRunInput
        {
            get { int d; return int.TryParse(config[22], out d) ? d : 0; }
            set { config[22] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(23)]
        [Interfaces.DataType(2)]
        [DisplayName("Clip Level")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string ClipLevel { get { return config[23]; } set { config[23] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(24)]
        [Interfaces.DataType(3)]
        [DisplayName("Display Module")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(YesNoOptions.No)]
        [EnumList(typeof(YesNoOptions))]
        public YesNoOptions DisplayModule
        {
            get
            {
                return (String.IsNullOrEmpty(config[24])) ? YesNoOptions.No : (YesNoOptions)Enum.Parse(typeof(YesNoOptions), config[24]);
            }
            set { config[24] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(25)]
        [Interfaces.DataType(9)]
        [DisplayName("Enabled")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(EnabledOptions.Enabled)]
        [EnumList(typeof(EnabledOptions))]
        public EnabledOptions Enabled
        {
            get
            {
                return
    (String.IsNullOrEmpty(config[25])) ? EnabledOptions.Enabled :
    (EnabledOptions)Enum.Parse(typeof(EnabledOptions), config[25]);
            }
            set { config[25] = value.ToString(); }
        }

    }
}
