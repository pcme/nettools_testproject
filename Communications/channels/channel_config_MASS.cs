﻿using CustomControls.ComboBox;
using CustomControls.Data;
using CustomControls.Forms;
using CustomControls.Rule;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomControls;

namespace Communications
{
    public class channel_config_MASS : channel_config
    {
        public channel_config_MASS() : base(20)
        {
            DustChannel = MassChannelOptions.NoneSelected.GetDescription();
            VelocityChannl = MassChannelOptions.NoneSelected.GetDescription();
            AreaOfStack = "1";
            UnitMultiplier = "3.6";
            UnitName = "g/hr";
            InstantWarningAlarm = "0";
            InstantLimitAlarm = "0";
            AveragingTime = 60;
            AverageWarningAlarm = "0";
            AverageLimitAlarm = "0";
            LowWarningAlarm = "0";
            LowLimitAlarm = "0";
            AveLowLimitAlarm = "0";
            AveLowWarningAlarm = "0";
            AlarmDelay = 5;
            LogOptions = "P";
            Enabled = EnabledOptions.Enabled;
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(3)]
        [Interfaces.DataType(25)]
        [DisplayName("Dust Channel")]
        [Category("Config")]
        [Editor(typeof(EnumDescriptionGridComboBox), typeof(UITypeEditor))]
        [DefaultValue("None Selected")]
        [EnumList(typeof(MassChannelOptions))]
        public string DustChannel
        {
            get
            {
                return
                    (String.IsNullOrEmpty(config[3])) ? MassChannelOptions.NoneSelected.GetDescription() :
                    config[3];
            }
            set { config[3] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(4)]
        [Interfaces.DataType(25)]
        [DisplayName("Velocity Channel")]
        [Category("Config")]
        [Editor(typeof(EnumDescriptionGridComboBox), typeof(UITypeEditor))]
        [DefaultValue("None Selected")]
        [EnumList(typeof(MassChannelOptions))]
        public string VelocityChannl
        {
            get
            {
                return
                    (String.IsNullOrEmpty(config[4])) ? MassChannelOptions.NoneSelected.GetDescription() :
                    config[4];
            }
            set { config[4] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(5)]
        [Interfaces.DataType(2)]
        [DisplayName("Area Of Stack")]
        [Description("Area of Stack (4 decimal places)")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string AreaOfStack { get { return config[5]; } set { config[5] = value; } }


        [JsonIgnore]
        [Interfaces.DisplayOrder(6)]
        [Interfaces.DataType(2)]
        [DisplayName("Unit Multiplier")]     
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string UnitMultiplier { get { return config[6]; } set { config[6] = value; } }



        [JsonIgnore]
        [Interfaces.DisplayOrder(7)]
        [Interfaces.DataType(23)]
        [DisplayName("Units Name")]
        [Category("Config")]
        [LengthRule(0, 18)]
        public string UnitName { get { return config[7]; } set { config[7] = value; } }

        [JsonIgnore]
        [Interfaces.DisplayOrder(8)]
        [Interfaces.DataType(2)]
        [DisplayName("Instant Warning Alarm")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string InstantWarningAlarm { get { return config[8]; } set { config[8] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(9)]
        [Interfaces.DataType(2)]
        [DisplayName("Instant Limit Alarm")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string InstantLimitAlarm { get { return config[9]; } set { config[9] = value; } }


        [JsonIgnore]
        [Interfaces.DisplayOrder(10)]
        [Interfaces.DataType(2)]
        [DisplayName("Low Warning Alarm")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string LowWarningAlarm { get { return config[10]; } set { config[10] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(11)]
        [Interfaces.DataType(2)]
        [DisplayName("Low Limit Alarm")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string LowLimitAlarm { get { return config[11]; } set { config[11] = value; } }



        [JsonIgnore]
        [Interfaces.DisplayOrder(12)]
        [Interfaces.DataType(0)]
        [DisplayName("Averaging Time")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int AveragingTime
        {
            get { int d; return int.TryParse(config[12], out d) ? d : 0; }
            set { config[12] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(13)]
        [Interfaces.DataType(2)]
        [DisplayName("Average Warning Alarm")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string AverageWarningAlarm { get { return config[13]; } set { config[13] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(14)]
        [Interfaces.DataType(2)]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        [DisplayName("Average Limit Alarm")]
        [Category("Config")]
        public string AverageLimitAlarm { get { return config[14]; } set { config[14] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(15)]
        [Interfaces.DataType(2)]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        [DisplayName("Average Low Warning Alarm")]
        [Category("Config")]
        public string AveLowWarningAlarm { get { return config[15]; } set { config[15] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(16)]
        [Interfaces.DataType(2)]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        [DisplayName("Average Low Limit Alarm")]
        [Category("Config")]
        public string AveLowLimitAlarm { get { return config[16]; } set { config[16] = value; } }

        [JsonIgnore]
        [Interfaces.DisplayOrder(17)]
        [Interfaces.DataType(0)]
        [DisplayName("Alarm Delay")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int AlarmDelay
        {
            get { int d; return int.TryParse(config[17], out d) ? d : 0; }
            set { config[17] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(18)]
        [Interfaces.DataType(30)]
        [DisplayName("Log Options")]
        [Category("Config")]
        [Editor(typeof(LogOptionsESEditor), typeof(UITypeEditor))]
        public string LogOptions { get { return config[18]; } set { config[18] = value; } }

        [JsonIgnore]
        [Interfaces.DisplayOrder(19)]
        [Interfaces.DataType(9)]
        [DisplayName("Enabled")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(EnabledOptions.Enabled)]
        [EnumList(typeof(EnabledOptions))]
        public EnabledOptions Enabled
        {
            get
            {
                return
    (String.IsNullOrEmpty(config[19])) ? EnabledOptions.Enabled :
    (EnabledOptions)Enum.Parse(typeof(EnabledOptions), config[19]);
            }
            set { config[19] = value.ToString(); }
        }
    }
}
