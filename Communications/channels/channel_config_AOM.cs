﻿using CustomControls.ComboBox;
using CustomControls.Data;
using CustomControls.Forms;
using CustomControls.Rule;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomControls;

namespace Communications
{
    public class channel_config_AOM : channel_config
    {
        public channel_config_AOM() : base(37)
        {
            Relay1Source = AOMRelaySources.NoneSelected.GetDescription();
            Output1Zero = "0";
            Output1Span = "100";
            Output1Filter = 1;
            Relay2Source = AOMRelaySources.NoneSelected.GetDescription();
            Output2Zero = "0";
            Output2Span = "100";
            Output2Filter = 1;
            Relay3Source = AOMRelaySources.NoneSelected.GetDescription();
            Output3Zero = "0";
            Output3Span = "100";
            Output3Filter = 1;
            Relay4Source = AOMRelaySources.NoneSelected.GetDescription();
            Output4Zero = "0";
            Output4Span = "100";
            Output4Filter = 1;
            Relay5Source = AOMRelaySources.NoneSelected.GetDescription();
            Output5Zero = "0";
            Output5Span = "100";
            Output5Filter = 1;
            Relay6Source = AOMRelaySources.NoneSelected.GetDescription();
            Output6Zero = "0";
            Output6Span = "100";
            Output6Filter = 1;
            Relay7Source = AOMRelaySources.NoneSelected.GetDescription();
            Output7Zero = "0";
            Output7Span = "100";
            Output7Filter = 1;
            Relay8Source = AOMRelaySources.NoneSelected.GetDescription();
            Output8Zero = "0";
            Output8Span = "100";
            Output8Filter = 1;

            PollRate = 1000;
            Enabled = EnabledOptions.Enabled;

        }



        [JsonIgnore]
        [Interfaces.DisplayOrder(3)]
        [Interfaces.DataType(27)]
        [DisplayName("Output 1 Source")]
        [Category("Config")]
        [Editor(typeof(EnumDescriptionGridComboBox), typeof(UITypeEditor))]
        [DefaultValue("None Selected")]
        [EnumList(typeof(AOMRelaySources))]
        public string Relay1Source
        {
            get
            {
                return
                    (String.IsNullOrEmpty(config[3])) ? AOMRelaySources.NoneSelected.GetDescription() :
                    config[3];
            }
            set { config[3] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(4)]
        [Interfaces.DataType(2)]
        [DisplayName("Output 1 Zero")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string Output1Zero { get { return config[4]; } set { config[4] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(5)]
        [Interfaces.DataType(2)]
        [DisplayName("Output 1 Span")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string Output1Span { get { return config[5]; } set { config[5] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(6)]
        [Interfaces.DataType(0)]
        [DisplayName("Output 1 Filter")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int Output1Filter
        {
            get { int d; return int.TryParse(config[6], out d) ? d : 0; }
            set { config[6] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(7)]
        [Interfaces.DataType(27)]
        [DisplayName("Output 2 Source")]
        [Category("Config")]
        [Editor(typeof(EnumDescriptionGridComboBox), typeof(UITypeEditor))]
        [DefaultValue("None Selected")]
        [EnumList(typeof(AOMRelaySources))]
        public string Relay2Source
        {
            get
            {
                return
                    (String.IsNullOrEmpty(config[7])) ? AOMRelaySources.NoneSelected.GetDescription() :
                    config[7];
            }
            set { config[7] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(8)]
        [Interfaces.DataType(2)]
        [DisplayName("Output 2 Zero")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string Output2Zero { get { return config[8]; } set { config[8] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(9)]
        [Interfaces.DataType(2)]
        [DisplayName("Output 2 Span")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string Output2Span { get { return config[9]; } set { config[9] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(10)]
        [Interfaces.DataType(0)]
        [DisplayName("Output 2 Filter")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int Output2Filter
        {
            get { int d; return int.TryParse(config[10], out d) ? d : 0; }
            set { config[10] = value.ToString(); }
        }


        [JsonIgnore]
        [Interfaces.DisplayOrder(11)]
        [Interfaces.DataType(27)]
        [DisplayName("Output 3 Source")]
        [Category("Config")]
        [Editor(typeof(EnumDescriptionGridComboBox), typeof(UITypeEditor))]
        [DefaultValue("None Selected")]
        [EnumList(typeof(AOMRelaySources))]
        public string Relay3Source
        {
            get
            {
                return
                    (String.IsNullOrEmpty(config[11])) ? AOMRelaySources.NoneSelected.GetDescription() :
                    config[11];
            }
            set { config[11] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(12)]
        [Interfaces.DataType(2)]
        [DisplayName("Output 3 Zero")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string Output3Zero { get { return config[12]; } set { config[12] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(13)]
        [Interfaces.DataType(2)]
        [DisplayName("Output 3 Span")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string Output3Span { get { return config[13]; } set { config[13] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(14)]
        [Interfaces.DataType(0)]
        [DisplayName("Output 3 Filter")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int Output3Filter
        {
            get { int d; return int.TryParse(config[14], out d) ? d : 0; }
            set { config[14] = value.ToString(); }
        }


        [JsonIgnore]
        [Interfaces.DisplayOrder(15)]
        [Interfaces.DataType(27)]
        [DisplayName("Output 4 Source")]
        [Category("Config")]
        [Editor(typeof(EnumDescriptionGridComboBox), typeof(UITypeEditor))]
        [DefaultValue("None Selected")]
        [EnumList(typeof(AOMRelaySources))]
        public string Relay4Source
        {
            get
            {
                return
                    (String.IsNullOrEmpty(config[15])) ? AOMRelaySources.NoneSelected.GetDescription() :
                    config[15];
            }
            set { config[15] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(16)]
        [Interfaces.DataType(2)]
        [DisplayName("Output 4 Zero")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string Output4Zero { get { return config[16]; } set { config[16] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(17)]
        [Interfaces.DataType(2)]
        [DisplayName("Output 4 Span")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string Output4Span { get { return config[17]; } set { config[17] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(18)]
        [Interfaces.DataType(0)]
        [DisplayName("Output 4 Filter")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int Output4Filter
        {
            get { int d; return int.TryParse(config[18], out d) ? d : 0; }
            set { config[18] = value.ToString(); }
        }


        [JsonIgnore]
        [Interfaces.DisplayOrder(19)]
        [Interfaces.DataType(27)]
        [DisplayName("Output 5 Source")]
        [Category("Config")]
        [Editor(typeof(EnumDescriptionGridComboBox), typeof(UITypeEditor))]
        [DefaultValue("None Selected")]
        [EnumList(typeof(AOMRelaySources))]
        public string Relay5Source
        {
            get
            {
                return
                    (String.IsNullOrEmpty(config[19])) ? AOMRelaySources.NoneSelected.GetDescription() :
                    config[19];
            }
            set { config[19] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(20)]
        [Interfaces.DataType(2)]
        [DisplayName("Output 5 Zero")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string Output5Zero { get { return config[20]; } set { config[20] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(21)]
        [Interfaces.DataType(2)]
        [DisplayName("Output 5 Span")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string Output5Span { get { return config[21]; } set { config[21] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(22)]
        [Interfaces.DataType(0)]
        [DisplayName("Output 5 Filter")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int Output5Filter
        {
            get { int d; return int.TryParse(config[22], out d) ? d : 0; }
            set { config[22] = value.ToString(); }
        }


        [JsonIgnore]
        [Interfaces.DisplayOrder(23)]
        [Interfaces.DataType(27)]
        [DisplayName("Output 6 Source")]
        [Category("Config")]
        [Editor(typeof(EnumDescriptionGridComboBox), typeof(UITypeEditor))]
        [DefaultValue("None Selected")]
        [EnumList(typeof(AOMRelaySources))]
        public string Relay6Source
        {
            get
            {
                return
                    (String.IsNullOrEmpty(config[23])) ? AOMRelaySources.NoneSelected.GetDescription() :
                    config[23];
            }
            set { config[23] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(24)]
        [Interfaces.DataType(2)]
        [DisplayName("Output 6 Zero")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string Output6Zero { get { return config[24]; } set { config[24] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(25)]
        [Interfaces.DataType(2)]
        [DisplayName("Output 6 Span")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string Output6Span { get { return config[25]; } set { config[25] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(26)]
        [Interfaces.DataType(0)]
        [DisplayName("Output 6 Filter")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int Output6Filter
        {
            get { int d; return int.TryParse(config[26], out d) ? d : 0; }
            set { config[26] = value.ToString(); }
        }


        [JsonIgnore]
        [Interfaces.DisplayOrder(27)]
        [Interfaces.DataType(27)]
        [DisplayName("Output 7 Source")]
        [Category("Config")]
        [Editor(typeof(EnumDescriptionGridComboBox), typeof(UITypeEditor))]
        [DefaultValue("None Selected")]
        [EnumList(typeof(AOMRelaySources))]
        public string Relay7Source
        {
            get
            {
                return
                    (String.IsNullOrEmpty(config[27])) ? AOMRelaySources.NoneSelected.GetDescription() :
                    config[27];
            }
            set { config[27] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(28)]
        [Interfaces.DataType(2)]
        [DisplayName("Output 7 Zero")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string Output7Zero { get { return config[28]; } set { config[28] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(29)]
        [Interfaces.DataType(2)]
        [DisplayName("Output 7 Span")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string Output7Span { get { return config[29]; } set { config[29] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(30)]
        [Interfaces.DataType(0)]
        [DisplayName("Output 7 Filter")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int Output7Filter
        {
            get { int d; return int.TryParse(config[30], out d) ? d : 0; }
            set { config[30] = value.ToString(); }
        }


        [JsonIgnore]
        [Interfaces.DisplayOrder(31)]
        [Interfaces.DataType(27)]
        [DisplayName("Output 8 Source")]
        [Category("Config")]
        [Editor(typeof(EnumDescriptionGridComboBox), typeof(UITypeEditor))]
        [DefaultValue("None Selected")]
        [EnumList(typeof(AOMRelaySources))]
        public string Relay8Source
        {
            get
            {
                return
                    (String.IsNullOrEmpty(config[31])) ? AOMRelaySources.NoneSelected.GetDescription() :
                    config[31];
            }
            set { config[31] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(32)]
        [Interfaces.DataType(2)]
        [DisplayName("Output 8 Zero")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string Output8Zero { get { return config[32]; } set { config[32] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(33)]
        [Interfaces.DataType(2)]
        [DisplayName("Output 8 Span")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string Output8Span { get { return config[33]; } set { config[33] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(34)]
        [Interfaces.DataType(0)]
        [DisplayName("Output 8 Filter")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int Output8Filter
        {
            get { int d; return int.TryParse(config[34], out d) ? d : 0; }
            set { config[34] = value.ToString(); }
        }






        [JsonIgnore]
        [Interfaces.DisplayOrder(35)]
        [Interfaces.DataType(1)]
        [DisplayName("Poll Rate")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int PollRate
        {
            get { int d; return int.TryParse(config[35], out d) ? d : 0; }
            set { config[35] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(36)]
        [Interfaces.DataType(9)]
        [DisplayName("Enabled")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(EnabledOptions.Enabled)]
        [EnumList(typeof(EnabledOptions))]
        public EnabledOptions Enabled
        {
            get
            {
                return
    (String.IsNullOrEmpty(config[36])) ? EnabledOptions.Enabled :
    (EnabledOptions)Enum.Parse(typeof(EnabledOptions), config[36]);
            }
            set { config[36] = value.ToString(); }
        }
    }
}
