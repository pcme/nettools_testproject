﻿using CustomControls.ComboBox;
using CustomControls.Data;
using CustomControls.Forms;
using CustomControls.Rule;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomControls;

namespace Communications
{
    public class channel_config_WS : channel_config
    {

        public channel_config_WS() : base(26)
        {
            HeaterTemperature = "280";
            TraceTemperature = "100";
            NozzleSize = "17";
            PumpSpeed = 0;
            SamplingVelocity = "0";
            Value4 = "0";
            Value20 = "100";
            GOTempOffset = "0";
            OTAlarmReset = YesNoOptions.No;
            SamplingMode = SamplingModeOptions.FixedPump.GetDescription();
            TraceEnabled = YesNoOptions.No;
            PurgeEnabled = YesNoOptions.No;
            BeaconEnabled = YesNoOptions.No;
            resetEnabled = YesNoOptions.No;
            ZeroPressureValue = 3470;
            TemperatureChannel = TemperatureChannelOptions.NoneSelected.GetDescription();
            PressureChannel = TemperatureChannelOptions.NoneSelected.GetDescription();
            FixedTemperature = "25";
            FixedPressure = "1013";
            PressureReflevel = "1013";
            PollRate = 1000;
            DisplayModule = YesNoOptions.No;
            Enabled = EnabledOptions.Enabled;
        }


        [JsonIgnore]
        [Interfaces.DisplayOrder(3)]
        [Interfaces.DataType(2)]
        [DisplayName("Heater Temperature")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string HeaterTemperature { get { return config[3]; } set { config[3] = value; } }

        [JsonIgnore]
        [Interfaces.DisplayOrder(4)]
        [Interfaces.DataType(2)]
        [DisplayName("Trace Temperature")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string TraceTemperature { get { return config[4]; } set { config[4] = value; } }

        [JsonIgnore]
        [Interfaces.DisplayOrder(5)]
        [Interfaces.DataType(2)]
        [DisplayName("Nozzle Size")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string NozzleSize { get { return config[5]; } set { config[5] = value; } }

        [JsonIgnore]
        [Interfaces.DisplayOrder(6)]
        [Interfaces.DataType(0)]
        [DisplayName("Pump Speed")]
        [Category("Config")]
        [IntRule(0, 100)]
        public int PumpSpeed
        {
            get { int d; return int.TryParse(config[6], out d) ? d : 0; }
            set { config[6] = value.ToString(); }
        }


        [JsonIgnore]
        [Interfaces.DisplayOrder(7)]
        [Interfaces.DataType(2)]
        [DisplayName("Sampling Velocity")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string SamplingVelocity { get { return config[7]; } set { config[7] = value; } }


        [JsonIgnore]
        [Interfaces.DisplayOrder(8)]
        [Interfaces.DataType(2)]
        [DisplayName("4mA Value (m/s)")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string Value4 { get { return config[8]; } set { config[8] = value; } }

        [JsonIgnore]
        [Interfaces.DisplayOrder(9)]
        [Interfaces.DataType(2)]
        [DisplayName("20mA Value (m/s)")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string Value20 { get { return config[9]; } set { config[9] = value; } }

        [JsonIgnore]
        [Interfaces.DisplayOrder(10)]
        [Interfaces.DataType(2)]
        [DisplayName("GO Temp Offset")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string GOTempOffset { get { return config[10]; } set { config[10] = value; } }

        [JsonIgnore]
        [Interfaces.DisplayOrder(11)]
        [Interfaces.DataType(3)]
        [DisplayName("OT Alarm Reset")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(YesNoOptions.No)]
        [EnumList(typeof(YesNoOptions))]
        public YesNoOptions OTAlarmReset
        {
            get
            {
                return
                    (String.IsNullOrEmpty(config[11])) ? YesNoOptions.No :
                    (YesNoOptions)Enum.Parse(typeof(YesNoOptions), config[11]);
            }
            set { config[11] = value.ToString(); }
        }


        [JsonIgnore]
        [Interfaces.DisplayOrder(12)]
        [Interfaces.DataType(14)]
        [DisplayName("Sampling Mode")]
        [Category("Config")]
        [Editor(typeof(EnumDescriptionGridComboBox), typeof(UITypeEditor))]
        [DefaultValue("None Selected")]
        [EnumList(typeof(SamplingModeOptions))]
        public string SamplingMode
        {
            get
            {
                return
                    (String.IsNullOrEmpty(config[12])) ? SamplingModeOptions.FixedPump.GetDescription() :
                    config[12];
            }
            set { config[12] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(13)]
        [Interfaces.DataType(3)]
        [DisplayName("Trace Enabled")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(YesNoOptions.No)]
        [EnumList(typeof(YesNoOptions))]
        public YesNoOptions TraceEnabled
        {
            get
            {
                return (String.IsNullOrEmpty(config[13])) ? YesNoOptions.No : (YesNoOptions)Enum.Parse(typeof(YesNoOptions), config[13]);
            }
            set { config[13] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(14)]
        [Interfaces.DataType(3)]
        [DisplayName("Purge Enabled")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(YesNoOptions.No)]
        [EnumList(typeof(YesNoOptions))]
        public YesNoOptions PurgeEnabled
        {
            get
            {
                return (String.IsNullOrEmpty(config[14])) ? YesNoOptions.No : (YesNoOptions)Enum.Parse(typeof(YesNoOptions), config[14]);
            }
            set { config[14] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(15)]
        [Interfaces.DataType(3)]
        [DisplayName("Beacon Enabled")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(YesNoOptions.No)]
        [EnumList(typeof(YesNoOptions))]
        public YesNoOptions BeaconEnabled
        {
            get
            {
                return (String.IsNullOrEmpty(config[15])) ? YesNoOptions.No : (YesNoOptions)Enum.Parse(typeof(YesNoOptions), config[15]);
            }
            set { config[15] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(16)]
        [Interfaces.DataType(3)]
        [DisplayName("Reset Enabled")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(YesNoOptions.No)]
        [EnumList(typeof(YesNoOptions))]
        public YesNoOptions resetEnabled
        {
            get
            {
                return (String.IsNullOrEmpty(config[16])) ? YesNoOptions.No : (YesNoOptions)Enum.Parse(typeof(YesNoOptions), config[16]);
            }
            set { config[16] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(17)]
        [Interfaces.DataType(0)]
        [DisplayName("Zero Pressure Value")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int ZeroPressureValue
        {
            get { int d; return int.TryParse(config[17], out d) ? d : 0; }
            set { config[17] = value.ToString(); }
        }


        [JsonIgnore]
        [Interfaces.DisplayOrder(18)]
        [Interfaces.DataType(26)]
        [DisplayName("Temperature Channel")]
        [Category("Config")]
        [Editor(typeof(EnumDescriptionGridComboBox), typeof(UITypeEditor))]
        [DefaultValue("None Selected")]
        [EnumList(typeof(TemperatureChannelOptions))]
        public string TemperatureChannel
        {
            get
            {
                return
                    (String.IsNullOrEmpty(config[18])) ? TemperatureChannelOptions.NoneSelected.GetDescription() :
                    config[18];
            }
            set { config[18] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(18)]
        [Interfaces.DataType(26)]
        [DisplayName("Pressure Channel")]
        [Category("Config")]
        [Editor(typeof(EnumDescriptionGridComboBox), typeof(UITypeEditor))]
        [DefaultValue("None Selected")]
        [EnumList(typeof(TemperatureChannelOptions))]
        public string PressureChannel
        {
            get
            {
                return
                    (String.IsNullOrEmpty(config[19])) ? TemperatureChannelOptions.NoneSelected.GetDescription() :
                    config[19];
            }
            set { config[19] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(20)]
        [Interfaces.DataType(2)]
        [DisplayName("Fixed Temperature (C)")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string FixedTemperature { get { return config[20]; } set { config[20] = value; } }

        [JsonIgnore]
        [Interfaces.DisplayOrder(21)]
        [Interfaces.DataType(2)]
        [DisplayName("Fixed Pressure (mbar)")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string FixedPressure { get { return config[21]; } set { config[21] = value; } }

        [JsonIgnore]
        [Interfaces.DisplayOrder(22)]
        [Interfaces.DataType(2)]
        [DisplayName("Pressure Ref Level (mbar)")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string PressureReflevel { get { return config[22]; } set { config[22] = value; } }


        [JsonIgnore]
        [Interfaces.DisplayOrder(23)]
        [Interfaces.DataType(1)]
        [DisplayName("Poll Rate")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int PollRate
        {
            get { int d; return int.TryParse(config[23], out d) ? d : 0; }
            set { config[23] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(24)]
        [Interfaces.DataType(3)]
        [DisplayName("Display Module")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(YesNoOptions.No)]
        [EnumList(typeof(YesNoOptions))]
        public YesNoOptions DisplayModule
        {
            get
            {
                return (String.IsNullOrEmpty(config[24])) ? YesNoOptions.No : (YesNoOptions)Enum.Parse(typeof(YesNoOptions), config[24]);
            }
            set { config[24] = value.ToString(); }
        }


        [JsonIgnore]
        [Interfaces.DisplayOrder(25)]
        [Interfaces.DataType(9)]
        [DisplayName("Enabled")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(EnabledOptions.Enabled)]
        [EnumList(typeof(EnabledOptions))]
        public EnabledOptions Enabled
        {
            get
            {
                return
    (String.IsNullOrEmpty(config[25])) ? EnabledOptions.Enabled :
    (EnabledOptions)Enum.Parse(typeof(EnabledOptions), config[25]);
            }
            set { config[25] = value.ToString(); }
        }
    }
}
