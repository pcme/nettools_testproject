﻿using CustomControls.ComboBox;
using CustomControls.Data;
using CustomControls.Forms;
using CustomControls.Rule;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomControls;

namespace Communications
{
    public class channel_config_ROM : channel_config
    {
        public channel_config_ROM() : base(23)
        {
            Relay1Source = ROMRelaySources.NoneSelected.GetDescription();
            Relay1AlarmType = ROMAlarmTypes.LimitAlarm.GetDescription();

            Relay2Source = ROMRelaySources.NoneSelected.GetDescription();
            Relay2AlarmType = ROMAlarmTypes.LimitAlarm.GetDescription();
            Relay3Source = ROMRelaySources.NoneSelected.GetDescription();
            Relay3AlarmType = ROMAlarmTypes.LimitAlarm.GetDescription();
            Relay4Source = ROMRelaySources.NoneSelected.GetDescription();
            Relay4AlarmType = ROMAlarmTypes.LimitAlarm.GetDescription();
            Relay5Source = ROMRelaySources.NoneSelected.GetDescription();
            Relay5AlarmType = ROMAlarmTypes.LimitAlarm.GetDescription();
            Relay6Source = ROMRelaySources.NoneSelected.GetDescription();
            Relay6AlarmType = ROMAlarmTypes.LimitAlarm.GetDescription();
            Relay7Source = ROMRelaySources.NoneSelected.GetDescription();
            Relay7AlarmType = ROMAlarmTypes.LimitAlarm.GetDescription();
            Relay8Source = ROMRelaySources.NoneSelected.GetDescription();
            Relay8AlarmType = ROMAlarmTypes.LimitAlarm.GetDescription();

            LatchAlarms = YesNoOptions.No;
            FailSafeRelays = YesNoOptions.No;
            PollRate = 1000;
            Enabled = EnabledOptions.Enabled;

        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(3)]
        [Interfaces.DataType(26)]
        [DisplayName("Relay 1 Source")]
        [Category("Config")]
        [Editor(typeof(EnumDescriptionGridComboBox), typeof(UITypeEditor))]
        [DefaultValue("None Selected")]
        [EnumList(typeof(ROMRelaySources))]
        public string Relay1Source
        {
            get
            {
                return
                    (String.IsNullOrEmpty(config[3])) ? ROMRelaySources.NoneSelected.GetDescription() :
                    config[3];
            }
            set { config[3] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(4)]
        [Interfaces.DataType(31)]
        [DisplayName("Relay 1 Alarm Type")]
        [Category("Config")]
        [Editor(typeof(EnumDescriptionGridComboBox), typeof(UITypeEditor))]
        [DefaultValue("None Selected")]
        [EnumList(typeof(ROMAlarmTypes))]
        public string Relay1AlarmType
        {
            get
            {
                return
                    (String.IsNullOrEmpty(config[4])) ? ROMAlarmTypes.LimitAlarm.GetDescription() :
                    config[4];
            }
            set { config[4] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(5)]
        [Interfaces.DataType(26)]
        [DisplayName("Relay 2 Source")]
        [Category("Config")]
        [Editor(typeof(EnumDescriptionGridComboBox), typeof(UITypeEditor))]
        [DefaultValue("None Selected")]
        [EnumList(typeof(ROMRelaySources))]
        public string Relay2Source
        {
            get
            {
                return
                    (String.IsNullOrEmpty(config[5])) ? ROMRelaySources.NoneSelected.GetDescription() :
                    config[5];
            }
            set { config[5] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(6)]
        [Interfaces.DataType(31)]
        [DisplayName("Relay 2 Alarm Type")]
        [Category("Config")]
        [Editor(typeof(EnumDescriptionGridComboBox), typeof(UITypeEditor))]
        [DefaultValue("None Selected")]
        [EnumList(typeof(ROMAlarmTypes))]
        public string Relay2AlarmType
        {
            get
            {
                return
                    (String.IsNullOrEmpty(config[6])) ? ROMAlarmTypes.LimitAlarm.GetDescription() :
                    config[6];
            }
            set { config[6] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(7)]
        [Interfaces.DataType(26)]
        [DisplayName("Relay 3 Source")]
        [Category("Config")]
        [Editor(typeof(EnumDescriptionGridComboBox), typeof(UITypeEditor))]
        [DefaultValue("None Selected")]
        [EnumList(typeof(ROMRelaySources))]
        public string Relay3Source
        {
            get
            {
                return
                    (String.IsNullOrEmpty(config[7])) ? ROMRelaySources.NoneSelected.GetDescription() :
                    config[7];
            }
            set { config[7] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(8)]
        [Interfaces.DataType(31)]
        [DisplayName("Relay 3 Alarm Type")]
        [Category("Config")]
        [Editor(typeof(EnumDescriptionGridComboBox), typeof(UITypeEditor))]
        [DefaultValue("None Selected")]
        [EnumList(typeof(ROMAlarmTypes))]
        public string Relay3AlarmType
        {
            get
            {
                return
                    (String.IsNullOrEmpty(config[8])) ? ROMAlarmTypes.LimitAlarm.GetDescription() :
                    config[8];
            }
            set { config[8] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(9)]
        [Interfaces.DataType(26)]
        [DisplayName("Relay 4 Source")]
        [Category("Config")]
        [Editor(typeof(EnumDescriptionGridComboBox), typeof(UITypeEditor))]
        [DefaultValue("None Selected")]
        [EnumList(typeof(ROMRelaySources))]
        public string Relay4Source
        {
            get
            {
                return
                    (String.IsNullOrEmpty(config[9])) ? ROMRelaySources.NoneSelected.GetDescription() :
                    config[9];
            }
            set { config[9] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(10)]
        [Interfaces.DataType(31)]
        [DisplayName("Relay 4 Alarm Type")]
        [Category("Config")]
        [Editor(typeof(EnumDescriptionGridComboBox), typeof(UITypeEditor))]
        [DefaultValue("None Selected")]
        [EnumList(typeof(ROMAlarmTypes))]
        public string Relay4AlarmType
        {
            get
            {
                return
                    (String.IsNullOrEmpty(config[10])) ? ROMAlarmTypes.LimitAlarm.GetDescription() :
                    config[10];
            }
            set { config[10] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(11)]
        [Interfaces.DataType(26)]
        [DisplayName("Relay 5 Source")]
        [Category("Config")]
        [Editor(typeof(EnumDescriptionGridComboBox), typeof(UITypeEditor))]
        [DefaultValue("None Selected")]
        [EnumList(typeof(ROMRelaySources))]
        public string Relay5Source
        {
            get
            {
                return
                    (String.IsNullOrEmpty(config[11])) ? ROMRelaySources.NoneSelected.GetDescription() :
                    config[11];
            }
            set { config[11] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(12)]
        [Interfaces.DataType(31)]
        [DisplayName("Relay 5 Alarm Type")]
        [Category("Config")]
        [Editor(typeof(EnumDescriptionGridComboBox), typeof(UITypeEditor))]
        [DefaultValue("None Selected")]
        [EnumList(typeof(ROMAlarmTypes))]
        public string Relay5AlarmType
        {
            get
            {
                return
                    (String.IsNullOrEmpty(config[12])) ? ROMAlarmTypes.LimitAlarm.GetDescription() :
                    config[12];
            }
            set { config[12] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(13)]
        [Interfaces.DataType(26)]
        [DisplayName("Relay 6 Source")]
        [Category("Config")]
        [Editor(typeof(EnumDescriptionGridComboBox), typeof(UITypeEditor))]
        [DefaultValue("None Selected")]
        [EnumList(typeof(ROMRelaySources))]
        public string Relay6Source
        {
            get
            {
                return
                    (String.IsNullOrEmpty(config[13])) ? ROMRelaySources.NoneSelected.GetDescription() :
                    config[13];
            }
            set { config[13] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(14)]
        [Interfaces.DataType(31)]
        [DisplayName("Relay 6 Alarm Type")]
        [Category("Config")]
        [Editor(typeof(EnumDescriptionGridComboBox), typeof(UITypeEditor))]
        [DefaultValue("None Selected")]
        [EnumList(typeof(ROMAlarmTypes))]
        public string Relay6AlarmType
        {
            get
            {
                return
                    (String.IsNullOrEmpty(config[14])) ? ROMAlarmTypes.LimitAlarm.GetDescription() :
                    config[14];
            }
            set { config[14] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(15)]
        [Interfaces.DataType(26)]
        [DisplayName("Relay 7 Source")]
        [Category("Config")]
        [Editor(typeof(EnumDescriptionGridComboBox), typeof(UITypeEditor))]
        [DefaultValue("None Selected")]
        [EnumList(typeof(ROMRelaySources))]
        public string Relay7Source
        {
            get
            {
                return
                    (String.IsNullOrEmpty(config[15])) ? ROMRelaySources.NoneSelected.GetDescription() :
                    config[15];
            }
            set { config[15] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(16)]
        [Interfaces.DataType(31)]
        [DisplayName("Relay 7 Alarm Type")]
        [Category("Config")]
        [Editor(typeof(EnumDescriptionGridComboBox), typeof(UITypeEditor))]
        [DefaultValue("None Selected")]
        [EnumList(typeof(ROMAlarmTypes))]
        public string Relay7AlarmType
        {
            get
            {
                return
                    (String.IsNullOrEmpty(config[16])) ? ROMAlarmTypes.LimitAlarm.GetDescription() :
                    config[16];
            }
            set { config[16] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(17)]
        [Interfaces.DataType(26)]
        [DisplayName("Relay 8 Source")]
        [Category("Config")]
        [Editor(typeof(EnumDescriptionGridComboBox), typeof(UITypeEditor))]
        [DefaultValue("None Selected")]
        [EnumList(typeof(ROMRelaySources))]
        public string Relay8Source
        {
            get
            {
                return
                    (String.IsNullOrEmpty(config[17])) ? ROMRelaySources.NoneSelected.GetDescription() :
                    config[17];
            }
            set { config[17] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(18)]
        [Interfaces.DataType(31)]
        [DisplayName("Relay 8 Alarm Type")]
        [Category("Config")]
        [Editor(typeof(EnumDescriptionGridComboBox), typeof(UITypeEditor))]
        [DefaultValue("None Selected")]
        [EnumList(typeof(ROMAlarmTypes))]
        public string Relay8AlarmType
        {
            get
            {
                return
                    (String.IsNullOrEmpty(config[18])) ? ROMAlarmTypes.LimitAlarm.GetDescription() :
                    config[18];
            }
            set { config[18] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(19)]
        [Interfaces.DataType(3)]
        [DisplayName("Latch Alarms")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(YesNoOptions.No)]
        [EnumList(typeof(YesNoOptions))]
        public YesNoOptions LatchAlarms
        {
            get
            {
                return (String.IsNullOrEmpty(config[19])) ? YesNoOptions.No : (YesNoOptions)Enum.Parse(typeof(YesNoOptions), config[19]);
            }
            set { config[19] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(20)]
        [Interfaces.DataType(3)]
        [DisplayName("Fail Safe Relays")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(YesNoOptions.No)]
        [EnumList(typeof(YesNoOptions))]
        public YesNoOptions FailSafeRelays
        {
            get
            {
                return (String.IsNullOrEmpty(config[20])) ? YesNoOptions.No : (YesNoOptions)Enum.Parse(typeof(YesNoOptions), config[20]);
            }
            set { config[20] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(21)]
        [Interfaces.DataType(1)]
        [DisplayName("Poll Rate")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int PollRate
        {
            get { int d; return int.TryParse(config[21], out d) ? d : 0; }
            set { config[21] = value.ToString(); }
        }


        [JsonIgnore]
        [Interfaces.DisplayOrder(22)]
        [Interfaces.DataType(9)]
        [DisplayName("Enabled")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(EnabledOptions.Enabled)]
        [EnumList(typeof(EnabledOptions))]
        public EnabledOptions Enabled
        {
            get
            {
                return
    (String.IsNullOrEmpty(config[22])) ? EnabledOptions.Enabled :
    (EnabledOptions)Enum.Parse(typeof(EnabledOptions), config[22]);
            }
            set { config[22] = value.ToString(); }
        }

    }
}
