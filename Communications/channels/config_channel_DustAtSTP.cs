﻿using CustomControls.ComboBox;
using CustomControls.Data;
using CustomControls.Forms;
using CustomControls.Rule;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomControls;

namespace Communications
{
    public class channel_config_DustAtSTP : channel_config
    {
        public channel_config_DustAtSTP() : base(30)
        {
            UnitName = "Nmg/m3";
          
            NegativeOffset = "0";

            DustChannel = DustSTPInputOptions.NoneSelected.GetDescription();
            TemperatureChannel = DustSTPInputOptions.NoneSelected.GetDescription();
            PressureChannel = DustSTPInputOptions.NoneSelected.GetDescription(); 
            OxygenChannel = DustSTPInputOptions.NoneSelected.GetDescription();
            MoistureChannel1 = DustSTPInputOptions.NoneSelected.GetDescription(); ;
            FixedTemperature = "0";
            FixedPressure = "1013";
            FixedOxygen = "11";
            FixedMoisture = "0";
            TemperatureRef = "0";
            PressureRef = "1013";
            OxygenRef = "11";
            MultiCalFactos = YesNoOptions.No;
            CalRange = 0;

            InstantWarningAlarm = "0";
            InstantLimitAlarm = "0";
            AveragingTime = 60;
            AverageWarningAlarm = "0";
            AverageLimitAlarm = "0";
            AlarmDelay = 5;

         
            LogOptions = "E";
            PlantRunInput = 0;
            ClipLevel = "1000";
        


            Enabled = EnabledOptions.Enabled;

        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(3)]
        [Interfaces.DataType(25)]
        [DisplayName("Dust Channel")]
        [Category("Config")]
        [Editor(typeof(EnumDescriptionGridComboBox), typeof(UITypeEditor))]
        [DefaultValue("None Selected")]
        [EnumList(typeof(DustSTPInputOptions))]
        public string DustChannel
        {
            get
            {
                return
                    (String.IsNullOrEmpty(config[3])) ? DustSTPInputOptions.NoneSelected.GetDescription() :
                    config[3];
            }
            set { config[3] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(4)]
        [Interfaces.DataType(26)]
        [DisplayName("Temperature Channel")]
        [Category("Config")]
        [Editor(typeof(EnumDescriptionGridComboBox), typeof(UITypeEditor))]
        [DefaultValue("None Selected")]
        [EnumList(typeof(DustSTPInputOptions))]
        public string TemperatureChannel
        {
            get
            {
                return
                    (String.IsNullOrEmpty(config[4])) ? 
                    DustSTPInputOptions.NoneSelected.GetDescription() :
                    config[4];
            }
            set { config[4] = value.ToString(); }
        }


        [JsonIgnore]
        [Interfaces.DisplayOrder(5)]
        [Interfaces.DataType(26)]
        [DisplayName("Pressure Channel")]
        [Category("Config")]
        [Editor(typeof(EnumDescriptionGridComboBox), typeof(UITypeEditor))]
        [DefaultValue("None Selected")]
        [EnumList(typeof(DustSTPInputOptions))]
        public string PressureChannel
        {
            get
            {
                return
                    (String.IsNullOrEmpty(config[5])) ? DustSTPInputOptions.NoneSelected.GetDescription() :
                    config[5];
            }
            set { config[5] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(6)]
        [Interfaces.DataType(26)]
        [DisplayName("Oxygen Channel")]
        [Category("Config")]
        [Editor(typeof(EnumDescriptionGridComboBox), typeof(UITypeEditor))]
        [DefaultValue("None Selected")]
        [EnumList(typeof(DustSTPInputOptions))]
        public string OxygenChannel
        {
            get
            {
                return
                    (String.IsNullOrEmpty(config[6])) ? DustSTPInputOptions.NoneSelected.GetDescription() :
                    config[6];
            }
            set { config[6] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(7)]
        [Interfaces.DataType(26)]
        [DisplayName("Moisture Channel 1")]
        [Category("Config")]
        [Editor(typeof(EnumDescriptionGridComboBox), typeof(UITypeEditor))]
        [DefaultValue("None Selected")]
        [EnumList(typeof(DustSTPInputOptions))]
        public string MoistureChannel1
        {
            get
            {
                return
                    (String.IsNullOrEmpty(config[7])) ? DustSTPInputOptions.NoneSelected.GetDescription() :
                    config[7];
            }
            set { config[7] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(8)]
        [Interfaces.DataType(2)]
        [DisplayName("Fixed Temperature")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string FixedTemperature { get { return config[8]; } set { config[8] = value; } }


        [JsonIgnore]
        [Interfaces.DisplayOrder(9)]
        [Interfaces.DataType(2)]
        [DisplayName("Fixed Pressure mbar")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string FixedPressure { get { return config[9]; } set { config[9] = value; } }


        [JsonIgnore]
        [Interfaces.DisplayOrder(10)]
        [Interfaces.DataType(2)]
        [DisplayName("Fixed Oxygen %")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string FixedOxygen { get { return config[10]; } set { config[10] = value; } }


        [JsonIgnore]
        [Interfaces.DisplayOrder(11)]
        [Interfaces.DataType(2)]
        [DisplayName("Fixed Moisture %")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string FixedMoisture { get { return config[11]; } set { config[11] = value; } }


        [JsonIgnore]
        [Interfaces.DisplayOrder(12)]
        [Interfaces.DataType(2)]
        [DisplayName("Temp Ref Level")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string TemperatureRef { get { return config[12]; } set { config[12] = value; } }


        [JsonIgnore]
        [Interfaces.DisplayOrder(13)]
        [Interfaces.DataType(2)]
        [DisplayName("Press Ref level mbar")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string PressureRef { get { return config[13]; } set { config[13] = value; } }


        [JsonIgnore]
        [Interfaces.DisplayOrder(14)]
        [Interfaces.DataType(2)]
        [DisplayName("Oxygen Ref Level %")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string OxygenRef { get { return config[14]; } set { config[14] = value; } }


        [JsonIgnore]
        [Interfaces.DisplayOrder(15)]
        [Interfaces.DataType(23)]
        [DisplayName("Units Name")]
        [Category("Config")]
        [LengthRule(0, 18)]
        public string UnitName { get { return config[15]; } set { config[15] = value; } }
      
        [JsonIgnore]
        [Interfaces.DisplayOrder(16)]
        [Interfaces.DataType(2)]
        [DisplayName("Calibration Factor")]
        [Description("Calibration Factor (4 decimal places)")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string CalibrationFactor { get { return config[16]; } set { config[16] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(17)]
        [Interfaces.DataType(2)]
        [DisplayName("Negative Offset")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string NegativeOffset { get { return config[17]; } set { config[17] = value; } }


        [JsonIgnore]
        [Interfaces.DisplayOrder(18)]
        [Interfaces.DataType(3)]
        [DisplayName("Multi Cal Factos")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(YesNoOptions.Yes)]
        [EnumList(typeof(YesNoOptions))]
        public YesNoOptions MultiCalFactos
        {
            get
            {
                return (String.IsNullOrEmpty(config[18])) ? 
                    YesNoOptions.No : (YesNoOptions)Enum.Parse(typeof(YesNoOptions), config[18]);
            }
            set { config[18] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(19)]
        [Interfaces.DataType(0)]
        [DisplayName("Cal Range")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int CalRange
        {
            get { int d; return int.TryParse(config[19], out d) ? d : 0; }
            set { config[19] = value.ToString(); }
        }



        [JsonIgnore]
        [Interfaces.DisplayOrder(20)]
        [Interfaces.DataType(2)]
        [DisplayName("Instant Warning Alarm")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string InstantWarningAlarm { get { return config[20]; } set { config[20] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(21)]
        [Interfaces.DataType(2)]
        [DisplayName("Instant Limit Alarm")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string InstantLimitAlarm { get { return config[21]; } set { config[21] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(22)]
        [Interfaces.DataType(0)]
        [DisplayName("Averaging Time")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int AveragingTime
        {
            get { int d; return int.TryParse(config[22], out d) ? d : 0; }
            set { config[22] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(23)]
        [Interfaces.DataType(2)]
        [DisplayName("Average Warning Alarm")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string AverageWarningAlarm { get { return config[23]; } set { config[23] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(24)]
        [Interfaces.DataType(2)]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        [DisplayName("Average Limit Alarm")]
        [Category("Config")]
        public string AverageLimitAlarm { get { return config[24]; } set { config[24] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(25)]
        [Interfaces.DataType(0)]
        [DisplayName("Alarm Delay")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int AlarmDelay
        {
            get { int d; return int.TryParse(config[25], out d) ? d : 0; }
            set { config[25] = value.ToString(); }
        }


        [JsonIgnore]
        [Interfaces.DisplayOrder(26)]
        [Interfaces.DataType(30)]
        [DisplayName("Log Options")]
        [Category("Config")]
        [Editor(typeof(LogOptionsESEditor), typeof(UITypeEditor))]
        public string LogOptions { get { return config[26]; } set { config[26] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(27)]
        [Interfaces.DataType(0)]
        [DisplayName("Plant Run Input")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int PlantRunInput
        {
            get { int d; return int.TryParse(config[27], out d) ? d : 0; }
            set { config[27] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(28)]
        [Interfaces.DataType(2)]
        [DisplayName("Clip Level")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string ClipLevel { get { return config[28]; } set { config[28] = value; } }

       
        [JsonIgnore]
        [Interfaces.DisplayOrder(29)]
        [Interfaces.DataType(9)]
        [DisplayName("Enabled")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(EnabledOptions.Enabled)]
        [EnumList(typeof(EnabledOptions))]
        public EnabledOptions Enabled
        {
            get
            {
                return
    (String.IsNullOrEmpty(config[29])) ? EnabledOptions.Enabled :
    (EnabledOptions)Enum.Parse(typeof(EnabledOptions), config[29]);
            }
            set { config[29] = value.ToString(); }
        }

    }
}