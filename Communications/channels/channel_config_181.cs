﻿using CustomControls.ComboBox;
using CustomControls.Data;
using CustomControls.Forms;
using CustomControls.Rule;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Communications
{
    public class channel_config_181 : channel_config
    {
        public channel_config_181() : base(34)
        {
            UnitName = "units";
            CalibrationType = CalibrationTypeOptions.Uncalibrated;
            CalibrationFactor = "1";
            NegativeOffset = "0";

            Sensitivity = SensitivityOptions.High;
            CalibrationRange = 0;
            PS11CalMethod = PS11CalMethodOptions.Linear;
            b0 = "0";
            b1 = "1";
            b2 = "0";
            InstantWarningAlarm = "0";
            InstantAlarmLimit = "0";
            AverageFilterTime = 60;
            AverageWarningAlarm = "0";
            AverageLimitAlarm = "0";
            AlarmDelay = 5;
            SelfTestsActive = YesNoOptions.Yes;
            ZeroSpanAlarm = YesNoOptions.Yes;
            PurgeAlarmLevel = "15";
            ChecksOn4_20mA = YesNoOptions.No;
            AutoPS11Cal = YesNoOptions.Yes;
            PS11CalTime_Hr = 5;
            PS11CalTime_min = 0;
            PS11UpscalePoint = 100;
            ForcePS11Cal = YesNoOptions.Yes;
            PollRate = 1000;
            LogOptions = "P";
            PlantRunInput = 0;
            ClipLevel = "1000";
            DisplayModule = YesNoOptions.Yes;

            Enabled = EnabledOptions.Enabled;

        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(3)]
        [Interfaces.DataType(23)]
        [DisplayName("Units Name")]
        [Category("Config")]
        [LengthRule(0, 18)]
        public string UnitName { get { return config[3]; } set { config[3] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(4)]
        [Interfaces.DataType(13)]
        [DisplayName("Calibration Type")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(CalibrationTypeOptions.Uncalibrated)]
        [EnumList(typeof(CalibrationTypeOptions))]
        public CalibrationTypeOptions CalibrationType
        {
            get
            {
                return (String.IsNullOrEmpty(config[4])) ? CalibrationTypeOptions.Uncalibrated : (CalibrationTypeOptions)Enum.Parse(typeof(CalibrationTypeOptions), config[4]);
            }
            set { config[4] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(5)]
        [Interfaces.DataType(2)]
        [DisplayName("Calibration Factor")]
        [Description("Calibration Factor (4 decimal places)")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string CalibrationFactor { get { return config[5]; } set { config[5] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(6)]
        [Interfaces.DataType(2)]
        [DisplayName("Negative Offset")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string NegativeOffset { get { return config[6]; } set { config[6] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(7)]
        [Interfaces.DataType(4)]
        [DisplayName("Sensitivity")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(SensitivityOptions.High)]
        [EnumList(typeof(SensitivityOptions))]
        public SensitivityOptions Sensitivity
        {
            get
            {
                return (String.IsNullOrEmpty(config[7])) ? SensitivityOptions.High : (SensitivityOptions)Enum.Parse(typeof(SensitivityOptions), config[7]);
            }
            set { config[7] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(8)]
        [Interfaces.DataType(0)]
        [DisplayName("Calibration Range")]
        [Description("Calibration Range (0-100)")]
        [Category("Config")]
        [IntRule(0, 100)]
        public int CalibrationRange
        {
            get { int d; return int.TryParse(config[8], out d) ? d : 0; }
            set { config[8] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(9)]
        [Interfaces.DataType(15)]
        [DisplayName("PS11 Cal Method")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(PS11CalMethodOptions.Linear)]
        [EnumList(typeof(PS11CalMethodOptions))]
        public PS11CalMethodOptions PS11CalMethod
        {
            get
            {
                return (String.IsNullOrEmpty(config[9])) ? PS11CalMethodOptions.Linear : (PS11CalMethodOptions)Enum.Parse(typeof(PS11CalMethodOptions), config[9]);
            }
            set { config[9] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(10)]
        [Interfaces.DataType(2)]
        [DisplayName("b0")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string b0 { get { return config[10]; } set { config[10] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(11)]
        [Interfaces.DataType(2)]
        [DisplayName("b1")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string b1 { get { return config[11]; } set { config[11] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(12)]
        [Interfaces.DataType(2)]
        [DisplayName("b2")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string b2 { get { return config[12]; } set { config[12] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(13)]
        [Interfaces.DataType(2)]
        [DisplayName("Instant Warning Alarm")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string InstantWarningAlarm { get { return config[13]; } set { config[13] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(14)]
        [Interfaces.DataType(2)]
        [DisplayName("Instant Limit Alarm")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string InstantAlarmLimit { get { return config[14]; } set { config[14] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(15)]
        [Interfaces.DataType(0)]
        [DisplayName("Average Filter Time")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int AverageFilterTime
        {
            get { int d; return int.TryParse(config[15], out d) ? d : 0; }
            set { config[15] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(16)]
        [Interfaces.DataType(2)]
        [DisplayName("Average Warning Alarm")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string AverageWarningAlarm { get { return config[16]; } set { config[16] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(17)]
        [Interfaces.DataType(2)]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        [DisplayName("Average Limit Alarm")]
        [Category("Config")]
        public string AverageLimitAlarm { get { return config[17]; } set { config[17] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(18)]
        [Interfaces.DataType(0)]
        [DisplayName("Alarm Delay")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int AlarmDelay
        {
            get { int d; return int.TryParse(config[18], out d) ? d : 0; }
            set { config[18] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(19)]
        [Interfaces.DataType(3)]
        [DisplayName("Self Tests Active")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(YesNoOptions.Yes)]
        [EnumList(typeof(YesNoOptions))]
        public YesNoOptions SelfTestsActive
        {
            get
            {
                return (String.IsNullOrEmpty(config[19])) ? YesNoOptions.No : (YesNoOptions)Enum.Parse(typeof(YesNoOptions), config[19]);
            }
            set { config[19] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(20)]
        [Interfaces.DataType(3)]
        [DisplayName("Zero Span Alarm")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(YesNoOptions.No)]
        [EnumList(typeof(YesNoOptions))]
        public YesNoOptions ZeroSpanAlarm
        {
            get
            {
                return (String.IsNullOrEmpty(config[20])) ? YesNoOptions.No : (YesNoOptions)Enum.Parse(typeof(YesNoOptions), config[20]);
            }
            set { config[20] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(21)]
        [Interfaces.DataType(2)]
        [DisplayName("Purge Alarm Level")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string PurgeAlarmLevel { get { return config[21]; } set { config[21] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(22)]
        [Interfaces.DataType(3)]
        [DisplayName("Checks On 4-20mA")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(YesNoOptions.No)]
        [EnumList(typeof(YesNoOptions))]
        public YesNoOptions ChecksOn4_20mA
        {
            get
            {
                return (String.IsNullOrEmpty(config[22])) ? YesNoOptions.No : (YesNoOptions)Enum.Parse(typeof(YesNoOptions), config[22]);
            }
            set { config[21] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(23)]
        [Interfaces.DataType(3)]
        [DisplayName("Auto PS11 Cal")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(YesNoOptions.No)]
        [EnumList(typeof(YesNoOptions))]
        public YesNoOptions AutoPS11Cal
        {
            get
            {
                return (String.IsNullOrEmpty(config[23])) ? YesNoOptions.No : (YesNoOptions)Enum.Parse(typeof(YesNoOptions), config[23]);
            }
            set { config[23] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(24)]
        [Interfaces.DataType(0)]
        [DisplayName("PS11 Cal Time (hr)")]
        [Category("Config")]
        [IntRule(0, 24)]
        public int PS11CalTime_Hr
        {
            get { int d; return int.TryParse(config[24], out d) ? d : 0; }
            set { config[24] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(25)]
        [Interfaces.DataType(0)]
        [DisplayName("PS11 Cal Time (min)")]
        [Category("Config")]
        [IntRule(0, 59)]
        public int PS11CalTime_min
        {
            get { int d; return int.TryParse(config[25], out d) ? d : 0; }
            set { config[25] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(26)]
        [Interfaces.DataType(0)]
        [DisplayName("PS11 Upscale Point")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int PS11UpscalePoint
        {
            get { int d; return int.TryParse(config[26], out d) ? d : 0; }
            set { config[26] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(27)]
        [Interfaces.DataType(3)]
        [DisplayName("Force PS11 Cal")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(YesNoOptions.No)]
        [EnumList(typeof(YesNoOptions))]
        public YesNoOptions ForcePS11Cal
        {
            get
            {
                return (String.IsNullOrEmpty(config[27])) ? YesNoOptions.No : (YesNoOptions)Enum.Parse(typeof(YesNoOptions), config[27]);
            }
            set { config[27] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(28)]
        [Interfaces.DataType(1)]
        [DisplayName("Poll Rate")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int PollRate
        {
            get { int d; return int.TryParse(config[28], out d) ? d : 0; }
            set { config[28] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(29)]
        [Interfaces.DataType(30)]
        [DisplayName("Log Options")]
        [Category("Config")]
        [Editor(typeof(LogOptionsEditor), typeof(UITypeEditor))]
        public string LogOptions { get { return config[29]; } set { config[29] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(30)]
        [Interfaces.DataType(0)]
        [DisplayName("Plant Run Input")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int PlantRunInput
        {
            get { int d; return int.TryParse(config[30], out d) ? d : 0; }
            set { config[30] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(31)]
        [Interfaces.DataType(2)]
        [DisplayName("Clip Level")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string ClipLevel { get { return config[31]; } set { config[31] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(32)]
        [Interfaces.DataType(3)]
        [DisplayName("Display Module")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(YesNoOptions.No)]
        [EnumList(typeof(YesNoOptions))]
        public YesNoOptions DisplayModule
        {
            get
            {
                return (String.IsNullOrEmpty(config[32])) ? YesNoOptions.No : (YesNoOptions)Enum.Parse(typeof(YesNoOptions), config[32]);
            }
            set { config[32] = value.ToString(); }
        }
       
        [JsonIgnore]
        [Interfaces.DisplayOrder(33)]
        [Interfaces.DataType(9)]
        [DisplayName("Enabled")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(EnabledOptions.Enabled)]
        [EnumList(typeof(EnabledOptions))]
        public EnabledOptions Enabled
        {
            get
            {
                return (String.IsNullOrEmpty(config[33])) ? EnabledOptions.Enabled : (EnabledOptions)Enum.Parse(typeof(EnabledOptions), config[33]);
            }
            set { config[33] = value.ToString(); }
        }

    }

}
