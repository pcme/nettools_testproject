﻿using CustomControls.ComboBox;
using CustomControls.Data;
using CustomControls.Forms;
using CustomControls.Rule;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomControls;
using CustomControls.ComboBox;
using CustomControls.Data;
using CustomControls.Forms;
using CustomControls.Rule;

namespace Communications
{
    public class channel_config_SF200 : channel_config
    {


        public channel_config_SF200() : base(36)
        {
            UnitName = "units";
            DisplayUnits = "Velocity (m/s)";
            Density = "1.293";
            PitotSensor = "0.75";
            StackArea = "1";
            CalA = "0";
            CalB = "1";
            CalC = "0";
            PDIFFCalA = "0";
            PDIFFCalB = "1";
            PDIFFCalC = "0";
            InstantWarningAlarm = "0";
            InstantLimitAlarm = "30";
            AveragingTime = 60;
            AverageWarningAlarm = "0";
            AverageLimitAlarm = "0";
            LowWarningAlarm = "0";
            LowLimitAlarm = "-30";
            AveLowWarningAlarm = "0";
            AveLowLimitAlarm = "0";
            AlarmDelay = 5;

            Local4_40mAZero = "0";
            Local4_20mASpan = "0";
            Local4_20mAFilter = "30";
            BackFlushDuration = 10;
            BackFlushPeriod = 60;
            BackFlushDeadTime = 30;


            PollRate = 1000;
            LogOptions = "*";
            PlantRunInput = 0;
  
            DisplayModule = YesNoOptions.No;
            Enabled = EnabledOptions.Enabled;

        }


        [JsonIgnore]
        [Interfaces.DisplayOrder(3)]
        [Interfaces.DataType(23)]
        [DisplayName("Units Name")]
        [Category("Config")]
        [LengthRule(0, 18)]
        public string UnitName { get { return config[3]; } set { config[3] = value; } }


        [JsonIgnore]
        [Interfaces.DisplayOrder(4)]
        [Interfaces.DataType(17)]
        [DisplayName("Display Units")]
        [Category("Config")]
        [Editor(typeof(EnumDescriptionGridComboBox), typeof(UITypeEditor))]
        [DefaultValue("Velocity (m/s)")]
        [EnumList(typeof(DisplayUnitsOptions))]
        public string DisplayUnits
        {
            get
            {
                return
                    (String.IsNullOrEmpty(config[4])) ? DisplayUnitsOptions.Velocity.GetDescription() :
                    config[4];
            }
            set { config[4] = value.ToString(); }
        }

      

        [JsonIgnore]
        [Interfaces.DisplayOrder(5)]
        [Interfaces.DataType(2)]
        [DisplayName("Density (kg/m3)")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string Density { get { return config[5]; } set { config[5] = value; } }

        [JsonIgnore]
        [Interfaces.DisplayOrder(6)]
        [Interfaces.DataType(2)]
        [DisplayName("Pitot Sensor K")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string PitotSensor { get { return config[6]; } set { config[6] = value; } }

        [JsonIgnore]
        [Interfaces.DisplayOrder(7)]
        [Interfaces.DataType(2)]
        [DisplayName("Stack Area")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string StackArea { get { return config[7]; } set { config[7] = value; } }

        [JsonIgnore]
        [Interfaces.DisplayOrder(8)]
        [Interfaces.DataType(2)]
        [DisplayName("Speed WAF")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string SpeedWAF { get { return config[8]; } set { config[8] = value; } }

        [JsonIgnore]
        [Interfaces.DisplayOrder(9)]
        [Interfaces.DataType(2)]
        [DisplayName("Speed Cal a")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string CalA { get { return config[9]; } set { config[9] = value; } }

        [JsonIgnore]
        [Interfaces.DisplayOrder(10)]
        [Interfaces.DataType(2)]
        [DisplayName("Speed Cal b")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string CalB { get { return config[10]; } set { config[10] = value; } }

        [JsonIgnore]
        [Interfaces.DisplayOrder(11)]
        [Interfaces.DataType(2)]
        [DisplayName("Speed Cal c")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string CalC { get { return config[11]; } set { config[11] = value; } }


        [JsonIgnore]
        [Interfaces.DisplayOrder(12)]
        [Interfaces.DataType(2)]
        [DisplayName("Pdiff Cal a")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string PDIFFCalA { get { return config[12]; } set { config[12] = value; } }

        [JsonIgnore]
        [Interfaces.DisplayOrder(13)]
        [Interfaces.DataType(2)]
        [DisplayName("Pdiff Cal b")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string PDIFFCalB { get { return config[13]; } set { config[13] = value; } }

        [JsonIgnore]
        [Interfaces.DisplayOrder(14)]
        [Interfaces.DataType(2)]
        [DisplayName("Pdiff Cal c")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string PDIFFCalC { get { return config[14]; } set { config[14] = value; } }


        [JsonIgnore]
        [Interfaces.DisplayOrder(15)]
        [Interfaces.DataType(2)]
        [DisplayName("Instant Warning Alarm")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string InstantWarningAlarm { get { return config[15]; } set { config[15] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(16)]
        [Interfaces.DataType(2)]
        [DisplayName("Instant Limit Alarm")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string InstantLimitAlarm { get { return config[16]; } set { config[16] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(17)]
        [Interfaces.DataType(0)]
        [DisplayName("Averaging Time")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int AveragingTime
        {
            get { int d; return int.TryParse(config[17], out d) ? d : 0; }
            set { config[17] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(18)]
        [Interfaces.DataType(2)]
        [DisplayName("Average Warning Alarm")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string AverageWarningAlarm { get { return config[18]; } set { config[18] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(19)]
        [Interfaces.DataType(2)]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        [DisplayName("Average Limit Alarm")]
        [Category("Config")]
        public string AverageLimitAlarm { get { return config[19]; } set { config[19] = value; } }

        [JsonIgnore]
        [Interfaces.DisplayOrder(20)]
        [Interfaces.DataType(2)]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        [DisplayName("Low Warning Alarm")]
        [Category("Config")]
        public string LowWarningAlarm { get { return config[20]; } set { config[20] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(21)]
        [Interfaces.DataType(2)]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        [DisplayName("Low Limit Alarm")]
        [Category("Config")]
        public string LowLimitAlarm { get { return config[21]; } set { config[21] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(22)]
        [Interfaces.DataType(2)]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        [DisplayName("Average Low Warning Alarm")]
        [Category("Config")]
        public string AveLowWarningAlarm { get { return config[22]; } set { config[22] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(23)]
        [Interfaces.DataType(2)]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        [DisplayName("Average Low Limit Alarm")]
        [Category("Config")]
        public string AveLowLimitAlarm { get { return config[23]; } set { config[23] = value; } }

        [JsonIgnore]
        [Interfaces.DisplayOrder(24)]
        [Interfaces.DataType(0)]
        [DisplayName("Alarm Delay")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int AlarmDelay
        {
            get { int d; return int.TryParse(config[24], out d) ? d : 0; }
            set { config[24] = value.ToString(); }
        }


        [JsonIgnore]
        [Interfaces.DisplayOrder(25)]
        [Interfaces.DataType(2)]
        [DisplayName("Local 4-20mA Zero")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string Local4_40mAZero { get { return config[25]; } set { config[25] = value; } }

        [JsonIgnore]
        [Interfaces.DisplayOrder(26)]
        [Interfaces.DataType(2)]
        [DisplayName("Local 4-20mA Span")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string Local4_20mASpan { get { return config[26]; } set { config[26] = value; } }



        [JsonIgnore]
        [Interfaces.DisplayOrder(27)]
        [Interfaces.DataType(0)]
        [DisplayName("Local 4-20mA Filter")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string Local4_20mAFilter { get { return config[27]; } set { config[27] = value; } }


        [JsonIgnore]
        [Interfaces.DisplayOrder(28)]
        [Interfaces.DataType(0)]
        [DisplayName("Backflush duration(s)")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int BackFlushDuration
        {
            get { int d; return int.TryParse(config[28], out d) ? d : 0; }
            set { config[28] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(29)]
        [Interfaces.DataType(0)]
        [DisplayName("Backflush period (s))")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int BackFlushPeriod
        {
            get { int d; return int.TryParse(config[29], out d) ? d : 0; }
            set { config[29] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(30)]
        [Interfaces.DataType(0)]
        [DisplayName("Backflush dead time(s)")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int BackFlushDeadTime
        {
            get { int d; return int.TryParse(config[30], out d) ? d : 0; }
            set { config[30] = value.ToString(); }
        }


        [JsonIgnore]
        [Interfaces.DisplayOrder(31)]
        [Interfaces.DataType(1)]
        [DisplayName("Poll Rate")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int PollRate
        {
            get { int d; return int.TryParse(config[31], out d) ? d : 0; }
            set { config[31] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(32)]
        [Interfaces.DataType(30)]
        [DisplayName("Log Options")]
        [Category("Config")]
        [Editor(typeof(LogOptionsEditor), typeof(UITypeEditor))]
        public string LogOptions { get { return config[32]; } set { config[32] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(33)]
        [Interfaces.DataType(0)]
        [DisplayName("Plant Run Input")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int PlantRunInput
        {
            get { int d; return int.TryParse(config[33], out d) ? d : 0; }
            set { config[33] = value.ToString(); }
        }

       

        [JsonIgnore]
        [Interfaces.DisplayOrder(34)]
        [Interfaces.DataType(3)]
        [DisplayName("Display Module")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(YesNoOptions.No)]
        [EnumList(typeof(YesNoOptions))]
        public YesNoOptions DisplayModule
        {
            get
            {
                return (String.IsNullOrEmpty(config[34])) ? YesNoOptions.No :
                   (YesNoOptions)Enum.Parse(typeof(YesNoOptions), config[34]);
            }
            set { config[34] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(35)]
        [Interfaces.DataType(9)]
        [DisplayName("Enabled")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(EnabledOptions.Enabled)]
        [EnumList(typeof(EnabledOptions))]
        public EnabledOptions Enabled
        {
            get
            {
                return
    (String.IsNullOrEmpty(config[35])) ? EnabledOptions.Enabled :
    (EnabledOptions)Enum.Parse(typeof(EnabledOptions), config[35]);
            }
            set { config[35] = value.ToString(); }
        }

    }
}

