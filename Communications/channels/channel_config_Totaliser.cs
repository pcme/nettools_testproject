﻿using CustomControls.ComboBox;
using CustomControls.Data;
using CustomControls.Forms;
using CustomControls.Rule;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomControls;

namespace Communications
{
    public class channel_config_Totaliser : channel_config
    {
        public channel_config_Totaliser() : base(11)
        {
            InputChannel = TotaliserInputOptions.NoneSelected.GetDescription();
            UnitsDivider = "1000";
            UnitName = "Kg";
            InstantWarningAlarm = "0";
            InstantLimitAlarm = "0";
            LogOptions = "P";
            Enabled = EnabledOptions.Enabled;
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(3)]
        [Interfaces.DataType(25)]
        [DisplayName("Input Channel")]
        [Category("Config")]
        [Editor(typeof(EnumDescriptionGridComboBox), typeof(UITypeEditor))]
        [DefaultValue("None Selected")]
        [EnumList(typeof(TotaliserInputOptions))]
        public string InputChannel
        {
            get
            {
                return
                    (String.IsNullOrEmpty(config[3])) ? TotaliserInputOptions.NoneSelected.GetDescription() :
                    config[3];
            }
            set { config[3] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(4)]
        [Interfaces.DataType(2)]
        [DisplayName("Units divider")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string UnitsDivider { get { return config[4]; } set { config[4] = value; } }


        [JsonIgnore]
        [Interfaces.DisplayOrder(5)]
        [Interfaces.DataType(23)]
        [DisplayName("Units Name")]
        [Category("Config")]
        [LengthRule(0, 18)]
        public string UnitName { get { return config[5]; } set { config[5] = value; } }

        [JsonIgnore]
        [Interfaces.DisplayOrder(6)]
        [Interfaces.DataType(2)]
        [DisplayName("Instant Warning Alarm")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string InstantWarningAlarm { get { return config[6]; } set { config[6] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(7)]
        [Interfaces.DataType(2)]
        [DisplayName("Instant Limit Alarm")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string InstantLimitAlarm { get { return config[7]; } set { config[7] = value; } }


        [JsonIgnore]
        [Interfaces.DisplayOrder(8)]
        [Interfaces.DataType(30)]
        [DisplayName("Log Options")]
        [Category("Config")]
        [Editor(typeof(LogOptionsESEditor), typeof(UITypeEditor))]
        public string LogOptions { get { return config[8]; } set { config[8] = value; } }

        [JsonIgnore]
        [Interfaces.DisplayOrder(9)]
        [Interfaces.DataType(3)]
        [DisplayName("Reset")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(YesNoOptions.No)]
        [EnumList(typeof(YesNoOptions))]
        public YesNoOptions Reset
        {
            get
            {
                return (String.IsNullOrEmpty(config[9])) ? 
                    YesNoOptions.No :
                    (YesNoOptions)Enum.Parse(typeof(YesNoOptions), config[9]);
            }
            set { config[9] = value.ToString(); }
        }


        [JsonIgnore]
        [Interfaces.DisplayOrder(10)]
        [Interfaces.DataType(9)]
        [DisplayName("Enabled")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(EnabledOptions.Enabled)]
        [EnumList(typeof(EnabledOptions))]
        public EnabledOptions Enabled
        {
            get
            {
                return
    (String.IsNullOrEmpty(config[10])) ? EnabledOptions.Enabled :
    (EnabledOptions)Enum.Parse(typeof(EnabledOptions), config[10]);
            }
            set { config[10] = value.ToString(); }
        }
    }
}
