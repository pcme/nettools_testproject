﻿using CustomControls.ComboBox;
using CustomControls.Data;
using CustomControls.Forms;
using CustomControls.Rule;
using Newtonsoft.Json;
using System;
using System.ComponentModel;
using System.Drawing.Design;

namespace Communications
{
    public class channel_config_CNX800 : channel_config
    {
        public channel_config_CNX800() : base(21)
        {
     
            ScalingFactor = "1";
            NegativeOffset = "0";

            Sensitivity = SensitivityOptions.High;

            InstantWarningAlarm = "0";
            InstantLimitAlarm = "0";
            AveragingTime = 60;
            AverageWarningAlarm = "0";
            AverageLimitAlarm = "0";
            AlarmDelay = 5;

            SelfTestsActive = YesNoOptions.Yes;

            ZeroSpanAlarm = YesNoOptions.Yes;
            ShortCircuitAlarm = YesNoOptions.Yes;

            ChecksOn4_20mA = YesNoOptions.No;
            PollRate = 1000;
            LogOptions = "P";
            PlantRunInput = 0;
            ClipLevel = "1000";

            Enabled = EnabledOptions.Enabled;

        }


        

        [JsonIgnore]
        [DisplayName("Scaling Factor")]
        [Description("Scaling Factor (4 decimal places)")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string ScalingFactor { get { return config[3]; } set { config[3] = value; } }
        [JsonIgnore]
        [DisplayName("Negative Offset")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string NegativeOffset { get { return config[4]; } set { config[4] = value; } }

        [JsonIgnore]
        [DisplayName("Sensitivity")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(SensitivityOptions.High)]
        [EnumList(typeof(SensitivityOptions))]
        public SensitivityOptions Sensitivity
        {
            get
            {
                return (String.IsNullOrEmpty(config[5])) ? SensitivityOptions.High : (SensitivityOptions)Enum.Parse(typeof(SensitivityOptions), config[5]);
            }
            set { config[5] = value.ToString(); }
        }

        [JsonIgnore]
        [DisplayName("Instant Warning Alarm")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string InstantWarningAlarm { get { return config[6]; } set { config[6] = value; } }
        [JsonIgnore]
        [DisplayName("Instant Limit Alarm")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string InstantLimitAlarm { get { return config[7]; } set { config[7] = value; } }
        [JsonIgnore]
        [DisplayName("Averaging Time")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int AveragingTime
        {
            get { int d; return int.TryParse(config[8], out d) ? d : 0; }
            set { config[8] = value.ToString(); }
        }
        [JsonIgnore]
        [DisplayName("Average Warning Alarm")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string AverageWarningAlarm { get { return config[9]; } set { config[9] = value; } }
        [JsonIgnore]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        [DisplayName("Average Limit Alarm")]
        [Category("Config")]
        public string AverageLimitAlarm { get { return config[10]; } set { config[10] = value; } }
        [JsonIgnore]
        [DisplayName("Alarm Delay")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int AlarmDelay
        {
            get { int d; return int.TryParse(config[11], out d) ? d : 0; }
            set { config[11] = value.ToString(); }
        }


        [JsonIgnore]
        [DisplayName("Self Tests Active")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(YesNoOptions.Yes)]
        [EnumList(typeof(YesNoOptions))]
        public YesNoOptions SelfTestsActive
        {
            get
            {
                return (String.IsNullOrEmpty(config[12])) ? YesNoOptions.No : (YesNoOptions)Enum.Parse(typeof(YesNoOptions), config[12]);
            }
            set { config[12] = value.ToString(); }
        }

        [JsonIgnore]
        [DisplayName("Zero Span Alarm")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(YesNoOptions.No)]
        [EnumList(typeof(YesNoOptions))]
        public YesNoOptions ZeroSpanAlarm
        {
            get
            {
                return (String.IsNullOrEmpty(config[13])) ? YesNoOptions.No : (YesNoOptions)Enum.Parse(typeof(YesNoOptions), config[13]);
            }
            set { config[13] = value.ToString(); }
        }

        [JsonIgnore]
        [DisplayName("Short Circuit Alarm")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(YesNoOptions.No)]
        [EnumList(typeof(YesNoOptions))]
        public YesNoOptions ShortCircuitAlarm
        {
            get
            {
                return (String.IsNullOrEmpty(config[14])) ? YesNoOptions.No : (YesNoOptions)Enum.Parse(typeof(YesNoOptions), config[14]);
            }
            set { config[14] = value.ToString(); }
        }

        [JsonIgnore]
        [DisplayName("Checks On 4-20mA")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(YesNoOptions.No)]
        [EnumList(typeof(YesNoOptions))]
        public YesNoOptions ChecksOn4_20mA
        {
            get
            {
                return (String.IsNullOrEmpty(config[15])) ? YesNoOptions.No : (YesNoOptions)Enum.Parse(typeof(YesNoOptions), config[15]);
            }
            set { config[15] = value.ToString(); }
        }

        [JsonIgnore]
        [DisplayName("Poll Rate")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int PollRate
        {
            get { int d; return int.TryParse(config[16], out d) ? d : 0; }
            set { config[16] = value.ToString(); }
        }
        [JsonIgnore]
        [DisplayName("Log Options")]
        [Category("Config")]
        [Editor(typeof(LogOptionsPEEditor), typeof(UITypeEditor))]
        public string LogOptions { get { return config[17]; } set { config[17] = value; } }
        [JsonIgnore]
        [DisplayName("Plant Run Input")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int PlantRunInput
        {
            get { int d; return int.TryParse(config[18], out d) ? d : 0; }
            set { config[18] = value.ToString(); }
        }
        [JsonIgnore]
        [DisplayName("Clip Level")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string ClipLevel { get { return config[19]; } set { config[19] = value; } }

        [JsonIgnore]
        [DisplayName("Enabled")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(EnabledOptions.Enabled)]
        [EnumList(typeof(EnabledOptions))]
        public EnabledOptions Enabled
        {
            get
            {
                return
    (String.IsNullOrEmpty(config[20])) ? EnabledOptions.Enabled :
    (EnabledOptions)Enum.Parse(typeof(EnabledOptions), config[20]);
            }
            set { config[20] = value.ToString(); }
        }

    }
}