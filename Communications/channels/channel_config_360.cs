﻿using CustomControls.ComboBox;
using CustomControls.Data;
using CustomControls.Forms;
using CustomControls.Rule;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Communications
{
    public class channel_config_360 : channel_config
    {
        public channel_config_360() : base(29)
        {
            UnitName = "units";
            CalibrationFactor = "1";
            NegativeOffset = "0";
            InstantWarningAlarm = "0";
            InstantLimitAlarm = "0";
            AveragingTime = 60;
            AverageWarningAlarm = "0";
            AverageLimitAlarm = "0";
            AlarmDelay = 5;
            Local4_20mASpan = "100";
            Local4_20mAFilter = 10;
            SelfTestsActive = YesNoOptions.Yes;
            ZeroSpanAlarm = YesNoOptions.Yes;
            DISPFILT = 5;
            DIGI = DIGIOptions.MSC;
            SHEN = SHENOptions.DSBL;
            SHUT = SHUTOptions.OPEN;
            LASR = OnOffOptions.ON;
            AUD = OnOffOptions.OFF;
            PollRate = 1000;
            LogOptions = "P";
            PlantRunInput = 0;
            ClipLevel = "1000";
            Enabled = EnabledOptions.Enabled;
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(3)]
        [Interfaces.DataType(23)]
        [DisplayName("Units Name")]
        [Category("Config")]
        [LengthRule(0, 18)]
        public string UnitName { get { return config[3]; } set { config[3] = value; } }

        [JsonIgnore]
        [Interfaces.DisplayOrder(4)]
        [Interfaces.DataType(2)]
        [DisplayName("Calibration Factor")]
        [Description("Calibration Factor (4 decimal places)")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string CalibrationFactor { get { return config[4]; } set { config[4] = value; } }

        [JsonIgnore]
        [Interfaces.DisplayOrder(5)]
        [Interfaces.DataType(2)]
        [DisplayName("Negative Offset")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string NegativeOffset { get { return config[5]; } set { config[5] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(6)]
        [Interfaces.DataType(2)]
        [DisplayName("Instant Warning Alarm")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string InstantWarningAlarm { get { return config[6]; } set { config[6] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(7)]
        [Interfaces.DataType(2)]
        [DisplayName("Instant Limit Alarm")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string InstantLimitAlarm { get { return config[7]; } set { config[7] = value; } }

        [JsonIgnore]
        [Interfaces.DisplayOrder(8)]
        [Interfaces.DataType(0)]
        [DisplayName("Averaging Time")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int AveragingTime
        {
            get { int d; return int.TryParse(config[8], out d) ? d : 0; }
            set { config[8] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(9)]
        [Interfaces.DataType(2)]
        [DisplayName("Average Warning Alarm")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string AverageWarningAlarm { get { return config[9]; } set { config[9] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(10)]
        [Interfaces.DataType(2)]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        [DisplayName("Average Limit Alarm")]
        [Category("Config")]
        public string AverageLimitAlarm { get { return config[10]; } set { config[10] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(11)]
        [Interfaces.DataType(0)]
        [DisplayName("Alarm Delay")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int AlarmDelay
        {
            get { int d; return int.TryParse(config[11], out d) ? d : 0; }
            set { config[11] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(12)]
        [Interfaces.DataType(2)]
        [DisplayName("Local 4-20mA Span")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string Local4_20mASpan { get { return config[12]; } set { config[12] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(13)]
        [Interfaces.DataType(0)]
        [DisplayName("Local 4-20mA Filter")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int Local4_20mAFilter
        {
            get { int d; return int.TryParse(config[13], out d) ? d : 0; }
            set { config[13] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(14)]
        [Interfaces.DataType(3)]
        [DisplayName("Self Tests Active")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(YesNoOptions.Yes)]
        [EnumList(typeof(YesNoOptions))]
        public YesNoOptions SelfTestsActive
        {
            get
            {
                return (String.IsNullOrEmpty(config[14])) ? YesNoOptions.No : (YesNoOptions)Enum.Parse(typeof(YesNoOptions), config[14]);
            }
            set { config[14] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(15)]
        [Interfaces.DataType(3)]
        [DisplayName("Zero Span Alarm")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(YesNoOptions.No)]
        [EnumList(typeof(YesNoOptions))]
        public YesNoOptions ZeroSpanAlarm
        {
            get
            {
                return (String.IsNullOrEmpty(config[15])) ? YesNoOptions.No : (YesNoOptions)Enum.Parse(typeof(YesNoOptions), config[15]);
            }
            set { config[15] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(16)]
        [Interfaces.DataType(0)]
        [DisplayName("DISP FILT")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int DISPFILT
        {
            get { int d; return int.TryParse(config[16], out d) ? d : 0; }
            set { config[16] = value.ToString(); }
        }


        [JsonIgnore]
        [Interfaces.DisplayOrder(17)]
        [Interfaces.DataType(19)]
        [DisplayName("DIGI")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(DIGIOptions.MSC)]
        [EnumList(typeof(DIGIOptions))]
        public DIGIOptions DIGI
        {
            get
            {
                return (String.IsNullOrEmpty(config[17])) ? DIGIOptions.MSC : (DIGIOptions)Enum.Parse(typeof(DIGIOptions), config[17]);
            }
            set { config[17] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(18)]
        [Interfaces.DataType(20)]
        [DisplayName("SH.EN")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(SHENOptions.DSBL)]
        [EnumList(typeof(SHENOptions))]
        public SHENOptions SHEN
        {
            get
            {
                return (String.IsNullOrEmpty(config[18])) ? SHENOptions.DSBL : (SHENOptions)Enum.Parse(typeof(SHENOptions), config[18]);
            }
            set { config[18] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(19)]
        [Interfaces.DataType(21)]
        [DisplayName("SHUT")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(SHUTOptions.OPEN)]
        [EnumList(typeof(SHUTOptions))]
        public SHUTOptions SHUT
        {
            get
            {
                return (String.IsNullOrEmpty(config[19])) ? SHUTOptions.OPEN : (SHUTOptions)Enum.Parse(typeof(SHUTOptions), config[19]);
            }
            set { config[19] = value.ToString(); }
        }


        [JsonIgnore]
        [Interfaces.DisplayOrder(20)]
        [Interfaces.DataType(22)]
        [DisplayName("LASR")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(OnOffOptions.ON)]
        [EnumList(typeof(OnOffOptions))]
        public OnOffOptions LASR
        {
            get
            {
                return (String.IsNullOrEmpty(config[20])) ? OnOffOptions.ON : (OnOffOptions)Enum.Parse(typeof(OnOffOptions), config[20]);
            }
            set { config[20] = value.ToString(); }
        }


        [JsonIgnore]
        [Interfaces.DisplayOrder(21)]
        [Interfaces.DataType(22)]
        [DisplayName("AUD")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(OnOffOptions.ON)]
        [EnumList(typeof(OnOffOptions))]
        public OnOffOptions AUD
        {
            get
            {
                return (String.IsNullOrEmpty(config[21])) ? OnOffOptions.ON : (OnOffOptions)Enum.Parse(typeof(OnOffOptions), config[21]);
            }
            set { config[21] = value.ToString(); }
        }



        [JsonIgnore]
        [Interfaces.DisplayOrder(22)]
        [Interfaces.DataType(22)]
        [DisplayName("Use Scaled Span")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(OnOffOptions.ON)]
        [EnumList(typeof(OnOffOptions))]
        public OnOffOptions UseScaledSpan
        {
            get
            {
                return (String.IsNullOrEmpty(config[22])) ? OnOffOptions.ON : (OnOffOptions)Enum.Parse(typeof(OnOffOptions), config[22]);
            }
            set { config[22] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(23)]
        [Interfaces.DataType(1)]
        [DisplayName("Scaled Span Value")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int ScaledSpanValue
        {
            get { int d; return int.TryParse(config[23], out d) ? d : 0; }
            set { config[23] = value.ToString(); }
        }


        [JsonIgnore]
        [Interfaces.DisplayOrder(24)]
        [Interfaces.DataType(1)]
        [DisplayName("Poll Rate")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int PollRate
        {
            get { int d; return int.TryParse(config[24], out d) ? d : 0; }
            set { config[24] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(25)]
        [Interfaces.DataType(30)]
        [DisplayName("Log Options")]
        [Category("Config")]
        [Editor(typeof(LogOptionsEditor), typeof(UITypeEditor))]
        public string LogOptions { get { return config[25]; } 
            set { config[25] = value; } }

        [JsonIgnore]
        [Interfaces.DisplayOrder(26)]
        [Interfaces.DataType(0)]
        [DisplayName("Plant Run Input")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int PlantRunInput
        {
            get { int d; return int.TryParse(config[26], out d) ? d : 0; }
            set { config[26] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(27)]
        [Interfaces.DataType(2)]
        [DisplayName("Clip Level")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string ClipLevel { get { return config[27]; } set { config[27] = value; } }

        [JsonIgnore]
        [Interfaces.DisplayOrder(28)]
        [Interfaces.DataType(9)]
        [DisplayName("Enabled")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(EnabledOptions.Enabled)]
        [EnumList(typeof(EnabledOptions))]
        public EnabledOptions Enabled
        {
            get
            {
                return
                    (String.IsNullOrEmpty(config[28])) ? EnabledOptions.Enabled :
                    (EnabledOptions)Enum.Parse(typeof(EnabledOptions), config[28]);
            }
            set { config[28] = value.ToString(); }
        }
    }
}
