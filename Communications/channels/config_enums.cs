﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Communications
{
    class config_enums
    {
    }

    public enum DIGIOptions
    {
        MSC,
        PSOF,
        PSZE,
        PSDU
    }

    public enum SHENOptions
    {
        DSBL,
        ENBL,
        NOFL
    }

    public enum SHUTOptions
    {
        OPEN,
        BLOC
    }

    public enum OnOffOptions
    {
        ON,
        OFF
    }

    public enum PS11CalMethodOptions
    {
        Linear,
        Polynomial,
        Logarithmic,
        Exponential,
        Power
    }

    public enum EnabledOptions
    {
        Enabled,
        Maintenance,
        Disabled
    }

    public enum CalibrationTypeOptions
    {
        Uncalibrated,
        Qualitive,
        Quantative,
        Normalised
    }



    public enum YesNoOptions
    {
        Yes,
        No
    }

    public enum LocalRelayOptions
    {
        Warning,
        Limit
    }

    public enum LocalRelaySourceOptions
    {
        None,
        Inst,
        Ave,
        Both
    }

    public enum SensitivityOptions
    {
        High,
        Medium,
        Low
    }

    public enum FlowDirectionOptions
    {
        Upstream,
        Downstream
    }

    public enum RelayFlowOptions
    {
        [Description("Relay Off")]
        RelayOff,
        [Description("High Flow")]
        HighFlow,
        [Description("Low Flow")]
        LowFlow,
        [Description("Flow Alarm")]
        FlowAlarm,
        [Description("Fault Alarm")]
        FaultAlarm,
        [Description("All Alarms")]
        AllAlarms,
        [Description("Test Running")]
        TestRunning
    }

    public enum DisplayUnitsOptions
    {
        [Description("Velocity (m/s)")]
        Velocity,
        [Description("Flow (m3/min)")]
        Flowm3pm,
        [Description("Flow (km3/min)")]
        Flowkm3pm,
        [Description("Flow (m3/hr)")]
        Flowm3phr,
        [Description("Flow (km3/hr)")]
        Flowmkm3phr,
        [Description("Flow (CFM)")]
        FlowCFM,
        [Description("Flow (KCFM)")]
        FlowKCFM,
        [Description("Flow (CFH)")]
        FlowCFH,
        [Description("Flow (KCFH)")]
        FlowKCFH


    }

    public enum ROMRelaySources
    {
        [Description("None Selected")]
        NoneSelected,
        [Description("Tests 991 Test")]
        Tests991Test,
        [Description("Tests 181 Test")]
        Tests181Test,
        [Description("Tests 260 Test")]
        Tests260Test,
        [Description("Tests 360 Test")]
        Tests360Test,
        [Description("Tests 990 Test")]
        Tests990Test,
        [Description("Tests 990X Test")]
        Tests990XTest,
        [Description("Tests 602 Dust Test")]
        Tests602DustTest,
        [Description("Tests 602 Opacity Test")]
        Tests602OpaciyTest,
        [Description("Tests 710 Opacity Test")]
        Tests710OpacityTest,
        [Description("Tests 710 PM Dust Test")]
        Tests710PMDustTest,
        [Description("Tests 370 Test")]
        Tests370Test,
        [Description("Tests 373 Test")]
        Tests373Test,
        [Description("Tests 800 Test")]
        Tests800Test,
        [Description("Tests CNX800 Test")]
        TestsCNX800Test,
        [Description("Tests 660 Test")]
        Tests660Test,
        [Description("Tests 550 Test")]
        Tests550Test,
        [Description("Tests 220 Test")]
        Tests220Test,
        [Description("Tests 320 Test")]
        Tests320Test,
        [Description("Tests AIM Test")]
        TestsAIMTest,
        [Description("Output ROM Test")]
        OutputROMTest
    }

    public enum ROMAlarmTypes
    {
        [Description("Warning Alarm")]
        WarningAlarm,
        [Description("Limit Alarm")]
        LimitAlarm,
        [Description("Comms Error")]
        CommsError,
        [Description("Self Test Fail")]
        SelfTestFail,
        [Description("In Cal")]
        InCal,
        [Description("Zero Measure")]
        ZeroMeasure,
        [Description("Upscale Measure")]
        UpscaleMeasure,
        [Description("Test Running")]
        TestRunning,
        [Description("Maintanence")]
        Maintanence,
        [Description("Zero Span Fail")]
        ZeroSpanFail,
        [Description("Contam Fail")]
        ContamFail,
        [Description("Low Flow")]
        LowFlow,
        [Description("All Alarms")]
        AllAlarms
    }



    public enum AOMRelaySources
    {
        [Description("None Selected")]
        NoneSelected,
        [Description("Tests 991 Test")]
        Tests991Test,
        [Description("Tests 181 Test")]
        Tests181Test,
        [Description("Tests 260 Test")]
        Tests260Test,
        [Description("Tests 360 Test")]
        Tests360Test,
        [Description("Tests 990 Test")]
        Tests990Test,
        [Description("Tests 990X Test")]
        Tests990XTest,
        [Description("Tests 602 Dust Test")]
        Tests602DustTest,
        [Description("Tests 602 Opacity Test")]
        Tests602OpaciyTest,
        [Description("Tests 710 Opacity Test")]
        Tests710OpacityTest,
        [Description("Tests 710 PM Dust Test")]
        Tests710PMDustTest,
        [Description("Tests 370 Test")]
        Tests370Test,
        [Description("Tests 373 Test")]
        Tests373Test,
        [Description("Tests 800 Test")]
        Tests800Test,
        [Description("Tests CNX800 Test")]
        TestsCNX800Test,
        [Description("Tests 660 Test")]
        Tests660Test,
        [Description("Tests 550 Test")]
        Tests550Test,
        [Description("Tests AIM Test")]
        TestsAIMTest,
    }

    public enum OTAlarmOptions
    {
        Yes,
        Normalised
    }

    public enum SamplingModeOptions
    {
        [Description("Disabled")]
        Disabled,
        [Description("Manual")]
        Manual,
        [Description("External")]
        External,
        [Description("Fixed Pump")]
        FixedPump,
        [Description("Fixed Velocity")]
        FixedVelocity,
    }

    public enum TemperatureChannelOptions
    {
        [Description("None Selected")]
        NoneSelected,
        [Description("Tests 991 Test")]
        Tests991Test,
        [Description("Tests 181 Test")]
        Tests181Test,
        [Description("Tests 260 Test")]
        Tests260Test,
        [Description("Tests 360 Test")]
        Tests360Test,
        [Description("Tests 990 Test")]
        Tests990Test,
        [Description("Tests 990X Test")]
        Tests990XTest,
        [Description("Tests 602 Dust Test")]
        Tests602DustTest,
        [Description("Tests 602 Opacity Test")]
        Tests602OpaciyTest,
        [Description("Tests 710 Opacity Test")]
        Tests710OpacityTest,
        [Description("Tests 710 PM Dust Test")]
        Tests710PMDustTest,
        [Description("Tests 370 Test")]
        Tests370Test,
        [Description("Tests 373 Test")]
        Tests373Test,
        [Description("Tests 800 Test")]
        Tests800Test,
        [Description("Tests CNX800 Test")]
        TestsCNX800Test,
        [Description("Tests 660 Test")]
        Tests660Test,
        [Description("Tests 550 Test")]
        Tests550Test,
        [Description("Tests 220 Test")]
        Tests220Test,
        [Description("Tests 320 Test")]
        Tests320Test,
        [Description("Tests AIM Test")]
        TestsAIMTest,
        [Description("Output ROM Test")]
        OutputROMTest,


        [Description("Output AOM Test")]
        OutoutAOMTest,
        [Description("Tests AIM Digital Test")]
        TestsAIMDigitialTest,
        [Description("Tests Filter Test")]
        TestsFilterTest,
        [Description("Tests Fixed Value Test")]
        TestsFixedValueTest,
        [Description("Tests Mass Test")]
        TestsMassTest,
        [Description("Tests Sum Test")]
        TestsSumTest,
        [Description("Tests Dust@ STP 02 Test")]
        TestsDustAtSTPTest,
        [Description("Tests Totaliser Test")]
        TestsTotaliserTest,
        [Description("Tests Averager Test")]
        TestsAveragerTest,
        [Description("Tests Change Rate Test")]
        TestsChangeRateTest,
        [Description("Tests Min Max Test")]
        TestsMinMaxTest,
        [Description("Tests 991 QAL3 Zero Test")]
        Tests991QAL3ZeroTest,
        [Description("Tests 991 QAL3 Span Test")]
        Tests991QAL3SpanTest,
        [Description("Tests 991 QAL3Sc Test")]
        Tests991QAL3ScTest,
        [Description("Tests 990 Contam Ring Test")]
        Tests990ContamRingTest,
        [Description("Tests Modbus Input Test")]
        TestsModbusInputTest,
        [Description("Tests Modbus Read Test")]
        TestsModbusReadTest,
        [Description("Tests Modbus Write Test")]
        TestsModbusWriteTest,
        [Description("Tests Wetsack Test")]
        TestsWetsackTest


    }

    public enum ModbusRegisterTypes
    {
        [Description("Short")]
        Short,
        [Description("Long")]
        Long,
        [Description("Long Swap")]
        LongSwap,
        [Description("Float")]
        Float,
        [Description("Float Swap")]
        FloatSwap
    }

    public enum MassChannelOptions
    {
        [Description("None Selected")]
        NoneSelected,
        [Description("Tests 991 Test")]
        Tests991Test,
        [Description("Tests 181 Test")]
        Tests181Test,
        [Description("Tests 260 Test")]
        Tests260Test,
        [Description("Tests 360 Test")]
        Tests360Test,
        [Description("Tests 990 Test")]
        Tests990Test,
        [Description("Tests 990X Test")]
        Tests990XTest,
        [Description("Tests 602 Dust Test")]
        Tests602DustTest,
        [Description("Tests 602 Opacity Test")]
        Tests602OpaciyTest,
        [Description("Tests 710 Opacity Test")]
        Tests710OpacityTest,
        [Description("Tests 710 PM Dust Test")]
        Tests710PMDustTest,
        [Description("Tests 370 Test")]
        Tests370Test,
        [Description("Tests 373 Test")]
        Tests373Test,
        [Description("Tests 800 Test")]
        Tests800Test,
        [Description("Tests CNX800 Test")]
        TestsCNX800Test,
        [Description("Tests 660 Test")]
        Tests660Test,
        [Description("Tests 550 Test")]
        Tests550Test,
        [Description("Tests 220 Test")]
        Tests220Test,
        [Description("Tests 320 Test")]
        Tests320Test,
        [Description("Tests AIM Test")]
        TestsAIMTest,
        [Description("Output ROM Test")]
        OutputROMTest,


        [Description("Output AOM Test")]
        OutoutAOMTest,
        [Description("Tests AIM Digital Test")]
        TestsAIMDigitialTest,
        [Description("Tests Filter Test")]
        TestsFilterTest,
        [Description("Tests Fixed Value Test")]
        TestsFixedValueTest,
        [Description("Tests Mass Test")]
        TestsMassTest,
    }

    public enum DustSTPInputOptions
    {
        [Description("None Selected")]
        NoneSelected,
        [Description("Tests 991 Test")]
        Tests991Test,
        [Description("Tests 181 Test")]
        Tests181Test,
        [Description("Tests 260 Test")]
        Tests260Test,
        [Description("Tests 360 Test")]
        Tests360Test,
        [Description("Tests 990 Test")]
        Tests990Test,
        [Description("Tests 990X Test")]
        Tests990XTest,
        [Description("Tests 602 Dust Test")]
        Tests602DustTest,
        [Description("Tests 602 Opacity Test")]
        Tests602OpaciyTest,
        [Description("Tests 710 Opacity Test")]
        Tests710OpacityTest,
        [Description("Tests 710 PM Dust Test")]
        Tests710PMDustTest,
        [Description("Tests 370 Test")]
        Tests370Test,
        [Description("Tests 373 Test")]
        Tests373Test,
        [Description("Tests 800 Test")]
        Tests800Test,
        [Description("Tests CNX800 Test")]
        TestsCNX800Test,
        [Description("Tests 660 Test")]
        Tests660Test,
        [Description("Tests 550 Test")]
        Tests550Test,
        [Description("Tests 220 Test")]
        Tests220Test,
        [Description("Tests 320 Test")]
        Tests320Test,
        [Description("Tests AIM Test")]
        TestsAIMTest,
        [Description("Output ROM Test")]
        OutputROMTest,


        [Description("Output AOM Test")]
        OutoutAOMTest,
        [Description("Tests AIM Digital Test")]
        TestsAIMDigitialTest,
        [Description("Tests Filter Test")]
        TestsFilterTest,
        [Description("Tests Fixed Value Test")]
        TestsFixedValueTest,
        [Description("Tests Mass Test")]
        TestsMassTest,
        [Description("Tests Sum Test")]
        TestsSumTest,
        [Description("Tests Dust @ STP 02 Test")]
        TestsSustSTPTest

    }


    public enum TotaliserInputOptions
    {
        [Description("None Selected")]
        NoneSelected,
        [Description("Tests 991 Test")]
        Tests991Test,
        [Description("Tests 181 Test")]
        Tests181Test,
        [Description("Tests 260 Test")]
        Tests260Test,
        [Description("Tests 360 Test")]
        Tests360Test,
        [Description("Tests 990 Test")]
        Tests990Test,
        [Description("Tests 990X Test")]
        Tests990XTest,
        [Description("Tests 602 Dust Test")]
        Tests602DustTest,
        [Description("Tests 602 Opacity Test")]
        Tests602OpaciyTest,
        [Description("Tests 710 Opacity Test")]
        Tests710OpacityTest,
        [Description("Tests 710 PM Dust Test")]
        Tests710PMDustTest,
        [Description("Tests 370 Test")]
        Tests370Test,
        [Description("Tests 373 Test")]
        Tests373Test,
        [Description("Tests 800 Test")]
        Tests800Test,
        [Description("Tests CNX800 Test")]
        TestsCNX800Test,
        [Description("Tests 660 Test")]
        Tests660Test,
        [Description("Tests 550 Test")]
        Tests550Test,
        [Description("Tests 220 Test")]
        Tests220Test,
        [Description("Tests 320 Test")]
        Tests320Test,
        [Description("Tests AIM Test")]
        TestsAIMTest,
        [Description("Output ROM Test")]
        OutputROMTest,


        [Description("Output AOM Test")]
        OutoutAOMTest,
        [Description("Tests AIM Digital Test")]
        TestsAIMDigitialTest,
        [Description("Tests Filter Test")]
        TestsFilterTest,
        [Description("Tests Fixed Value Test")]
        TestsFixedValueTest,
        [Description("Tests Mass Test")]
        TestsMassTest,
        [Description("Tests Sum Test")]
        TestsSumTest,
        [Description("Tests Dust @ STP 02 Test")]
        TestsSustSTPTest,
        [Description("Tests Totaliser Test")]
        TestsTotaliserTest,
    }


    public enum FilterInputOptions
    {
        [Description("None Selected")]
        NoneSelected,
        [Description("Tests 991 Test")]
        Tests991Test,
        [Description("Tests 181 Test")]
        Tests181Test,
        [Description("Tests 260 Test")]
        Tests260Test,
        [Description("Tests 360 Test")]
        Tests360Test,
        [Description("Tests 990 Test")]
        Tests990Test,
        [Description("Tests 990X Test")]
        Tests990XTest,
        [Description("Tests 602 Dust Test")]
        Tests602DustTest,
        [Description("Tests 602 Opacity Test")]
        Tests602OpaciyTest,
        [Description("Tests 710 Opacity Test")]
        Tests710OpacityTest,
        [Description("Tests 710 PM Dust Test")]
        Tests710PMDustTest,
        [Description("Tests 370 Test")]
        Tests370Test,
        [Description("Tests 373 Test")]
        Tests373Test,
        [Description("Tests 800 Test")]
        Tests800Test,
        [Description("Tests CNX800 Test")]
        TestsCNX800Test,
        [Description("Tests 660 Test")]
        Tests660Test,
        [Description("Tests 550 Test")]
        Tests550Test,
        [Description("Tests 220 Test")]
        Tests220Test,
        [Description("Tests 320 Test")]
        Tests320Test,
        [Description("Tests AIM Test")]
        TestsAIMTest,
        [Description("Output ROM Test")]
        OutputROMTest,


        [Description("Output AOM Test")]
        OutoutAOMTest,
        [Description("Tests AIM Digital Test")]
        TestsAIMDigitialTest,
        [Description("Tests Filter Test")]
        TestsFilterTest,
        
    }



    public enum SumInputOptions
    {
        [Description("None Selected")]
        NoneSelected,
        [Description("Tests 991 Test")]
        Tests991Test,
        [Description("Tests 181 Test")]
        Tests181Test,
        [Description("Tests 260 Test")]
        Tests260Test,
        [Description("Tests 360 Test")]
        Tests360Test,
        [Description("Tests 990 Test")]
        Tests990Test,
        [Description("Tests 990X Test")]
        Tests990XTest,
        [Description("Tests 602 Dust Test")]
        Tests602DustTest,
        [Description("Tests 602 Opacity Test")]
        Tests602OpaciyTest,
        [Description("Tests 710 Opacity Test")]
        Tests710OpacityTest,
        [Description("Tests 710 PM Dust Test")]
        Tests710PMDustTest,
        [Description("Tests 370 Test")]
        Tests370Test,
        [Description("Tests 373 Test")]
        Tests373Test,
        [Description("Tests 800 Test")]
        Tests800Test,
        [Description("Tests CNX800 Test")]
        TestsCNX800Test,
        [Description("Tests 660 Test")]
        Tests660Test,
        [Description("Tests 550 Test")]
        Tests550Test,
        [Description("Tests 220 Test")]
        Tests220Test,
        [Description("Tests 320 Test")]
        Tests320Test,
        [Description("Tests AIM Test")]
        TestsAIMTest,
        [Description("Output ROM Test")]
        OutputROMTest,


        [Description("Output AOM Test")]
        OutoutAOMTest,
        [Description("Tests AIM Digital Test")]
        TestsAIMDigitialTest,
        [Description("Tests Filter Test")]
        TestsFilterTest,
        [Description("Tests Fixed Value Test")]
        TestsFixedValueTest,
        [Description("Tests Mass Test")]
        TestsMassTest,
        [Description("Tests Sum Test")]
        TestsSumTest,

    }

    public enum AverageInputOptions
    {
        [Description("None Selected")]
        NoneSelected,
        [Description("Tests 991 Test")]
        Tests991Test,
        [Description("Tests 181 Test")]
        Tests181Test,
        [Description("Tests 260 Test")]
        Tests260Test,
        [Description("Tests 360 Test")]
        Tests360Test,
        [Description("Tests 990 Test")]
        Tests990Test,
        [Description("Tests 990X Test")]
        Tests990XTest,
        [Description("Tests 602 Dust Test")]
        Tests602DustTest,
        [Description("Tests 602 Opacity Test")]
        Tests602OpaciyTest,
        [Description("Tests 710 Opacity Test")]
        Tests710OpacityTest,
        [Description("Tests 710 PM Dust Test")]
        Tests710PMDustTest,
        [Description("Tests 370 Test")]
        Tests370Test,
        [Description("Tests 373 Test")]
        Tests373Test,
        [Description("Tests 800 Test")]
        Tests800Test,
        [Description("Tests CNX800 Test")]
        TestsCNX800Test,
        [Description("Tests 660 Test")]
        Tests660Test,
        [Description("Tests 550 Test")]
        Tests550Test,
        [Description("Tests 220 Test")]
        Tests220Test,
        [Description("Tests 320 Test")]
        Tests320Test,
        [Description("Tests AIM Test")]
        TestsAIMTest,
        [Description("Output ROM Test")]
        OutputROMTest,


        [Description("Output AOM Test")]
        OutoutAOMTest,
        [Description("Tests AIM Digital Test")]
        TestsAIMDigitialTest,
        [Description("Tests Filter Test")]
        TestsFilterTest,
        [Description("Tests Fixed Value Test")]
        TestsFixedValueTest,
        [Description("Tests Mass Test")]
        TestsMassTest,
        [Description("Tests Sum Test")]
        TestsSumTest,
        [Description("Tests Dust @ STP 02 Test")]
        TestsSustSTPTest,
        [Description("Tests Totaliser Test")]
        TestsTotaliserTest,
        [Description("Tests Averager Test")]
        TestsAveragerTest,

    }

    public enum ChangeRateInputOptions
    {
        [Description("None Selected")]
        NoneSelected,
        [Description("Tests 991 Test")]
        Tests991Test,
        [Description("Tests 181 Test")]
        Tests181Test,
        [Description("Tests 260 Test")]
        Tests260Test,
        [Description("Tests 360 Test")]
        Tests360Test,
        [Description("Tests 990 Test")]
        Tests990Test,
        [Description("Tests 990X Test")]
        Tests990XTest,
        [Description("Tests 602 Dust Test")]
        Tests602DustTest,
        [Description("Tests 602 Opacity Test")]
        Tests602OpaciyTest,
        [Description("Tests 710 Opacity Test")]
        Tests710OpacityTest,
        [Description("Tests 710 PM Dust Test")]
        Tests710PMDustTest,
        [Description("Tests 370 Test")]
        Tests370Test,
        [Description("Tests 373 Test")]
        Tests373Test,
        [Description("Tests 800 Test")]
        Tests800Test,
        [Description("Tests CNX800 Test")]
        TestsCNX800Test,
        [Description("Tests 660 Test")]
        Tests660Test,
        [Description("Tests 550 Test")]
        Tests550Test,
        [Description("Tests 220 Test")]
        Tests220Test,
        [Description("Tests 320 Test")]
        Tests320Test,
        [Description("Tests AIM Test")]
        TestsAIMTest,
        [Description("Output ROM Test")]
        OutputROMTest,


        [Description("Output AOM Test")]
        OutoutAOMTest,
        [Description("Tests AIM Digital Test")]
        TestsAIMDigitialTest,
        [Description("Tests Filter Test")]
        TestsFilterTest,
        [Description("Tests Fixed Value Test")]
        TestsFixedValueTest,
        [Description("Tests Mass Test")]
        TestsMassTest,
        [Description("Tests Sum Test")]
        TestsSumTest,
        [Description("Tests Dust @ STP 02 Test")]
        TestsSustSTPTest,
        [Description("Tests Totaliser Test")]
        TestsTotaliserTest,
        [Description("Tests Averager Test")]
        TestsAveragerTest,
        [Description("Tests Change Rate Test")]
        TestsChangeRateTest,
    }

    public enum MinMaxInputOptions
    {
        [Description("None Selected")]
        NoneSelected,
        [Description("Tests 991 Test")]
        Tests991Test,
        [Description("Tests 181 Test")]
        Tests181Test,
        [Description("Tests 260 Test")]
        Tests260Test,
        [Description("Tests 360 Test")]
        Tests360Test,
        [Description("Tests 990 Test")]
        Tests990Test,
        [Description("Tests 990X Test")]
        Tests990XTest,
        [Description("Tests 602 Dust Test")]
        Tests602DustTest,
        [Description("Tests 602 Opacity Test")]
        Tests602OpaciyTest,
        [Description("Tests 710 Opacity Test")]
        Tests710OpacityTest,
        [Description("Tests 710 PM Dust Test")]
        Tests710PMDustTest,
        [Description("Tests 370 Test")]
        Tests370Test,
        [Description("Tests 373 Test")]
        Tests373Test,
        [Description("Tests 800 Test")]
        Tests800Test,
        [Description("Tests CNX800 Test")]
        TestsCNX800Test,
        [Description("Tests 660 Test")]
        Tests660Test,
        [Description("Tests 550 Test")]
        Tests550Test,
        [Description("Tests 220 Test")]
        Tests220Test,
        [Description("Tests 320 Test")]
        Tests320Test,
        [Description("Tests AIM Test")]
        TestsAIMTest,
        [Description("Output ROM Test")]
        OutputROMTest,


        [Description("Output AOM Test")]
        OutoutAOMTest,
        [Description("Tests AIM Digital Test")]
        TestsAIMDigitialTest,
        [Description("Tests Filter Test")]
        TestsFilterTest,
        [Description("Tests Fixed Value Test")]
        TestsFixedValueTest,
        [Description("Tests Mass Test")]
        TestsMassTest,
        [Description("Tests Sum Test")]
        TestsSumTest,
        [Description("Tests Dust @ STP 02 Test")]
        TestsSustSTPTest,
        [Description("Tests Totaliser Test")]
        TestsTotaliserTest,
        [Description("Tests Averager Test")]
        TestsAveragerTest,
        [Description("Tests Change Rate Test")]
        TestsChangeRateTest,
        [Description("Tests Min Max Test")]
        TestsMinMaxTest,

    }


    public enum MaxMinOptions
    {
        Min,
        Max
    }


    public enum ContamRingInputOptions
    {
        [Description("None Selected")]
        NoneSelected,
        [Description("Tests 991 Test")]
        Tests991Test,
        [Description("Tests 181 Test")]
        Tests181Test,
        [Description("Tests 260 Test")]
        Tests260Test,
        [Description("Tests 360 Test")]
        Tests360Test,
        [Description("Tests 990 Test")]
        Tests990Test,
        [Description("Tests 990X Test")]
        Tests990XTest,
        [Description("Tests 602 Dust Test")]
        Tests602DustTest,
        [Description("Tests 602 Opacity Test")]
        Tests602OpaciyTest,
        [Description("Tests 710 Opacity Test")]
        Tests710OpacityTest,
        [Description("Tests 710 PM Dust Test")]
        Tests710PMDustTest,
        [Description("Tests 370 Test")]
        Tests370Test,
        [Description("Tests 373 Test")]
        Tests373Test,
        [Description("Tests 800 Test")]
        Tests800Test,
        [Description("Tests CNX800 Test")]
        TestsCNX800Test,
        [Description("Tests 660 Test")]
        Tests660Test,
        [Description("Tests 550 Test")]
        Tests550Test,
        [Description("Tests 220 Test")]
        Tests220Test,
        [Description("Tests 320 Test")]
        Tests320Test,
        [Description("Tests AIM Test")]
        TestsAIMTest,
        [Description("Output ROM Test")]
        OutputROMTest,


        [Description("Output AOM Test")]
        OutoutAOMTest,
        [Description("Tests AIM Digital Test")]
        TestsAIMDigitialTest,
        [Description("Tests Filter Test")]
        TestsFilterTest,
        [Description("Tests Fixed Value Test")]
        TestsFixedValueTest,
        [Description("Tests Mass Test")]
        TestsMassTest,
        [Description("Tests Sum Test")]
        TestsSumTest,
        [Description("Tests Dust @ STP 02 Test")]
        TestsSustSTPTest,
        [Description("Tests Totaliser Test")]
        TestsTotaliserTest,
        [Description("Tests Averager Test")]
        TestsAveragerTest,
        [Description("Tests Change Rate Test")]
        TestsChangeRateTest,
        [Description("Tests Min Max Test")]
        TestsMinMaxTest,
        [Description("Tests 991 QAL3 Zero Test")]
        Tests991Zero,
        [Description("Tests 991 QAL3 Span Test")]
        Tests991Span,
        [Description("Tests 991 QAL3Sc Test")]
        Tests991SC,
        [Description("Tests Contam Ring Test")]
        TestsContamRingTest,

    }

}






