﻿using CustomControls.ComboBox;
using CustomControls.Data;
using CustomControls.Forms;
using CustomControls.Rule;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomControls;

namespace Communications
{
    public class channel_config_991Zero : channel_config
    {
        public channel_config_991Zero() : base(4)
        {
            Enabled = EnabledOptions.Enabled;
        }


        [JsonIgnore]
        [Interfaces.DisplayOrder(3)]
        [Interfaces.DataType(9)]
        [DisplayName("Enabled")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(EnabledOptions.Enabled)]
        [EnumList(typeof(EnabledOptions))]
        public EnabledOptions Enabled
        {
            get
            {
                return
    (String.IsNullOrEmpty(config[3])) ? EnabledOptions.Enabled :
    (EnabledOptions)Enum.Parse(typeof(EnabledOptions), config[3]);
            }
            set { config[3] = value.ToString(); }
        }
    }

    public class channel_config_991Span : channel_config
    {
        public channel_config_991Span() : base(4)
        {
            Enabled = EnabledOptions.Enabled;
        }


        [JsonIgnore]
        [Interfaces.DisplayOrder(3)]
        [Interfaces.DataType(9)]
        [DisplayName("Enabled")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(EnabledOptions.Enabled)]
        [EnumList(typeof(EnabledOptions))]
        public EnabledOptions Enabled
        {
            get
            {
                return
    (String.IsNullOrEmpty(config[3])) ? EnabledOptions.Enabled :
    (EnabledOptions)Enum.Parse(typeof(EnabledOptions), config[3]);
            }
            set { config[3] = value.ToString(); }
        }
    }

    public class channel_config_991SC : channel_config
    {
        public channel_config_991SC() : base(4)
        {
            Enabled = EnabledOptions.Enabled;
        }


        [JsonIgnore]
        [Interfaces.DisplayOrder(3)]
        [Interfaces.DataType(9)]
        [DisplayName("Enabled")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(EnabledOptions.Enabled)]
        [EnumList(typeof(EnabledOptions))]
        public EnabledOptions Enabled
        {
            get
            {
                return
    (String.IsNullOrEmpty(config[3])) ? EnabledOptions.Enabled :
    (EnabledOptions)Enum.Parse(typeof(EnabledOptions), config[3]);
            }
            set { config[3] = value.ToString(); }
        }
    }


}
