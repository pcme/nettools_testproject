﻿using CustomControls.ComboBox;
using CustomControls.Data;
using CustomControls.Forms;
using CustomControls.Rule;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomControls;

namespace Communications
{
    public class channel_config_Averager : channel_config
    {
        public channel_config_Averager() : base(16)
        {
            InputChannel1 = AverageInputOptions.NoneSelected.GetDescription();
            PeriodDays = 0;
            PeriodHours = 0;
            PeriodMins = 0;
            PeriodSecs = 0;
            UnitName = "";
            AverageWarningAlarm = "0";
            AverageLimitAlarm = "0";
            AlarmDelay = 5;
            LogOptions = "P";
            Enabled = EnabledOptions.Enabled;

        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(3)]
        [Interfaces.DataType(25)]
        [DisplayName("Input Channel")]
        [Category("Config")]
        [Editor(typeof(EnumDescriptionGridComboBox), typeof(UITypeEditor))]
        [DefaultValue("None Selected")]
        [EnumList(typeof(AverageInputOptions))]
        public string InputChannel1
        {
            get
            {
                return
                    (String.IsNullOrEmpty(config[3])) ? AverageInputOptions.NoneSelected.GetDescription() :
                    config[3];
            }
            set { config[3] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(4)]
        [Interfaces.DataType(23)]
        [DisplayName("Units Name")]
        [Category("Config")]
        [LengthRule(0, 18)]
        public string UnitName { get { return config[4]; } set { config[4] = value; } }



        [JsonIgnore]
        [Interfaces.DisplayOrder(5)]
        [Interfaces.DataType(0)]
        [DisplayName("Ave: Period Days")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int PeriodDays
        {
            get { int d; return int.TryParse(config[5], out d) ? d : 0; }
            set { config[5] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(6)]
        [Interfaces.DataType(0)]
        [DisplayName("Ave: Period Hours")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int PeriodHours
        {
            get { int d; return int.TryParse(config[6], out d) ? d : 0; }
            set { config[6] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(7)]
        [Interfaces.DataType(0)]
        [DisplayName("Ave: Period Mins")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int PeriodMins
        {
            get { int d; return int.TryParse(config[7], out d) ? d : 0; }
            set { config[7] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(8)]
        [Interfaces.DataType(0)]
        [DisplayName("Ave: Period Secs")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int PeriodSecs
        {
            get { int d; return int.TryParse(config[8], out d) ? d : 0; }
            set { config[8] = value.ToString(); }
        }


    
        [JsonIgnore]
        [Interfaces.DisplayOrder(9)]
        [Interfaces.DataType(2)]
        [DisplayName("Average Warning Alarm")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string AverageWarningAlarm { get { return config[9]; } set { config[9] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(10)]
        [Interfaces.DataType(2)]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        [DisplayName("Average Limit Alarm")]
        [Category("Config")]
        public string AverageLimitAlarm { get { return config[10]; } set { config[10] = value; } }

        [JsonIgnore]
        [Interfaces.DisplayOrder(11)]
        [Interfaces.DataType(0)]
        [DisplayName("Alarm Delay")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int AlarmDelay
        {
            get { int d; return int.TryParse(config[11], out d) ? d : 0; }
            set { config[11] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(12)]
        [Interfaces.DataType(30)]
        [DisplayName("Log Options")]
        [Category("Config")]
        [Editor(typeof(LogOptionsEditor), typeof(UITypeEditor))]
        public string LogOptions { get { return config[12]; } set { config[12] = value; } }


        [JsonIgnore]
        [Interfaces.DisplayOrder(13)]
        [Interfaces.DataType(3)]
        [DisplayName("Reset")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(YesNoOptions.No)]
        [EnumList(typeof(YesNoOptions))]
        public YesNoOptions Reset
        {
            get
            {
                return (String.IsNullOrEmpty(config[13])) ?
                    YesNoOptions.No :
                    (YesNoOptions)Enum.Parse(typeof(YesNoOptions), config[13]);
            }
            set { config[13] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(14)]
        [Interfaces.DataType(0)]
        [DisplayName("Reset Delay")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int ResetDelay
        {
            get { int d; return int.TryParse(config[14], out d) ? d : 0; }
            set { config[14] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(15)]
        [Interfaces.DataType(9)]
        [DisplayName("Enabled")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(EnabledOptions.Enabled)]
        [EnumList(typeof(EnabledOptions))]
        public EnabledOptions Enabled
        {
            get
            {
                return
    (String.IsNullOrEmpty(config[15])) ? EnabledOptions.Enabled :
    (EnabledOptions)Enum.Parse(typeof(EnabledOptions), config[15]);
            }
            set { config[15] = value.ToString(); }
        }

    }
}
