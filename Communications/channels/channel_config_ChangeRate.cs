﻿using CustomControls.ComboBox;
using CustomControls.Data;
using CustomControls.Forms;
using CustomControls.Rule;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomControls;

namespace Communications
{
    public class channel_config_ChangeRate : channel_config
    {


        public channel_config_ChangeRate() : base(23)
        {

            UnitName = "units";
            ScalingFactor = "1";
            NegativeOffset = "0";

            InstantWarningAlarm = "0";
            InstantLimitAlarm = "0";
            AveragingTime = 60;
            AverageWarningAlarm = "0";
            AverageLimitAlarm = "0";
            LowWarningAlarm = "0";
            LowLimitAlarm = "0";
            AveLowLimitAlarm = "0";
            AveLowWarningAlarm = "0";
            AlarmDelay = 5;

            ClipLevelMax = "1000";
            ClipLevelMin = "-1000";
            DustChannel = ChangeRateInputOptions.NoneSelected.GetDescription();


            PollRate = 1000;
            LogOptions = "P";
            PlantRunInput = 0;

            Enabled = EnabledOptions.Enabled;

        }


        [JsonIgnore]
        [Interfaces.DisplayOrder(3)]
        [Interfaces.DataType(25)]
        [DisplayName("Dust Channel")]
        [Category("Config")]
        [Editor(typeof(EnumDescriptionGridComboBox), typeof(UITypeEditor))]
        [DefaultValue("None Selected")]
        [EnumList(typeof(ChangeRateInputOptions))]
        public string DustChannel
        {
            get
            {
                return
                    (String.IsNullOrEmpty(config[3])) ? ChangeRateInputOptions.NoneSelected.GetDescription() :
                    config[3];
            }
            set { config[3] = value.ToString(); }
        }


        [JsonIgnore]
        [Interfaces.DisplayOrder(4)]
        [Interfaces.DataType(23)]
        [DisplayName("Units Name")]
        [Category("Config")]
        [LengthRule(0, 18)]
        public string UnitName { get { return config[4]; } set { config[4] = value; } }

        [JsonIgnore]
        [Interfaces.DisplayOrder(5)]
        [Interfaces.DataType(2)]
        [DisplayName("Scaling Factor")]

        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string ScalingFactor { get { return config[5]; } set { config[5] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(6)]
        [Interfaces.DataType(2)]
        [DisplayName("Negative Offset")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string NegativeOffset { get { return config[6]; } set { config[6] = value; } }


        [JsonIgnore]
        [Interfaces.DisplayOrder(7)]
        [Interfaces.DataType(2)]
        [DisplayName("Instant Warning Alarm")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string InstantWarningAlarm { get { return config[7]; } set { config[7] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(8)]
        [Interfaces.DataType(2)]
        [DisplayName("Instant Limit Alarm")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string InstantLimitAlarm { get { return config[8]; } set { config[8] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(9)]
        [Interfaces.DataType(0)]
        [DisplayName("Averaging Time")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int AveragingTime
        {
            get { int d; return int.TryParse(config[9], out d) ? d : 0; }
            set { config[9] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(10)]
        [Interfaces.DataType(2)]
        [DisplayName("Average Warning Alarm")]
        [Category("Config")]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        public string AverageWarningAlarm { get { return config[10]; } set { config[10] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(11)]
        [Interfaces.DataType(2)]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        [DisplayName("Average Limit Alarm")]
        [Category("Config")]
        public string AverageLimitAlarm { get { return config[11]; } set { config[11] = value; } }

        [JsonIgnore]
        [Interfaces.DisplayOrder(12)]
        [Interfaces.DataType(2)]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        [DisplayName("Low Warning Alarm")]
        [Category("Config")]
        public string LowWarningAlarm { get { return config[12]; } set { config[12] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(13)]
        [Interfaces.DataType(2)]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        [DisplayName("Low Limit Alarm")]
        [Category("Config")]
        public string LowLimitAlarm { get { return config[13]; } set { config[13] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(14)]
        [Interfaces.DataType(2)]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        [DisplayName("Average Low Warning Alarm")]
        [Category("Config")]
        public string AveLowWarningAlarm { get { return config[14]; } set { config[14] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(15)]
        [Interfaces.DataType(2)]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        [DisplayName("Average Low Limit Alarm")]
        [Category("Config")]
        public string AveLowLimitAlarm { get { return config[15]; } set { config[15] = value; } }






        [JsonIgnore]
        [Interfaces.DisplayOrder(16)]
        [Interfaces.DataType(0)]
        [DisplayName("Alarm Delay")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int AlarmDelay
        {
            get { int d; return int.TryParse(config[16], out d) ? d : 0; }
            set { config[16] = value.ToString(); }
        }


      



        [JsonIgnore]
        [Interfaces.DisplayOrder(17)]
        [Interfaces.DataType(1)]
        [DisplayName("Poll Rate")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int PollRate
        {
            get { int d; return int.TryParse(config[17], out d) ? d : 0; }
            set { config[17] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(18)]
        [Interfaces.DataType(30)]
        [DisplayName("Log Options")]
        [Category("Config")]
        [Editor(typeof(LogOptionsEditor), typeof(UITypeEditor))]
        public string LogOptions { get { return config[18]; } set { config[18] = value; } }
        [JsonIgnore]
        [Interfaces.DisplayOrder(19)]
        [Interfaces.DataType(0)]
        [DisplayName("Plant Run Input")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int PlantRunInput
        {
            get { int d; return int.TryParse(config[19], out d) ? d : 0; }
            set { config[19] = value.ToString(); }
        }

        [JsonIgnore]
        [Interfaces.DisplayOrder(20)]
        [Interfaces.DataType(2)]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        [DisplayName("Clip Level Max")]
        [Category("Config")]
        public string ClipLevelMax { get { return config[20]; } set { config[20] = value; } }

        [JsonIgnore]
        [Interfaces.DisplayOrder(21)]
        [Interfaces.DataType(2)]
        [PatternRule(@"^(?!0\d|$)\d*(\.\d{1,4})?$")]
        [DisplayName("Clip Level Min")]
        [Category("Config")]
        public string ClipLevelMin { get { return config[21]; } set { config[21] = value; } }



        [JsonIgnore]
        [Interfaces.DisplayOrder(22)]
        [Interfaces.DataType(9)]
        [DisplayName("Enabled")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(EnabledOptions.Enabled)]
        [EnumList(typeof(EnabledOptions))]
        public EnabledOptions Enabled
        {
            get
            {
                return
    (String.IsNullOrEmpty(config[22])) ? EnabledOptions.Enabled :
    (EnabledOptions)Enum.Parse(typeof(EnabledOptions), config[22]);
            }
            set { config[22] = value.ToString(); }
        }

    }
}
