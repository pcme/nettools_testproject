﻿using CustomControls.ComboBox;
using CustomControls.Data;
using CustomControls.Forms;
using CustomControls.Rule;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomControls;

namespace Communications
{
    class channel_config_ModbusInput : channel_config
    {
        public channel_config_ModbusInput() : base(8)
        {
            RegisterNumber = 1;
            RegisterType = ModbusRegisterTypes.Short.GetDescription();
            PollRate = 1000;
            LogOptions = "E";
            Enabled = EnabledOptions.Enabled;
        }


        [JsonIgnore]
        [Interfaces.DisplayOrder(3)]
        [Interfaces.DataType(0)]
        [DisplayName("Register Number")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int RegisterNumber
        {
            get { int d; return int.TryParse(config[3], out d) ? d : 0; }
            set { config[3] = value.ToString(); }
        }


        [JsonIgnore]
        [Interfaces.DisplayOrder(4)]
        [Interfaces.DataType(7)]
        [DisplayName("Register Type")]
        [Category("Config")]
        [Editor(typeof(EnumDescriptionGridComboBox), typeof(UITypeEditor))]
        [DefaultValue("None Selected")]
        [EnumList(typeof(ModbusRegisterTypes))]
        public string RegisterType
        {
            get
            {
                return
                    (String.IsNullOrEmpty(config[4])) ? ModbusRegisterTypes.Short.GetDescription() :
                    config[4];
            }
            set { config[4] = value.ToString(); }
        }


        [JsonIgnore]
        [Interfaces.DisplayOrder(5)]
        [Interfaces.DataType(1)]
        [DisplayName("Poll Rate")]
        [Category("Config")]
        [IntRule(0, int.MaxValue)]
        public int PollRate
        {
            get { int d; return int.TryParse(config[5], out d) ? d : 0; }
            set { config[5] = value.ToString(); }
        }
        [JsonIgnore]
        [Interfaces.DisplayOrder(6)]
        [Interfaces.DataType(30)]
        [DisplayName("Log Options")]
        [Category("Config")]
        [Editor(typeof(LogOptionsEditor), typeof(UITypeEditor))]
        public string LogOptions { get { return config[6]; } set { config[6] = value; } }
       

        [JsonIgnore]
        [Interfaces.DisplayOrder(7)]
        [Interfaces.DataType(9)]
        [DisplayName("Enabled")]
        [Category("Config")]
        [Editor(typeof(EnumGridComboBox), typeof(UITypeEditor))]
        [DefaultValue(EnabledOptions.Enabled)]
        [EnumList(typeof(EnabledOptions))]
        public EnabledOptions Enabled
        {
            get
            {
                return
    (String.IsNullOrEmpty(config[7])) ? EnabledOptions.Enabled :
    (EnabledOptions)Enum.Parse(typeof(EnabledOptions), config[7]);
            }
            set { config[7] = value.ToString(); }
        }


    }
}
