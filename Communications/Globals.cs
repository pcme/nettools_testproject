﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Communications
{
    public class Globals
    {
        public static string jwt { get; set; }

        public static string URL { get; set; }

        public static int Port { get; set; }
    }
}
