﻿using MQTTnet;
using MQTTnet.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Security.Cryptography;

using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Generators;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.OpenSsl;
using Org.BouncyCastle.Security;
using log4net;
using Interfaces;

namespace Communications
{
    public class AWSIoT
    {

        private static readonly ILog Log = LogManager.GetLogger(typeof(AWSIoT));
        IMqttClient _client;

   
        public Interfaces.IOTErrors LastError
        {
            get; set;
        }

        public async Task<bool> Connect(Interfaces.IAWSConnectDetail connector)
        {
            Log.Debug("Connect to " + Globals.URL + " for client id " + connector.ClientId);

            var broker = Globals.URL; //  @"a34ko8n2b19x21-ats.iot.eu-west-2.amazonaws.com"; //<AWS-IoT-Endpoint>           
            var port = Globals.Port;
            var clientId = connector.ClientId;

            X509Certificate caCert = null;
            X509Certificate deviceCert = null;
            try
            {

                caCert = X509Certificate.CreateFromCertFile(connector.SecurityCertificate);

                // var deviceCertPath = Path.Combine(certificatesPath, "certificate.cert.pfx");
                // var deviceCert = new X509Certificate(deviceCertPath, certPass);

                deviceCert = GetCertificate(connector);

              
            }
            catch (Exception ee)
            {
                LastError = IOTErrors.CertificateError;
                throw ee;
            }

            var options = new MqttClientOptionsBuilder()
                      .WithClientId(clientId)
                      .WithTcpServer(broker, port)
                      .WithKeepAlivePeriod(new TimeSpan(0, 0, 0, 300))
                      .WithCleanSession(true)
                      .WithTls(new MqttClientOptionsBuilderTlsParameters()
                      {
                          UseTls = true,
                          AllowUntrustedCertificates = true,
                          IgnoreCertificateChainErrors = true,
                          IgnoreCertificateRevocationErrors = true,
                          SslProtocol = System.Security.Authentication.SslProtocols.Tls12,
                          Certificates = new List<X509Certificate>
                          {
                            deviceCert, caCert
                          },
                      })
                      .Build();

            var factory = new MqttFactory();
            _client = factory.CreateMqttClient();
            await _client.ConnectAsync(options);
            Log.Debug("Connected: " + _client.IsConnected);
            if (!_client.IsConnected)
            {        
                LastError = IOTErrors.NoConnection;
            }
            else
            {
                LastError = IOTErrors.None;
            }
            return _client.IsConnected;
        }


        public async Task Publish(string topic, string data)
        {

            if (_client != null && _client.IsConnected)
            {
                Log.Debug("Publish to " + topic);

                try
                {
                    await _client.PublishStringAsync(topic, data);
                }
                catch (Exception ee)
                {
                    LastError = IOTErrors.PublishFail;
                    Log.Debug("Publish exception " + ee);
                    throw ee;
                }
              
            }
            else
                throw new Exception("Not connected");

        }

        private Task _client_ApplicationMessageReceivedAsync(MqttApplicationMessageReceivedEventArgs arg)
        {
            throw new NotImplementedException();
        }

        public async void Disconnect()
        {
            Log.Debug("Disconnect");

            if (_client != null)
            {
               // _client.ApplicationMessageReceivedAsync -= Client_ApplicationMessageReceivedAsync;
                await _client.DisconnectAsync();
            }
         
        }


        X509Certificate GetCertificate(Interfaces.IAWSConnectDetail connector)
        {
            var rsa = PrivateKeyFromPemFile(connector.KeyPath);
            var pemText = File.ReadAllText(connector.ThingCertificate);
            var bytes = Encoding.ASCII.GetBytes(pemText);
            var ClientCert = new X509Certificate2(bytes);
            ClientCert = ClientCert.CopyWithPrivateKey(rsa);
            ClientCert = new X509Certificate2(ClientCert.Export(X509ContentType.Pfx, "12345678"), "12345678");
            return ClientCert;
        }

        public static RSACryptoServiceProvider PrivateKeyFromPemFile(String filePath)
        {
            using (TextReader privateKeyTextReader = new StringReader(File.ReadAllText(filePath)))
            {
                AsymmetricCipherKeyPair readKeyPair = (AsymmetricCipherKeyPair)new PemReader(privateKeyTextReader).ReadObject();


                RsaPrivateCrtKeyParameters privateKeyParams = ((RsaPrivateCrtKeyParameters)readKeyPair.Private);
                RSACryptoServiceProvider cryptoServiceProvider = new RSACryptoServiceProvider();
                RSAParameters parms = new RSAParameters();

                parms.Modulus = privateKeyParams.Modulus.ToByteArrayUnsigned();
                parms.P = privateKeyParams.P.ToByteArrayUnsigned();
                parms.Q = privateKeyParams.Q.ToByteArrayUnsigned();
                parms.DP = privateKeyParams.DP.ToByteArrayUnsigned();
                parms.DQ = privateKeyParams.DQ.ToByteArrayUnsigned();
                parms.InverseQ = privateKeyParams.QInv.ToByteArrayUnsigned();
                parms.D = privateKeyParams.Exponent.ToByteArrayUnsigned();
                parms.Exponent = privateKeyParams.PublicExponent.ToByteArrayUnsigned();

                cryptoServiceProvider.ImportParameters(parms);

                return cryptoServiceProvider;
            }
        }

    }
}
