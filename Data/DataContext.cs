﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using log4net;

namespace Data
{
    public class DataContext : DbContext
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(DataContext));

        public DataContext()
        {
            Database.SetInitializer<DataContext>(null);

            // this.Database.Log = s => Log.Debug(s);
        }

        public DbSet<Controller> Controllers { get; set; }

        public DbSet<Group> Groups { get; set; }

        public DbSet<Channel> Channels { get; set; }

        public DbSet<Log> Logs { get; set; }

        public DbSet<ChannelConfig> ChannelConfigs { get; set; }

        public DbSet<PredictConfig> PredictConfigs { get; set; }

        public DbSet<LinkedConfig> LinkedConfigs { get; set; }

        public DbSet<PlantConfig> PlantConfigs { get; set; }


        /*
        public DbSet<TestParameters> TestParameters { get; set; }
        public DbSet<Users> Users { get; set; }

        public DbSet<Settings> Settings { get; set; }
        public DbSet<FileLocations> FileLocations { get; set; }

        public DbSet<Thresholds> Thresholds { get; set; }

        public DbSet<TestUnit> TestUnits { get; set; }

        public DbSet<TestRun> TestRuns { get; set; }

        public DbSet<TestResult> TestResults { get; set; }

        public DbSet<RunResult> RunResults { get; set; }

        */

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            // base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Channel>().HasRequired<Group>(s => s.Group).WithMany(g => g.Channels).HasForeignKey<int>(k => k.GroupId);
            modelBuilder.Entity<ChannelConfig>().HasRequired<Channel>(s => s.Channel).WithMany(g => g.Configs).HasForeignKey<int>(k => k.ChannelId);

        }
    }
}
