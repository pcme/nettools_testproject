﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using log4net;

namespace Data
{
    public class AlarmLog
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Log));
        public int Id { get; set; }
        public int ControllerId { get; set; }
        public uint Value { get; set; }
        public int ChannelId { get; set; }

        public int ChannelNumber { get; set; }
        public int SentFlags { get; set; }

        public string SerialNumber { get; set; }

        public int StartTS { get; set; }

        public int EndTS { get; set; }

        public int Status { get; set; }
        public int Active { get; set; }
        public int AlarmType { get; set; }

        public string Label { get; set; }

        private static List<AlarmLog> _logs;

        public static Mutex mutex;

        private static int _count;

        static AlarmLog()
        {
            mutex = new Mutex();
            _logs = new List<AlarmLog>();
            _count = 0;
        }


        public static bool AddLog(AlarmLog logs)
        {
            if (!mutex.WaitOne(1000))
            {
                log.Error("Failed to get mutex to AddLogs");
                return false;
            }
            try
            {

                logs.Id = _count++;
                _logs.Add(logs);
               
                return true;
            }
            finally
            {
                mutex.ReleaseMutex();
            }
        }

        public static AlarmLog GetLog(int controller_id, int channel_id, int status, string label)
        {
            return _logs.FirstOrDefault(f => f.ControllerId == controller_id && f.ChannelId == channel_id && f.Status == status && f.Label == label);
        }


        public static bool DeleteLogs(List<AlarmLog> logs)
        {
            if (logs.Count == 0)
                return true;

            if (!mutex.WaitOne(5000))
            {
                log.Error("Failed to get mutex to DeleteLogs");
                return false;
            }
            try
            {
                log.Debug("Deleting " + logs.Count + " AlarmLogs");
                foreach (var log in logs)
                    _logs.Remove(log);

                return true;
            }
            finally
            {
                mutex.ReleaseMutex();
            }
        }


        public static void MarkAsSent(List<int> ids)
        {
            if (!mutex.WaitOne(5000))
            {
                log.Error("Failed to get mutex to MarkAsSent");
                return;
            }
            try
            {
                foreach (var id in ids)
                {

                    var ll = _logs.FirstOrDefault(f => f.Id == id);
                    if (ll != null)
                    {
                        if (ll.Active == 0)
                            _logs.Remove(ll);
                        else
                            ll.SentFlags |= 1;
                    }

                }
            }
            finally
            {
                mutex.ReleaseMutex();
            }
        }


        public static List<AlarmLog> GetUnprocessedLogs(int controller_id)
        {
            if (!mutex.WaitOne(5000))
            {
                log.Error("Failed to get mutex to GetUnprocessedLogs");
                return new List<AlarmLog>();
            }
            try
            {
                return _logs.Where(f => f.ControllerId == controller_id && f.SentFlags == 0).ToList();
            }
            finally
            {
                mutex.ReleaseMutex();
            }
        }



    }
}
