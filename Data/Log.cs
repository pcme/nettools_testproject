﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using log4net;

namespace Data
{
    public class Log
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(Log));
        public int Id { get; set; }
        public int ControllerId { get; set; }
        public int ConfigId { get; set; }
        public int LogType { get; set; }
        public int Status { get; set; }
        public DateTime TimeStamp { get; set; }
        public double Value { get; set; }
        public int ChannelId { get; set; }

        public int Processed { get; set; }

        public static Mutex mutex;

        private static List<Log> _logs;

        private static int _count;
        static Log()
        {
            mutex = new Mutex();
            _logs = new List<Log>();
            _count = 0;
        }

        public static List<Log> GetUnprocessedLogs(int controller_id)
        {
            if (!mutex.WaitOne(5000))
            {
                log.Error("Failed to get mutex to GetUnprocessedLogs");
                return new List<Log>();
            }
            try
            {
                return _logs.Where(f => f.ControllerId == controller_id && f.Processed == 0).ToList();

                /*
                using (var db = new DataContext())
                {
                    return db.Logs.Where(f => f.ControllerId == controller_id && f.Processed == 0).ToList();
                }
                */
            }
            finally
            {
                mutex.ReleaseMutex();
            }
        }

        public static bool AddLogs(List<Log> logs)
        {
            if (!mutex.WaitOne(1000))
            {
                log.Error("Failed to get mutex to AddLogs");
                return false;
            }
            try
            {
                
                foreach(var l in logs)
                {
                    l.Id = _count++;
                }
                _logs.AddRange(logs);
                /*
                using (var db = new DataContext())
                {
                    db.Logs.AddRange(logs);
                    db.SaveChanges();
                }
                */
                return true;
            }
            finally
            {
                mutex.ReleaseMutex();
            }
        }

      


        public static bool DeleteLogs(List<Log> logs)
        {
            if (logs.Count == 0)
                return true;

            var ids = logs.Select<Log, int>(f => f.Id).ToList();
            var parameters = ids.Select((id, index) => new SQLiteParameter(string.Format("@p{0}", index), id));
            var parameterNames = string.Join(", ", parameters.Select(p => p.ParameterName));
            var query = string.Format("delete from Log where Id in ({0})", parameterNames);

            if (!mutex.WaitOne(5000))
            {
                log.Error("Failed to get mutex to DeleteLogs");
                return false;
            }
            try
            {
                log.Debug("Deleting " + logs.Count + " Logs");
                foreach (var log in logs)
                    _logs.Remove(log);

                /*
               
                using (var db = new DataContext())
                {
                    db.Database.ExecuteSqlCommand(query, parameters.ToArray());
                }
                */
                return true;
            }
            finally
            {
                mutex.ReleaseMutex();
            }
        }


    }
}
