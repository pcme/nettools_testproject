﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class PredictConfig
    {
        public int Id { get; set; }
        public int ControllerId { get; set; }

        public double PeakToPeak { get; set; }

        public double Frequency { get; set; }

        public int NumberOfCycles { get; set; }
        public int MissingCycle { get; set; }
        public int HighPeakCycle { get; set; }
        public double HighPeakValue { get; set; }
        public int DigitalInput { get; set; }

        public int Interval { get; set; }
      
        public int LagTime { get; set; }
        public bool Enabled { get; set; }
        public PredictConfig()
        {
            Interval = 30;
        }

        public static PredictConfig Get(int controller_id)
        {
            using (var db = new DataContext())
            {
                return db.PredictConfigs.FirstOrDefault(f => f.ControllerId == controller_id);
            }
        }

        public void Save()
        {
            using (var db = new DataContext())
            {
                if (this.Id == 0)
                {
                    db.PredictConfigs.Add(this);
                }
                else
                {
                    var c = db.PredictConfigs.FirstOrDefault(f => f.Id == this.Id);
                    c.HighPeakCycle = HighPeakCycle;
                    c.HighPeakValue = HighPeakValue;
                    c.Frequency = Frequency;
                    c.ControllerId = ControllerId;
                    c.DigitalInput = DigitalInput;
                    c.MissingCycle = MissingCycle;
                    c.NumberOfCycles = NumberOfCycles;
                    c.PeakToPeak = PeakToPeak;
                    c.Interval = Interval;
                    c.LagTime = LagTime;
                    c.Enabled = Enabled;
                }
                db.SaveChanges();
            }
        }
    }
}
