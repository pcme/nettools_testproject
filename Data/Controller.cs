﻿using Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class Controller : Interfaces.IAWSConnectDetail 
    {
        public int Id { get; set; }

        public int Number { get; set; }
        public string Name { get; set; }
        public string ControllerType { get; set; }
        public string SerialNumber { get; set; }

        public string MacAddress { get; set; }

        public string FirmwareVersion { get; set; }
        public string Date { get; set; }
        public string GitId { get; set; }
        public string ProtocolVersion { get; set; }
        public string SecurityCertificate { get; set; }

        public string KeyPath { get; set; }
        public string ThingCertificate { get; set; }

        public string ClientId { get; set; }

        // for pulse generator
        public double Frequency { get; set; }
        public double PeakToPeak { get; set; }

        // zero span config
        public double ZeroPercent { get; set; }
        public double ZeroAlarm { get; set; }
        public double SpanPercent { get; set; }
        public double SpanAlarm { get; set; }

        public int TestDuration { get; set; }

        [NotMapped]
        public int TransferId { get; set; }

        [NotMapped]
        public int LogId { get; set; }

        [NotMapped]
        private bool digital1;
        [NotMapped]
        public bool Digital1
        {
            get { return digital1; }
            set { 
                digital1 = value;
                DigitalChanged?.Invoke(0, digital1);
            }
        }

        [NotMapped]
        private bool digital2;
        [NotMapped]
        public bool Digital2
        {
            get { return digital2; }
            set
            {
                digital2 = value;
                DigitalChanged?.Invoke(1, digital2);
            }
        }

        [NotMapped]
        private bool digital3;
        [NotMapped]
        public bool Digital3
        {
            get { return digital3; }
            set
            {
                digital3 = value;
                DigitalChanged?.Invoke(2, digital3);
            }
        }

        [NotMapped]
        private bool digital4;
        [NotMapped]
        public bool Digital4
        {
            get { return digital4; }
            set
            {
                digital4 = value;
                DigitalChanged?.Invoke(3, digital4);
            }
        }

        [NotMapped]
        public bool ConfigChanged { get; set; }

        private List<LinkedConfig> _linkedConfigs;
        [NotMapped]
        public List<LinkedConfig> LinkedConfigs
        {
            get => _linkedConfigs;
            set
            {
                _linkedConfigs = value;
            }
              
        }
        public List<LinkedConfig> GetLinkedConfigs()
        {
            using (var db = new DataContext())
            {
                LinkedConfigs = db.LinkedConfigs.Where(f => f.ControllerId == this.Id).ToList();
            }
            return LinkedConfigs;
        }

        public List<LinkedConfig> GetUnsentConfigs()
        {
            List<LinkedConfig> ret = new List<LinkedConfig>();
            using (var db = new DataContext())
            {
                ret = db.LinkedConfigs.Where(f => f.ControllerId == this.Id && f.Sent == 0).ToList();
            }
            return ret;
        }

        public void MarkAllConfigsSent()
        {
            var query = string.Format("update LinkedConfig set Sent = '1' where ControllerId = {0}", this.Id);
            using (var db = new DataContext())
            {
                db.Database.ExecuteSqlCommand(query);
            }
        }

        public void UpdateConfig(string json_data, string config_hash, DateTime utc)
        {
            if (LinkedConfigs == null)
                GetLinkedConfigs();

            using (var db = new DataContext())
            {
                var config = new LinkedConfig();
                config.ConfigId = LinkedConfigs.Count + 1;
                config.UTC = utc;
                config.ControllerId = this.Id;
                config.Hash = config_hash;
                config.JSONData = json_data;
                config.Sent = 0;
                db.LinkedConfigs.Add(config);
                db.SaveChanges();
            }

            this.ConfigChanged = false;
            GetLinkedConfigs();
        }

        public void ClearAllDigitalEventHandlers()
        {
            if (DigitalChanged == null)
                return;
            foreach(Delegate d in DigitalChanged.GetInvocationList())
            {
                DigitalChanged -= (HandleDigitalChanged)d;
            }
        }

        private ICloudDataStatus _commStatus;
        [NotMapped]
        public ICloudDataStatus CommsStatus 
        {
            get => _commStatus;
            set
            {
                _commStatus = value;
                StatusChanged?.Invoke(this, _commStatus);
            }
        }



        public delegate void HandleStatusChanged(Controller c, ICloudDataStatus status);
        public event HandleStatusChanged StatusChanged;

        public delegate void HandleDigitalChanged(int channel, bool value);
        public event HandleDigitalChanged DigitalChanged;


        public static List<Controller> _controllerCache;

        
        public bool CanEnable(bool check_channels, out string why)
        {
            if (String.IsNullOrEmpty(SerialNumber))
            {
                why = "Serial number needed";
                return false;
            }
            if (String.IsNullOrEmpty(MacAddress))
            {
                why = "Mac address needed";
                return false;
            }
            if (String.IsNullOrEmpty(FirmwareVersion))
            {
                why = "Firmware version needed";
                return false;
            }
            if (String.IsNullOrEmpty(GitId))
            {
                why = "GitId needed";
                return false;
            }
            if (String.IsNullOrEmpty(Name))
            {
                why = "Name needed";
                return false;
            }
            if (String.IsNullOrEmpty(Date))
            {
                why = "Firmware date needed";
                return false;
            }
            if (String.IsNullOrEmpty(ProtocolVersion))
            {
                why = "Protocol version needed";
                return false;
            }
            if (String.IsNullOrEmpty(SecurityCertificate))
            {
                why = "Security Certificate needed";
                return false;
            }




            if (check_channels)
            {

                if (!HasChannels())
                {
                    why = "No channels added";
                    return false;
                }
            }

            why = "";
            return true;

        }


        public static Controller GetController(int id)
        {
            return _controllerCache.FirstOrDefault(f => f.Id == id);
            /*
            using (var db = new DataContext())
            {
                return db.Controllers.FirstOrDefault(f => f.Id == id);
            }
            */
        }


        public bool HasChannels()
        {
            using (var db = new DataContext())
            {
                var groups = db.Groups.Where(f => f.ControllerId == this.Id);
                foreach(var g in groups)
                {
                    var channels = db.Channels.Count(f => f.GroupId == g.Id);
                    if (channels > 0)
                        return true;
                }
            }
            return false;
        }
        public static List<Controller> GetControllers()
        {
            return _controllerCache;

            /*
            using (var db = new DataContext())
            {
                return db.Controllers.ToList();
            }
            */
        }

        public static void Update(Controller c)
        {
            using (var db = new DataContext())
            {
                var set = db.Controllers.FirstOrDefault(f => f.Id == c.Id);
                if (set == null)
                {

                }
                else
                {
                    set.MacAddress = c.MacAddress;
                    set.ControllerType = c.ControllerType;
                    set.Date = c.Date;
                    set.FirmwareVersion = c.FirmwareVersion;
                    set.GitId = c.GitId;
                    set.Name = c.Name;
                    set.ProtocolVersion = c.ProtocolVersion;
                    set.SecurityCertificate = c.SecurityCertificate;
                    set.SerialNumber = c.SerialNumber;
                    set.ThingCertificate = c.ThingCertificate;
                    set.KeyPath = c.KeyPath;
                    set.ClientId = c.ClientId;

                    set.Frequency = c.Frequency;
                    set.PeakToPeak = c.PeakToPeak;

                    set.ZeroAlarm = c.ZeroAlarm;
                    set.ZeroPercent = c.ZeroPercent;
                    set.SpanAlarm = c.SpanAlarm;
                    set.SpanPercent = c.SpanPercent;
                    set.TestDuration = c.TestDuration;

                    db.SaveChanges();
                }
            }
        }

        private static void CheckAndCreateController(DataContext db, int number)
        {


            if (null == db.Controllers.FirstOrDefault(f => f.Number == number))
            {
                Controller s = new Controller()
                {
                    Number = number,
                    Name = "TestApp" + number.ToString(),
                    ControllerType = "Netcontroller",
                    FirmwareVersion = "9.99",
                    GitId = "3d6a323",
                    Date = "2019-07-10",
                    ProtocolVersion = "1",
                    KeyPath = "",
                    ThingCertificate = "",
                    ClientId = "client" + number.ToString(),

                    SecurityCertificate = "",
                // @"eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1NjY4MDc2MzEsInRvIjo5OTk5LCJ0aGluZ19pZCI6IjA4MmMzMDgwLTZhMDUtNDZlZi04ZTRlLWE0OWM4NDY0YWU4YiJ9.nW9NkhiaIZBZk-xrvAnH-Crd-CbXYBCvmSOEIXu017TP4pgEzCeMmbYz11BovUaF_ypvLRYsU8U5vGq_kKXnhyCYmoByxGzu-SEbZ8efcV4P5QGkMGbr4B-svJ-emqplbEeuub6qFr9i0U5mOythgJm8f9g8hjTlXAIDB6KhZRl7Qeq4K9k4yYIGorith6Fqd7fFnahs1qYqCnWOga_CpnGaDaFB1qXrNGop32kCvcJt8d4qXl1P_7T36WYl1I1DT143EEKHho3pFm5ao8vUQ8FX13uXq5gx-v-7x2qycbdlHzxU15PJDlB_ZmqjMdkqq_TJr4JHB34kE_omJl8-TA",

                    Frequency = 0.1,
                    PeakToPeak = 10.0,
                    ZeroPercent = 10,
                    SpanPercent = 10,
                    ZeroAlarm = 109,
                    SpanAlarm = 10900,
                    TestDuration = 60


                };
                db.Controllers.Add(s);
            }


        }

       

        public static void CreateDefaultControllers()
        {
            using (var db = new DataContext())
            {
                for (int i = 0; i < 100; i++)
                {
                    CheckAndCreateController(db, i+1);
                }
                db.SaveChanges();


                _controllerCache = db.Controllers.ToList();
                foreach(var c in _controllerCache)
                {
                    c.TransferId = 0;
                    c.LogId = 0;
                    c.GetLinkedConfigs();
                }
            }
        }
    }
}
