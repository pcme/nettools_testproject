﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;


namespace Data
{
    public class Group
    {
        public int Id { get; set; }

        public int Number { get; set; }
        public string Name { get; set; }
        public int ControllerId { get; set; }


        public List<Channel> Channels { get; set; }


        public Group()
        {
            Channels = new List<Channel>();
        }

        public static List<Group> GetGroups(int controller_id)
        {
            using (var db = new DataContext())
            {
                // todo - load linked channels
                return db.Groups.Where(f => f.ControllerId == controller_id).ToList();
            }
        }

        public static Group GetGroup(int id)
        {
            using (var db = new DataContext())
            {
                // todo load linked channels
                return db.Groups.Include("Channels").FirstOrDefault(f => f.Id == id);
            }
        }

        public static void DeleteGroups(List<Group> groups)
        {
            using (var db = new DataContext())
            {
                // todo - remove linked channels
                foreach(var g in groups)
                {

                    var channels = db.Channels.Where(f => f.GroupId == g.Id).ToList();

                    foreach (var ch in channels)
                    {
                        db.Database.ExecuteSqlCommand("delete from ChannelConfig where ChannelId = @p0", ch.Id);
                        db.Channels.Remove(ch);
                    }
                    db.Groups.Remove(db.Groups.FirstOrDefault(f => f.Id == g.Id));
                }
              
                db.SaveChanges();
                
            }
        }


        public static void DeleteGroup(Group g)
        {
            using (var db = new DataContext())
            {
             
                var channels = db.Channels.Where(f => f.GroupId == g.Id).ToList();

                foreach (var ch in channels)
                {
                    db.Database.ExecuteSqlCommand("delete from ChannelConfig where ChannelId = @p0", ch.Id);
                    db.Channels.Remove(ch);
                }
                db.Groups.Remove(db.Groups.FirstOrDefault(f => f.Id == g.Id));
            
                db.SaveChanges();

            }
        }




        public static void AddGroup(Group g)
        {
            using (var db = new DataContext())
            {
                db.Groups.Add(g);
                db.SaveChanges();
            }
        }
    }
}
