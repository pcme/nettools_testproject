﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class PlantConfig
    {
        public int Id { get; set; }
        public int ControllerId { get; set; }

        public bool Enabled { get; set; }

        public DateTime Start { get; set; }

        public DateTime Stop { get; set; }

        public int DigitalInput { get; set; }

        public PlantConfig()
        {
            DigitalInput = 1;
            Start = new DateTime(2000, 1, 1, 9, 0, 0);
            Stop = new DateTime(2000, 1, 1, 17, 0, 0);
        }

        public static PlantConfig Get(int controller_id)
        {
            using (var db = new DataContext())
            {
                return db.PlantConfigs.FirstOrDefault(f => f.ControllerId == controller_id);
            }
        }

        public void Save()
        {
            using (var db = new DataContext())
            {
                if (this.Id == 0)
                {
                    db.PlantConfigs.Add(this);
                }
                else
                {
                    var c = db.PlantConfigs.FirstOrDefault(f => f.Id == this.Id);
                    c.Start = Start;
                    c.Stop = Stop;
                  
                    c.ControllerId = ControllerId;
                    c.DigitalInput = DigitalInput;
                  
                    c.Enabled = Enabled;
                }
                db.SaveChanges();
            }
        }
    }
}
