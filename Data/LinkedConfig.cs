﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class LinkedConfig
    {
        public int Id { get; set; }

        public int ControllerId { get; set; }

        public int ConfigId { get; set; }

        public DateTime UTC { get; set; }

        public string Hash { get; set; }

        public string JSONData { get; set; }

        public int Sent { get; set; }
    }
}
