﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class ChannelConfig
    {
        public int Id { get; set; }
        public int Index { get; set; }

       
        public string Value { get; set; }
        public int ChannelId { get; set; }

        public Channel Channel { get; set; }


        public ChannelConfig()
        {

        }


  

        public static List<ChannelConfig> GetConfigs(int channel_id)
        {
            using (var db = new DataContext())
            {
                return db.ChannelConfigs.Where(f => f.ChannelId == channel_id).OrderBy(f => f.Index).ToList();
            }
        }

        public static void AddConfigs(List<ChannelConfig> c)
        {
            using (var db = new DataContext())
            {
                db.ChannelConfigs.AddRange(c);
                db.SaveChanges();
            }
        }

        public void Update()
        {
            using (var db = new DataContext())
            {
                ChannelConfig cc;
                if (this.Id > 0)
                {
                    cc = db.ChannelConfigs.FirstOrDefault(f => f.Id == this.Id);
                    if (cc == null)
                    {
                        cc = this;
                        cc.Id = 0;
                    }
                    else
                    {
                        cc.Index = this.Index;
                        cc.Value = this.Value;
                        cc.ChannelId = this.ChannelId;
                        cc.Channel = this.Channel;
                    }
                }
                else
                {
                    cc = this;
                }
                if (cc.Id == 0)
                    db.ChannelConfigs.Add(cc);
                db.SaveChanges();
            }
        }
    }
}
