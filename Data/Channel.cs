﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class Channel
    {
        public int Id { get; set; }

        public int Number { get; set; }
        public string Name { get; set; }
        public int ChannelType { get; set; }
        public string ChannelTypeName { get; set; }

        public string ModbusAddress { get; set; }
        public int GroupId { get; set; }
        public Group Group { get; set; }

        public string AlarmMin { get; set; }
        public string AlarmMax { get; set; }

        public List<ChannelConfig> Configs { get; set; }

       
        [NotMapped]
        public bool SelfTestEnabled { get; set; }

        [NotMapped]
        public double ShortAccumulator { get; set; }

        [NotMapped]
        public double LongAccumulator { get; set; }
        [NotMapped]
        public int ShortCount { get; set; }
        [NotMapped]
        public int LongCount { get; set; }

        [NotMapped]
        public string LogOptions { get; set; }

        [NotMapped]
        public double LastValue { get; set; }

        public Channel()
        {
            Configs = new List<ChannelConfig>();
        }


 

        public static Channel GetChannel(int id)
        {
            using (var db = new DataContext())
            {
                //  load linked channels
                return db.Channels.Include("Configs").FirstOrDefault(f => f.Id == id);
            }
        }


        public static List<Channel> GetChannels(int group_id)
        {
            using (var db = new DataContext())
            {
                //  load linked channels
                return db.Channels.Include("Configs").Where(f => f.GroupId == group_id).ToList();
            }
        }

        public void Update()
        {
            using (var db = new DataContext())
            {
                var c = db.Channels.FirstOrDefault(f => f.Id == this.Id);
                if (c != null)
                {
                    c.AlarmMax = AlarmMax;
                    c.AlarmMin = AlarmMin;
                    c.Name = Name;

                    db.SaveChanges();
                }
            }
        }

        public static void AddChannel(Channel ch)
        {
            using (var db = new DataContext())
            {
                db.Channels.Add(ch);
                db.SaveChanges();
            }
        }
        public static void DeleteChannel(Channel ch)
        {
            using (var db = new DataContext())
            {
                var c = db.Channels.FirstOrDefault(f => f.Id == ch.Id);
                if (c != null)
                {
                    db.Database.ExecuteSqlCommand("delete from ChannelConfig where ChannelId = @p0", c.Id);
                    db.Channels.Remove(c);
                    db.SaveChanges();
                }
            }
        }

        public static void DeleteChannels(List<Channel> ch)
        {
            using (var db = new DataContext())
            {
                
                foreach (var g in ch)
                {
                    db.Database.ExecuteSqlCommand("delete from ChannelConfig where ChannelId = @p0", g.Id);

                    db.Channels.Remove(db.Channels.FirstOrDefault(f => f.Id == g.Id));
                }

                db.SaveChanges();

            }
        }

        public static List<Channel> GetChannelsInGroup(int c)
        {
            List<Channel> ret = new List<Channel>();
            using (var db = new DataContext())
            {
                foreach (var g in Group.GetGroups(c))
                {
                    ret.AddRange(db.Channels.Where(f => f.GroupId == g.Id).ToList());
                }
            }
            return ret;
        }
        public static int CountNonDigitalChannels(int c)
        {
            int count = 0;
            using (var db = new DataContext())
            {
                foreach(var g in Group.GetGroups(c))
                {
                    count += db.Channels.Count(f => f.GroupId == g.Id && f.Number < 60);
                }
            }
            return count;

        }

        public static List<Channel> GetDigitalChannels(int c)
        {
            List<Channel> ret = new List<Channel>();
            using (var db = new DataContext())
            {
                foreach (var g in Group.GetGroups(c))
                {
                    ret.AddRange(db.Channels.Where(f => f.GroupId == g.Id && f.Number >= 60));
                }
            }
            return ret;

        }

    }
}
