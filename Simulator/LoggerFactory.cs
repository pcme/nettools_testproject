﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using Interfaces;

namespace Simulator
{
    public class LoggerFactory
    {

        private static LoggerFactory _instance;
        public static LoggerFactory Instance { get { if (_instance == null) _instance = new LoggerFactory(); return _instance; } }

        private static readonly ILog Log = LogManager.GetLogger(typeof(LoggerFactory));


        public double NoiseAmplitude { get; set; }

        List<ILogger> loggers;
        private LoggerFactory()
        {
            loggers = new List<ILogger>();

        }

        public void CloseFactory()
        {
            Log.Debug("Log Factory closing");
            foreach(var logger in loggers)
            {
                logger.KillLogger();
            }
        }


        public ILogger GetLogger<T>(Data.Controller Controller) where T : IGenerator, new()
        {
           
            var logger = loggers.FirstOrDefault(f => f.ControllerId == Controller.Id && f.Generator.GetType() == typeof(T));

            if (logger == null)
            {
                Log.Debug("Log Factory creating new logger for " + Controller.Id + " with generator " + typeof(T).Name);

                if (typeof(T) == typeof(DigitalGenerator))
                    logger = new DigitalLogger();
                else
                    logger = new Logger();

                logger.NoiseAmplitude = NoiseAmplitude;
          
                logger.Controller = Controller;
                logger.CreateGenerator<T>();

                loggers.Add(logger);
            }

            return logger;

        }


        public bool IsControllerEnabled(Data.Controller Controller)
        {

            var active_loggers = loggers.Where(f => f.ControllerId == Controller.Id);
            foreach(var logger in active_loggers)
            {
                if (logger.Enabled)
                    return true;
            }

            if (Controller.Number > 10)
            {
                int base_number = 1 + ((Controller.Number - 1) /10) *10;

                active_loggers = loggers.Where(f => f.ControllerNumber == base_number);
                foreach (var logger in active_loggers)
                {
                    if (logger.Enabled)
                        return true;
                }
            }

            return false;
        }
      


    }
}
