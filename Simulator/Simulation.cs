﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Communications;
using Interfaces;
using log4net;

namespace Simulator
{
    public class Simulation : IStatusProvider
    {

        private static Simulation _instance;
        public static Simulation Instance { get { if (_instance == null) _instance = new Simulation(); return _instance; } }

        private static readonly ILog Log = LogManager.GetLogger(typeof(Simulation));

        private ManualResetEvent _killEvent;
        private AutoResetEvent [] _dataEvent;
        const int NumberRunners = 10;


        private Simulation()
        {
            _abort = false;
            _killEvent = new ManualResetEvent(false);
            _dataEvent = new AutoResetEvent[NumberRunners];
            for (int i = 0; i < NumberRunners; i++)
                _dataEvent[i] = new AutoResetEvent(false);
            

            Status = (ICloudConnectionStatus)(new utc_response());
            StartRunner();
            StartUploader();
        }

        bool _abort;

        public ICloudConnectionStatus Status { get; set; }

        public event HandleConnectionStatus ConnectionStatus;


        private int _seconds;
        private async void HandleTick(object sender, ProgressChangedEventArgs e)
        {
            _seconds++;


            if ((_seconds == 30) || (_seconds >= 60))
            {
                try
                {
                    Status = await utc.PingCloud();
                    ConnectionStatus?.Invoke(Status);
                }
                catch (Exception ee)
                {
                    Log.Error("Exception in HandleTick PingCloud " + ee.Message, ee);
                }
            }
            if (_seconds >= 60)
            {
                for (int i = 0; i < NumberRunners; i++)
                    _dataEvent[i].Set();
      
                _seconds = 0;
            }
        }


        private void StartRunner()
        {

            Task.Run(async () =>
            {
                Log.Debug("Starting connection watcher task");

                _seconds = 29;
                Tick t = new Tick(HandleTick);
                t.Microseconds = 1000000;
                t.Start();
                while (!_abort)
                {
                    _killEvent.WaitOne(1000);
                }
                t.Stop();


                Log.Debug("Ending connection watcher task");
            });



        }



        private void RunUploader(int index)
        {
            Task.Run(async () =>
            {
                
                int first_controller = 1 + (index * (100 / NumberRunners));
                int last_controller = first_controller + (100 / NumberRunners);
               

                Log.Debug("Starting uploader task " + index);
                while (!_abort)
                {
                    int ret = WaitHandle.WaitAny(new WaitHandle[] { _killEvent, _dataEvent[index] });
                    if (ret == 1)
                    {
                        try
                        {
                            ProcessControllers(first_controller, last_controller);
                        }
                        catch (AggregateException ae)
                        {
                            ae.Handle(ex =>
                            {
                                return true;
                            });
                            foreach (Exception eee in ae.InnerExceptions)
                            {
                                Log.Error(eee);
                            }
                        }
                        catch (Exception ee)
                        {
                            Log.Error("Exception in processcontrollers " + ee.Message, ee);
                        }
                    }
                }

                Log.Debug("Ending uploader task " + index);
            });

        }

        private void StartUploader()
        {
            for(int i=0; i<NumberRunners; i++)
            {
                RunUploader(i);
            } 
        }


        private void ProcessControllers(int start, int end)
        {
            Log.Debug("ProcessControllers");

            var controllers = Data.Controller.GetControllers().Where(f => f.Number >= start && f.Number < end).ToList();
            foreach (var controller in controllers)
            {
                if (LoggerFactory.Instance.IsControllerEnabled(controller))
                {
                    controller.CommsStatus = new CloudDataStatus() { TimeStamp = DateTime.Now, ControllerId = controller.Id, Response = "Sending..." };
                }
            }

            foreach (var controller in controllers)
            {
                if (LoggerFactory.Instance.IsControllerEnabled(controller))
                {
                    Log.Debug("ProcessControllers: controller " + controller.Id);

                    ProcessControllerAWS(controller);          
                }
                else
                {
                  //  Log.Debug("ProcessControllers controller " + controller.Id + " not enabled");
                }
            }
        }


        private async void ProcessControllerAWS(Data.Controller controller)
        {

            var jb = new JSONBuilder(controller);
            var logs = jb.GetLogPackets();
            var config = controller.GetUnsentConfigs();

    
            var iot = new Communications.AWSIoT();
            try
            {
               
                CloudDataStatus status = new CloudDataStatus();
                status.ControllerId = controller.Id;
                status.TimeStamp = DateTime.Now;
                status.Failed = false;
                status.Response = "Connecting...";
                controller.CommsStatus = status;
                bool connected = await iot.Connect(controller);
                if (!connected)
                {
                    status.LastError = iot.LastError;
                    status.Response = "Server not found";
                    status.Failed = true;
                }
                else
                {
                    status.TimeStamp = DateTime.Now;
                    status.Response = "Connected";
                    controller.CommsStatus = status;

                    if (config.Count > 0)
                    {
                        status.TimeStamp = DateTime.Now;
                        status.Response = "Sending " + config.Count.ToString() + " configs...";
                        Log.Debug("Sending " + config.Count.ToString() + " configs...");
                        controller.CommsStatus = status;
                        //  - send config changes
                        foreach (var c in config)
                        {
                            await iot.Publish("dcg/netcontroller/" + controller.Name + "/config", c.JSONData);
                        }

                        status.TimeStamp = DateTime.Now;
                        status.Response = config.Count.ToString() + " configs sent OK";
                        status.Failed = false;
                        status.LastError = Interfaces.IOTErrors.None;
                        controller.CommsStatus = status;

                        Log.Debug("Marking configs sent for controller " + controller.Id);
                        controller.MarkAllConfigsSent();
                    }

                    var alarm_log = jb.GetAlarmLogPacketsJSON();
                    if (alarm_log != null)
                    {
                        status.TimeStamp = DateTime.Now;
                        status.Response = "Sending alarm logs...";
                        controller.CommsStatus = status;

                        await iot.Publish("dcg/netcontroller/" + controller.Name + "/alarm", alarm_log.JSON);

                        Log.Debug("Marking sent alarm logs for controller " + controller.Id);
                        var ids = alarm_log.alarm.Select<AlarmLogEntry, int>(f => f.Id).ToList();
                        Data.AlarmLog.MarkAsSent(ids);
                    }

                    if (logs.Count > 0)
                    {
                        status.TimeStamp = DateTime.Now;
                        status.Response = "Sending " + jb.Logs.Count.ToString() + " logs...";
                        controller.CommsStatus = status;

                        foreach (var l in logs)
                        {
                            await iot.Publish("dcg/netcontroller/" + controller.Name + "/sensor/" + l.LogTypeAsString, l.JSON);
                        }

                        Log.Debug("Deleting " + jb.Logs.Count + " logs for controller " + controller.Id);
                        Data.Log.DeleteLogs(jb.Logs);

                        status.TimeStamp = DateTime.Now;
                        status.Response = jb.Logs.Count.ToString() + " logs sent OK";
                        status.Failed = false;
         
                        status.LastError = IOTErrors.None;
                        controller.CommsStatus = status;
                    }

                  

                  
                }


                status.TimeStamp = DateTime.Now;
                Log.Debug("Send status for " + status.ControllerId + " " + status.Response);

                status.TimeStamp = DateTime.Now;
                controller.CommsStatus = status;
            }
            catch (Exception ee)
            {
                Log.Error("ProcessController exception " + ee.Message, ee);
                var status = new CloudDataStatus();
                status.ControllerId = controller.Id;
                status.TimeStamp = DateTime.Now;
                status.Count = 0;
                status.Response = ee.Message;
                status.Failed = true;
                status.LastError = iot.LastError;
                controller.CommsStatus = status;
            }
            finally
            {
                iot.Disconnect();
            }

        }


        public Task SendAlarmLog(Data.Controller controller)
        {
            return Task.Run(async () =>
            {
                var jb = new JSONBuilder(controller);
                var alarm_log = jb.GetAlarmLogPacketsJSON();

                if (alarm_log != null)
                {
                    var iot = new Communications.AWSIoT();
                    try
                    {
                        Log.Debug("Sending alarm log for controller " + controller.Id);

                        bool connected = await iot.Connect(controller);

                        await iot.Publish("dcg/netcontroller/" + controller.Name + "/alarm", alarm_log.JSON);

                        Log.Debug("Marking sent alarm logs for controller " + controller.Id);
                        var ids = alarm_log.alarm.Select<AlarmLogEntry, int>(f => f.Id).ToList();
                        Data.AlarmLog.MarkAsSent(ids);
                    }
                    catch (Exception ee)
                    {
                        Log.Error("ProcessController exception " + ee.Message, ee);
                    }
                    finally
                    {
                        iot.Disconnect();
                    }
                }
            });

        }

        private  void ProcessController( Data.Controller controller)
        {
            try
            {
                var jb = new JSONBuilder(controller);
                string json = jb.GenerateJSON();

                if (controller.Number == 1)
                {
                    string path = "total_" + DateTime.Now.ToString("yyyy-MM-dd_HHmmss") + ".json";
                    File.WriteAllText(path, json);
                }

                logindata d = new logindata();
                d.credentials = controller.SecurityCertificate;

                Cloud cloud = new Cloud();
                CloudDataStatus status = new CloudDataStatus();
                status.ControllerId = controller.Id;
                status.TimeStamp = DateTime.Now;
                status.Failed = false;
                status.Response = "Login...";
                controller.CommsStatus = status;

                status = new CloudDataStatus();
                status.ControllerId = controller.Id;
                status.TimeStamp = DateTime.Now;
                status.Count = jb.Logs.Count;
                string url = Communications.Globals.URL + "/login";
                var resp = cloud.Send<loginresponse>(url, d);
                if (resp == null)
                {
                    status.Response = "Server not found";
                    status.Failed = true;
                }
                else
                {
                    if (resp.mwloginstatus)
                    {
                        Log.Debug("Sending " + jb.Logs.Count + " logs for controller " + controller.Id);
                        string nurl = Communications.Globals.URL + "/dropbox";
                        var nresp = cloud.Send<payloadresponse>(nurl, resp.jwt, json);
                        if (nresp == null)
                        {
                            status.Response = "Payload Failed";
                            status.Failed = true;

                        }
                        else
                        {
                            if (nresp.status)
                            {
                                status.Response = jb.Logs.Count.ToString() + " logs sent OK";
                                status.Failed = false;

                                Log.Debug("Deleting " + jb.Logs.Count + " logs for controller " + controller.Id);
                                Data.Log.DeleteLogs(jb.Logs);

                                Log.Debug("Marking configs sent for controller " + controller.Id);
                                controller.MarkAllConfigsSent();
                            }
                            else
                            {
                                status.Response = nresp.error;
                                status.Failed = true;
                            }
                        }

                    }
                    else
                    {

                        status.Response = "Login Failed";
                        status.Failed = true;

                    }
                }

                status.TimeStamp = DateTime.Now;
                Log.Debug("Send status for " + status.ControllerId + " " + status.Response);

                controller.CommsStatus = status;
            }
            catch (Exception ee)
            {
                Log.Error("ProcessController exception " + ee.Message, ee);
                var status = new CloudDataStatus();
                status.ControllerId = controller.Id;
                status.TimeStamp = DateTime.Now;
                status.Count = 0;
                status.Response = ee.Message;
                status.Failed = true;
                controller.CommsStatus = status;
            }
            finally
            {

            }
        
        }

        public void StopRunner()
        {
            _abort = true;
            _killEvent.Set();
        }
    }

    public class CloudDataStatus : ICloudDataStatus
    {
        public int ControllerId { get; set; }

        public string Response { get; set; }

        public bool Failed { get; set; }

        public DateTime TimeStamp { get; set; }

        public int Count { get; set; }

        public Interfaces.IOTErrors LastError { get; set; }
    }
}
