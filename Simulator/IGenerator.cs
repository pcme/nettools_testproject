﻿using Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simulator
{

    public delegate void HandleNewData(IGeneratorData data);
    public interface IGenerator
    {

        event HandleNewData NewData;
       
        double NoiseAmplitude { get; set; }

        Data.Controller Controller { get; set; }

        void UpdateSettings();

        void StopGenerator();
        void StartGenerator();

        void SetValue(object v);
    }
}
