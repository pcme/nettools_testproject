﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simulator
{

    public delegate void HandleEnabledChanged(ILogger logger);

    public interface ILogger
    {
        void KillLogger();
        int ControllerId
        {
            get;

        }

        double NoiseAmplitude { get; set; }

        int ControllerNumber { get; }

        IGenerator Generator { get; set; }

        event HandleEnabledChanged EnabledChanged;
        bool Enabled { get; set; }

        Data.Controller Controller { get; set; }

        List<Data.Controller> Controllers { get; set; }

        void CreateGenerator<T>() where T : IGenerator, new();

        void UpdateSettings();

        void SetValue(object v);

        Data.Channel GetChannel(int channel_id);
    }

}
