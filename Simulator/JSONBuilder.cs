﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using System.IO;
using System.Globalization;
using System.Reflection;

namespace Simulator
{
    public class JSONBuilder
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(JSONBuilder));

        public DateTime UTC { get; set; }

        public List<Data.Log> Logs;

        private Data.Controller controller;

        public JSONBuilder(Data.Controller c)
        {
            controller = c;
            Logs = Data.Log.GetUnprocessedLogs(controller.Id);
            Log.Debug("Got " + Logs.Count + " logs to send for controller " + controller.Id);
        }

        public JSONBuilder()
        {
            
        }


        public static int ConvertDatetimeToUnixTimeStamp(DateTime date)
        {
            var dateTimeOffset = new DateTimeOffset(date);
            var unixDateTime = dateTimeOffset.ToUnixTimeSeconds();
            return (int)unixDateTime;
        }


        public AlarmLogPacket GetAlarmLogPacketsJSON()
        {
            var alarm_logs = Data.AlarmLog.GetUnprocessedLogs(controller.Id);

            if (alarm_logs.Count == 0)
            {
                Log.Debug("No alarm logs to send");
                return null;
            }
            Log.Debug(alarm_logs.Count.ToString() + " alarm logs to send");


            AlarmLogPacket p = new AlarmLogPacket();
            p.serial_number = controller.SerialNumber;
            p.version = 2;
           
            foreach(var log in alarm_logs)
            {
                AlarmLogEntry entry = new AlarmLogEntry();
                entry.active = log.Active;
                if (entry.active == 0)
                    entry.action = "update";
                else
                    entry.action = "insert";
                if (log.ChannelNumber >= 60)
                    entry.channel_id = log.ChannelNumber;
                else
                    entry.channel_id = log.ChannelNumber - 1;
                entry.status = log.Status;
                entry.data = log.Value;
                entry.end_ts = log.EndTS;
                entry.start_ts = log.StartTS;
                entry.type = log.AlarmType;
                entry.Id = log.Id;

                p.AddEntry(entry);
            }

           

            var settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.Converters.Add(new Communications.DecimalFormatConvertor());
            settings.Formatting = Formatting.Indented;
            p.JSON = JsonConvert.SerializeObject(p, settings);
            return p;

        }

        public List<SensorLogPacket> GetLogPackets()
        {
            List<SensorLogPacket> ret = new List<SensorLogPacket>();

            NumberFormatInfo nfi = new NumberFormatInfo();
            nfi.NumberDecimalSeparator = ".";
            nfi.NumberDecimalDigits = 2;

            foreach (var l in Logs)
            {
                var packet = ret.FirstOrDefault(f => f.ChannelId == l.ChannelId && f.LogType == l.LogType);
                if (packet == null)
                {
                    packet = new SensorLogPacket() { ChannelId = l.ChannelId, LogType = l.LogType };

                    packet.Logs = new BaseLog();
                    switch (l.LogType)
                    {
                        case 1:
                            packet.LogTypeAsString = "pulse";
                            packet.Logs = new PulseLog();
                            break;
                        case 2:
                            packet.LogTypeAsString = "shortterm";
                            packet.Logs = new ShortLog();
                            break;
                        case 3:
                            packet.LogTypeAsString = "longterm";
                            packet.Logs = new LongLog();
                            break;
                        default:
                            break;
                    }
                    packet.Logs.channel_id = l.ChannelId.ToString();
                    packet.Logs.version = 2;
                    packet.Logs.serial_number = controller.SerialNumber;
                    ret.Add(packet);
                }

                packet.Logs.AddEntry(new SensorLogEntry() { status = 0, timestamp = ConvertDatetimeToUnixTimeStamp(l.TimeStamp), data = l.Value });
  
            }

            var settings = new JsonSerializerSettings();
            settings.Converters.Add(new Communications.DecimalFormatConvertor());
            settings.Formatting = Formatting.Indented;

            foreach (var packet in ret)
            {
                packet.JSON = JsonConvert.SerializeObject(packet.Logs, settings);
            }

            return ret;
        }


        public string GetConfig()
        {
            var configs = controller.GetUnsentConfigs();
            return "";
        }

        public string GenerateJSON()
        {
            UTC = ClockMode.GetTime; // DateTime.UtcNow;


            // var cdata = Generate_cdata(controller);

            var payload = Generate_payload(controller);


            var p = new Communications.payloaddata();


            p.payload = payload.ConvertToJson();


            if (controller.Number == 1)
            {
                string path = "payload_" + controller.Number + "_" + UTC.ToString("yyyy-MM-dd_HHmmss") + ".json";
                File.WriteAllText(path, payload.ConvertToJson(Formatting.Indented));
            }

            var settings = new JsonSerializerSettings();
            string s = JsonConvert.SerializeObject(p, Formatting.Indented, settings);

            return s;
        }



        public string GetConfigHash(Data.Controller c, DateTime utc, Data.Controller channel_base, out string hash)
        {
            UTC = utc;
            var cdata = Generate_cdata(c, channel_base);

            var settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.DefaultValueHandling = DefaultValueHandling.Ignore;
            settings.Formatting = Formatting.Indented;
            settings.Converters.Add(new Communications.ChannelTypeConvertor());
            settings.Converters.Add(new Communications.GroupsConvertor());
            settings.Converters.Add(new Communications.DecimalFormatConvertor());

            string json_data = JsonConvert.SerializeObject(cdata, settings);

            if (true) // (c.Number == 1)
            {
                string path = "config_" + c.Number + "_" + UTC.ToString("yyyy-MM-dd_HHmmss") + ".json";
                settings.Formatting = Formatting.Indented;
                File.WriteAllText(path, JsonConvert.SerializeObject(cdata, settings));
            }

            hash = cdata.hash;
            return json_data;
        }

        public string GetConfigHash(Data.Controller c, DateTime utc, out string hash)
        {
            UTC = utc;
            var cdata = Generate_cdata(c, c);

            var settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.DefaultValueHandling = DefaultValueHandling.Ignore;
            settings.Formatting = Formatting.None;

            settings.Converters.Add(new Communications.ChannelTypeConvertor()); 
            
            settings.Converters.Add(new Communications.GroupsConvertor());
            settings.Converters.Add(new Communications.DecimalFormatConvertor());

            string json_data = JsonConvert.SerializeObject(cdata, settings);

            if (c.Number == 1)
            {
                string path = "config_" + UTC.ToString("yyyy-MM-dd_HHmmss") + ".json";
                settings.Formatting = Formatting.Indented;
                File.WriteAllText(path, JsonConvert.SerializeObject(cdata, settings));
            }

            hash = cdata.hash;
            return json_data;
        }

        public Communications.ctransfer GenerateTransfer(Data.Controller controller)
        {
            Communications.ctransfer transfer = new Communications.ctransfer();
            transfer.tranfer_id = controller.TransferId.ToString();
            controller.TransferId++;
            if (controller.TransferId >= 300000)
                controller.TransferId = 0;
            transfer.uuid = Guid.NewGuid().ToString("N");   // 32 characters
            transfer.uuid += Guid.NewGuid().ToString("N").Substring(0, 8);      // get 40 character string

            transfer.utc_start = UTC.ToString("yyyy-MM-dd HH:mm:ss");
            transfer.utc_end = null;
            transfer.status = "1";

            return transfer;
        }

        public Communications.cpayload Generate_payload(Data.Controller controller)
        {
            Communications.cpayload payload = new Communications.cpayload();

            payload.data = null; // data
            payload.proc_sn = controller.SerialNumber;
            payload.transfer.Add( GenerateTransfer(controller));

            var configs = controller.GetUnsentConfigs();
            foreach (var data in configs)
            {
                var config = Generate_cconfig(controller, data);
                payload.config.Add(config);
            }

            NumberFormatInfo nfi = new NumberFormatInfo();
            nfi.NumberDecimalSeparator = ".";
            nfi.NumberDecimalDigits = 2;
            // logs
            foreach (var l in Logs)
            {
                Communications.clogs ll = new Communications.clogs();
                ll.channel_id = l.ChannelId.ToString();
                ll.config_id = l.ConfigId.ToString();
                ll.log_type = l.LogType.ToString();
                ll.status = l.Status.ToString();
                ll.ts = l.TimeStamp.ToString("yyyy-MM-dd HH:mm:ss");
                ll.value = l.Value.ToString("F2", nfi);
              
                ll.logs_id = controller.LogId.ToString();
                controller.LogId++;
                if (controller.LogId >= 300000)
                    controller.LogId = 0;

                payload.logs.Add(ll);
            }
      

            // alarms ??

            // linked configs

            foreach(var c in controller.LinkedConfigs)
            {
                var lc = new Communications.clinked_config()
                {
                    config_id = c.ConfigId.ToString(),
                    serialnumber = controller.SerialNumber,
                    utc = c.UTC.ToString("yyyy-MM-dd HH:mm:ss"),
                    hash = c.Hash
                };

                payload.linked_configs.Add(lc);
            }

         

            return payload;

        }

        public Communications.cconfig Generate_cconfig(Data.Controller controller, Data.LinkedConfig data)
        {
            Communications.cconfig ret = new Communications.cconfig();

            ret.config_id = data.ConfigId.ToString();
            ret.processed = "0";
            ret.serialnumber = controller.SerialNumber;
            ret.utc = data.UTC.ToString("yyyy-MM-dd HH:mm:ss"); 
            ret.json_data = data.JSONData;
            ret.hash = data.Hash;

            return ret;

        }

        public Communications.cdata Generate_cdata(Data.Controller controller, Data.Controller channel_base)
        {
            var ret = new Communications.cdata();

           
            ret.mac_address = controller.MacAddress;
            ret.serial_number = controller.SerialNumber;

            ret.fw_version = controller.FirmwareVersion;
            ret.fw_hash = controller.GitId;
            ret.fw_date = controller.Date;

            var groups = Data.Group.GetGroups(channel_base.Id);
            int channel_number = 1;

            List<int> channel_types = new List<int>();
            foreach(var g in groups)
            {

                ret.groups.groups.Add(new Communications.cgroup() { group_id = (g.Number - 1).ToString(), group_name = g.Name });

                


                foreach(var c in Data.Channel.GetChannels(g.Id))
                {
                    var dt = Communications.DeviceTypes.Instance.deviceTypes.FirstOrDefault(f => f.ChannelTypeId == c.ChannelType);
                    var Config = Activator.CreateInstance(dt.Config.GetType()) as Communications.channel_config;

                    if (c.Number >= 60)
                        Config.channel_id = (c.Number).ToString();
                    else
                        Config.channel_id = (c.Number - 1).ToString();
              
                    Config.channel_type_id = dt.ChannelTypeId.ToString();

                    
                   

                    Config.channel_type = dt.Device;
                    Config.channel_type_code = dt.ChannelTypeCode.ToString();
                   

                    Config.group_id = (g.Number - 1).ToString();
                    Config.group_name = g.Name;
                    Config.modbus_addr = c.ModbusAddress;

                    if (!channel_types.Contains(c.ChannelType))
                    {
                        channel_types.Add(c.ChannelType);
                    }

          
                    // Config.alarm_thresholds
                  //  Config.alarm_thresholds.min = c.AlarmMin;
                  //  Config.alarm_thresholds.max = c.AlarmMax;

                    for(int i = 0; i< Config.NumberOfConfigs; i++)
                    {
                        Config.config[i] = c.Configs.FirstOrDefault(f => f.Index == i)?.Value ?? "";
                    }
                    Config.channel_name = c.Name;
                    Config.modbus_addr = c.ModbusAddress;

                    var prop = Config.GetType().GetProperty("UnitName");
                    if (prop != null)
                    {
                        Config.unit = prop.GetValue(Config).ToString();
                    }
                    else
                    {
                        Config.unit = "";
                    }

                    ret.channels.Add(Config);

                  
                  
                    channel_number++;
                }
            }

            int type_number = 1;
            foreach(var t in channel_types)
            {
                var dt = Communications.DeviceTypes.Instance.deviceTypes.FirstOrDefault(f => f.ChannelTypeId == t);

                Communications.cchannel_type tt = new Communications.cchannel_type();
                tt.channel_type_id = dt.ChannelTypeId.ToString();
                tt.channel_type_name = dt.Device;
                ret.channel_types.channel_type.Add(tt);

                Communications.channel_types_v2 t2 = new Communications.channel_types_v2();
                t2.id = dt.ChannelTypeId.ToString();
                t2.name = dt.Device;
                t2.code = t.ToString();

                var chan_config = Activator.CreateInstance(dt.Config.GetType());

                List<SortableNames> attributes = new List<SortableNames>();
                foreach (var ccc in chan_config.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance))
                {
                    var atttribute = ccc.GetCustomAttribute<System.ComponentModel.DisplayNameAttribute>();
                    if (atttribute != null)
                    {
                        var ss = new SortableNames();
                        ss.Name = atttribute.DisplayName;

                        var order = ccc.GetCustomAttribute<Interfaces.DisplayOrder>();
                        if (order == null)
                            ss.Order = 1000;
                        else
                            ss.Order = order.Order;

                        var datatype = ccc.GetCustomAttribute<Interfaces.DataType>();
                        if (datatype == null)
                            ss.DataType = "0";
                        else
                            ss.DataType = datatype.Value.ToString();

                        attributes.Add(ss);
                    }      
                }

                var sorted = attributes.OrderBy(f => f.Order);
                foreach (var att in sorted)
                {
                    t2.attribute_names.Add(att.Name);
                    t2.attribute_datatypes.Add(att.DataType);
                }
          
                ret.channel_types_v2.Add(t2);


                type_number++;
            }

            // todo - JSON
            // cpredict_config
            // ret.predict_config = Communications.cpredict_config.GenerateTestData();

            ret.CalculateHash();

    

            return ret;
        }

    }

    public class SortableNames
    {
        public int Order { get; set; }
        public string Name { get; set; }

        public string DataType { get; set; }
    }

    public class AlarmLogEntry
    {
        [JsonIgnore]
        public int Id { get; set; }
        public int channel_id { get; set; }
        public int status { get; set; }
        public int start_ts { get; set; }
        public int end_ts { get; set; }
        public int active { get; set; }
        public int type { get; set; } 
        public uint data { get; set; }

        public string action { get; set; }
    }

    public class AlarmLogPacket
    {

        public int version { get; set; }
        public string serial_number { get; set; }


        [JsonIgnore]
        public string JSON { get; set; }

        public List<AlarmLogEntry> alarm { get; set; }

        public AlarmLogPacket()
        {
            alarm = new List<AlarmLogEntry>();
        }

        public void AddEntry(AlarmLogEntry e)
        {
            alarm.Add(e);
        }
    }
    public class SensorLogPacket
    {
        public int ChannelId { get; set; }
        public int LogType { get; set; }

        public string LogTypeAsString { get; set; }

        public string JSON { get; set; }

        public BaseLog Logs { get; set; }

        public SensorLogPacket()
        {
          
        }
    }

    public class SensorLogEntry
    {
        public int timestamp { get; set; }
        public int status { get; set; }
        public double data { get; set; }
    }

    public class BaseLog
    {
        public int version { get; set; }
        public string channel_id { get; set; }
        public string serial_number { get; set; }


        public virtual void AddEntry(SensorLogEntry e)
        {

        }
    }

    public class PulseLog : BaseLog
    {
        public List<SensorLogEntry> pulse { get; set; }

        public PulseLog()
        {
            pulse = new List<SensorLogEntry>();
        }

        public override void AddEntry(SensorLogEntry e)
        {
            pulse.Add(e);
        }

    }

    public class ShortLog : BaseLog
    {
        public List<SensorLogEntry> shortterm { get; set; }

        public ShortLog()
        {
            shortterm = new List<SensorLogEntry>();
        }
        public override void AddEntry(SensorLogEntry e)
        {
            shortterm.Add(e);
        }
    }

    public class LongLog : BaseLog
    {
        public List<SensorLogEntry> longterm { get; set; }

        public LongLog()
        {
            longterm = new List<SensorLogEntry>();
        }

        public override void AddEntry(SensorLogEntry e)
        {
            longterm.Add(e);
        }
    }
}
