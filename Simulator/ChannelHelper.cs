﻿using Communications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace Simulator
{
    public class ChannelHelper
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(ChannelHelper));

        public static void AddChannelConfigs(Data.Channel channel)
        {
            var dt = Communications.DeviceTypes.Instance.deviceTypes.FirstOrDefault(f => f.ChannelTypeId == channel.ChannelType);
            var config = Activator.CreateInstance(dt.Config.GetType()) as Communications.channel_config;

            var channel_configs = new List<Data.ChannelConfig>();
            int i = 0;
            while (i < config.NumberOfConfigs)
            {
                Data.ChannelConfig ch = new Data.ChannelConfig();
                ch.ChannelId = channel.Id;
                ch.Value = config.config[i];
                ch.Index = i++;
                channel_configs.Add(ch);

            }
            Data.ChannelConfig.AddConfigs(channel_configs);
        }

        public static Communications.channel_config GetChannelConfig(Data.Channel channel)
        {
            var dt = Communications.DeviceTypes.Instance.deviceTypes.FirstOrDefault(f => f.ChannelTypeId == channel.ChannelType);
            var Config = Activator.CreateInstance(dt.Config.GetType()) as Communications.channel_config;

            for (int i = 0; i < dt.Config.NumberOfConfigs; i++)
            {
                Config.config[i] = channel.Configs.FirstOrDefault(f => f.Index == i)?.Value ?? "";
            }
            return Config;
        }


        public static string GetLogOptions(Data.Channel channel)
        {
            string ret = "";

            var Config = GetChannelConfig(channel);

            var prop = Config.GetType().GetProperty("LogOptions", System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance);
            if (prop != null)
            {
                ret = prop.GetValue(Config) as string;
            }
            else
            {
                Log.Debug("Channel type " + Config.GetType().Name + " does not have property LogOptions - assuming 'None'");
            }

            return ret;
        }

        public static EnabledOptions GetEnabled(Data.Channel channel)
        {
            EnabledOptions ret = EnabledOptions.Disabled;

            var Config = GetChannelConfig(channel);

            var prop = Config.GetType().GetProperty("Enabled", System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance);
            if (prop != null)
            {
                ret = (EnabledOptions)prop.GetValue(Config);
            }
            else
            {
                Log.Debug("Channel type " + Config.GetType().Name + " does not have property Enabled - assuming 'Disabled'");
            }

            return ret;
        }

        public static YesNoOptions GetSelfTestEnabled(Data.Channel channel)
        {
            YesNoOptions ret = YesNoOptions.No;

            var Config = GetChannelConfig(channel);

            var prop = Config.GetType().GetProperty("SelfTestsActive", System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance);
            if (prop != null)
            {
                ret = (YesNoOptions)prop.GetValue(Config);
            }
            else
            {
                Log.Debug("Channel type " + Config.GetType().Name + " does not have property SelfTestsActive - assuming 'No'");
            }

            return ret;
        }
    }
}
