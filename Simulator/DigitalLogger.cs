﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using Interfaces;

namespace Simulator
{
    public class DigitalLogger :  ILogger
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(DigitalLogger));

        public int ControllerId { get => Controller.Id; }
        public IGenerator Generator { get; set; }

        public double NoiseAmplitude { get; set; }
        public List<Data.Channel> Channels { get; set; }

        public int ControllerNumber
        {
            get
            {
                return Controller.Number;
            }
        }

        public List<Data.Controller> Controllers { get; set; }

        public event HandleEnabledChanged EnabledChanged;

        public Data.Channel GetChannel(int channel_id)
        {
            return Channels.FirstOrDefault(f => f.Id == channel_id);
        }

        private bool _Enabled;
        public bool Enabled 
        { 
            get => _Enabled; 
            set
            {
                _Enabled = value;
                if (_Enabled)
                {
                    if (Generator != null)
                    {
                        BuildChannelList();
                        Log.Debug("Logger Enable Generator " + Generator.GetType().Name + " for " + Controller.Id);
                      
                        Generator.UpdateSettings();
                        Generator.NewData += Generator_NewData;
                        Generator.StartGenerator();

                    }
                }
                else
                {
                    Log.Debug("Logger Disable Generator " + Generator.GetType().Name + " for " + Controller.Id);
                    Generator.StopGenerator();
                    Generator.NewData -= Generator_NewData;
                }
                EnabledChanged?.Invoke(this);
            }
        }

       

        public Controller Controller { get; set; }

        public DigitalLogger()
        {
            _Enabled = false;

            _currentState = new List<DigitalData>();
            for (int i = 0; i < 4; i++)
            {
                _currentState.Add(new DigitalData() { Channel = i, Value = 0 });
            }
        }

        public void CreateGenerator<T>() where T : IGenerator, new()
        {
            Log.Debug("Logger Creating Generator " + typeof(T).Name + " for " + Controller.Id);
            Generator = new T();
            Generator.Controller = Controller;
        
            BuildChannelList();
        }

        public void KillLogger()
        {
            Log.Debug("Kill Logger with generator " + Generator.GetType().Name + " for " + Controller.Id);
            Enabled = false;
            Generator.StopGenerator();
        }
        void BuildChannelList()
        {
            Channels = new List<Data.Channel>();
            var groups = Data.Group.GetGroups(Controller.Id);
            foreach (var g in groups)
            {
                var channels = Data.Channel.GetChannels(g.Id);
                foreach (var c in channels)
                {
                    if (c.ChannelType == 26)            // digital
                    {
                        Channels.Add(c);
                    }
                   
                }
            }
        }

        private List<DigitalData> _currentState;

        public void UpdateSettings()
        {
            Log.Debug("Logger Updating settings for Generator " + Generator.GetType().Name + " for " + Controller.Id);
            Generator.UpdateSettings();
            BuildChannelList();
        }
        private void Generator_NewData(IGeneratorData value)
        {
        
            try
            {
                Log.Debug("Value from Generator " + Controller.Id + " " + value.TimeStamp.ToString("HH:mm:ss") + " " + value.Value.ToString());


                var logs = new List<Data.Log>();
                DigitalData d = value as DigitalData;

                if (d != null)
                {
                    // var existing = _currentState.FirstOrDefault(f => f.Channel == d.Channel);
                    // if ((existing != null) && (existing.Value != d.Value))
                    {
                        // existing.Value = d.Value;
                        var ch = Channels.FirstOrDefault(f => f.Number == d.Channel + 60);
                        if (ch != null)
                        {
                            foreach (var c in Controllers)
                            {
                                var log = new Data.Log();
                                log.ControllerId = c.Id;
                                log.TimeStamp = value.TimeStamp;
                                log.LogType = 1;
                                log.Status = 0;
                                log.Value = d.Value;
                                log.ConfigId = c.LinkedConfigs.Last().ConfigId;
                                log.ChannelId = ch.Number;
                                log.Processed = 0;
                                logs.Add(log);

                                Log.Debug("Digital Log Controller " + c.Id + " Channel " + ch.Number + " setting " + value.TimeStamp.ToString("HH:mm:ss") + " " + log.Value.ToString());
                            }
                            ch.LastValue = d.Value;
                        }
                    }
                }

                if (logs.Count > 0)
                    Data.Log.AddLogs(logs);

            }
            catch (Exception ee)
            {
                Log.Error("Exception in OnNext: " + ee.Message, ee);
            }
        }

      

        public void SetValue(object v)
        {
            Generator.SetValue(v);
        }
    }
}
