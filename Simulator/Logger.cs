﻿using Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using log4net;
using System.IO;

namespace Simulator
{
    public class Logger :  ILogger
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(Logger));



        public List<Data.Controller> Controllers { get; set; }
        public int ControllerId { get => Controller.Id; }

        public int ControllerNumber
        {
            get
            {
                return Controller.Number;
            }
        }
        public Data.Controller Controller { get; set; }

        public IGenerator Generator { get; set; }


        public double NoiseAmplitude { get; set; }

        public List<Data.Channel> Channels { get; set; }

        void BuildChannelList()
        {
            Channels = new List<Data.Channel>();
            var groups = Data.Group.GetGroups(Controller.Id);
            foreach(var g in groups)
            {
                var channels = Data.Channel.GetChannels(g.Id);
                foreach(var c in channels)
                {
                    c.LogOptions = ChannelHelper.GetLogOptions(c);
                    if (c.LogOptions.Contains("P") && (ChannelHelper.GetEnabled(c) != Communications.EnabledOptions.Disabled))
                    {
                       
                
                        c.SelfTestEnabled = (ChannelHelper.GetSelfTestEnabled(c) == Communications.YesNoOptions.Yes);
                        c.LongAccumulator = 0.0;
                        c.ShortAccumulator = 0.0;
                        c.LongCount = 0;
                        c.ShortCount = 0;

                        Log.Debug("Adding channel for Grp " + g.Name + " channel: " + c.Number + " " + c.ChannelTypeName + " " + c.Name + " to logger for Controller " + ControllerId + " self test enabled: " + c.SelfTestEnabled);


                        Channels.Add(c);
                    }
                }
            }
        }

        public Data.Channel GetChannel(int channel_id)
        {
            return Channels.FirstOrDefault(f => f.Id == channel_id);
        }


        public void UpdateSettings()
        {
            Log.Debug("Logger Updating settings for Generator " + Generator.GetType().Name + " for " + Controller.Id);
            Generator.UpdateSettings();
            BuildChannelList();
        }

        public void CreateGenerator<T>() where T : IGenerator, new()
        {
            Log.Debug("Logger Creating Generator " + typeof(T).Name + " for " + Controller.Id);
            Generator = new T();
            Generator.Controller = Controller;
            Generator.NoiseAmplitude = NoiseAmplitude;
           
            BuildChannelList();
        }


      

        public event HandleEnabledChanged EnabledChanged;

        private bool _Enabled;
        public bool Enabled 
        {
            get => _Enabled;
            set
            {
                _Enabled = value;
                if (_Enabled)
                {
                    if (Generator != null)
                    {
                        BuildChannelList();
                        Log.Debug("Logger Enable Generator " + Generator.GetType().Name + " for " + Controller.Id);
                        ClockMode.Start();
                        LastSelfTest = ClockMode.GetTime;
                        selfTestState = SelfTestState.Waiting;
                        random = new Random();
                        Generator.UpdateSettings();
                        Generator.StartGenerator();
                        Generator.NewData += Generator_NewData;
                        
                    }
                }
                else
                {
                    ClockMode.Stop();
                    Log.Debug("Logger Disable Generator " + Generator.GetType().Name + " for " + Controller.Id);
                    Generator.StopGenerator();
                    Generator.NewData -= Generator_NewData;
                }

                EnabledChanged?.Invoke(this);
            }
        }

        

        public Logger()
        {
            _Enabled = false;
        }


        public void KillLogger()
        {
            Log.Debug("Kill Logger with generator " + Generator.GetType().Name + " for " + Controller.Id);
            Enabled = false;
            Generator.StopGenerator();
        }

        private DateTime LastSelfTest;

        private enum SelfTestState
        {
            Waiting,
            Zeroing,
            Spanning
        }

        private SelfTestState selfTestState;
        private Random random;
        private bool CheckSelfTest(Data.Log log, Data.Channel c)
        {
            if (!c.SelfTestEnabled)
            {
                return false;
            }

            if (selfTestState == SelfTestState.Waiting)
            {
                if (log.TimeStamp.Subtract(LastSelfTest).TotalSeconds >= 3600)        // 1 hour
                {
                    Log.Debug("Starting self test");
                    selfTestState = SelfTestState.Zeroing;
                    LastSelfTest = log.TimeStamp;
                }
                else
                {
                    return false;
                }
            }

            double v = log.Value;
        
            switch(selfTestState)
            {
                case SelfTestState.Zeroing:
                    v = 100;
                    v += (random.NextDouble() - 0.5) * 2 * Controller.ZeroPercent * v / 100;
                    if (log.TimeStamp.Subtract(LastSelfTest).TotalSeconds >= Controller.TestDuration)
                    {
                        Log.Debug("Starting self test - span");
                        selfTestState = SelfTestState.Spanning;
                    }
                    break;
                case SelfTestState.Spanning:
                    v = 10000;
                    v += (random.NextDouble() - 0.5) * 2 * Controller.SpanPercent * v / 100;
                    if (log.TimeStamp.Subtract(LastSelfTest).TotalSeconds >= 2*Controller.TestDuration)
                    {
                        selfTestState = SelfTestState.Waiting;
                        Log.Debug("Ending self test");
                        return false;
                    }
                    break;
            }

          
            Log.Debug("Self test: Fixed Value " + v.ToString() + " replacing log for channel " + c.Name);
            log.Value = v;
            
            return true;

        }

        private void Generator_NewData(IGeneratorData value)
        {
     
            try
            {
                Log.Debug("Value from Generator " + Controller.Id + " " + value.TimeStamp.ToString("HH:mm:ss") + " " + value.Value.ToString());
                // todo - store value for each channel in Controller which wants logging of the typeof(Generator)

                /*
                using (var sw = new StreamWriter("Logger" + Controller.Id + " .txt", true))
                {
                    sw.WriteLine(value.TimeStamp.ToString("HH:mm:ss") + "," + value.Value);
                }
                */

                var logs = new List<Data.Log>();
                foreach (var c in Channels)
                {
                    Data.Log log;
                    foreach (var control in Controllers)
                    {

                        log = new Data.Log();
                        log.ControllerId = control.Id;
                        log.TimeStamp = value.TimeStamp;
                        log.LogType = 1;
                        log.Status = 0;
                        log.Value = value.Value;
                        log.ConfigId = control.LinkedConfigs.Last().ConfigId;
                        log.ChannelId = c.Number-1;
                        log.Processed = 0;

                    

                        CheckSelfTest(log, c);

                        logs.Add(log);

                       // Log.Debug("Log Controller " + control.Id + " Channel " + c.Number + " setting " + value.TimeStamp.ToString("HH:mm:ss") + " " + log.Value.ToString());
                    }

                    c.LastValue = value.Value;
                    c.ShortAccumulator += value.Value;
                    c.LongAccumulator += value.Value;
                    c.LongCount++;
                    c.ShortCount++;

                    if (c.ShortCount >= 60) // 1 minute
                    {
                        if (c.LogOptions.Contains("S"))
                        {
                            foreach (var control in Controllers)
                            {
                                log = new Data.Log();
                                log.ControllerId = control.Id;
                                log.TimeStamp = value.TimeStamp;
                                log.LogType = 2;
                                log.Status = 0;
                                log.Value = c.ShortAccumulator / c.ShortCount;
                                log.ConfigId = control.LinkedConfigs.Last().ConfigId; ;
                                log.ChannelId = c.Number-1;
                                log.Processed = 0;
                                logs.Add(log);

                              //  Log.Debug("Short Log Controller " + control.Id + " Channel " + c.Number + " setting " + value.TimeStamp.ToString("HH:mm:ss") + " " + log.Value.ToString());

                      
                            }

                        }
                        c.ShortAccumulator = 0;
                        c.ShortCount = 0;
                    }

                    if (c.LongCount >= 900) // 15 minute
                    {
                        if (c.LogOptions.Contains("L"))
                        {
                            foreach (var control in Controllers)
                            {
                                log = new Data.Log();
                                log.ControllerId = control.Id;
                                log.TimeStamp = value.TimeStamp;
                                log.LogType = 3;
                                log.Status = 0;
                                log.Value = c.LongAccumulator / c.LongCount;
                                log.ConfigId = control.LinkedConfigs.Last().ConfigId;
                                log.ChannelId = c.Number-1;
                                log.Processed = 0;
                                logs.Add(log);

                            //    Log.Debug("Long Log Controller " + control.Id + " Channel " + c.Number + " setting " + value.TimeStamp.ToString("HH:mm:ss") + " " + log.Value.ToString());

                            
                            }
                        }
                        c.LongAccumulator = 0;
                        c.LongCount = 0;
                    }


                }
                if (logs.Count > 0)
                    Data.Log.AddLogs(logs);
            }
            catch (Exception ee)
            {
                Log.Error("Exception in OnNext " + ee.Message, ee);
            }

        }

        public void SetValue(object v)
        {

        }

       
    }
}
