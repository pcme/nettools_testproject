﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace Simulator
{
    public class Tick1s
    {

        private static Tick1s _instance;
        public static Tick1s Instance {  get { if (_instance == null) _instance = new Tick1s(); return _instance; } }
        private Tick1s()
        {
            Start();
        }
        public bool Ticking { get; private set; }

        private void RunStopwatch()
        {
            double diff;
            double seconds = 1;
            //Start the stopwatch
            DateTime now = DateTime.UtcNow;
            while (Ticking)
            {
         
                do
                {
                    //Don't cause a wasteful tight CPU loop
                    Thread.Sleep(0);
                    //See if interval has passed
                    diff = DateTime.UtcNow.Subtract(now).TotalMilliseconds;
                } while (diff < 1000 * seconds);
                //Interval passed, tell UI
                seconds++;

                ProgressChanged?.Invoke(this, new ProgressChangedEventArgs((int)seconds, null));
       


            }
        }

        public event ProgressChangedEventHandler ProgressChanged;

     
     

        public void Start()
        {
            if (!Ticking)
            {
                Task.Run(() =>
                {
                    Ticking = true;
                    while (Ticking)
                    {
                        RunStopwatch();
                    }
                });
            }
        }
        //Stop the high speed events
        public void Stop()
        {
            Ticking = false;
        }
    }


    public class Tick
    {
        //.NET hIgh speed timer
        Stopwatch sw = new Stopwatch();
        //State of the high speed events
        public bool Ticking { get; private set; }
        //Number of ticks per microsecond (varies by CPU)
        public readonly long TicksPerMicrosecond = Stopwatch.Frequency / 1000000L;
        //Used to time the events, default 100 microseconds
        long ticksToCount = 100L * Stopwatch.Frequency / 1000000L;
        public long TicksToCount
        {
            get { return ticksToCount; }
        }
        //Default to 100 microsecond events, good for most small routines
        long microseconds = 100L;
        //Set or get the number of microseconds for each event
        public long Microseconds
        {
            get { return Microseconds; }
            set
            {
                microseconds = value;
                ticksToCount = value * TicksPerMicrosecond;
            }
        }

        //Do the microsecond timing
        private void RunStopwatch()
        {
            //For calculating timing difference
            long nextTrigger;
            //Store difference between ticks (waits while > 0)
            long diff;

            int seconds = 0;
            //Start the stopwatch
            sw.Start();
            //Store the state
            Ticking = sw.IsRunning;
            while (Ticking)
            {
                //Get interval to wait
                nextTrigger = sw.ElapsedTicks + ticksToCount;
                do
                {
                    //Don't cause a wasteful tight CPU loop
                    Thread.Sleep(0);
                    //See if interval has passed
                    diff = nextTrigger - sw.ElapsedTicks;
                } while (diff > 0);
                //Interval passed, tell UI
                Task.Run(() =>
                {
                    seconds++;
                    ProgressChanged?.Invoke(this, new ProgressChangedEventArgs(seconds, null));
                });
               
              
            }
            //Stop and reset the stopwatch
            sw.Stop();
            sw.Reset();
        }

        public event ProgressChangedEventHandler ProgressChanged;

        //Use the constructor to setup the BackgroundWorker
        public Tick(ProgressChangedEventHandler CallOnTick)
        {
    
            ProgressChanged += CallOnTick;
           
        }
        //Start the high speed events
        public void Start()
        {
            if (!Ticking)
            {
                Task.Run(() =>
                {
                    Ticking = true;
                    while (Ticking)
                    {
                        RunStopwatch();
                    }
                });
            }
        }
        //Stop the high speed events
        public void Stop()
        {
            Ticking = false;
        }
    }
}
