﻿using Data;
using Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using System.Reactive.Disposables;
using System.Threading;
using System.ComponentModel;

namespace Simulator
{
    public class DigitalGenerator : IGenerator
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(DigitalGenerator));

        public double NoiseAmplitude { get; set; }

        private Data.Controller _controller;
        public Data.Controller Controller
        {
            get => _controller;
            set
            {
                _controller = value;
                UpdateSettings();
            }
        }

        private AutoResetEvent _killEvent;
        private AutoResetEvent _readingEvent;
        bool _abort;

        private Queue<DigitalData> _dataList;

        public DigitalGenerator()
        {
            State = new List<DigitalData>();
            _abort = false;
            _killEvent = new AutoResetEvent(false);
            _readingEvent = new AutoResetEvent(false);
            _dataList = new Queue<DigitalData>();
        }


        private void HandleTick(object sender, ProgressChangedEventArgs e)
        {


            foreach(var d in State)
            {
                d.TimeStamp = ClockMode.GetTime; // DateTime.UtcNow;
                _dataList.Enqueue(d);
            }
            
            _readingEvent.Set();

        }


        public event HandleNewData NewData;

        public void StartGenerator()
        {
            _abort = false;
            Log.Debug("Starting Digital generator for " + Controller.Id);
            Task.Run(async () =>
            {
                Tick1s.Instance.ProgressChanged += HandleTick;
                while (!_abort)
                {

                    try
                    {

                        // when digital state change, call ob.Next(d) where d is DigitalData


                        int evt = WaitHandle.WaitAny(new WaitHandle[] { _readingEvent, _killEvent });
                        if (evt == 0)
                        {
                            Log.Debug("Got digital reading event in Generator");
                            int count = 0;
                            while (_dataList.Count() > 0)
                            {
                                var d = _dataList.Dequeue();
                                NewData?.Invoke(d);
                                count++;
                            }

                        }
                
                    }
                    catch (Exception ee)
                    {
                        Log.Error("Exception in StartGenerator Run " + ee.Message, ee);
                    }
                }

                Tick1s.Instance.ProgressChanged -= HandleTick;
                Log.Debug("Stopped Digital generator for " + Controller.Id);
            });
             
         
        }

        public void SetValue(object v)
        {
            var d = v as DigitalData;
            if (d != null)
                SetDigitalState(d);
        }


        public void SetDigitalState(DigitalData d)
        {
            d.TimeStamp = ClockMode.GetTime; // DateTime.UtcNow;

            var dd = State.FirstOrDefault(f => f.Channel == d.Channel);
            if (dd == null)
            {
                State.Add(d);
            }
            else
            {
                dd.TimeStamp = d.TimeStamp;
                dd.Value = d.Value;
            }    


        }

        public void StopGenerator()
        {
            Log.Debug("Request to stop Digital generator for " + Controller.Id);
            _abort = true;
            _killEvent.Set(); 
            Controller.DigitalChanged -= Controller_DigitalChanged;

        }

        public void UpdateSettings()
        {
        

            Controller.ClearAllDigitalEventHandlers();
            Controller.DigitalChanged += Controller_DigitalChanged;
        }

        private void Controller_DigitalChanged(int channel, bool value)
        {
            Log.Debug("Got digital changed event");
            SetDigitalState(new DigitalData() { Channel = channel, Value = (value ? 1 : 0) });
        }

        private List<DigitalData> State;
    }

    public class DigitalData : IGeneratorData
    {
        public int Channel { get; set; }
        public double Value { get; set; }

        public DateTime TimeStamp { get; set; }
    }
}
