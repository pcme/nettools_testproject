﻿using Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using log4net;
using System.Diagnostics;
using System.ComponentModel;

namespace Simulator
{
    public class PulseGenerator : IGenerator
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(PulseGenerator));

        public double NoiseAmplitude { get; set; }
        public double Frequency { get; set; }
        public double PeakToPeak { get; set; }

        public Data.PredictConfig PredictConfig { get; set; }
        public Data.PlantConfig PlantConfig { get; set; }

        private Data.Controller _controller;
        public Data.Controller Controller 
        {
            get => _controller;
            set
            {
                _controller = value;
                UpdateSettings();
            }
        }

        public event HandleNewData NewData;

        private AutoResetEvent _killEvent;
        private AutoResetEvent _readingEvent;
        bool _abort;

        public void UpdateSettings()
        {
            // todo set Frequency and PeakToPeak from Controller
            Frequency = Controller.Frequency;
            PeakToPeak = Controller.PeakToPeak;
            Log.Debug("Pulse generator for " + Controller.Id + " Set to: " + Frequency.ToString() + "Hz " + PeakToPeak.ToString() + " peak-to-peak");
            PredictConfig = Data.PredictConfig.Get(Controller.Id);
            PlantConfig = Data.PlantConfig.Get(Controller.Id);
        }

        public PulseGenerator()
        {
            _abort = false;
            _killEvent = new AutoResetEvent(false);
            _readingEvent = new AutoResetEvent(false);
            _dataList = new Queue<PulseData>();
        }

        private Random r;
        private double ApplySomeNoise(double v)
        {
            if (r == null)
                r = new Random();
             return v + NoiseAmplitude * (r.NextDouble() - 0.5);
        }


        private double GetPulseValue(double time)
        {
            var v = Math.Sin(2 * Math.PI * time * Frequency);
            v += 1.0;       // always positive
            v *= PeakToPeak/2.0;
            v = ApplySomeNoise(v);
            return v;
        }

        private enum PredictState
        {
            Waiting,
            Running
        }
        private PredictState predictState;
        private double last_predict_time;

        double missing_cycle_start_time;
        double missing_cycle_end_time;
        double high_cycle_start_time;
        double high_cycle_end_time;

        int predict_lag_state;


        void HandlePredictDigital(double predict_time_offset)
        {
            Log.Debug("HandlePredictDigital: " + predict_time_offset);
            if (PredictConfig.DigitalInput > 0 && PredictConfig.DigitalInput < 5)
            {
                if (predict_time_offset == 0)
                {
                    var prop = Controller.GetType().GetProperty("Digital" + PredictConfig.DigitalInput, System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance);
                    if (prop != null)
                    {
                        Log.Debug("Predict digital is on");
                        prop.SetValue(Controller, true);
                    }
                }
                else if (predict_time_offset == 1)
                {
                    var prop = Controller.GetType().GetProperty("Digital" + PredictConfig.DigitalInput, System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance);
                    if (prop != null)
                    {
                        Log.Debug("Predict digital is off");
                        prop.SetValue(Controller, false);
                    }
                }
            }
        }

        private bool Predicting(double time, out double value)
        {
            value = 0.0;
            if (!PredictConfig.Enabled)
            {
                predict_lag_state = 0;
                return false;
            }

            if (predictState == PredictState.Waiting)
            {
                if ((time - last_predict_time) >= PredictConfig.Interval * 60)
                {
                    predictState = PredictState.Running;
                    last_predict_time = time;
                    Log.Debug("Starting Predict Cycle at time " + time.ToString());

                    if (PredictConfig.MissingCycle > 0)
                    {
                        missing_cycle_start_time = (double)(PredictConfig.MissingCycle - 1) / PredictConfig.Frequency;
                        missing_cycle_end_time = (double)(PredictConfig.MissingCycle) / PredictConfig.Frequency;
                        Log.Debug("Missing Cycle start at time " + missing_cycle_start_time.ToString());
                        Log.Debug("Missing Cycle end at time " + missing_cycle_end_time.ToString());
                    }
                    if (PredictConfig.HighPeakCycle > 0)
                    {
                        high_cycle_start_time = (double)(PredictConfig.HighPeakCycle - 1) / PredictConfig.Frequency;
                        high_cycle_end_time = ((double)(PredictConfig.HighPeakCycle) - 0.5) / PredictConfig.Frequency;
                        Log.Debug("High Cycle start at time " + high_cycle_start_time.ToString());
                        Log.Debug("High Cycle end at time " + high_cycle_end_time.ToString());
                    }

                }
                else
                {
                    if (PredictConfig.LagTime > 0)
                    {
                        switch (predict_lag_state)
                        {
                            case 0:
                                if ((time - last_predict_time + PredictConfig.LagTime) >= PredictConfig.Interval * 60)
                                {
                                    predict_lag_state++;
                                    HandlePredictDigital(0);
                                }
                                break;
                            case 1:
                                predict_lag_state++;
                                HandlePredictDigital(1);
                                break;
                        }


                    }


                    return false;
                }
            }

            

            double predict_time_offset = time - last_predict_time;
            double total_predict_time = (double)PredictConfig.NumberOfCycles / PredictConfig.Frequency;
            if (predict_time_offset >= total_predict_time)
            {
                predict_lag_state = 0;
                Log.Debug("Ending Predict Cycle at time " + time.ToString());
                predictState = PredictState.Waiting;
                return false;
            }

            if (PredictConfig.LagTime == 0)
            {
                HandlePredictDigital(predict_time_offset);
            }
            else
            {  
                switch (predict_lag_state)
                {
                    case 1:
                        predict_lag_state++;
                        HandlePredictDigital(1);
                        break;
                }
            }




            var v = Math.Sin(2 * Math.PI * predict_time_offset * PredictConfig.Frequency);
            v += 1.0;       // always positive
            v = ApplySomeNoise(v);

            bool special = false;
            if (PredictConfig.HighPeakCycle > 0)
            {
                if (predict_time_offset >= high_cycle_start_time && predict_time_offset <= high_cycle_end_time)
                {
                    special = true;
                    v *= PredictConfig.HighPeakValue / 2;
                    Log.Debug("Predict: " + predict_time_offset + " High Cycle - value " + v.ToString());
                }
            }

            if (PredictConfig.MissingCycle > 0)
            {
                if (!special &&
                    (predict_time_offset >= missing_cycle_start_time && predict_time_offset <= missing_cycle_end_time))
                {
                    special = true;
                    v *= PredictConfig.PeakToPeak /4;  // 50% of normal
                    Log.Debug("Predict: " + predict_time_offset + " Missing Cycle - value " + v.ToString());
                }
            }

            if (!special)
            {
                v *= PredictConfig.PeakToPeak / 2;
                Log.Debug("Predict: " + predict_time_offset + " Normal Cycle - value " + v.ToString());
            }
            value = v;


            return true;
        }

        private double time;

        private PulseData _data;

        private Queue<PulseData> _dataList;


        private void HandlePlantControl()
        {
            if (PlantConfig != null)
            {
                if (PlantConfig.Enabled)
                {

                    DateTime now = ClockMode.GetTime;
                    bool state;
                    if (PlantConfig.Start.TimeOfDay <= now.TimeOfDay && PlantConfig.Stop.TimeOfDay > now.TimeOfDay)
                    {
                        state = true;
                    }
                    else
                    {
                        state = false;
                    }

                    var prop = Controller.GetType().GetProperty("Digital" + PlantConfig.DigitalInput, System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance);
                    if (prop != null)
                    {
                        prop.SetValue(Controller, state);
                    }
                }
            }
        }

        private void HandleTick(object sender, ProgressChangedEventArgs e)
        {
            _data = new PulseData();

            _data.TimeStamp = ClockMode.GetTime; // DateTime.UtcNow;
            double v;
            if (!Predicting(time, out v))
                _data.Value = GetPulseValue(time);
            else
                _data.Value = v;
            _dataList.Enqueue(_data);
            HandlePlantControl();
            _readingEvent.Set();
            
            time += 1.0;
        }

        public void StartGenerator()
        {
            _abort = false;
            Log.Debug("Starting Pulse generator for " + Controller.Id);
            Task.Run(async () =>
            {
                while (!_abort)
                {

                    try
                    {
                        predictState = PredictState.Waiting;
                        time = 0.0;
                        last_predict_time = 0.0;
                        predict_lag_state = 0;
                        Tick1s.Instance.ProgressChanged += HandleTick;
                      

                        while (!_abort)
                        {

                            int evt = WaitHandle.WaitAny(new WaitHandle[] { _readingEvent, _killEvent });
                            if (evt == 0)
                            {
                                int count = 0;
                                while (_dataList.Count() > 0)
                                {
                                    var d = _dataList.Dequeue();
                                    NewData?.Invoke(d);
                                    count++;
                                }

                            }

                        }

                        Tick1s.Instance.ProgressChanged -= HandleTick;
                       
                    }
                    catch (Exception ee)
                    {
                        Log.Error("Exception in StartGenerator Run " + ee.Message, ee);
                    }
                }
                Log.Debug("Stopped Pulse generator for " + Controller.Id);
            });
   

        }

        public void StopGenerator()
        {
            Log.Debug("Request to stop Pulse generator for " + Controller.Id);
            _abort = true;
            _killEvent.Set();
        }


        public void SetValue(object v)
        {

        }

    }

    public class PulseData : IGeneratorData
    {
        public double Value { get; set; }

        public DateTime TimeStamp { get; set; }
    }
}
