﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simulator
{
    public class ClockMode
    {
        static ClockMode()
        {
            Running = false;
        }

        public static bool Mode { get; set; }

        private static DateTime _StartDate;
        public static DateTime StartDate
        {
            get => _StartDate;
            set
            {
                _StartDate = value;
                _EnabledTime = value;
            }
        }

        static int _run_count = 0;

        public delegate void HandleRunChanged(bool running);

        public static event HandleRunChanged RunChanged;
        public static bool Running { get; set; }
        public static void Start()
        {
            
            _run_count++;
            if (!Running)
            {
                Running = true;
                RunChanged?.Invoke(true);
                _EnabledTime = DateTime.UtcNow;
            }
        }

        public static void Stop()
        {
            _run_count--;
            if (_run_count <= 0)
            {
                Running = false;
                RunChanged?.Invoke(false);
                _run_count = 0;
            }
        }

        static DateTime _EnabledTime;


        public static DateTime GetTime
        {
            get
            {
                if (ClockMode.Mode)
                {
                    if (Running)
                        return StartDate.Add(DateTime.UtcNow.Subtract(_EnabledTime));
                    else
                        return StartDate;
                }
                else
                {
                    return DateTime.UtcNow;
                }
            }
        }
    }
}
