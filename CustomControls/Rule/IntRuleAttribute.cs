﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomControls.Rule
{
    public class IntRuleAttribute : RuleBaseAttribute
    {
        #region Data Members

        private readonly int _min;
        private readonly int _max;

		#endregion

		#region Constructor

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="minLength"></param>
		/// <param name="maxLength"></param>
		public IntRuleAttribute(int min, int max)
		{
			this._min = min;
			this._max = max;
		}

		#endregion

		#region Properties

		/// <summary>
		/// Get/Set for MinLength
		/// </summary>
		public int Min
		{
			get { return (_min); }
		}

		/// <summary>
		/// Get/Set for MaxLength
		/// </summary>
		public int Max
		{
			get { return (_max); }
		}

		#endregion

		#region Methods - Public

		/// <summary>
		/// Validate the input data
		/// </summary>
		/// <param name="dataObject"></param>
		/// <returns></returns>
		public override bool IsValid(object dataObject)
		{
			this.ErrorMessage = string.Empty;

			if (dataObject is int)
			{
				int data = (int)dataObject;

				if ((_min <= data) && (data <= _max))
				{
					return (true);
				}
			}
			ErrorMessage = String.Format("Enter a value between {0} and {1}", _min, _max);
			return false;
			

			
		}

		#endregion

	}
}
