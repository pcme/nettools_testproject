﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CustomControls.Forms
{
    public partial class frmLogOptions : Form
    {
        public frmLogOptions()
        {
            InitializeComponent();
        }


        public bool EnableLong { get; set; }

        public bool EnableShort { get; set; }

        public bool EnablePulse { get; set; }

        public string Value { get; set; }

        private void btnOK_Click(object sender, EventArgs e)
        {
            Value = "";
            if (cbEventLog.Checked) Value = Value + "E";
            if (cbLongTerm.Checked) Value = Value + "L";
            if (cbPulseLog.Checked) Value = Value + "P";
            if (cbShortTerm.Checked) Value = Value + "S";
            DialogResult = DialogResult.OK;
            this.Close();

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void frmLogOptions_Load(object sender, EventArgs e)
        {
            cbEventLog.Checked = Value.Contains("E");
            cbLongTerm.Checked = Value.Contains("L");
            cbPulseLog.Checked = Value.Contains("P");
            cbShortTerm.Checked = Value.Contains("S");

            cbPulseLog.Enabled = EnablePulse;
            cbLongTerm.Enabled = EnableLong;
            cbShortTerm.Enabled = EnableShort;

        }
    }
}
