﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace CustomControls.Forms
{
    public class LogOptionsEditor : UITypeEditor
    {

        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
        {
            return UITypeEditorEditStyle.Modal;
        }

        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
        {
            IWindowsFormsEditorService svc = provider.GetService(typeof(IWindowsFormsEditorService)) as IWindowsFormsEditorService;
            string vv = value as string;
            if (svc != null)
            {
                Forms.frmLogOptions f = new frmLogOptions();
                f.Value = vv;
                f.EnableLong = true;
                f.EnableShort = true;
                f.EnablePulse = true;
                if (DialogResult.OK == f.ShowDialog())
                {
                    value = f.Value;
                }
            }
            return value;
        }
    }


    public class LogOptionsPEEditor : UITypeEditor
    {

        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
        {
            return UITypeEditorEditStyle.Modal;
        }

        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
        {
            IWindowsFormsEditorService svc = provider.GetService(typeof(IWindowsFormsEditorService)) as IWindowsFormsEditorService;
            string vv = value as string;
            if (svc != null)
            {
                Forms.frmLogOptions f = new frmLogOptions();
                f.Value = vv;
                f.EnableLong = false;
                f.EnableShort = false;
                f.EnablePulse = true;
                if (DialogResult.OK == f.ShowDialog())
                {
                    value = f.Value;
                }
            }
            return value;
        }
    }


    public class LogOptionsEEditor : UITypeEditor
    {

        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
        {
            return UITypeEditorEditStyle.Modal;
        }

        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
        {
            IWindowsFormsEditorService svc = provider.GetService(typeof(IWindowsFormsEditorService)) as IWindowsFormsEditorService;
            string vv = value as string;
            if (svc != null)
            {
                Forms.frmLogOptions f = new frmLogOptions();
                f.Value = vv;
                f.EnableLong = false;
                f.EnableShort = false;
                f.EnablePulse = false;
                if (DialogResult.OK == f.ShowDialog())
                {
                    value = f.Value;
                }
            }
            return value;
        }
    }


    public class LogOptionsESEditor : UITypeEditor
    {

        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
        {
            return UITypeEditorEditStyle.Modal;
        }

        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
        {
            IWindowsFormsEditorService svc = provider.GetService(typeof(IWindowsFormsEditorService)) as IWindowsFormsEditorService;
            string vv = value as string;
            if (svc != null)
            {
                Forms.frmLogOptions f = new frmLogOptions();
                f.Value = vv;
                f.EnableLong = false;
                f.EnablePulse = false;
                f.EnableShort = true;
                if (DialogResult.OK == f.ShowDialog())
                {
                    value = f.Value;
                }
            }
            return value;
        }
    }
}
