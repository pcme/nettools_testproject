﻿
namespace CustomControls.Forms
{
    partial class frmLogOptions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbLongTerm = new System.Windows.Forms.CheckBox();
            this.cbShortTerm = new System.Windows.Forms.CheckBox();
            this.cbPulseLog = new System.Windows.Forms.CheckBox();
            this.cbEventLog = new System.Windows.Forms.CheckBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cbLongTerm
            // 
            this.cbLongTerm.AutoSize = true;
            this.cbLongTerm.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbLongTerm.Location = new System.Drawing.Point(91, 42);
            this.cbLongTerm.Name = "cbLongTerm";
            this.cbLongTerm.Size = new System.Drawing.Size(98, 17);
            this.cbLongTerm.TabIndex = 0;
            this.cbLongTerm.Text = "Long Term Log";
            this.cbLongTerm.UseVisualStyleBackColor = true;
            // 
            // cbShortTerm
            // 
            this.cbShortTerm.AutoSize = true;
            this.cbShortTerm.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbShortTerm.Location = new System.Drawing.Point(91, 77);
            this.cbShortTerm.Name = "cbShortTerm";
            this.cbShortTerm.Size = new System.Drawing.Size(99, 17);
            this.cbShortTerm.TabIndex = 1;
            this.cbShortTerm.Text = "Short Term Log";
            this.cbShortTerm.UseVisualStyleBackColor = true;
            // 
            // cbPulseLog
            // 
            this.cbPulseLog.AutoSize = true;
            this.cbPulseLog.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbPulseLog.Location = new System.Drawing.Point(116, 110);
            this.cbPulseLog.Name = "cbPulseLog";
            this.cbPulseLog.Size = new System.Drawing.Size(73, 17);
            this.cbPulseLog.TabIndex = 2;
            this.cbPulseLog.Text = "Pulse Log";
            this.cbPulseLog.UseVisualStyleBackColor = true;
            // 
            // cbEventLog
            // 
            this.cbEventLog.AutoSize = true;
            this.cbEventLog.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbEventLog.Location = new System.Drawing.Point(114, 143);
            this.cbEventLog.Name = "cbEventLog";
            this.cbEventLog.Size = new System.Drawing.Size(75, 17);
            this.cbEventLog.TabIndex = 3;
            this.cbEventLog.Text = "Event Log";
            this.cbEventLog.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(51, 193);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 4;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(203, 193);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // frmLogOptions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(348, 255);
            this.ControlBox = false;
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.cbEventLog);
            this.Controls.Add(this.cbPulseLog);
            this.Controls.Add(this.cbShortTerm);
            this.Controls.Add(this.cbLongTerm);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmLogOptions";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Log Options";
            this.Load += new System.EventHandler(this.frmLogOptions_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox cbLongTerm;
        private System.Windows.Forms.CheckBox cbShortTerm;
        private System.Windows.Forms.CheckBox cbPulseLog;
        private System.Windows.Forms.CheckBox cbEventLog;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
    }
}